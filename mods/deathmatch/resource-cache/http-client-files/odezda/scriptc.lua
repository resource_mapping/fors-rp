local window = {}
local style = { [0] = "Shirt", [1] = "Head", [2] = "Trousers", [3] = "Shoes", [4] = "Tattoos: Left Upper Arm", [5] = "Tattoos: Left Lower Arm", [6] = "Tattoos: Right Upper Arm", [7] = "Tattoos: Right Lower Arm", [8] = "Tattoos: Back", [9] = "Tattoos: Left Chest", [10] = "Tattoos: Right Chest", [11] = "Tattoos: Stomach", [12] = "Tattoos: Lower Back", [13] = "Necklace", [14] = "Watches", [15] = "Glasses", [16] = "Hats", [17] = "Extra", [18] = "Body-Fat", [19] = "Body- Muscle"}
local edit = {}
local grid = {}
local but = {}
local lbl = {}
local ntable = { [0] = "Shirt", [1] = "Head", [3] = "Trousers", [4] = "Shoes", [5] = "Tattoos: Left Upper Arm", [6] = "Tattoos: Left Lower Arm", [7] = "Tattoos: Right Upper Arm", [8] = "Tattoos: Back", [9] = "Tattoos: Left Chest", [10] = "Tattoos: Right Chest", [11] = "Tattoos: Stomach", [12] = "Tattoos: Lower Back", [13] = "Necklace", [14] = "Watches", [15] = "Glasses", [16] = "Hats", [17] = "Extra" }
function newSkinGui()
window[1] = guiCreateWindow(0.01,0.4,0.35,0.6,"Player Customization Panel",true)
lbl[1] = guiCreateLabel(0.01,0.01,0.95,0.18,"Skin",true,window[1])
guiLabelSetColor(lbl[1],3,249,61)
guiLabelSetHorizontalAlign(lbl[1],"center",true)
guiLabelSetVerticalAlign(lbl[1],"center",true)
but[1] = guiCreateButton(0.03,0.5,0.3,0.4,"Previous",true,lbl[1])
addEventHandler("onClientGUIClick",but[1],previouskin,false)
but[2] = guiCreateButton(0.7,0.5,0.3,0.4,"Next",true,lbl[1])
addEventHandler("onClientGUIClick",but[2],previouskin,false)

lbl[2] = guiCreateLabel(0.050,0.2,0.9,0.2,"Cj Skin Only",true,window[1])
guiLabelSetColor(lbl[2],3,249,61)
guiLabelSetHorizontalAlign(lbl[2],"center",true)

grid[2] = guiCreateGridList(0.05,0.3,0.9,0.55,true,window[1])
but[3] = guiCreateButton(0.03,0.88,0.3,0.08,"Select",true,window[1])
addEventHandler("onClientGUIClick",but[3],selectClothMode,false)
but[4] = guiCreateButton(0.7,0.88,0.3,0.08,"Close",true,window[1])
guiGridListAddColumn(grid[2],"Choose whatever you want, Sir!",0.9)
	for i, v in pairs(style) do
		local row = guiGridListAddRow(grid[2])
		guiGridListSetItemText(grid[2],row,1,v,false,false)
		guiGridListSetItemData(grid[2],row,1,v)
	end 
window[2] = guiCreateWindow(0.68,0.57,0.3,0.4,"Please Select Your Clothes",true)
lbl[3] = guiCreateLabel(0.01,0.01,0.95,0.2,"Clothes",true,window[2])
guiLabelSetColor(lbl[3],3,249,61)
guiLabelSetHorizontalAlign(lbl[3],"center",true)
guiLabelSetVerticalAlign(lbl[3],"center",true)
grid[1] = guiCreateGridList(0.05,0.22,0.94,0.5,true,window[2])
but[23] = guiCreateButton(0.075,0.8,0.25,0.15,"Try",true,window[2])
but[24] = guiCreateButton(0.4,0.8,0.25,0.15,"Close",true,window[2])
addEventHandler("onClientGUIClick",but[4],closecloth,false)
addEventHandler("onClientGUIClick",but[24],closecloth,false)


guiGridListAddColumn(grid[1],"Texture",0.5)
guiGridListAddColumn(grid[1],"Model",0.5)
window[3] = guiCreateWindow(0.369,0.4,0.3,0.3,"",true)
lbl[4] = guiCreateLabel(0.05,0.1,0.9,0.2,"Please type a number from 0 to 999",true,window[3])
guiLabelSetVerticalAlign(lbl[4],"center",true)
guiLabelSetColor(lbl[4],3,249,61)
edit[1] = guiCreateEdit(0.3,0.4,0.4,0.2,"",true,window[3])
guiEditSetMaxLength(edit[1],3)
--guiSetText(window[3],"BodyFat")
but[25] = guiCreateButton(0.2,0.7,0.2,0.2,"Done",true,window[3])
but[26] = guiCreateButton(0.6,0.7,0.2,0.2,"Close",true,window[3])
addEventHandler("onClientGUIClick",but[25],showstyle,false)
addEventHandler("onClientGUIClick",but[26],closefat,false)

addEventHandler("onClientGUIClick",but[23],trycloth,false)

guiSetVisible(window[1],false)
guiSetVisible(window[2],false)
guiSetVisible(window[3],false)
end

addEventHandler("onClientResourceStart",getResourceRootElement(getThisResource()),
function()
newSkinGui()
end)
------------------------------------------tidy this code plz----------------------------------------------------------
function closecloth(button,state)
	if button == "left" and state =="up" then
		guiSetVisible(window[1],false)
		guiSetVisible(window[2],false)
		guiSetVisible(window[3],false)
		showCursor(false)
		unbindKey("b","up",function()
		if isCursorShowing(getLocalPlayer()) then
			showCursor(false)
		else
			showCursor(true)
		end
		end)
	end
end
function closefat(button,state)
	if button == "left" and state == "up" then
		guiSetVisible(window[3],false)
	end
end

function isguivisible(thePlayer,command)
if guiGetVisible(window[1]) == true then
	guiSetVisible(window[1],false)
	guiSetVisible(window[2],false)
	
	showCursor(false)
	unbindKey("b","up",function()
		if isCursorShowing(getLocalPlayer()) then
			showCursor(false)
		else
			showCursor(true)
		end
	end)
else
	guiSetVisible(window[1],true)
	guiSetVisible(window[2],true)
	
	showCursor(true)
	bindKey("b","up",function()
		if isCursorShowing(getLocalPlayer()) then
		showCursor(false)
		else
		showCursor(true)
		end
	end)
end
end
addCommandHandler("showme", isguivisible)
local id = { [3] = true, [4] = true, [5] = true, [6] = true, [8] = true, [42] = true, [65] = true, [74] = true, [86] = true, [119] = true, [149] = true, [208] = true, [239] = true, [273] = true, [289] = true}
function previouskin(button,state)
	if button == "left" and state == "up" then
		if source == but[1] then
			local skin = getElementModel(getLocalPlayer())
			if skin == 0 then
				skin = 312
			else
				skin = skin - 1
				while id[skin] do
					skin = skin - 1
					--outputChatBox(skin)
				end
			end
			setElementModel(getLocalPlayer(),skin)
		elseif source == but[2] then
			local skin = getElementModel(getLocalPlayer())
			if skin == 312 then
				skin = 0
			else
				skin = skin + 1
				while id[skin] do
					skin = skin + 1
				end
			end
			setElementModel(getLocalPlayer(),skin)
		end
	end
end

function selectClothMode(button,state)
	if button == "left" and state == "up" then
		local row,col = guiGridListGetSelectedItem(grid[2])
		if row and col and row ~= -1 and col ~= -1 then
			local style = guiGridListGetItemData(grid[2],row,col)
			if style == "Body- Muscle" or style == "Body-Fat" then
				showDemtheStyle(style)
			else
				triggerServerEvent("displayclothes",getLocalPlayer(),style)
				--outputChatBox(style)
			end
		end
	end
end

function showDemtheStyle(style)
		guiSetVisible(window[3],true)
		guiSetText(window[3],style)
end

function showstyle(button,state)
	if button == "left" and state == "up" then
		local text = guiGetText(window[3])
		local box = guiGetText(edit[1])
		if box ~= nil then
			box = 0
		end
		--outputChatBox(box)
		box = tonumber(box)
		if box >= 0 then
			triggerServerEvent("givethemthestyle",getLocalPlayer(),text,box)
		else
			outputChatBox("please enter a positive number only")
		end
	end
end
function trycloth(button,state)
	if button == "left" and state == "up" then
		local row,col = guiGridListGetSelectedItem(grid[1])
		if row and col and row ~= -1 and col ~= -1 then
			local styles = guiGridListGetItemData(grid[1],row,col)
			local text = guiGetText(lbl[3])
			--outputChatBox(styles["index"])
			--[[ for m, n in pairs(style) do
				if n == text then
					lola = m
				end
			end]]
			local mmm = styles["index"]
			--local clothtex, clothmod = getPedClothes(getLocalPlayer(),mmm)
			local peda = getElementModel(getLocalPlayer())
			
			
			if peda ~= 0 then
				--local ped = getElementModel(getLocalPlayer())
				setElementModel(getLocalPlayer(),0)
				addPedClothes(getLocalPlayer(),styles["Texture"],styles["Model"],mmm)
				
			else
				--local clothtex,clothmod = getPedClothes(getLocalPlayer(),lola)
				addPedClothes(getLocalPlayer(),styles["Texture"],styles["Model"],mmm)
				
			end 
		end
	end
end





function displaymodeon(clothes,styles)
	if guiGetVisible(window[2]) then
		guiGridListClear(grid[1])
		guiGridListClear(grid[1])
		guiGridListClear(grid[1])
		guiSetText(lbl[3],styles)
		for m, n in pairs(style) do
			if n == styles then
				lol = m
			end
		end
		
			for i, v in pairs(clothes[lol]) do
				local row = guiGridListAddRow(grid[1])
				local model = v["Model"]
				local texture = v["Texture"]
				
				for j, k in pairs(style) do
					if k == styles then
						--outputChatBox(k.. "      is        "..j)
						v["index"] = j
						
					end
				end
				guiGridListSetItemText(grid[1],row,1,texture,false,false)
				guiGridListSetItemText(grid[1],row,2,model,false,false)
				
				guiGridListSetItemData(grid[1],row,1,v)
			end
		
	end
end

addEvent("displayClothOn",true)
addEventHandler("displayClothOn",root,displaymodeon)

addEvent("showDemClothGui",true)
addEventHandler("showDemClothGui",root,isguivisible)


	