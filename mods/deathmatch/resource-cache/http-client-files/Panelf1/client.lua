--ALLIGATOR
local screenW, screenH = guiGetScreenSize()
local x, y = (screenW/1366), (screenH/768)

local font_Menu = dxCreateFont("files/font/font.ttf", 15)
local font_SubMenu = dxCreateFont("files/font/font.ttf", 12)
local font_MenuStatus = dxCreateFont("files/font/font.ttf", 10)
local font_Default = dxCreateFont("files/font/defaultFont.ttf", 40)

cor = {}

color = false
usuario1 = false
usuario2 = false
usuario3 = false
usuario4 = false
usuario5 = false
usuario6 = false
usuario7 = false
usuario8 = false
usuario9 = false
usuario10 = false
usuario11 = false
usuario12 = false
usuario13 = false
usuario14 = false
usuario15 = false
usuario16 = false
usuario17 = false
usuario18 = false
usuario19 = false
usuario20 = false
usuario21 = false
usuario22 = false
usuario23 = false
usuario24 = false
usuario25 = false
usuario26 = false
usuario27 = false
usuario28 = false
usuario29 = false
usuario30 = false
usuario31 = false
usuario32 = false
usuario33 = false
usuario34 = false
usuario35 = false

function dxUsuario1()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 100, 150, 0, ((getTickCount() - tick1) / 100), "OutBounce")
    local posX2, posY2, posZ2 = interpolateBetween(0, -160, 0, 0, 150, 0, ((getTickCount() - tick1) / 100), "OutBounce")
    local posX3, posY3, posZ3 = interpolateBetween(-260, -260, -260, 76, 70, 90, ((getTickCount() - tick1) / 100), "OutBounce")
    local posX4, posY4, posZ4 = interpolateBetween(-260, 0, 0, 110, 0, 0, ((getTickCount() - tick1) / 100), "OutBounce")
    local alphaFundo, posLinearY, posLinearZ = interpolateBetween(0, 0, 0, 100, 0, 0, ((getTickCount() - tick1) / 100), "Linear")

    alpha1 = 0
    alpha2 = 0
    alpha3 = 0
    alpha4 = 0
    alpha5 = 0
    alpha6 = 0

    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 150)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 150)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 150)
    cor[7] = tocolor(255, 255, 255, 255)
    cor[8] = tocolor(255, 255, 255, 255)
    cor[9] = tocolor(255, 255, 255, 255)
    cor[10] = tocolor(255, 255, 255, 255)
    cor[11] = tocolor(255, 255, 255, 255)
    cor[12] = tocolor(255, 255, 255, 255)

    if color == true then

        -- //#Cores Retangulos

        if cursorPosition(x*78, y*150, x*200, y*40) then cor[1] = tocolor(255, 255, 255, 255) end
        if cursorPosition(x*280, y*150, x*200, y*40) then cor[2] = tocolor(255, 255, 255, 255) end
        if cursorPosition(x*482, y*150, x*200, y*40) then cor[3] = tocolor(255, 255, 255, 255) end
        if cursorPosition(x*684, y*150, x*200, y*40) then cor[4] = tocolor(255, 255, 255, 255) end
        if cursorPosition(x*886, y*150, x*200, y*40) then cor[5] = tocolor(255, 255, 255, 255) end
        if cursorPosition(x*1088, y*150, x*200, y*40) then cor[6] = tocolor(255, 255, 255, 255) end

        -- //#Cores Textos

        if cursorPosition(x*78, y*150, x*200, y*40) then cor[7] = tocolor(0, 0, 0, 255); alpha1 = 255 end
        if cursorPosition(x*280, y*150, x*200, y*40) then cor[8] = tocolor(0, 0, 0, 255); alpha2 = 255 end
        if cursorPosition(x*482, y*150, x*200, y*40) then cor[9] = tocolor(0, 0, 0, 255); alpha3 = 255 end
        if cursorPosition(x*684, y*150, x*200, y*40) then cor[10] = tocolor(0, 0, 0, 255); alpha4 = 255 end
        if cursorPosition(x*886, y*150, x*200, y*40) then cor[11] = tocolor(0, 0, 0, 255); alpha5 = 255 end
        if cursorPosition(x*1088, y*150, x*200, y*40) then cor[12] = tocolor(0, 0, 0, 255); alpha6 = 255 end
    end

    local nomePlayer = getPlayerName(getLocalPlayer())
    local id = getElementData(getLocalPlayer(), "ID") or "N/A"
    local time = getRealTime()
    local day = {"Воскресенье", "Понедельник", "Вторник", "Время начала игры", "Четверг", "Пятница", "Суббота"}
    local dia = day[time.weekday + 1]
    local hours = time.hour
    local minutes = time.minute
    if (hours >= 0 and hours < 10) then
        hours = "0"..time.hour
    end
    if (minutes >= 0 and minutes < 10) then
        minutes = "0"..time.minute
    end
    local dinheiro = ("%09d"):format(getPlayerMoney(getLocalPlayer()))

        dxDrawRectangle(x*0, y*0, x*1366, y*768, tocolor(0, 0, 0, alphaFundo), false)
        dxDrawRectangle(x*78, y*posY1, x*200, y*40, cor[1], false)
        dxDrawRectangle(x*280, y*posY1, x*200, y*40, cor[2], false)
        dxDrawRectangle(x*482, y*posY1, x*200, y*40, cor[3], false)
        dxDrawRectangle(x*684, y*posY1, x*200, y*40, cor[4], false)
        dxDrawRectangle(x*886, y*posY1, x*200, y*40, cor[5], false)
        dxDrawRectangle(x*1088, y*posY1, x*200, y*40, cor[6], false)

        dxDrawText("              Меню | Reventon RP", x*78, y*posX3, x*328, y*126, tocolor(255, 0, 0, 255), x*1.00, font_Default, "center", "center", false, false, false, false, false)
        dxDrawText(nomePlayer.." #FFFFFF("..id..")", x*1243, y*posY3, x*1288, y*90, tocolor(255, 255, 255, 255), x*1.00, font_MenuStatus, "right", "center", false, false, false, true, false)
        dxDrawText(dia.."  "..hours..":"..minutes, x*1203, y*posZ3, x*1288, y*110, tocolor(255, 255, 255, 255), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("USD "..dinheiro, x*1203, y*posX4, x*1288, y*130, tocolor(255, 255, 255, 255), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)

        dxDrawRectangle(x*78, y*140, x*200, y*10, tocolor(255, 0, 0, alpha1), false)
        dxDrawRectangle(x*280, y*140, x*200, y*10, tocolor(255, 0, 0, alpha2), false)
        dxDrawRectangle(x*482, y*140, x*200, y*10, tocolor(255, 0, 0, alpha3), false)
        dxDrawRectangle(x*684, y*140, x*200, y*10, tocolor(255, 0, 0, alpha4), false)
        dxDrawRectangle(x*886, y*140, x*200, y*10, tocolor(255, 0, 0, alpha5), false)
        dxDrawRectangle(x*1088, y*140, x*200, y*10, tocolor(255, 0, 0, alpha6), false)

        dxDrawText("Статистика", x*78, y*posY2, x*278, y*190, cor[7], x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Оружие", x*280, y*posY2, x*480, y*190, cor[8], x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Одежда", x*482, y*posY2, x*682, y*190, cor[9], x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Транспорт", x*684, y*posY2, x*884, y*190, cor[10], x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Починка", x*886, y*posY2, x*1086, y*190, cor[11], x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Параметры", x*1088, y*posY2, x*1288, y*190, cor[12], x*1.00, font_Menu, "center", "center", false, false, false, false, false)
end

function dxUsuario2()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 402, 255, 0, ((getTickCount() - tick3) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 150)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 150)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[1] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*237, x*402, y*35) then cor[2] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*274, x*402, y*35) then cor[3] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*311, x*402, y*35) then cor[4] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*348, x*402, y*35) then cor[5] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*385, x*402, y*35) then cor[6] = tocolor(255, 255, 255, 255) end

    -- //#Cores Textos
    cor[7] = tocolor(255, 255, 255, posY1)
    cor[8] = tocolor(255, 255, 255, posY1)
    cor[9] = tocolor(255, 255, 255, posY1)
    cor[10] = tocolor(255, 255, 255, posY1)
    cor[11] = tocolor(255, 255, 255, posY1)
    cor[12] = tocolor(255, 255, 255, posY1)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[7] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*78, y*237, x*402, y*35) then cor[8] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*78, y*274, x*402, y*35) then cor[9] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*78, y*311, x*402, y*35) then cor[10] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*78, y*348, x*402, y*35) then cor[11] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*78, y*385, x*402, y*35) then cor[12] = tocolor(255, 0, 0, 255) end

        dxDrawRectangle(x*78, y*200, x*posX1, y*35, cor[1], false)
        dxDrawRectangle(x*78, y*237, x*posX1, y*35, cor[2], false)
        dxDrawRectangle(x*78, y*274, x*posX1, y*35, cor[3], false)
        dxDrawRectangle(x*78, y*311, x*posX1, y*35, cor[4], false)
        dxDrawRectangle(x*78, y*348, x*posX1, y*35, cor[5], false)
        dxDrawRectangle(x*78, y*385, x*posX1, y*35, cor[6], false)

        dxDrawRectangle(x*78, y*140, x*200, y*10, tocolor(255, 0, 0, 255), false)
        dxDrawRectangle(x*78, y*150, x*200, y*40, tocolor(255, 255, 255, 255), false)
        dxDrawText("Статистика", x*78, y*150, x*278, y*190, tocolor(0, 0, 0, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)

        dxDrawText("Игрок", x*78, y*200, x*480, y*235, cor[7], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        dxDrawText("Состояние", x*78, y*237, x*480, y*272, cor[8], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        dxDrawText("Местоположение", x*78, y*274, x*480, y*309, cor[9], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        dxDrawText("Оружие", x*78, y*311, x*480, y*346, cor[10], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        dxDrawText("Транспорт", x*78, y*348, x*480, y*383, cor[11], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        dxDrawText("Статистика", x*78, y*385, x*480, y*420, cor[12], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
end

function dxUsuario3()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 0, ((getTickCount() - tick4) / 400), "Linear")

    local nome = getPlayerName(getLocalPlayer())
    local id = getElementData(getLocalPlayer(), "") or "N/A"

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawText("Никнейм", x*492, y*200, x*885, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД", x*492, y*237, x*885, y*274, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText(nome, x*885, y*200, x*1278, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, true, false)
        dxDrawText(id, x*885, y*237, x*1278, y*274, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario4()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick5) / 400), "Linear")

    local vida1 = math.floor(getElementHealth(getLocalPlayer()))
    local vida2 = getPedMaxHealth(getLocalPlayer())
    local colete = math.floor(getPedArmor(getLocalPlayer()))
    local oxigenio = math.floor(getPedOxygenLevel(getLocalPlayer()))
    local skin = getElementModel(getLocalPlayer())
    local conta = tostring(getElementData(getLocalPlayer(), "account-name"))

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*1078, y*208, x*200, y*posZ1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*1078, y*246, x*200, y*posZ1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*1078, y*282, x*200, y*posZ1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*1078, y*208, x*200/vida2*vida1, y*posZ1, tocolor(150, 205, 70, 255), false)
        dxDrawRectangle(x*1078, y*246, x*200/100*colete, y*posZ1, tocolor(50, 150, 255, 255), false)
        dxDrawRectangle(x*1078, y*282, x*200/1000*oxigenio, y*posZ1, tocolor(205, 130, 40, 255), false)
        dxDrawEmptyRec(x*1078, y*208, x*200, y*posZ1, tocolor(0, 0, 0, 255), 1)
        dxDrawEmptyRec(x*1078, y*246, x*200, y*posZ1, tocolor(0, 0, 0, 255), 1)
        dxDrawEmptyRec(x*1078, y*282, x*200, y*posZ1, tocolor(0, 0, 0, 255), 1)
        dxDrawText("Жизни", x*492, y*200, x*885, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Броня", x*492, y*237, x*885, y*274, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Выносливость", x*492, y*274, x*885, y*311, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Одежда", x*492, y*311, x*885, y*348, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Логин", x*492, y*348, x*885, y*385, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText(vida1.."%", x*1078, y*209, x*1278, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
        dxDrawText(colete.."%", x*1078, y*247, x*1278, y*267, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
        dxDrawText(oxigenio.."%", x*1078, y*283, x*1278, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
        dxDrawText(skin, x*885, y*311, x*1278, y*348, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(conta, x*885, y*348, x*1278, y*385, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario5()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick6) / 400), "Linear")

    local interior = getElementInterior(getLocalPlayer())
    local dimensao = getElementDimension(getLocalPlayer())
    local posX, posY, posZ = getElementPosition(getLocalPlayer())
    local localidade = getZoneName(posX, posY, posZ)

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawText("Интерьер", x*492, y*200, x*885, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Измерение", x*492, y*237, x*885, y*274, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Локация", x*492, y*274, x*885, y*311, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Координаты", x*492, y*311, x*885, y*348, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText(interior, x*885, y*209, x*1278, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(dimensao, x*885, y*247, x*1278, y*267, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(localidade, x*885, y*283, x*1278, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(math.floor(posX)..", "..math.floor(posY)..", "..math.floor(posZ), x*885, y*311, x*1278, y*348, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario6()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick7) / 400), "Linear")

    local id = getPedWeapon(getLocalPlayer())
    local arma = getWeaponNameFromID(id)
    local municao = getPedTotalAmmo(getLocalPlayer())

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawText("Оружие", x*492, y*200, x*885, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД", x*492, y*237, x*885, y*274, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Количество", x*492, y*274, x*885, y*311, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText(arma, x*885, y*209, x*1278, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(id, x*885, y*247, x*1278, y*267, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(municao, x*885, y*283, x*1278, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario7()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick8) / 400), "Linear")

    local veiculo = getPedOccupiedVehicle(getLocalPlayer())

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawText("Модель", x*492, y*200, x*885, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Жизни", x*492, y*237, x*885, y*274, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Азот (Донат)", x*492, y*274, x*885, y*311, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawRectangle(x*1078, y*246, x*200, y*posZ1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*1078, y*282, x*200, y*posZ1, tocolor(0, 0, 0, 150), false)
    if veiculo then
        local nome = getVehicleName(veiculo, getLocalPlayer())
        local nitro = getVehicleNitroLevel(veiculo)
        if (getElementHealth(veiculo) >= 1000) then
            danos = 100
        else
            danos = math.floor(getElementHealth(veiculo) / 10)
        end
        dxDrawRectangle(x*1078, y*246, x*200/100*danos, y*posZ1, tocolor(130, 100, 50, 255), false)
        dxDrawText(nome, x*885, y*209, x*1278, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(danos.."%", x*1078, y*247, x*1278, y*267, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
        if nitro ~= false and nitro ~= nil and nitro > 0 then
            dxDrawRectangle(x*1078, y*282, x*200/1*nitro, y*posZ1, tocolor(100, 165, 185, 255), false)
            dxDrawText((math.floor(nitro/1*100)).."%", x*1078, y*283, x*1278, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
        else
        dxDrawText("__", x*1078, y*283, x*1278, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
        end
    else
    dxDrawText("__", x*885, y*209, x*1278, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
    dxDrawText("__", x*1078, y*247, x*1278, y*267, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    dxDrawText("__", x*1078, y*283, x*1278, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end
        dxDrawEmptyRec(x*1078, y*246, x*200, y*posZ1, tocolor(0, 0, 0, 255), 1)
        dxDrawEmptyRec(x*1078, y*282, x*200, y*posZ1, tocolor(0, 0, 0, 255), 1)
end

function dxUsuario8()

    -- //#Анимация
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick9) / 400), "Linear")

    local dinheiro = string.format("%.8i", getPlayerMoney(getLocalPlayer()))
    local equipe = getPlayerTeam(getLocalPlayer())
    local procurado = getPlayerWantedLevel(getLocalPlayer())
    local ip = getElementData(getLocalPlayer(), "IP")
    local serial = getPlayerSerial(getLocalPlayer())
    local ping = getPlayerPing(getLocalPlayer())
    if getElementData(getLocalPlayer(), "FPS") then
        fps = getElementData(getLocalPlayer(), "FPS")
    else
        fps = 0
    end
    local tempoOn = getElementData(getLocalPlayer(), "TempoON")
    local getVip = getElementData(getLocalPlayer(), "vip:expire")
    local time = getRealTime()
    local meses = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябль", "Ноябрь", "Декабрь"}
    local dias = {"Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "SABADO"}
    local dia = ("%02d"):format(time.monthday)
    local mes = meses[time.month + 1]
    local ano = ("%02d"):format(time.year + 1900)
    local diaSemana = dias[time.weekday + 1]
    local hours = time.hour
    local minutes = time.minute
    local seconds = time.second
        if (hours >= 0 and hours < 10) then
            hours = "0"..time.hour
        end
        if (minutes >= 0 and minutes < 10) then
            minutes = "0"..time.minute
        end
        if (seconds >= 0 and seconds < 10) then
            seconds = "0"..time.second
        end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*422, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*459, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*496, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*533, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*570, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*607, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        dxDrawText("Деньги", x*492, y*200, x*885, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Группировка", x*492, y*237, x*885, y*274, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Уровень розыска", x*492, y*274, x*885, y*311, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Номер айпи", x*492, y*311, x*885, y*348, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Серийный номер", x*492, y*348, x*885, y*385, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Пинг", x*492, y*385, x*885, y*422, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Фпс", x*492, y*422, x*885, y*459, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Время онлайна", x*492, y*459, x*885, y*496, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Время популярности", x*492, y*496, x*885, y*533, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("День недели", x*492, y*533, x*885, y*570, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Дата", x*492, y*570, x*885, y*607, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Время", x*492, y*607, x*885, y*644, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText(dinheiro, x*885, y*209, x*1278, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        if equipe then
            dxDrawText(getTeamName(equipe), x*885, y*247, x*1278, y*267, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        else
        dxDrawText("__", x*885, y*247, x*1278, y*267, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        end
        dxDrawText(procurado, x*885, y*283, x*1278, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText((ip or "__"), x*885, y*321, x*1278, y*341, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(serial, x*885, y*356, x*1278, y*376, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(ping, x*885, y*394, x*1278, y*414, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(fps, x*885, y*431, x*1278, y*451, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(tempoOn, x*885, y*468, x*1278, y*488, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        if getVip then
            local time = getRealTime()
            local realTime = time.timestamp
            local getVip = getElementData(getLocalPlayer(), "vip:expire") or 0
            local tempoVip = (getVip - realTime)
            dxDrawText((secondsToTimeDesc(tempoVip) or "__"), x*885, y*505, x*1278, y*525, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        else
        dxDrawText("__", x*885, y*505, x*1278, y*525, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        end
        dxDrawText(diaSemana, x*885, y*542, x*1278, y*562, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(dia.." / "..mes.." / "..ano, x*885, y*579, x*1278, y*599, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText(hours.." : "..minutes.." : "..seconds, x*885, y*616, x*1278, y*636, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario9()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 402, 255, 0, ((getTickCount() - tick10) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 150)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 150)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 150)
    cor[7] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[1] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*237, x*402, y*35) then cor[2] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*274, x*402, y*35) then cor[3] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*311, x*402, y*35) then cor[4] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*348, x*402, y*35) then cor[5] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*385, x*402, y*35) then cor[6] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*422, x*402, y*35) then cor[7] = tocolor(255, 255, 255, 255) end

    -- //#Cores Textos
    cor[9] = tocolor(255, 255, 255, posY1)
    cor[10] = tocolor(255, 255, 255, posY1)
    cor[11] = tocolor(255, 255, 255, posY1)
    cor[12] = tocolor(255, 255, 255, posY1)
    cor[13] = tocolor(255, 255, 255, posY1)
    cor[14] = tocolor(255, 255, 255, posY1)
    cor[15] = tocolor(255, 255, 255, posY1)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[9] = tocolor(255, 0, 0, 255) end
   -- if cursorPosition(x*78, y*237, x*402, y*35) then cor[10] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*274, x*402, y*35) then cor[11] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*311, x*402, y*35) then cor[12] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*348, x*402, y*35) then cor[13] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*385, x*402, y*35) then cor[14] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*422, x*402, y*35) then cor[15] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*78, y*200, x*posX1, y*35, cor[1], false)
        --dxDrawRectangle(x*78, y*237, x*posX1, y*35, cor[2], false)
       -- dxDrawRectangle(x*78, y*274, x*posX1, y*35, cor[3], false)
        --dxDrawRectangle(x*78, y*311, x*posX1, y*35, cor[4], false)
        --dxDrawRectangle(x*78, y*348, x*posX1, y*35, cor[5], false)
        --dxDrawRectangle(x*78, y*385, x*posX1, y*35, cor[6], false)
        --dxDrawRectangle(x*78, y*422, x*posX1, y*35, cor[7], false)

        dxDrawRectangle(x*280, y*140, x*200, y*10, tocolor(255, 0, 0, 255), false)
        dxDrawRectangle(x*280, y*150, x*200, y*40, tocolor(255, 255, 255, 255), false)
        dxDrawText("Оружие", x*280, y*150, x*480, y*190, tocolor(0, 0, 0, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)

        dxDrawText("Набор RP", x*78, y*200, x*480, y*235, cor[9], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
       -- dxDrawText("Список 2", x*78, y*237, x*480, y*272, cor[10], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Список 3", x*78, y*274, x*480, y*309, cor[11], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Список 4", x*78, y*311, x*480, y*346, cor[12], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Список 5", x*78, y*348, x*480, y*383, cor[13], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Список 6", x*78, y*385, x*480, y*420, cor[14], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Список 7", x*78, y*422, x*480, y*457, cor[15], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
end

function dxUsuario10()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick11) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[5] = tocolor(255, 255, 255, posX1)
    cor[6] = tocolor(255, 255, 255, posX1)
    cor[7] = tocolor(255, 255, 255, posX1)
    cor[8] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[5] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[6] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[7] = tocolor(255, 0, 0, 255) end
   -- if cursorPosition(x*482, y*311, x*806, y*37) then cor[8] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
     --   dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawText("Кастет (Использовать только для самообороны!)", x*492, y*200, x*885, y*237, cor[5], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Бита", x*492, y*237, x*885, y*274, cor[6], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Меч", x*492, y*274, x*885, y*311, cor[7], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
       -- dxDrawText("Нож", x*492, y*311, x*885, y*348, cor[8], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawImage(x*1221, y*196, x*50, y*50, "files/img/1.png", 0, 0, 0, cor[5], false)
        dxDrawImage(x*1221, y*232, x*50, y*50, "files/img/5.png", 45, 0, 0, cor[6], false)
        dxDrawImage(x*1221, y*265, x*50, y*50, "files/img/8.png", 50, 0, 0, cor[7], false)
        --dxDrawImage(x*1221, y*305, x*50, y*50, "files/img/4.png", 45, 0, 0, cor[8], false)
end

--[[function dxUsuario11()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick12) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[4] = tocolor(255, 255, 255, posX1)
    cor[5] = tocolor(255, 255, 255, posX1)
    cor[6] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[4] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[5] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[6] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawText("Пистолет 1", x*492, y*200, x*885, y*237, cor[4], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Пистолет 2", x*492, y*237, x*885, y*274, cor[5], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Пистолет с глушителем", x*492, y*274, x*885, y*311, cor[6], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawImage(x*1221, y*192, x*50, y*50, "files/img/24.png", 342, 0, 0, cor[4], false)
        dxDrawImage(x*1221, y*232, x*50, y*50, "files/img/22.png", 348, 0, 0, cor[5], false)
        dxDrawImage(x*1221, y*268, x*50, y*50, "files/img/23.png", 344, 0, 0, cor[6], false)
end

function dxUsuario12()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick13) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[4] = tocolor(255, 255, 255, posX1)
    cor[5] = tocolor(255, 255, 255, posX1)
    cor[6] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[4] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[5] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[6] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawText("Дробовик 1", x*492, y*200, x*885, y*237, cor[4], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Дробовик 2", x*492, y*237, x*885, y*274, cor[5], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Дробовик 3", x*492, y*274, x*885, y*311, cor[6], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawImage(x*1221, y*192, x*50, y*50, "files/img/26.png", 332, 0, 0, cor[4], false)
        dxDrawImage(x*1221, y*232, x*50, y*50, "files/img/25.png", 322, 0, 0, cor[5], false)
        dxDrawImage(x*1221, y*268, x*50, y*50, "files/img/27.png", 332, 0, 0, cor[6], false)
end

function dxUsuario13()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick14) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[4] = tocolor(255, 255, 255, posX1)
    cor[5] = tocolor(255, 255, 255, posX1)
    cor[6] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[4] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[5] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[6] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawText("МП5", x*492, y*200, x*885, y*237, cor[4], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Тек 9", x*492, y*237, x*885, y*274, cor[5], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Узи", x*492, y*274, x*885, y*311, cor[6], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawImage(x*1221, y*190, x*50, y*50, "files/img/29.png", 332, 0, 0, cor[4], false)
        dxDrawImage(x*1221, y*228, x*50, y*50, "files/img/32.png", 342, 0, 0, cor[5], false)
        dxDrawImage(x*1221, y*267, x*50, y*50, "files/img/28.png", 348, 0, 0, cor[6], false)
end

function dxUsuario14()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick15) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[4] = tocolor(255, 255, 255, posX1)
    cor[5] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[4] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[5] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawText("Ак 47", x*492, y*200, x*885, y*237, cor[4], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("М4", x*492, y*237, x*885, y*274, cor[5], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawImage(x*1221, y*190, x*50, y*50, "files/img/30.png", 332, 0, 0, cor[4], false)
        dxDrawImage(x*1221, y*228, x*50, y*50, "files/img/31.png", 324, 0, 0, cor[5], false)
end

function dxUsuario15()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick16) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[4] = tocolor(255, 255, 255, posX1)
    cor[5] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[4] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[5] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawText("Винтовка", x*492, y*200, x*885, y*237, cor[4], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Снайперская винтовка", x*492, y*237, x*885, y*274, cor[5], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawImage(x*1221, y*192, x*50, y*50, "files/img/33.png", 324, 0, 0, cor[4], false)
        dxDrawImage(x*1221, y*232, x*50, y*50, "files/img/34.png", 324, 0, 0, cor[5], false)
end

function dxUsuario16()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick17) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 180)
    cor[7] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[6] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[7] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[8] = tocolor(255, 255, 255, posX1)
    cor[9] = tocolor(255, 255, 255, posX1)
    cor[10] = tocolor(255, 255, 255, posX1)
    cor[11] = tocolor(255, 255, 255, posX1)
    cor[12] = tocolor(255, 255, 255, posX1)
    cor[13] = tocolor(255, 255, 255, posX1)
    cor[14] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[8] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[9] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[10] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[11] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[12] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[14] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, cor[6], false)
        dxDrawRectangle(x*482, y*422, x*806, y*posY1, cor[7], false)
        dxDrawText("Болончик с краской", x*492, y*200, x*885, y*237, cor[8], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Фотоапарат", x*492, y*237, x*885, y*274, cor[9], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Очки ночного видения 1", x*492, y*274, x*885, y*311, cor[10], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Очки ночного видения 2", x*492, y*311, x*885, y*348, cor[11], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Парашут", x*492, y*348, x*885, y*385, cor[12], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Букет цветов", x*492, y*385, x*885, y*422, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Фалос", x*492, y*422, x*885, y*459, cor[14], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawImage(x*1226, y*200, x*40, y*40, "files/img/41.png", 320, 0, 0, cor[8], true)
        dxDrawImage(x*1226, y*235, x*40, y*40, "files/img/43.png", 0, 0, 0, cor[9], true)
        dxDrawImage(x*1226, y*273, x*40, y*40, "files/img/44.png", 0, 0, 0, cor[10], true)
        dxDrawImage(x*1226, y*311, x*40, y*40, "files/img/44.png", 0, 0, 0, cor[11], true)
        dxDrawImage(x*1226, y*346, x*40, y*40, "files/img/46.png", 0, 0, 0, cor[12], true)
        dxDrawImage(x*1226, y*384, x*40, y*40, "files/img/14.png", 0, 0, 0, cor[13], true)
        dxDrawImage(x*1226, y*420, x*40, y*40, "files/img/10.png", 0, 0, 0, cor[14], true)
end]]

function dxUsuario17()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 402, 255, 0, ((getTickCount() - tick18) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 150)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 150)
    cor[5] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[1] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*237, x*402, y*35) then cor[2] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*274, x*402, y*35) then cor[3] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*311, x*402, y*35) then cor[4] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*348, x*402, y*35) then cor[5] = tocolor(255, 255, 255, 255) end

    -- //#Cores Textos
    cor[9] = tocolor(255, 255, 255, posY1)
    --cor[10] = tocolor(255, 255, 255, posY1)
    --cor[11] = tocolor(255, 255, 255, posY1)
    --cor[12] = tocolor(255, 255, 255, posY1)
    --cor[13] = tocolor(255, 255, 255, posY1)
    --cor[14] = tocolor(255, 255, 255, posY1)
    --cor[15] = tocolor(255, 255, 255, posY1)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[9] = tocolor(255, 0, 0, 255) end
   -- if cursorPosition(x*78, y*237, x*402, y*35) then cor[10] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*274, x*402, y*35) then cor[11] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*311, x*402, y*35) then cor[12] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*348, x*402, y*35) then cor[13] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*385, x*402, y*35) then cor[14] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*422, x*402, y*35) then cor[15] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*78, y*200, x*posX1, y*35, cor[1], false)
        --dxDrawRectangle(x*78, y*237, x*posX1, y*35, cor[2], false)
        --dxDrawRectangle(x*78, y*274, x*posX1, y*35, cor[3], false)
        --dxDrawRectangle(x*78, y*311, x*posX1, y*35, cor[4], false)
        --xDrawRectangle(x*78, y*348, x*posX1, y*35, cor[5], false)

        dxDrawRectangle(x*482, y*140, x*200, y*10, tocolor(255, 0, 0, 255), false)
        dxDrawRectangle(x*482, y*150, x*200, y*40, tocolor(255, 255, 255, 255), false)
        dxDrawText("Одежда", x*482, y*150, x*682, y*190, tocolor(0, 0, 0, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)

        dxDrawText("Начальная Одежда", x*78, y*200, x*480, y*235, cor[9], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Список 21", x*78, y*237, x*480, y*272, cor[10], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Список 3", x*78, y*274, x*480, y*309, cor[11], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Список 4", x*78, y*311, x*480, y*346, cor[12], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Список 5", x*78, y*348, x*480, y*383, cor[13], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
end

function dxUsuario18()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick19) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 180)
    cor[7] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[6] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[7] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[8] = tocolor(255, 255, 255, posX1)
    cor[9] = tocolor(255, 255, 255, posX1)
   -- cor[10] = tocolor(255, 255, 255, posX1)
   -- cor[11] = tocolor(255, 255, 255, posX1)
   -- cor[12] = tocolor(255, 255, 255, posX1)
    --cor[13] = tocolor(255, 255, 255, posX1)
    --cor[14] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[8] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[9] = tocolor(255, 0, 0, 255) end
   -- if cursorPosition(x*482, y*274, x*806, y*37) then cor[10] = tocolor(255, 0, 0, 255) end
    --if cursorPosition(x*482, y*311, x*806, y*37) then cor[11] = tocolor(255, 0, 0, 255) end
    --if cursorPosition(x*482, y*348, x*806, y*37) then cor[12] = tocolor(255, 0, 0, 255) end
    --if cursorPosition(x*482, y*385, x*806, y*37) then cor[13] = tocolor(255, 0, 0, 255) end
    --if cursorPosition(x*482, y*422, x*806, y*37) then cor[14] = tocolor(255, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        --dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        --dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        --dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        --dxDrawRectangle(x*482, y*385, x*806, y*posY1, cor[6], false)
        --dxDrawRectangle(x*482, y*422, x*806, y*posY1, cor[7], false)
        dxDrawText("Одежда для Бомжа 1", x*492, y*200, x*885, y*237, cor[8], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Одежда для Бомжа 2", x*492, y*237, x*885, y*274, cor[9], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
       -- dxDrawText("Одежда для Бомжа 3", x*492, y*274, x*885, y*311, cor[10], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        --dxDrawText("Одежда для Бомжа 4", x*492, y*311, x*885, y*348, cor[11], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        --dxDrawText("Одежда для Бомжа 5", x*492, y*348, x*885, y*385, cor[12], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        --dxDrawText("Одежда для Бомжа 6", x*492, y*385, x*885, y*422, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        --dxDrawText("Одежда для Бомжа 7", x*492, y*422, x*885, y*459, cor[14], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД: 14", x*885, y*209, x*1278, y*229, cor[8], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 23", x*885, y*247, x*1278, y*267, cor[9], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        --dxDrawText("ИД: 21", x*885, y*283, x*1278, y*303, cor[10], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        --dxDrawText("ИД: 53", x*885, y*321, x*1278, y*341, cor[11], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        --dxDrawText("ИД: 240", x*885, y*356, x*1278, y*376, cor[12], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        --dxDrawText("ИД: 299", x*885, y*394, x*1278, y*414, cor[13], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        --dxDrawText("ИД: 241", x*885, y*431, x*1278, y*451, cor[14], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

--[[function dxUsuario19()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick20) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 180)
    cor[7] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[6] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[7] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[8] = tocolor(255, 255, 255, posX1)
    cor[9] = tocolor(255, 255, 255, posX1)
    cor[10] = tocolor(255, 255, 255, posX1)
    cor[11] = tocolor(255, 255, 255, posX1)
    cor[12] = tocolor(255, 255, 255, posX1)
    cor[13] = tocolor(255, 255, 255, posX1)
    cor[14] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[8] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[9] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[10] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[11] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[12] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[14] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, cor[6], false)
        dxDrawRectangle(x*482, y*422, x*806, y*posY1, cor[7], false)
        dxDrawText("Скин 1", x*492, y*200, x*885, y*237, cor[8], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 2", x*492, y*237, x*885, y*274, cor[9], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 3", x*492, y*274, x*885, y*311, cor[10], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 4", x*492, y*311, x*885, y*348, cor[11], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 5", x*492, y*348, x*885, y*385, cor[12], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 6", x*492, y*385, x*885, y*422, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 7", x*492, y*422, x*885, y*459, cor[14], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД: 171", x*885, y*209, x*1278, y*229, cor[8], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 43", x*885, y*247, x*1278, y*267, cor[9], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 185", x*885, y*283, x*1278, y*303, cor[10], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 290", x*885, y*321, x*1278, y*341, cor[11], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 19", x*885, y*356, x*1278, y*376, cor[12], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 22", x*885, y*394, x*1278, y*414, cor[13], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 45", x*885, y*431, x*1278, y*451, cor[14], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario20()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick21) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[8] = tocolor(255, 255, 255, posX1)
    cor[9] = tocolor(255, 255, 255, posX1)
    cor[10] = tocolor(255, 255, 255, posX1)
    cor[11] = tocolor(255, 255, 255, posX1)
    cor[12] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[8] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[9] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[10] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[11] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[12] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        dxDrawText("Скин 1", x*492, y*200, x*885, y*237, cor[8], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 2", x*492, y*237, x*885, y*274, cor[9], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 3", x*492, y*274, x*885, y*311, cor[10], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 4", x*492, y*311, x*885, y*348, cor[11], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 5", x*492, y*348, x*885, y*385, cor[12], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД: 66", x*885, y*209, x*1278, y*229, cor[8], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 10", x*885, y*247, x*1278, y*267, cor[9], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 2", x*885, y*283, x*1278, y*303, cor[10], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 305", x*885, y*321, x*1278, y*341, cor[11], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 18", x*885, y*356, x*1278, y*376, cor[12], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario21()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick22) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 180)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[6] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[8] = tocolor(255, 255, 255, posX1)
    cor[9] = tocolor(255, 255, 255, posX1)
    cor[10] = tocolor(255, 255, 255, posX1)
    cor[11] = tocolor(255, 255, 255, posX1)
    cor[12] = tocolor(255, 255, 255, posX1)
    cor[13] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[8] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[9] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[10] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[11] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[12] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, cor[6], false)
        dxDrawText("Скин 1", x*492, y*200, x*885, y*237, cor[8], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 2", x*492, y*237, x*885, y*274, cor[9], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 3", x*492, y*274, x*885, y*311, cor[10], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 4", x*492, y*311, x*885, y*348, cor[11], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 5", x*492, y*348, x*885, y*385, cor[12], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 6", x*492, y*385, x*885, y*422, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД: 48", x*885, y*209, x*1278, y*229, cor[8], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 9", x*885, y*247, x*1278, y*267, cor[9], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 124", x*885, y*283, x*1278, y*303, cor[10], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 35", x*885, y*321, x*1278, y*341, cor[11], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 122", x*885, y*356, x*1278, y*376, cor[12], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 95", x*885, y*394, x*1278, y*414, cor[13], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario22()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 50, ((getTickCount() - tick23) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[8] = tocolor(255, 255, 255, posX1)
    cor[9] = tocolor(255, 255, 255, posX1)
    cor[10] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[8] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[9] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[10] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawText("Скин 1", x*492, y*200, x*885, y*237, cor[8], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 2", x*492, y*237, x*885, y*274, cor[9], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скин 3", x*492, y*274, x*885, y*311, cor[10], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ID: 15", x*885, y*209, x*1278, y*229, cor[8], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ID: 130", x*885, y*247, x*1278, y*267, cor[9], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ID: 249", x*885, y*283, x*1278, y*303, cor[10], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end]]

function dxUsuario23()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 402, 255, 0, ((getTickCount() - tick24) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 150)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[1] = tocolor(255, 255, 255, 255) end
   -- if cursorPosition(x*78, y*237, x*402, y*35) then cor[2] = tocolor(255, 255, 255, 255) end
  --  if cursorPosition(x*78, y*274, x*402, y*35) then cor[3] = tocolor(255, 255, 255, 255) end
   -- if cursorPosition(x*78, y*311, x*402, y*35) then cor[4] = tocolor(255, 255, 255, 255) end

    -- //#Cores Textos
    cor[9] = tocolor(255, 255, 255, posY1)
    cor[10] = tocolor(255, 255, 255, posY1)
    cor[11] = tocolor(255, 255, 255, posY1)
    cor[12] = tocolor(255, 255, 255, posY1)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[9] = tocolor(255, 0, 0, 255) end
   -- if cursorPosition(x*78, y*237, x*402, y*35) then cor[10] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*78, y*274, x*402, y*35) then cor[11] = tocolor(0, 0, 0, 255) end
   -- if cursorPosition(x*78, y*311, x*402, y*35) then cor[12] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*78, y*200, x*posX1, y*35, cor[1], false)
       -- dxDrawRectangle(x*78, y*237, x*posX1, y*35, cor[2], false)
       -- dxDrawRectangle(x*78, y*274, x*posX1, y*35, cor[3], false)
       -- dxDrawRectangle(x*78, y*311, x*posX1, y*35, cor[4], false)

        dxDrawRectangle(x*684, y*140, x*200, y*10, tocolor(255, 0, 0, 255), false)
        dxDrawRectangle(x*684, y*150, x*200, y*40, tocolor(255, 255, 255, 255), false)
        dxDrawText("Транспорт", x*684, y*150, x*884, y*190, tocolor(0, 0, 0, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)

        dxDrawText("Стартовый Набор", x*78, y*200, x*480, y*235, cor[9], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Автомобили список 2", x*78, y*237, x*480, y*272, cor[10], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Автомобили список 3", x*78, y*274, x*480, y*309, cor[11], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
       -- dxDrawText("Мотоциклы и велосипеды", x*78, y*311, x*480, y*346, cor[12], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
end

function dxUsuario24()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick25) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 180)
    cor[7] = tocolor(0, 0, 0, 150)
    cor[8] = tocolor(0, 0, 0, 180)
    cor[9] = tocolor(0, 0, 0, 150)
    cor[10] = tocolor(0, 0, 0, 180)
    cor[11] = tocolor(0, 0, 0, 150)
    cor[12] = tocolor(0, 0, 0, 180)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[6] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[7] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*459, x*806, y*37) then cor[8] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*496, x*806, y*37) then cor[9] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*533, x*806, y*37) then cor[10] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*570, x*806, y*37) then cor[11] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*607, x*806, y*37) then cor[12] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[13] = tocolor(255, 255, 255, posX1)
    cor[14] = tocolor(255, 255, 255, posX1)
    --cor[15] = tocolor(255, 255, 255, posX1)
    --cor[16] = tocolor(255, 255, 255, posX1)
    --cor[17] = tocolor(255, 255, 255, posX1)
    --cor[18] = tocolor(255, 255, 255, posX1)
    --cor[19] = tocolor(255, 255, 255, posX1)
    --cor[20] = tocolor(255, 255, 255, posX1)
    --cor[21] = tocolor(255, 255, 255, posX1)
    --cor[22] = tocolor(255, 255, 255, posX1)
    --cor[23] = tocolor(255, 255, 255, posX1)
    --cor[24] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[13] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[14] = tocolor(255, 0, 0, 255) end
    --[[if cursorPosition(x*482, y*274, x*806, y*37) then cor[15] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[16] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[17] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[18] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[19] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*459, x*806, y*37) then cor[20] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*496, x*806, y*37) then cor[21] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*533, x*806, y*37) then cor[22] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*570, x*806, y*37) then cor[23] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*607, x*806, y*37) then cor[24] = tocolor(0, 0, 0, 255) end]]

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
       --[[ dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, cor[6], false)
        dxDrawRectangle(x*482, y*422, x*806, y*posY1, cor[7], false)
        dxDrawRectangle(x*482, y*459, x*806, y*posY1, cor[8], false)
        dxDrawRectangle(x*482, y*496, x*806, y*posY1, cor[9], false)
        dxDrawRectangle(x*482, y*533, x*806, y*posY1, cor[10], false)
        dxDrawRectangle(x*482, y*570, x*806, y*posY1, cor[11], false)
        dxDrawRectangle(x*482, y*607, x*806, y*posY1, cor[12], false)]]
        dxDrawText("Мопед штата Reventon (Начальный)", x*492, y*200, x*885, y*237, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Горный Велосипед (Начальный)", x*492, y*237, x*885, y*274, cor[14], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
       --[[ dxDrawText("Автомобиль 3", x*492, y*274, x*885, y*311, cor[15], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 4", x*492, y*311, x*885, y*348, cor[16], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 5", x*492, y*348, x*885, y*385, cor[17], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 6", x*492, y*385, x*885, y*422, cor[18], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 7", x*492, y*422, x*885, y*459, cor[19], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 8", x*492, y*459, x*885, y*496, cor[20], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 9", x*492, y*496, x*885, y*533, cor[21], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 10", x*492, y*533, x*885, y*570, cor[22], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 11", x*492, y*570, x*885, y*607, cor[23], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 12", x*492, y*607, x*885, y*644, cor[24], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД: 474", x*885, y*209, x*1278, y*229, cor[13], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 527", x*885, y*247, x*1278, y*267, cor[14], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 565", x*885, y*283, x*1278, y*303, cor[15], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 526", x*885, y*321, x*1278, y*341, cor[16], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 529", x*885, y*356, x*1278, y*376, cor[17], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 545", x*885, y*394, x*1278, y*414, cor[18], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 559", x*885, y*431, x*1278, y*451, cor[19], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 405", x*885, y*468, x*1278, y*488, cor[20], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 436", x*885, y*505, x*1278, y*525, cor[21], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 475", x*885, y*542, x*1278, y*562, cor[22], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 547", x*885, y*579, x*1278, y*599, cor[23], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 550", x*885, y*616, x*1278, y*636, cor[24], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)]]
end

--[[function dxUsuario25()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick26) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 180)
    cor[7] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[6] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[7] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[8] = tocolor(255, 255, 255, posX1)
    cor[9] = tocolor(255, 255, 255, posX1)
    cor[10] = tocolor(255, 255, 255, posX1)
    cor[11] = tocolor(255, 255, 255, posX1)
    cor[12] = tocolor(255, 255, 255, posX1)
    cor[13] = tocolor(255, 255, 255, posX1)
    cor[14] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[8] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[9] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[10] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[11] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[12] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[14] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, cor[6], false)
        dxDrawRectangle(x*482, y*422, x*806, y*posY1, cor[7], false)
        dxDrawText("Автомобиль 13", x*492, y*200, x*885, y*237, cor[8], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 14", x*492, y*237, x*885, y*274, cor[9], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 15", x*492, y*274, x*885, y*311, cor[10], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 16", x*492, y*311, x*885, y*348, cor[11], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 17", x*492, y*348, x*885, y*385, cor[12], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 18", x*492, y*385, x*885, y*422, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 19", x*492, y*422, x*885, y*459, cor[14], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД: 500", x*885, y*209, x*1278, y*229, cor[8], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 507", x*885, y*247, x*1278, y*267, cor[9], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 587", x*885, y*283, x*1278, y*303, cor[10], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 422", x*885, y*321, x*1278, y*341, cor[11], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 540", x*885, y*356, x*1278, y*376, cor[12], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 516", x*885, y*394, x*1278, y*414, cor[13], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 541", x*885, y*431, x*1278, y*451, cor[14], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario26()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick27) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 180)
    cor[7] = tocolor(0, 0, 0, 150)
    cor[8] = tocolor(0, 0, 0, 180)
    cor[9] = tocolor(0, 0, 0, 150)
    cor[10] = tocolor(0, 0, 0, 180)
    cor[11] = tocolor(0, 0, 0, 150)
    cor[12] = tocolor(0, 0, 0, 180)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[6] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[7] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*459, x*806, y*37) then cor[8] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*496, x*806, y*37) then cor[9] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*533, x*806, y*37) then cor[10] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*570, x*806, y*37) then cor[11] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*607, x*806, y*37) then cor[12] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[13] = tocolor(255, 255, 255, posX1)
    cor[14] = tocolor(255, 255, 255, posX1)
    cor[15] = tocolor(255, 255, 255, posX1)
    cor[16] = tocolor(255, 255, 255, posX1)
    cor[17] = tocolor(255, 255, 255, posX1)
    cor[18] = tocolor(255, 255, 255, posX1)
    cor[19] = tocolor(255, 255, 255, posX1)
    cor[20] = tocolor(255, 255, 255, posX1)
    cor[21] = tocolor(255, 255, 255, posX1)
    cor[22] = tocolor(255, 255, 255, posX1)
    cor[23] = tocolor(255, 255, 255, posX1)
    cor[24] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[14] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[15] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[16] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[17] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[18] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[19] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*459, x*806, y*37) then cor[20] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*496, x*806, y*37) then cor[21] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*533, x*806, y*37) then cor[22] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*570, x*806, y*37) then cor[23] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*607, x*806, y*37) then cor[24] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, cor[6], false)
        dxDrawRectangle(x*482, y*422, x*806, y*posY1, cor[7], false)
        dxDrawRectangle(x*482, y*459, x*806, y*posY1, cor[8], false)
        dxDrawRectangle(x*482, y*496, x*806, y*posY1, cor[9], false)
        dxDrawText("Автомобиль 20", x*492, y*200, x*885, y*237, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("EАвтомобиль 21", x*492, y*237, x*885, y*274, cor[14], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 22", x*492, y*274, x*885, y*311, cor[15], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 23", x*492, y*311, x*885, y*348, cor[16], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 24", x*492, y*348, x*885, y*385, cor[17], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 25", x*492, y*385, x*885, y*422, cor[18], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 26", x*492, y*422, x*885, y*459, cor[19], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 27", x*492, y*459, x*885, y*496, cor[20], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Автомобиль 28", x*492, y*496, x*885, y*533, cor[21], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД: 602", x*885, y*209, x*1278, y*229, cor[13], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 480", x*885, y*247, x*1278, y*267, cor[14], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 429", x*885, y*283, x*1278, y*303, cor[15], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 589", x*885, y*321, x*1278, y*341, cor[16], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 415", x*885, y*356, x*1278, y*376, cor[17], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 543", x*885, y*394, x*1278, y*414, cor[18], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 546", x*885, y*431, x*1278, y*451, cor[19], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 426", x*885, y*468, x*1278, y*488, cor[20], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 445", x*885, y*505, x*1278, y*525, cor[21], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end]]

--[[function dxUsuario27()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick28) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 180)
    cor[7] = tocolor(0, 0, 0, 150)
    cor[8] = tocolor(0, 0, 0, 180)
    cor[9] = tocolor(0, 0, 0, 150)
    cor[10] = tocolor(0, 0, 0, 180)
    cor[11] = tocolor(0, 0, 0, 150)
    cor[12] = tocolor(0, 0, 0, 180)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[6] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[7] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*459, x*806, y*37) then cor[8] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*496, x*806, y*37) then cor[9] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*533, x*806, y*37) then cor[10] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*570, x*806, y*37) then cor[11] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*607, x*806, y*37) then cor[12] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[13] = tocolor(255, 255, 255, posX1)
    cor[14] = tocolor(255, 255, 255, posX1)
    cor[15] = tocolor(255, 255, 255, posX1)
    cor[16] = tocolor(255, 255, 255, posX1)
    cor[17] = tocolor(255, 255, 255, posX1)
    cor[18] = tocolor(255, 255, 255, posX1)
    cor[19] = tocolor(255, 255, 255, posX1)
    cor[20] = tocolor(255, 255, 255, posX1)
    cor[21] = tocolor(255, 255, 255, posX1)
    cor[22] = tocolor(255, 255, 255, posX1)
    cor[23] = tocolor(255, 255, 255, posX1)
    cor[24] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[14] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[15] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[16] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[17] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[18] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[19] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*459, x*806, y*37) then cor[20] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*496, x*806, y*37) then cor[21] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*533, x*806, y*37) then cor[22] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*570, x*806, y*37) then cor[23] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*607, x*806, y*37) then cor[24] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, cor[6], false)
        dxDrawRectangle(x*482, y*422, x*806, y*posY1, cor[7], false)
        dxDrawRectangle(x*482, y*459, x*806, y*posY1, cor[8], false)
        dxDrawRectangle(x*482, y*496, x*806, y*posY1, cor[9], false)
        dxDrawText("Мото 1", x*492, y*200, x*885, y*237, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Велосипед", x*492, y*237, x*885, y*274, cor[14], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Мото 2", x*492, y*274, x*885, y*311, cor[15], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Мото 3", x*492, y*311, x*885, y*348, cor[16], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Скутер", x*492, y*348, x*885, y*385, cor[17], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Мото 4", x*492, y*385, x*885, y*422, cor[18], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Мото 5", x*492, y*422, x*885, y*459, cor[19], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Мото 6", x*492, y*459, x*885, y*496, cor[20], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Мото 7", x*492, y*496, x*885, y*533, cor[21], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("ИД: 586", x*885, y*209, x*1278, y*229, cor[13], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 510", x*885, y*247, x*1278, y*267, cor[14], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 461", x*885, y*283, x*1278, y*303, cor[15], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 463", x*885, y*321, x*1278, y*341, cor[16], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 462", x*885, y*356, x*1278, y*376, cor[17], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 468", x*885, y*394, x*1278, y*414, cor[18], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 522", x*885, y*431, x*1278, y*451, cor[19], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 521", x*885, y*468, x*1278, y*488, cor[20], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("ИД: 581", x*885, y*505, x*1278, y*525, cor[21], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end]]

function dxUsuario28()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 402, 255, 0, ((getTickCount() - tick29) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 150)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 150)
    cor[5] = tocolor(0, 0, 0, 150)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[1] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*237, x*402, y*35) then cor[2] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*274, x*402, y*35) then cor[3] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*311, x*402, y*35) then cor[4] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*348, x*402, y*35) then cor[5] = tocolor(255, 255, 255, 255) end

    -- //#Cores Textos
    cor[9] = tocolor(255, 255, 255, posY1)
    cor[10] = tocolor(255, 255, 255, posY1)
    --cor[11] = tocolor(255, 255, 255, posY1)
    --cor[12] = tocolor(255, 255, 255, posY1)
    --cor[13] = tocolor(255, 255, 255, posY1)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[9] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*78, y*237, x*402, y*35) then cor[10] = tocolor(255, 0, 0, 255) end
    --if cursorPosition(x*78, y*274, x*402, y*35) then cor[11] = tocolor(255, 0, 0, 255) end
    --if cursorPosition(x*78, y*311, x*402, y*35) then cor[12] = tocolor(255, 0, 0, 255) end
    --if cursorPosition(x*78, y*348, x*402, y*35) then cor[13] = tocolor(255, 0, 0, 255) end

        dxDrawRectangle(x*78, y*200, x*posX1, y*35, cor[1], false)
        dxDrawRectangle(x*78, y*237, x*posX1, y*35, cor[2], false)
        --dxDrawRectangle(x*78, y*274, x*posX1, y*35, cor[3], false)
        --dxDrawRectangle(x*78, y*311, x*posX1, y*35, cor[4], false)
        --dxDrawRectangle(x*78, y*348, x*posX1, y*35, cor[5], false)

        dxDrawRectangle(x*886, y*140, x*200, y*10, tocolor(255, 0, 0, 255), false)
        dxDrawRectangle(x*886, y*150, x*200, y*40, tocolor(255, 255, 255, 255), false)
        dxDrawText("Починка", x*886, y*150, x*1086, y*190, tocolor(0, 0, 0, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)

        dxDrawText("Починить", x*78, y*200, x*480, y*235, cor[9], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        dxDrawText("Восстановить", x*78, y*237, x*480, y*272, cor[10], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
       -- dxDrawText("Внутренний тюнинг", x*78, y*274, x*480, y*309, cor[11], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        --dxDrawText("Колеса", x*78, y*311, x*480, y*346, cor[12], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
       -- dxDrawText("Сменить цвет", x*78, y*348, x*480, y*383, cor[13], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
end

--[[function dxUsuario29()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick30) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[13] = tocolor(255, 255, 255, posX1)
    cor[14] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[14] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawText("Гидравлика", x*492, y*200, x*885, y*237, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Нитро", x*492, y*237, x*885, y*274, cor[14], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*209, x*1278, y*229, cor[13], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*247, x*1278, y*267, cor[14], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario30()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick31) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, 150)
    cor[6] = tocolor(0, 0, 0, 180)
    cor[7] = tocolor(0, 0, 0, 150)
    cor[8] = tocolor(0, 0, 0, 180)
    cor[9] = tocolor(0, 0, 0, 150)
    cor[10] = tocolor(0, 0, 0, 180)
    cor[11] = tocolor(0, 0, 0, 150)
    cor[12] = tocolor(0, 0, 0, 180)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[5] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[6] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[7] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*459, x*806, y*37) then cor[8] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*496, x*806, y*37) then cor[9] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*533, x*806, y*37) then cor[10] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*570, x*806, y*37) then cor[11] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*607, x*806, y*37) then cor[12] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[13] = tocolor(255, 255, 255, posX1)
    cor[14] = tocolor(255, 255, 255, posX1)
    cor[15] = tocolor(255, 255, 255, posX1)
    cor[16] = tocolor(255, 255, 255, posX1)
    cor[17] = tocolor(255, 255, 255, posX1)
    cor[18] = tocolor(255, 255, 255, posX1)
    cor[19] = tocolor(255, 255, 255, posX1)
    cor[20] = tocolor(255, 255, 255, posX1)
    cor[21] = tocolor(255, 255, 255, posX1)
    cor[22] = tocolor(255, 255, 255, posX1)
    cor[23] = tocolor(255, 255, 255, posX1)
    cor[24] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[14] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[15] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[16] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*348, x*806, y*37) then cor[17] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*385, x*806, y*37) then cor[18] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*422, x*806, y*37) then cor[19] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*459, x*806, y*37) then cor[20] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*496, x*806, y*37) then cor[21] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*533, x*806, y*37) then cor[22] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*570, x*806, y*37) then cor[23] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*607, x*806, y*37) then cor[24] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, cor[2], false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, cor[3], false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, cor[4], false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, cor[5], false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, cor[6], false)
        dxDrawRectangle(x*482, y*422, x*806, y*posY1, cor[7], false)
        dxDrawRectangle(x*482, y*459, x*806, y*posY1, cor[8], false)
        dxDrawRectangle(x*482, y*496, x*806, y*posY1, cor[9], false)
        dxDrawRectangle(x*482, y*533, x*806, y*posY1, cor[10], false)
        dxDrawRectangle(x*482, y*570, x*806, y*posY1, cor[11], false)
        dxDrawRectangle(x*482, y*607, x*806, y*posY1, cor[12], false)
        dxDrawText("Вариант 1", x*492, y*200, x*885, y*237, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 2", x*492, y*237, x*885, y*274, cor[14], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 3", x*492, y*274, x*885, y*311, cor[15], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 4", x*492, y*311, x*885, y*348, cor[16], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 5", x*492, y*348, x*885, y*385, cor[17], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 6", x*492, y*385, x*885, y*422, cor[18], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 7", x*492, y*422, x*885, y*459, cor[19], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 8", x*492, y*459, x*885, y*496, cor[20], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 9", x*492, y*496, x*885, y*533, cor[21], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 10", x*492, y*533, x*885, y*570, cor[22], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 11", x*492, y*570, x*885, y*607, cor[23], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Вариант 12", x*492, y*607, x*885, y*644, cor[24], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*209, x*1278, y*229, cor[13], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*247, x*1278, y*267, cor[14], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*283, x*1278, y*303, cor[15], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*321, x*1278, y*341, cor[16], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*356, x*1278, y*376, cor[17], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*394, x*1278, y*414, cor[18], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*431, x*1278, y*451, cor[19], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*468, x*1278, y*488, cor[20], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*505, x*1278, y*525, cor[21], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*542, x*1278, y*562, cor[22], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*579, x*1278, y*599, cor[23], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("Нажмите что бы поставить", x*885, y*616, x*1278, y*636, cor[24], x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
end

function dxUsuario31()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick32) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end

    -- //#Cores Textos
    cor[13] = tocolor(255, 255, 255, posX1)
    cor[14] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[14] = tocolor(0, 0, 0, 255) end

    local veiculo = getPedOccupiedVehicle(getLocalPlayer())

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, cor[1], false)
    if veiculo then
        local r1, g1, b1, r2, g2, b2 = getVehicleColor(veiculo, true)
        dxDrawRectangle(x*1078, y*208, x*200, y*posZ1, tocolor(r1, g1, b1, 255), false)
    else
        dxDrawRectangle(x*1078, y*208, x*200, y*posZ1, tocolor(255, 255, 255, 0), false)
    end
    dxDrawText("Просмотреть доступные цвета", x*492, y*200, x*885, y*237, cor[13], x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
end]]

function dxUsuario32()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 402, 255, 0, ((getTickCount() - tick33) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 150)
    cor[3] = tocolor(255, 0, 0, 150)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[1] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*237, x*402, y*35) then cor[2] = tocolor(255, 255, 255, 255) end
    if cursorPosition(x*78, y*274, x*402, y*35) then cor[3] = tocolor(255, 255, 255, 255) end

    -- //#Cores Textos
    cor[9] = tocolor(255, 255, 255, posY1)
    cor[10] = tocolor(255, 255, 255, posY1)
    cor[11] = tocolor(0, 0, 0, posY1)

    if cursorPosition(x*78, y*200, x*402, y*35) then cor[9] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*78, y*237, x*402, y*35) then cor[10] = tocolor(255, 0, 0, 255) end
    if cursorPosition(x*78, y*274, x*402, y*35) then cor[11] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*78, y*200, x*posX1, y*35, cor[1], false)
        dxDrawRectangle(x*78, y*237, x*posX1, y*35, cor[2], false)
        dxDrawRectangle(x*78, y*274, x*posX1, y*35, cor[3], false)

        dxDrawRectangle(x*1088, y*140, x*200, y*10, tocolor(255, 0, 0, 255), false)
        dxDrawRectangle(x*1088, y*150, x*200, y*40, tocolor(255, 255, 255, 255), false)
        dxDrawText("Параметры", x*1088, y*150, x*1288, y*190, tocolor(0, 0, 0, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)

        dxDrawText("Компоненты", x*78, y*200, x*480, y*235, cor[9], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        dxDrawText("Смена никнейма", x*78, y*237, x*480, y*272, cor[10], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
        dxDrawText("Настройка графики (Отключен)", x*78, y*274, x*480, y*309, cor[11], x*1.00, font_SubMenu, "center", "center", false, false, false, false, false)
end

function dxUsuario33()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick34) / 400), "Linear")
    local alphaX, alphaY, alphaZ = interpolateBetween(0, 0, 0, 50, 150, 46, ((getTickCount() - tick34) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, alphaX)
    cor[6] = tocolor(0, 0, 0, alphaX)
    cor[7] = tocolor(0, 0, 0, alphaX)
    cor[8] = tocolor(0, 0, 0, alphaX)
    cor[9] = tocolor(0, 0, 0, alphaX)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*1178, y*207, x*98, y*25) then cor[5] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*244, x*98, y*27) then cor[6] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*281, x*98, y*27) then cor[7] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*318, x*98, y*27) then cor[8] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*358, x*98, y*27) then cor[9] = tocolor(0, 0, 0, 255) end

    -- //#Cores Textos
    cor[13] = tocolor(255, 255, 255, posX1)
    --cor[14] = tocolor(255, 255, 255, posX1)
    --cor[15] = tocolor(255, 255, 255, posX1)
    --cor[16] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[13] = tocolor(0, 0, 0, 255) end
   -- if cursorPosition(x*482, y*237, x*806, y*37) then cor[14] = tocolor(0, 0, 0, 255) end
    --if cursorPosition(x*482, y*274, x*806, y*37) then cor[15] = tocolor(0, 0, 0, 255) end
   -- if cursorPosition(x*482, y*311, x*806, y*37) then cor[16] = tocolor(0, 0, 0, 255) end

    local radarVisible = getElementData(getLocalPlayer(), "RADARVDP")
    --local hudVisible = getElementData(getLocalPlayer(), "HUDVDP")
   -- local velocimetroVisible = getElementData(getLocalPlayer(), "VELOCIMETROVDP")
    --local levelVisible = getElementData(getLocalPlayer(), "LEVELVDP")

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        --dxDrawRectangle(x*482, y*237, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        --dxDrawRectangle(x*482, y*274, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        --dxDrawRectangle(x*482, y*311, x*806, y*posY1, tocolor(0, 0, 0, 180), false)
        --dxDrawRectangle(x*482, y*348, x*806, y*posY1, tocolor(0, 0, 0, 150), false)
        dxDrawText("Чат", x*492, y*200, x*885, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Убрать весь худ на клавишу 'F10'", x*492, y*237, x*885, y*274, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        --dxDrawText("Худ", x*492, y*274, x*885, y*311, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        --dxDrawText("Спедометр", x*492, y*311, x*885, y*348, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        --dxDrawText("Уровень", x*492, y*348, x*385, y*385, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)

    if (isChatVisible()) then
        dxDrawRectangle(x*1178, y*207, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*210, x*92, y*19, cor[5], false)
        dxDrawRectangle(x*1181, y*210, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*210, x*1227, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (not isChatVisible()) then
        dxDrawRectangle(x*1178, y*207, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*210, x*92, y*19, cor[5], false)
        dxDrawRectangle(x*1227, y*210, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*210, x*1273, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    --[[if (radarVisible == true) then
        dxDrawRectangle(x*1178, y*244, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*247, x*92, y*19, cor[6], false)
        dxDrawRectangle(x*1181, y*247, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*247, x*1227, y*266, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (radarVisible == false) then
        dxDrawRectangle(x*1178, y*244, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*247, x*92, y*19, cor[6], false)
        dxDrawRectangle(x*1227, y*247, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*247, x*1273, y*266, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (hudVisible == true) then
        dxDrawRectangle(x*1178, y*281, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*284, x*92, y*19, cor[7], false)
        dxDrawRectangle(x*1181, y*284, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*284, x*1227, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (hudVisible == false) then
        dxDrawRectangle(x*1178, y*281, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*284, x*92, y*19, cor[7], false)
        dxDrawRectangle(x*1227, y*284, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*284, x*1273, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (velocimetroVisible == true) then
        dxDrawRectangle(x*1178, y*318, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*321, x*92, y*19, cor[8], false)
        dxDrawRectangle(x*1181, y*321, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*321, x*1227, y*340, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (velocimetroVisible == false) then
        dxDrawRectangle(x*1178, y*318, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*321, x*92, y*19, cor[8], false)
        dxDrawRectangle(x*1227, y*321, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*321, x*1273, y*340, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (levelVisible == true) then
        dxDrawRectangle(x*1178, y*355, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*358, x*92, y*19, cor[9], false)
        dxDrawRectangle(x*1181, y*358, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*358, x*1227, y*377, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (levelVisible == false) then
        dxDrawRectangle(x*1178, y*355, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*358, x*92, y*19, cor[9], false)
        dxDrawRectangle(x*1227, y*358, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*358, x*1273, y*377, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end]]
end

function dxUsuario34()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 100, ((getTickCount() - tick35) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, posZ1)

    if cursorPosition(x*1126, y*203, x*150, y*31) then cor[1] = tocolor(255, 0, 0, 180) end

    -- //#Cores Textos
    cor[13] = tocolor(255, 255, 255, posX1)

    if cursorPosition(x*1126, y*203, x*150, y*31) then cor[13] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*37, tocolor(0, 0, 0, posZ1), false)

        dxDrawRectangle(x*1126, y*203, x*150, y*31, cor[1], false)
        dxDrawText("Никнейм :", x*492, y*200, x*885, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Сменить", x*1126, y*203, x*1276, y*234, cor[13], x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)

    for k, self in pairs(editBox.instances) do
        if self.visible then
            local px, py, pw, ph = self:getPosition()
            local text = self.masked and string.gsub(self.text, ".", "•") or self.text
            local alignX = dxGetTextWidth(text, self.scale, self.font) <= pw and "left" or "right"
            dxDrawRectangle(px, py, pw, ph, tocolor(unpack(self.color)))
            dxDrawText(text, px + x*5, py, px - x*5 + pw, py + ph, tocolor(unpack(self.textColor)), self.scale, self.font, alignX, "center", true)
            if self.input and dxGetTextWidth(text, self.scale, self.font) <= pw then
                local lx = dxGetTextWidth(text, self.scale, self.font) + px + x*8
                local lx = dxGetTextWidth(text, self.scale, self.font) + px + x*8
                dxDrawLine(lx, py + y*6, lx, py + ph - y*6, tocolor(255, 255, 255, math.abs(math.sin(getTickCount() / 300))*200), 1)
            end
        end
    end

    if getKeyState("backspace") then
        for k, self in pairs(editBox.instances) do
            if self.visible and self.input then
                if not keyState then
                    keyState = getTickCount() + 400
                    self.text = string.sub(self.text, 1, string.len(self.text) - 1)
                elseif keyState and keyState < getTickCount() then
                    keyState = getTickCount() + 100
                    self.text = string.sub(self.text, 1, string.len(self.text) - 1)
                end
                return
            end
        end
        keyState = nil
    end
end

function dxUsuario35()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 0, 0, 255, 37, 20, ((getTickCount() - tick36) / 400), "Linear")
    local alphaX, alphaY, alphaZ = interpolateBetween(0, 0, 0, 50, 150, 46, ((getTickCount() - tick36) / 400), "Linear")

    -- //#Cores Retangulos
    cor[1] = tocolor(0, 0, 0, 150)
    cor[2] = tocolor(0, 0, 0, 180)
    cor[3] = tocolor(0, 0, 0, 150)
    cor[4] = tocolor(0, 0, 0, 180)
    cor[5] = tocolor(0, 0, 0, alphaX)
    cor[6] = tocolor(0, 0, 0, alphaX)
    cor[7] = tocolor(0, 0, 0, alphaX)
    cor[8] = tocolor(0, 0, 0, alphaX)
    cor[9] = tocolor(0, 0, 0, alphaX)
    cor[10] = tocolor(0, 0, 0, alphaX)
    cor[11] = tocolor(0, 0, 0, alphaX)
    cor[12] = tocolor(0, 0, 0, alphaX)
    cor[13] = tocolor(0, 0, 0, alphaX)

    if cursorPosition(x*482, y*200, x*806, y*37) then cor[1] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*237, x*806, y*37) then cor[2] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*274, x*806, y*37) then cor[3] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*482, y*311, x*806, y*37) then cor[4] = tocolor(255, 255, 255, 180) end
    if cursorPosition(x*1178, y*207, x*98, y*25) then cor[5] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*244, x*98, y*27) then cor[6] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*281, x*98, y*27) then cor[7] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*318, x*98, y*27) then cor[8] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*355, x*98, y*27) then cor[9] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*392, x*98, y*27) then cor[10] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*429, x*98, y*27) then cor[11] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*466, x*98, y*27) then cor[12] = tocolor(0, 0, 0, 255) end
    if cursorPosition(x*1178, y*503, x*98, y*27) then cor[13] = tocolor(0, 0, 0, 255) end

        dxDrawRectangle(x*482, y*200, x*806, y*posY1, tocolor(255, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*237, x*806, y*posY1, tocolor(255, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*274, x*806, y*posY1, tocolor(255, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*311, x*806, y*posY1, tocolor(255, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*348, x*806, y*posY1, tocolor(255, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*385, x*806, y*posY1, tocolor(255, 0, 0, 180), false)
        dxDrawRectangle(x*482, y*422, x*806, y*posY1, tocolor(255, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*459, x*806, y*posY1, tocolor(255, 0, 0, 180), false)
        dxDrawText("Улучшенные обьекты", x*492, y*200, x*885, y*237, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Блеск транспорта", x*492, y*237, x*885, y*274, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Контраст", x*492, y*274, x*885, y*311, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Яркое небо", x*492, y*311, x*885, y*348, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Качество деревьев", x*492, y*348, x*885, y*385, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Установка ENB", x*492, y*385, x*885, y*422, tocolor(255, 255, 255, posX1-155), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Качество игрового мира", x*492, y*422, x*885, y*459, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)
        dxDrawText("Качественные дороги", x*492, y*459, x*885, y*496, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "left", "center", false, false, false, false, false)

    if (getElementData(getLocalPlayer(), "Bloom") == true) then
        dxDrawRectangle(x*1178, y*207, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*210, x*92, y*19, cor[5], false)
        dxDrawRectangle(x*1181, y*210, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*210, x*1227, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Bloom") == false) then
        dxDrawRectangle(x*1178, y*207, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*210, x*92, y*19, cor[5], false)
        dxDrawRectangle(x*1227, y*210, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*210, x*1273, y*229, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "cprlEffectEnabled") == true) then
        dxDrawRectangle(x*1178, y*244, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*247, x*92, y*19, cor[6], false)
        dxDrawRectangle(x*1181, y*247, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*247, x*1227, y*266, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "cprlEffectEnabled") == false) then
        dxDrawRectangle(x*1178, y*244, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*247, x*92, y*19, cor[6], false)
        dxDrawRectangle(x*1227, y*247, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*247, x*1273, y*266, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Contrast") == true) then
        dxDrawRectangle(x*1178, y*281, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*284, x*92, y*19, cor[7], false)
        dxDrawRectangle(x*1181, y*284, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*284, x*1227, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Contrast") == false) then
        dxDrawRectangle(x*1178, y*281, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*284, x*92, y*19, cor[7], false)
        dxDrawRectangle(x*1227, y*284, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*284, x*1273, y*303, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "DoF") == true) then
        dxDrawRectangle(x*1178, y*318, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*321, x*92, y*19, cor[8], false)
        dxDrawRectangle(x*1181, y*321, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*321, x*1227, y*340, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "DoF") == false) then
        dxDrawRectangle(x*1178, y*318, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*321, x*92, y*19, cor[8], false)
        dxDrawRectangle(x*1227, y*321, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*321, x*1273, y*340, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Radial Blur") == true) then
        dxDrawRectangle(x*1178, y*355, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*358, x*92, y*19, cor[9], false)
        dxDrawRectangle(x*1181, y*358, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*358, x*1227, y*377, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Radial Blur") == false) then
        dxDrawRectangle(x*1178, y*355, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*358, x*92, y*19, cor[9], false)
        dxDrawRectangle(x*1227, y*358, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*358, x*1273, y*377, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Skybox") == true) then
        dxDrawRectangle(x*1178, y*392, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*395, x*92, y*19, cor[10], false)
        dxDrawRectangle(x*1181, y*395, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("ON", x*1181, y*395, x*1227, y*414, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Skybox") == false) then
        dxDrawRectangle(x*1178, y*392, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*395, x*92, y*19, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1227, y*395, x*46, y*19, tocolor(0, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*395, x*1273, y*414, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Water") == true) then
        dxDrawRectangle(x*1178, y*429, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*432, x*92, y*19, cor[11], false)
        dxDrawRectangle(x*1181, y*432, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*432, x*1227, y*451, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Water") == false) then
        dxDrawRectangle(x*1178, y*429, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*432, x*92, y*19, cor[11], false)
        dxDrawRectangle(x*1227, y*432, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*432, x*1273, y*451, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Wet Roads") == true) then
        dxDrawRectangle(x*1178, y*466, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*469, x*92, y*19, cor[12], false)
        dxDrawRectangle(x*1181, y*469, x*46, y*19, tocolor(0, 255, 0, alphaY), false)
        dxDrawText("Вкл", x*1181, y*469, x*1227, y*488, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end

    if (getElementData(getLocalPlayer(), "Wet Roads") == false) then
        dxDrawRectangle(x*1178, y*466, x*98, y*25, tocolor(0, 0, 0, alphaX), false)
        dxDrawRectangle(x*1181, y*469, x*92, y*19, cor[12], false)
        dxDrawRectangle(x*1227, y*469, x*46, y*19, tocolor(255, 0, 0, alphaY), false)
        dxDrawText("Выкл", x*1227, y*469, x*1273, y*488, tocolor(255, 255, 255, posX1), x*1.00, font_MenuStatus, "center", "center", false, false, false, false, false)
    end
end

function usuarioClick(botao, state)
    if usuario1 == true and usuario2 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*150, x*200, y*40) then
            color = true
            usuario1 = true
            usuario2 = true
            usuario9 = false
            usuario10 = false
            usuario17 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick3 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario2)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario9)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario17)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario1 == true and usuario9 == false and botao == "left" and state == "down" then
        if cursorPosition(x*280, y*150, x*200, y*40) then
            color = true
            usuario1 = true
            usuario9 = true
            usuario2 = false
            usuario3 = false
            usuario4 = false
            usuario5 = false
            usuario6 = false
            usuario7 = false
            usuario8 = false
            usuario17 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick10 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario9)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario2)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario17)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario1 == true and usuario17 == false and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*150, x*200, y*40) then
            color = true
            usuario1 = true
            usuario17 = true
            usuario2 = false
            usuario3 = false
            usuario4 = false
            usuario5 = false
            usuario6 = false
            usuario7 = false
            usuario8 = false
            usuario9 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick18 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario17)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario2)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario9)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario1 == true and usuario23 == false and botao == "left" and state == "down" then
        if cursorPosition(x*684, y*150, x*200, y*40) then
            color = true
            usuario1 = true
            usuario23 = true
            usuario2 = false
            usuario3 = false
            usuario4 = false
            usuario5 = false
            usuario6 = false
            usuario7 = false
            usuario8 = false
            usuario9 = false
            usuario17 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick24 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario2)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario9)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario17)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario1 == true and usuario28 == false and botao == "left" and state == "down" then
        if cursorPosition(x*886, y*150, x*200, y*40) then
            color = true
            usuario1 = true
            usuario28 = true
            usuario2 = false
            usuario3 = false
            usuario4 = false
            usuario5 = false
            usuario6 = false
            usuario7 = false
            usuario8 = false
            usuario9 = false
            usuario17 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick29 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario2)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario9)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario17)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario1 == true and usuario32 == false and botao == "left" and state == "down" then
        if cursorPosition(x*1088, y*150, x*200, y*40) then
            color = true
            usuario1 = true
            usuario32 = true
            usuario2 = false
            usuario3 = false
            usuario4 = false
            usuario5 = false
            usuario6 = false
            usuario7 = false
            usuario8 = false
            usuario9 = false
            usuario17 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick33 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario2)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario9)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario17)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario2 == true and usuario3 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*200, x*402, y*35) then
            color = true
            usuario2 = true
            usuario3 = true
            usuario4 = false
            usuario5 = false
            usuario6 = false
            usuario7 = false
            usuario8 = false
            tick4 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
        end
    end
    if usuario2 == true and usuario4 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*237, x*402, y*35) then
            color = true
            usuario2 = true
            usuario4 = true
            usuario3 = false
            usuario5 = false
            usuario6 = false
            usuario7 = false
            usuario8 = false
            tick5 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
        end
    end
    if usuario2 == true and usuario5 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*274, x*402, y*35) then
            color = true
            usuario2 = true
            usuario5 = true
            usuario3 = false
            usuario4 = false
            usuario6 = false
            usuario7 = false
            usuario8 = false
            tick6 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
        end
    end
    if usuario2 == true and usuario6 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*311, x*402, y*35) then
            color = true
            usuario2 = true
            usuario6 = true
            usuario3 = false
            usuario4 = false
            usuario5 = false
            usuario7 = false
            usuario8 = false
            tick7 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
        end
    end
    if usuario2 == true and usuario7 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*348, x*402, y*35) then
            color = true
            usuario2 = true
            usuario7 = true
            usuario3 = false
            usuario4 = false
            usuario5 = false
            usuario6 = false
            usuario8 = false
            tick8 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario7)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
        end
    end
    if usuario2 == true and usuario8 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*385, x*402, y*35) then
            color = true
            usuario2 = true
            usuario8 = true
            usuario3 = false
            usuario4 = false
            usuario5 = false
            usuario6 = false
            usuario7 = false
            tick9 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario8)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
        end
    end
    if usuario9 == true and usuario10 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*200, x*402, y*35) then
            color = true
            usuario9 = true
            usuario10 = true
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario16 = false
            tick11 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
        end
    end
    if usuario9 == true and usuario11 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*237, x*402, y*35) then
            color = true
            usuario9 = true
            usuario11 = true
            usuario10 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario16 = false
            tick12 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
        end
    end
    if usuario9 == true and usuario12 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*274, x*402, y*35) then
            color = true
            usuario9 = true
            usuario12 = true
            usuario10 = false
            usuario11 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario16 = false
            tick13 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
        end
    end
    if usuario9 == true and usuario13 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*311, x*402, y*35) then
            color = true
            usuario9 = true
            usuario13 = true
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario14 = false
            usuario15 = false
            usuario16 = false
            tick14 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
        end
    end
    if usuario9 == true and usuario14 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*348, x*402, y*35) then
            color = true
            usuario9 = true
            usuario14 = true
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario15 = false
            usuario16 = false
            tick15 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
        end
    end
    if usuario9 == true and usuario15 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*385, x*402, y*35) then
            color = true
            usuario9 = true
            usuario15 = true
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario16 = false
            tick16 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
        end
    end
    if usuario9 == true and usuario16 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*422, x*402, y*35) then
            color = true
            usuario9 = true
            usuario16 = true
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            tick17 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
        end
    end
    if usuario17 == true and usuario18 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*200, x*402, y*35) then
            color = true
            usuario17 = true
            usuario18 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick19 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario17 == true and usuario19 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*237, x*402, y*35) then
            color = true
            usuario17 = true
            usuario19 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario18 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick20 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario17 == true and usuario20 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*274, x*402, y*35) then
            color = true
            usuario17 = true
            usuario20 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario18 = false
            usuario19 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick21 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario17 == true and usuario21 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*311, x*402, y*35) then
            color = true
            usuario17 = true
            usuario21 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick22 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario17 == true and usuario22 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*348, x*402, y*35) then
            color = true
            usuario17 = true
            usuario22 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario32 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick23 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario23 == true and usuario24 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*200, x*402, y*35) then
            color = true
            usuario23 = true
            usuario24 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick25 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario23 == true and usuario25 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*237, x*402, y*35) then
            color = true
            usuario23 = true
            usuario25 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario24 = false
            usuario26 = false
            usuario27 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick26 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario23 == true and usuario26 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*274, x*402, y*35) then
            color = true
            usuario23 = true
            usuario26 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario24 = false
            usuario25 = false
            usuario27 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick27 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario23 == true and usuario27 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*311, x*402, y*35) then
            color = true
            usuario23 = true
            usuario27 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario18 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario29 = false
            usuario30 = false
            usuario31 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick28 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario9 == true and usuario10 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("arma1", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("arma2", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("arma3", getLocalPlayer())
        end
    end
   --[[ if usuario9 == true and usuario11 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("arma5", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("arma6", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("arma7", getLocalPlayer())
        end
    end
    if usuario9 == true and usuario12 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("arma8", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("arma9", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("arma10", getLocalPlayer())
        end
    end
    if usuario9 == true and usuario13 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("arma11", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("arma12", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("arma13", getLocalPlayer())
        end
    end
    if usuario9 == true and usuario14 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("arma14", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("arma15", getLocalPlayer())
        end
    end
    if usuario9 == true and usuario15 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("arma16", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("arma17", getLocalPlayer())
        end
    end
    if usuario9 == true and usuario16 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("arma18", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("arma19", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("arma20", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("arma21", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("arma22", getLocalPlayer())
        elseif cursorPosition(x*482, y*385, x*806, y*37) then
            triggerServerEvent("arma23", getLocalPlayer())
        elseif cursorPosition(x*482, y*422, x*806, y*37) then
            triggerServerEvent("arma24", getLocalPlayer())
        end
    end]]
    if usuario17 == true and usuario18 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("skin1", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("skin2", getLocalPlayer())
        --[[elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("skin3", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("skin18", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("skin19", getLocalPlayer())
        elseif cursorPosition(x*482, y*385, x*806, y*37) then
            triggerServerEvent("skin20", getLocalPlayer())
        elseif cursorPosition(x*482, y*422, x*806, y*37) then
            triggerServerEvent("skin21", getLocalPlayer())]]
        end
    end
    --[[if usuario17 == true and usuario19 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("skin4", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("skin5", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("skin6", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("skin22", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("skin23", getLocalPlayer())
        elseif cursorPosition(x*482, y*385, x*806, y*37) then
            triggerServerEvent("skin24", getLocalPlayer())
        elseif cursorPosition(x*482, y*422, x*806, y*37) then
            triggerServerEvent("skin25", getLocalPlayer())
        end
    end
    if usuario17 == true and usuario20 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("skin7", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("skin8", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("skin9", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("skin26", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("skin27", getLocalPlayer())
        end
    end
    if usuario17 == true and usuario21 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("skin10", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("skin11", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("skin12", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("skin13", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("skin14", getLocalPlayer())
        elseif cursorPosition(x*482, y*385, x*806, y*37) then
            triggerServerEvent("skin28", getLocalPlayer())
        end
    end
    if usuario17 == true and usuario22 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("skin15", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("skin16", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("skin17", getLocalPlayer())
        end
    end]]
    if usuario23 == true and usuario24 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("carro33", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("carro30", getLocalPlayer())
       --[[ elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("carro3", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("carro4", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("carro5", getLocalPlayer())
        elseif cursorPosition(x*482, y*385, x*806, y*37) then
            triggerServerEvent("carro6", getLocalPlayer())
        elseif cursorPosition(x*482, y*422, x*806, y*37) then
            triggerServerEvent("carro7", getLocalPlayer())
        elseif cursorPosition(x*482, y*459, x*806, y*37) then
            triggerServerEvent("carro8", getLocalPlayer())
        elseif cursorPosition(x*482, y*496, x*806, y*37) then
            triggerServerEvent("carro9", getLocalPlayer())
        elseif cursorPosition(x*482, y*533, x*806, y*37) then
            triggerServerEvent("carro10", getLocalPlayer())
        elseif cursorPosition(x*482, y*570, x*806, y*37) then
            triggerServerEvent("carro11", getLocalPlayer())
        elseif cursorPosition(x*482, y*607, x*806, y*37) then
            triggerServerEvent("carro12", getLocalPlayer())
        end
    end
    if usuario23 == true and usuario25 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("carro13", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("carro14", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("carro15", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("carro16", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("carro17", getLocalPlayer())
        elseif cursorPosition(x*482, y*385, x*806, y*37) then
            triggerServerEvent("carro18", getLocalPlayer())
        elseif cursorPosition(x*482, y*422, x*806, y*37) then
            triggerServerEvent("carro19", getLocalPlayer())
        end
    end
    if usuario23 == true and usuario26 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("carro20", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("carro21", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("carro22", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("carro23", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("carro24", getLocalPlayer())
        elseif cursorPosition(x*482, y*385, x*806, y*37) then
            triggerServerEvent("carro25", getLocalPlayer())
        elseif cursorPosition(x*482, y*422, x*806, y*37) then
            triggerServerEvent("carro26", getLocalPlayer())
        elseif cursorPosition(x*482, y*459, x*806, y*37) then
            triggerServerEvent("carro27", getLocalPlayer())
        elseif cursorPosition(x*482, y*496, x*806, y*37) then
            triggerServerEvent("carro28", getLocalPlayer())
        end
    end
    if usuario23 == true and usuario27 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("carro29", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("carro30", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("carro31", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("carro32", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("carro33", getLocalPlayer())
        elseif cursorPosition(x*482, y*385, x*806, y*37) then
            triggerServerEvent("carro34", getLocalPlayer())
        elseif cursorPosition(x*482, y*422, x*806, y*37) then
            triggerServerEvent("carro35", getLocalPlayer())
        elseif cursorPosition(x*482, y*459, x*806, y*37) then
            triggerServerEvent("carro36", getLocalPlayer())
        elseif cursorPosition(x*482, y*496, x*806, y*37) then
            triggerServerEvent("carro37", getLocalPlayer())]]
        end
    end
    if usuario1 == true and usuario28 == true and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*200, x*402, y*35) then
            triggerServerEvent("reparar", getLocalPlayer())
        elseif cursorPosition(x*78, y*237, x*402, y*35) then
            triggerServerEvent("virar", getLocalPlayer())
        end
    end
    if usuario28 == true and usuario29 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*274, x*402, y*35) then
            color = true
            usuario28 = true
            usuario29 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario30 = false
            usuario31 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick30 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario28 == true and usuario30 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*311, x*402, y*35) then
            color = true
            usuario28 = true
            usuario30 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario29 = false
            usuario31 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick31 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario28 == true and usuario31 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*348, x*402, y*35) then
            color = true
            usuario28 = true
            usuario31 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario29 = false
            usuario30 = false
            usuario33 = false
            usuario34 = false
            usuario35 = false
            tick32 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario28 == true and usuario29 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("hidraulica", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("nitro", getLocalPlayer())
        end
    end
   --[[ if usuario28 == true and usuario30 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            triggerServerEvent("roda1", getLocalPlayer())
        elseif cursorPosition(x*482, y*237, x*806, y*37) then
            triggerServerEvent("roda2", getLocalPlayer())
        elseif cursorPosition(x*482, y*274, x*806, y*37) then
            triggerServerEvent("roda3", getLocalPlayer())
        elseif cursorPosition(x*482, y*311, x*806, y*37) then
            triggerServerEvent("roda4", getLocalPlayer())
        elseif cursorPosition(x*482, y*348, x*806, y*37) then
            triggerServerEvent("roda5", getLocalPlayer())
        elseif cursorPosition(x*482, y*385, x*806, y*37) then
            triggerServerEvent("roda6", getLocalPlayer())
        elseif cursorPosition(x*482, y*422, x*806, y*37) then
            triggerServerEvent("roda7", getLocalPlayer())
        elseif cursorPosition(x*482, y*459, x*806, y*37) then
            triggerServerEvent("roda8", getLocalPlayer())
        elseif cursorPosition(x*482, y*496, x*806, y*37) then
            triggerServerEvent("roda9", getLocalPlayer())
        elseif cursorPosition(x*482, y*533, x*806, y*37) then
            triggerServerEvent("roda10", getLocalPlayer())
        elseif cursorPosition(x*482, y*570, x*806, y*37) then
            triggerServerEvent("roda11", getLocalPlayer())
        elseif cursorPosition(x*482, y*607, x*806, y*37) then
            triggerServerEvent("roda12", getLocalPlayer())
        end
    end]]
    if usuario28 == true and usuario31 == true and botao == "left" and state == "down" then
        if cursorPosition(x*482, y*200, x*806, y*37) then
            openColorPicker()
        end
    end
    if usuario32 == true and usuario33 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*200, x*402, y*35) then
            color = true
            usuario32 = true
            usuario33 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario34 = false
            usuario35 = false
            tick34 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario32 == true and usuario34 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*237, x*402, y*35) then
            color = true
            usuario32 = true
            usuario34 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario33 = false
            usuario35 = false
            g.nick.visible = true
            tick35 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario34)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario35)
        end
    end
    if usuario32 == true and usuario35 == false and botao == "left" and state == "down" then
        if cursorPosition(x*78, y*274, x*402, y*35) then
            color = true
            usuario32 = true
            usuario35 = true
            usuario9 = false
            usuario16 = false
            usuario10 = false
            usuario11 = false
            usuario12 = false
            usuario13 = false
            usuario14 = false
            usuario15 = false
            usuario17 = false
            usuario19 = false
            usuario20 = false
            usuario21 = false
            usuario22 = false
            usuario23 = false
            usuario24 = false
            usuario25 = false
            usuario26 = false
            usuario27 = false
            usuario28 = false
            usuario29 = false
            usuario30 = false
            usuario33 = false
            usuario34 = false
            tick36 = getTickCount()
            addEventHandler("onClientRender", getRootElement(), dxUsuario35)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
            removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
        end
    end
    if usuario32 == true and usuario33 == true and botao == "left" and state == "down" then
        if cursorPosition(x*1178, y*207, x*98, y*25) then
            if (isChatVisible()) then
                showChat(false)
            else
                showChat(true)
            end
        elseif cursorPosition(x*1178, y*244, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "RADARVDP") == true) then
                setElementData(getLocalPlayer(), "RADARVDP", false)
            else
                setElementData(getLocalPlayer(), "RADARVDP", true)
            end
        elseif cursorPosition(x*1178, y*281, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "HUDVDP") == true) then
                setElementData(getLocalPlayer(), "HUDVDP", false)
            else
                setElementData(getLocalPlayer(), "HUDVDP", true)
            end
        elseif cursorPosition(x*1178, y*318, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "VELOCIMETROVDP") == true) then
                setElementData(getLocalPlayer(), "VELOCIMETROVDP", false)
            else
                setElementData(getLocalPlayer(), "VELOCIMETROVDP", true)
            end
        elseif cursorPosition(x*1178, y*355, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "LEVELVDP") == true) then
                setElementData(getLocalPlayer(), "LEVELVDP", false)
            else
                setElementData(getLocalPlayer(), "LEVELVDP", true)
            end
        end
    end
    if usuario32 == true and usuario34 == true and botao == "left" and state == "down" then
        for k, self in pairs(editBox.instances) do
            if self.visible then
                if self.input then
                    self.input = nil
                    self.onOutput()
                end
                local x, y, w, h = self:getPosition()
                if cursorPosition(x, y, w, h) then
                    self.input = true
                    self.onInput()
                end
            end
        end
        if cursorPosition(x*1126, y*203, x*150, y*31) then
            triggerServerEvent("nickName", getLocalPlayer(), g.nick.text)
        end
    end
    if usuario32 == true and usuario35 == true and botao == "left" and state == "down" then
        if cursorPosition(x*1178, y*207, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "Bloom") == true) then
                setElementData(getLocalPlayer(), "Bloom", false)
            else
                setElementData(getLocalPlayer(), "Bloom", true)
            end
        elseif cursorPosition(x*1178, y*244, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "cprlEffectEnabled") == true) then
                setElementData(getLocalPlayer(), "cprlEffectEnabled", false)
            else
                setElementData(getLocalPlayer(), "cprlEffectEnabled", true)
            end
        elseif cursorPosition(x*1178, y*281, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "Contrast") == true) then
                setElementData(getLocalPlayer(), "Contrast", false)
            else
                setElementData(getLocalPlayer(), "Contrast", true)
            end
        elseif cursorPosition(x*1178, y*318, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "DoF") == true) then
                setElementData(getLocalPlayer(), "DoF", false)
            else
                setElementData(getLocalPlayer(), "DoF", true)
            end
        elseif cursorPosition(x*1178, y*355, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "Radial Blur") == true) then
                setElementData(getLocalPlayer(), "Radial Blur", false)
            else
                setElementData(getLocalPlayer(), "Radial Blur", true)
            end
        --[[elseif cursorPosition(x*1178, y*392, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "Skybox") == true) then
                setElementData(getLocalPlayer(), "Skybox", false)
            else
                setElementData(getLocalPlayer(), "Skybox", true)
            end]]--
        elseif cursorPosition(x*1178, y*429, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "Water") == true) then
                setElementData(getLocalPlayer(), "Water", false)
            else
                setElementData(getLocalPlayer(), "Water", true)
            end
        elseif cursorPosition(x*1178, y*466, x*98, y*25) then
            if (getElementData(getLocalPlayer(), "Wet Roads") == true) then
                setElementData(getLocalPlayer(), "Wet Roads", false)
            else
                setElementData(getLocalPlayer(), "Wet Roads", true)
            end
        end
    end
end

function abrirUsuario()
    if (usuario1 == false) then
	    showChat(false)
        tick1 = getTickCount()
        usuario1 = true
        showCursor(true)
        addEventHandler("onClientRender", getRootElement(), dxUsuario1)
        removeEventHandler("onClientRender", getRootElement(), dxUsuarioAnimationClose)

        setTimer(function()
            color = true
            addEventHandler("onClientClick", getRootElement(), usuarioClick)
        end, 100, 1)
    else
	    showChat(true)
        colorPicker.closeSelect()
        showCursor(false)
        color = false
        usuario1 = false
        usuario2 = false
        usuario3 = false
        usuario4 = false
        usuario5 = false
        usuario6 = false
        usuario7 = false
        usuario8 = false
        usuario9 = false
        usuario10 = false
        usuario11 = false
        usuario12 = false
        usuario13 = false
        usuario14 = false
        usuario15 = false
        usuario16 = false
        usuario17 = false
        usuario18 = false
        usuario19 = false
        usuario20 = false
        usuario21 = false
        usuario22 = false
        usuario23 = false
        usuario24 = false
        usuario25 = false
        usuario26 = false
        usuario27 = false
        usuario28 = false
        usuario29 = false
        usuario30 = false
        usuario31 = false
        usuario32 = false
        usuario33 = false
        usuario34 = false
        usuario35 = false
        removeEventHandler("onClientRender", getRootElement(), dxUsuario1)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario2)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario3)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario4)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario5)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario6)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario7)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario8)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario9)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario10)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario11)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario12)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario13)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario14)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario15)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario16)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario17)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario18)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario19)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario20)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario21)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario22)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario23)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario24)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario25)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario26)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario27)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario28)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario29)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario30)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario31)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario32)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario33)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario34)
        removeEventHandler("onClientRender", getRootElement(), dxUsuario35)

        addEventHandler("onClientRender", getRootElement(), dxUsuarioAnimationClose)
        tick2 = getTickCount()
        setTimer(function()
            removeEventHandler("onClientRender", getRootElement(), dxUsuarioAnimationClose)
            removeEventHandler("onClientClick", getRootElement(), usuarioClick)
        end, 100, 1)
    end
end
bindKey("F1", "down", abrirUsuario)

function dxUsuarioAnimationClose()

    -- //#Animação
    local posX1, posY1, posZ1 = interpolateBetween(0, 150, 0, 0, -50, 0, ((getTickCount() - tick2) / 100), "OutBounce")
    local posX2, posY2, posZ2 = interpolateBetween(0, 150, 0, 0, -250, 0, ((getTickCount() - tick2) / 100), "OutBounce")
    local posX3, posY3, posZ3 = interpolateBetween(76, 70, 90, -350, -350, -350, ((getTickCount() - tick2) / 100), "OutBounce")
    local posX4, posY4, posZ4 = interpolateBetween(110, 0, 0, -350, 0, 0, ((getTickCount() - tick2) / 100), "OutBounce")
    local alphaFundo, _, _ = interpolateBetween(100, 0, 0, 0, 0, 0, ((getTickCount() - tick2) / 100), "Linear")

        dxDrawRectangle(x*0, y*0, x*1366, y*768, tocolor(0, 0, 0, alphaFundo), false)

    local nomePlayer = getPlayerName(getLocalPlayer())
    local id = getElementData(getLocalPlayer(), "ID") or "N/A"
    local time = getRealTime()
    local day = {"Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"}
    local dia = day[time.weekday + 1]
    local hours = time.hour
    local minutes = time.minute
    if (hours >= 0 and hours < 10) then
        hours = "0"..time.hour
    end
    if (minutes >= 0 and minutes < 10) then
        minutes = "0"..time.minute
    end
    local dinheiro = ("%09d"):format(getPlayerMoney(getLocalPlayer()))

    dxDrawText("              Меню | Reventon RP", x*78, y*posX3, x*328, y*126, tocolor(255, 0, 0, 255), x*1.00, font_Default, "center", "center", false, false, false, false, false)
        dxDrawText(nomePlayer.." #FFFFFF("..id..")", x*1243, y*posY3, x*1288, y*90, tocolor(255, 255, 255, 255), x*1.00, font_MenuStatus, "right", "center", false, false, false, true, false)
        dxDrawText(dia.."  "..hours..":"..minutes, x*1203, y*posZ3, x*1288, y*110, tocolor(255, 255, 255, 255), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)
        dxDrawText("USD "..dinheiro, x*1203, y*posX4, x*1288, y*130, tocolor(255, 255, 255, 255), x*1.00, font_MenuStatus, "right", "center", false, false, false, false, false)

        dxDrawRectangle(x*78, y*posY1, x*200, y*40, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*280, y*posY1, x*200, y*40, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*482, y*posY1, x*200, y*40, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*684, y*posY1, x*200, y*40, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*886, y*posY1, x*200, y*40, tocolor(0, 0, 0, 150), false)
        dxDrawRectangle(x*1088, y*posY1, x*200, y*40, tocolor(0, 0, 0, 150), false)
        dxDrawText("Статистика", x*78, y*posY2, x*278, y*190, tocolor(255, 255, 255, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Оружие", x*280, y*posY2, x*480, y*190, tocolor(255, 255, 255, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Одежда", x*482, y*posY2, x*682, y*190, tocolor(255, 255, 255, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Транспорт", x*684, y*posY2, x*884, y*190, tocolor(255, 255, 255, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Починка", x*886, y*posY2, x*1086, y*190, tocolor(255, 255, 255, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)
        dxDrawText("Параметры", x*1088, y*posY2, x*1288, y*190, tocolor(255, 255, 255, 255), x*1.00, font_Menu, "center", "center", false, false, false, false, false)
end

editBox = {}
editBox.__index = editBox
editBox.instances = {}

function createEditBoxNick()
    font = dxCreateFont("files/font/fontNick.ttf", 20)
    g = {}
    g.nick = editBox.new()
    g.nick:setPosition(x*610, y*203, x*250, y*31)
    g.nick.color = {0, 0, 0, 100}
    g.nick.font = font
    g.nick.text = ""
    g.nick.visible = false

    g.nick.onInput = function()
        g.nick.color = {0, 0, 0, 150}
    end

    g.nick.onOutput = function()
        g.nick.color = {0, 0, 0, 100}
    end
end
addEventHandler("onClientResourceStart", getResourceRootElement(getThisResource()), createEditBoxNick)

function editBox.new()
    local self = setmetatable({}, editBox)
    self.text = ""
    self.maxLength = 22
    self.scale = x*0.7
    self.state = "normal"
    self.font = "sans"
    self.color = {255, 255, 255, 255}
    self.textColor = {255, 255, 255, 255}
    table.insert(editBox.instances, self)
    return self
end

function editBox:getPosition()
    return self.x, self.y, self.w, self.h
end

function editBox:setPosition(x, y, w, h)
    self.x, self.y, self.w, self.h = x, y, w, h
    return true
end

function onClientCharacter(character)
    if not isCursorShowing() then
        return
    end
    for k, self in pairs(editBox.instances) do
        if self.visible and self.input then
            if (string.len(self.text)) < self.maxLength then
                self.text = self.text..character
            end
        end
    end
end
addEventHandler("onClientCharacter", getRootElement(), onClientCharacter)