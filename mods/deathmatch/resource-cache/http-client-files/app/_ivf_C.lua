function updateSteeringWheelRotation()
if not getPedOccupiedVehicle (localPlayer) then return end
if getPedOccupiedVehicleSeat (localPlayer) ~= 0 then return end
vehicle = getPedOccupiedVehicle(localPlayer)
if not(getVehicleOccupant (vehicle, 0 ) == localPlayer) then return end 
	local t = getRealTime() 
	local h,m = tostring(t.hour), tostring(t.minute)
	local theVeh = getPedOccupiedVehicle(localPlayer)
	local rx, ry, rz = getVehicleComponentRotation(theVeh, "wheel_lf_dummy")
	setVehicleComponentRotation(theVeh, "rpb_sw", 0, -rz+180.00001525879, 0)
	local rpm = getVehicleRPM(theVeh)
	setVehicleComponentRotation(theVeh, "rpb_so", 0, rpm/35, 0)
	local spd = getElementSpeed(theVeh, "kmh")
	setVehicleComponentRotation(theVeh, "rpb_sm", 0, spd, 0)
	
	local oil = getElementData(getPedOccupiedVehicle ( localPlayer ), "fuel" ) or 0
	setVehicleComponentRotation(theVeh, "petrolok", 0, oil * 2, 0)

	setVehicleComponentRotation(theVeh, "hour", 0, h * 30, 0)
	setVehicleComponentRotation(theVeh, "st", 0, m * 6, 0)
end
addEventHandler("onClientRender", root, updateSteeringWheelRotation)

function getElementSpeed(element, unit)
	if (unit == nil) then
		unit = 0
	end
	if (isElement(element)) then
		local x, y, z = getElementVelocity(element)

		if (unit == "mph" or unit == 1 or unit == '1') then
			return math.floor((x^2 + y^2 + z^2) ^ 0.5 * 100)
		else
			return math.floor((x^2 + y^2 + z^2) ^ 0.5 * 100 * 1.609344)
		end
	else
		return false
	end
end

function getVehicleRPM(vehicle)
local vehicleRPM = 0
	if (vehicle) then
		if (getVehicleEngineState(vehicle) == true) then
			if getVehicleCurrentGear(vehicle) > 0 then
				vehicleRPM = math.floor(((getElementSpeed(vehicle, "kmh") / getVehicleCurrentGear(vehicle)) * 180) + 0.5)
				if (vehicleRPM < 650) then
					vehicleRPM = math.random(650, 750)
				elseif (vehicleRPM >= 9800) then
					vehicleRPM = math.random(9800, 9900)
				end
			else
				vehicleRPM = math.floor((getElementSpeed(vehicle, "kmh") * 180) + 0.5)
				if (vehicleRPM < 650) then
					vehicleRPM = math.random(650, 750)
				elseif (vehicleRPM >= 9800) then
					vehicleRPM = math.random(9800, 9900)
				end
			end
		else
			vehicleRPM = 0
		end
		return tonumber(vehicleRPM)
	else
		return 0
	end
end