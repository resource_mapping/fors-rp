﻿

g_Me        = getLocalPlayer()
gRoot       = getRootElement()
gResRoot    = getResourceRootElement( getThisResource () )
local screenW, screenH = guiGetScreenSize()


GUIEditor = {
    button = {},
    label = {},
    window = {},
}

GUIEditor.window[1] = guiCreateWindow((screenW - 321) / 2, (screenH - 103) / 2, 321, 103, "Работа на самосвале", false)
guiWindowSetSizable(GUIEditor.window[1], false)
guiSetVisible(GUIEditor.window[1],false)
GUIEditor.label[1] = guiCreateLabel(10, 23, 300, 41, "Вы хотите устроиться на работу водителем?", false, GUIEditor.window[1])
guiSetFont(GUIEditor.label[1], "default-bold-small")
guiLabelSetColor(GUIEditor.label[1], 253, 251, 0)
guiLabelSetHorizontalAlign(GUIEditor.label[1], "center", false)
guiLabelSetVerticalAlign(GUIEditor.label[1], "center")
GUIEditor.button[1] = guiCreateButton(9, 70, 94, 20, "Да", false, GUIEditor.window[1])
guiSetFont(GUIEditor.button[1], "default-bold-small")
guiSetProperty(GUIEditor.button[1], "NormalTextColour", "FF24FE00")
GUIEditor.button[2] = guiCreateButton(218, 70, 93, 20, "Нет", false, GUIEditor.window[1])
guiSetFont(GUIEditor.button[2], "default-bold-small")
guiSetProperty(GUIEditor.button[2], "NormalTextColour", "FF24FE00")

addEventHandler( "onClientGUIClick", gRoot,
      function ()
        if source == GUIEditor.button[1] then
                setElementData(g_Me,'Jobda',1)
				 closeGui(GUIEditor.window[1])
				 outputChatBox("Вы устроились на работу водителем!", 0, 255, 0, true )		      				  
        elseif source == GUIEditor.button[2] then
              closeGui(GUIEditor.window[1])
			  end
end)
	
function closeGui(windows_id)
if guiGetVisible(windows_id) == true then
  guiSetVisible(windows_id,false)
  showCursor(false)
 end
end

function OpenGui(windows_id)
if guiGetVisible(windows_id) == false then
  guiSetVisible(windows_id,true)
  showCursor(true)
 end
end


 
addEventHandler ( "onClientResourceStart",gResRoot,
   function()
          markerjob = createMarker( 315.52783, 877.94232, 19.5, 'Cylinder', 1.5, 0, 255, 0,255 )
		  blip = createBlipAttachedTo (markerjob, 51, 2, 255, 0, 0, 255, 0, 99999999)
end)

local jobPos = {
{"Задание: Доставь ПГС на стройку в лос сантосе",5000,2467.241, 1933.353, 7.078},
{"Задание: Доставь песок на стройку в сан фиеро",5000,-2076.8831, 251.379, 33.822},
}


addEventHandler("onClientVehicleEnter", getRootElement(),
    function(thePlayer, seat)
        if thePlayer == getLocalPlayer() then
            if ( getElementModel ( source ) == 455 ) then
			   if (  isElement ( marker ) ) and ( isElement ( blip ) ) then
                if isElement(marker) then 
                  destroyElement(marker) 
                end
                 if isElement(blip) then 
                  destroyElement(blip) 
                 end
	           end	  
               gruzmarker(thePlayer)
        end
    end
	end
)


			   
 addEventHandler ("onClientMarkerHit",gRoot,
function( hitElement, matchingDimension )
  if getElementType(hitElement) == "player" and (hitElement == g_Me) then
	if ( source == markerjob ) then
		if not isPedInVehicle ( hitElement ) then
		   if  getElementData(hitElement,'Jobda') then
                outputChatBox("Вы уже устроены на эту работу!", 255, 0, 0, true )				
				return 
			 end
		       OpenGui(GUIEditor.window[1])
	         end
		  elseif ( source == gmarker ) then 
		   if isElement(gmarker) then 
                  destroyElement(gmarker) 
                end
                 if isElement(gblip) then 
                  destroyElement(gblip) 
                 end
				 fadeCamera (  false, 1.0, 0, 0, 0 ) 
                    setTimer ( 
					function()
					fadeCamera(  true, 1.0 ) 
                    text,money,x,y,z = unpack ( jobPos [ math.random ( #jobPos ) ] )
	           marker = createMarker ( x, y, z, "cylinder", 15, 255, 255, 255, 90,thePlayer )
	           blip = createBlipAttachedTo (marker, 41, 2, 255, 0, 0, 255, 0, 99999999,thePlayer)
			   playSoundFrontEnd ( 43 )
			   outputChatBox(text, 255, 255, 0, true )
                    end,3000,1)	
		       
		   elseif ( source == marker ) then  
		      local vehicle = getPedOccupiedVehicle(hitElement)
		         if vehicle and getVehicleController(vehicle) == hitElement and ( getElementModel ( vehicle ) == 455 ) then
		          
				    fadeCamera (  false, 1.0, 0, 0, 0 ) 
                    setTimer ( 
					function()
					fadeCamera(  true, 1.0 ) 
                    triggerServerEvent("finRab", hitElement,money)	
					 outputChatBox("Вы успешно доставили груз ваша зарплата #ffffff"..money.."#00ff00$" , 0, 255, 0, true )
					if isElement(marker) then 
                      destroyElement(marker) 
				     playSoundFrontEnd ( 101 )
					 setTimer(gruzmarker,5000,1,hitElement)
                end
                   if isElement(blip) then 
                     destroyElement(blip) 
                 end
                    end,3000,1)					
	                 
		      else
		     outputChatBox(" это не тот грузовик", 255, 0, 0, true )
		  end
       end
	end
end				
)  


function gruzmarker(thePlayer)
 if (  isElement ( gmarker ) ) and ( isElement ( gblip ) ) then
   if isElement(gmarker) then 
             destroyElement(gmarker) 
     end
   if isElement(gblip) then 
             destroyElement(gblip) 
     end
 end	
    gmarker = createMarker ( 365.55902, 866.44183, 20.40625, "cylinder", 8, 255, 255, 0, 90,thePlayer )
	gblip = createBlipAttachedTo (gmarker, 56, 2, 255, 0, 0, 255, 0, 99999999,thePlayer)
    outputChatBox("Езжай к маркеру и загрузи груз!" , 0, 255, 0, true )
end