﻿
function postmanGui ()
	postWindow = guiCreateWindow(0.25,0.35,0.4,0.4, "Почта России", true)
	postLabel = guiCreateLabel(0.09,0.09,0.8,0.5,[[Добро пожаловать! Здесь вы можете устроиться на работу почтальоном. Ваша задача доставлять письма по адресам. Место доставки смотрите на карте (F11), ищите значок 'КРАСНЫй КРЕСТ'. Удачи!]], true,postWindow)
	guiCreateStaticImage(0.3,0.3,0.4,0.4,"logo.png",true, postWindow)
	guiLabelSetHorizontalAlign(postLabel,"center",true)
	btnAccept = guiCreateButton(0.115,0.7,0.25,0.2, "Работать",true, postWindow)
	addEventHandler("onClientGUIClick",btnAccept,postAccept)
	btnReject = guiCreateButton(0.615,0.7,0.25,0.2, "Выйти", true, postWindow)
	addEventHandler("onClientGUIClick",btnReject,postReject)
	guiSetVisible(postWindow, false)
end


addEventHandler("onClientResourceStart", getResourceRootElement(getThisResource()),
		function ()
				postmanGui ()
		end
)

function markerhit(thePlayer)
	if ( thePlayer == getLocalPlayer() ) then
		guiSetVisible(postWindow, true)
		showCursor(true)
	end
end
addEvent("showGui",true)
addEventHandler("showGui", root, markerhit)

function postReject ()
	guiSetVisible(postWindow, false)
	showCursor(false)
end

function postAccept ()
	triggerServerEvent("giveptJob", getLocalPlayer(), getLocalPlayer() )
	postReject()
	
end
