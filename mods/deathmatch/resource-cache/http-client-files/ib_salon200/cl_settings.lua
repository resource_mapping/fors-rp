GS.UI =
{
	RED = tocolor(200,50,50,255),
	GREEN = tocolor(50,200,50,255),
	BLUE = tocolor(50,50,200,255),
	BLACK = tocolor(10,10,10,255),
	GRAY = tocolor(150,150,150,255),
	WHITE = tocolor(255,255,255,255),
	LOGO = false,
	FONT = "default-bold",
}

GS.COLOR = tocolor(255,0,0)