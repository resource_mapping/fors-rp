﻿local minusHealth = 5 -- время в минутах, за которое отнимается хп
local minushp = 1 -- хп, которое отнимается

local markers = { -- маркеры
	{ -223.815521,-829.8234,21.8 },
	{ -39.7404899597,-908.34796142,20.976900 },
	{ 1927.922729,340.600585,60.795 } -- можно сколько угодно маркеров ставить
}

okno = guiCreateWindow(0.375, 0.375, 0.25, 0.25, "Еда", true)
closeButton = guiCreateButton(0.1, 0.75, 0.2, 0.2, "Закрыть", true, okno)
eatList = guiCreateGridList(0.05, 0.20, 0.9, 0.5, true,okno)
columnEat = guiGridListAddColumn(eatList, "Еда", 0.5)
columnXP = guiGridListAddColumn(eatList, "Здоровье", 0.2)
columnMoney = guiGridListAddColumn(eatList, "Цена", 0.2)
pizza = guiGridListAddRow(eatList,'Пицца','50хп','800$')
keks = guiGridListAddRow(eatList,'Кекс','30хп','200$')
pirog = guiGridListAddRow(eatList,'Пирог','50хп','400$')
kola = guiGridListAddRow(eatList,'Кола','30хп','10$')
pivo = guiGridListAddRow(eatList,'Пиво','40хп','15$')
sup = guiGridListAddRow(eatList,'Суп','70хп','1210$')
burger = guiGridListAddRow(eatList,'Бургер','40хп','400$')
guiWindowSetSizable(okno,false)
guiSetVisible(okno,false)

function eat()
	if (guiGetVisible(okno) == false) then
		guiSetVisible(okno,true)
		showCursor(true)
	end
end
for i, pos in ipairs(markers) do
	markers[i] = createBlip(pos[1], pos[2], pos[3],10,2,0,0,0,255,0,400) --это иконки на карте. Если нужно, можно удалить эту строку
	markers[i] = createMarker(pos[1], pos[2], pos[3],"cylinder",1,100,0,0,255)
	addEventHandler ( "onClientMarkerHit", markers[i], eat)
end

function closingGUI(button,state)
	if button == 'left' and state == 'up' then
		if (guiGetVisible(okno)==true) then
			guiSetVisible(okno,false)
			showCursor(false)
		end
	end
end
addEventHandler("onClientGUIClick", closeButton,closingGUI,false)

addEventHandler('onClientGUIDoubleClick', root, function()
	if source == eatList then 
		local row = guiGridListGetSelectedItem(eatList)
		if row == -1 then
			return
		end
		if guiGridListGetSelectedItem(eatList) == pizza then 
			triggerServerEvent('mcPizza',getLocalPlayer())
		end
		if guiGridListGetSelectedItem(eatList) == keks then 
			triggerServerEvent('mcKeks',getLocalPlayer())
		end
		if guiGridListGetSelectedItem(eatList) == pirog then 
			triggerServerEvent('mcPirog',getLocalPlayer())
		end
		if guiGridListGetSelectedItem(eatList) == kola then 
			triggerServerEvent('mcKola',getLocalPlayer())
		end
		if guiGridListGetSelectedItem(eatList) == pivo then 
			triggerServerEvent('mcPivo',getLocalPlayer())
		end
		if guiGridListGetSelectedItem(eatList) == sup then 
			triggerServerEvent('mcSup',getLocalPlayer())
		end
		if guiGridListGetSelectedItem(eatList) == burger then 
			triggerServerEvent('mcBurger',getLocalPlayer())
		end
		guiSetVisible(okno,false)
		showCursor(false)
	end
end)

setTimer(function()
		setElementHealth(getLocalPlayer(), getElementHealth(getLocalPlayer())-1)
end,minusHealth*60*1000,0)
