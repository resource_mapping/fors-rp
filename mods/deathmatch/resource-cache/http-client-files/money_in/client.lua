local playerMoney = getPlayerMoney ( localPlayer )
local messages =  { }

local screenW,screenH = guiGetScreenSize()
local px,py = 1600,900
local sw,sh = (screenW/px), (screenH/py)

addEventHandler ( "onClientRender", root, function ( )
	local tick = getTickCount ( )
	if ( playerMoney ~= getPlayerMoney ( localPlayer ) ) then
		local pM = getPlayerMoney ( localPlayer ) 
		if ( pM > playerMoney ) then
			local diff = pM - playerMoney
			table.insert ( messages, { diff, true, tick + 5000, 180 } )
		else
			local diff = playerMoney - pM
			table.insert ( messages, { diff, false, tick + 5000, 180 } )
		end
		playerMoney = pM
	end
	
	if ( #messages > 7 ) then
		table.remove ( messages, 1 )
	end
	
	for index, data in ipairs ( messages ) do
		local v1 = data[1]
		local v2 = data[2]
		local v3 = data[3]
		local v4 = data[4]
		dxDrawRectangle(15*sw, 650*sh -(index*25), 105*sw, 20*sh, tocolor ( 0, 0, 0, v4 ) )
		
		if ( v2 ) then
			dxDrawText ( "+ "..convertNumber ( v1 ).." ₽", 20*sw, 652*sh -(index*25), 50*sw, 20*sh, tocolor ( 0, 255, 0, v4+75 ), 1, 'default-bold' )
		else
			dxDrawText ( "- "..convertNumber ( v1 ).." ₽", 20*sw, 652*sh -(index*25), 50*sw, 20*sh, tocolor ( 255, 0, 0, v4+75 ), 1, 'default-bold' )
		end
		
		if ( tick >= v3 ) then
			messages[index][4] = v4-2
			if ( v4 <= 25 ) then
				table.remove ( messages, index )
			end
		end
	end
end )



function convertNumber ( number )  
	local formatted = number  
	while true do      
		formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1.%2')    
		if ( k==0 ) then      
			break   
		end  
	end  
	return formatted
end