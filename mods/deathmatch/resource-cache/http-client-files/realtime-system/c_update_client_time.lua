function updateClientTime(hour, minute)
	if getElementDimension(localPlayer) == 0 then
		setTime(hour, minute)
	else
		setTime(12, 0)
	end
end
addEvent ( "updateClientTime", true )
addEventHandler ( "updateClientTime", getRootElement(), updateClientTime )

function doStuff(commandName, time)
	if not tostring(time) or tostring(time) == nil then
		outputChatBox("Используйте: /settime [Day or Night]", 255, 194, 14)
		return
	end

	if getElementDimension(getLocalPlayer()) == 0 then
		outputChatBox("Вы не можете использовать это команду на улице.", 255, 0, 0)
		return
	end

	if string.lower(tostring(time)) == "day" then
		setTime(12, 0)

		outputChatBox("Установлено дневное время, предметы в доме будут выглядеть ярче.", 0, 255, 0)
	elseif string.lower(tostring(time)) == "night" then
		setTime(0, 0)

		outputChatBox("Установлено ночное время, предметы в доме будут выглядеть темнее.", 0, 255, 0)
	else
		outputChatBox("Используйте: /settime [Day or Night]", 255, 194, 14)
	end 
end
addCommandHandler("settime", doStuff)