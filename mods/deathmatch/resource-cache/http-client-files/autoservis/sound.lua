-----------------------------------------------------------------------------------------------------------------------
-- Just play custom sound of repair here.
--
-- Hitori (2012)
-----------------------------------------------------------------------------------------------------------------------

function onPlayRepairSound()
    local sound = playSound("repair.wav")
    setSoundVolume(sound, 0.5)
end
addEvent("playRepairSound", true)
addEventHandler("playRepairSound", root, onPlayRepairSound)
-----------------------------------------------------------------------------------------------------------------------
