local scx,scy = guiGetScreenSize()
local px = scx/1920
local sizeX, sizeY = 400*px,600*px
local posX,posY = 10*px,scy-sizeY-100*px
local screen = dxCreateScreenSource( scx,scy )

setCameraTarget(localPlayer)

local function isCursorOverRectangle(x,y,w,h)
	if isCursorShowing() then
		local mx,my = getCursorPosition() -- relative
		local cursorx,cursory = mx*scx,my*scy
		if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
			return true
		end
	end
return false
end

for k,v in pairs(vehShopsTable) do
	local marker = createMarker(v[1],v[2],v[3],"cylinder",2.5,0,0,150)
	setElementData(marker,"shop",k,false)
	local blip = createBlipAttachedTo( marker, 55 )
end

local shopWindow = false
local currentShop = "Wang Cars"
local currentMarker = nil
local veh = nil
local selection = 1
local currentColor = 1
local currentWheels = 1
local press = false
local scroll = 0
local scrollMax = 0
local rt = dxCreateRenderTarget( 400*px, 300*px, true )
local clk = false

function switchVehicleModel()
	setElementModel(veh,vehShopVehicles[currentShop][selection][1])
end

function drawVehicleApp()
	--showCursor(true)
	dxUpdateScreenSource( screen )
	dxDrawImage(0,0,scx,scy,screen)
	-- BG
	
	dxDrawImage((posX-10)*px,posY-500*px,1920*px,1200*px,"files/cam.png", 0, 0, 0, tocolor(255, 255, 255, 255) )
	dxDrawRectangle((posX)*px,posY-150*px,sizeX,sizeY-100*px, tocolor(0, 0, 0, 155), false)
    dxDrawRectangle((posX+4)*px,posY-146*px,sizeX-8*px,sizeY-550*px, tocolor(255, 0, 0, 255), false)
	dxDrawText("Покраска",(posX)*px,posY-270*px,posX+sizeX,posY+30*px,tocolor(240,240,240),2*px,"default-bold","center","center")
	
	--Colors
	local cbx = posX+20*px
	for k,v in pairs(vehAppColors) do
		local color = tocolor(v[1],v[2],v[3])
		if isCursorOverRectangle(cbx,posY-50*px,40*px,40*px) or currentColor == k then
			dxDrawRectangle(cbx-2*px,posY-48*px,44*px,44*px,color)
			if getKeyState("mouse1") then
				currentColor = k
				setVehicleColor(veh,v[1],v[2],v[3])
			end
		end
		dxDrawRectangle(cbx,posY-50*px,40*px,40*px,color)
		cbx = cbx + 45*px
	end
	dxDrawText("Выбери Цвет Автомобиля",(posX)*px,posY-170*px,posX+sizeX,posY+30*px,tocolor(240,240,240),2*px,"default-bold","center","center")
	
	-- WHEELS
	dxDrawText("Выберите диски Автомобиля",(posX)*px,posY-100*px,posX+sizeX,posY+150*px,tocolor(240,240,240),2*px,"default-bold","center","center")
	local wx,wy = posX+25*px, posY+50*px
	local row,column = 1,1
	for k,v in pairs(vehAppWheels) do
		if isCursorOverRectangle(wx+120*px*column-120*px,wy+50*px*row-50*px,100*px,40*px) or currentWheels == k  then
			dxDrawRectangle(wx+120*px*column-120*px,wy+50*px*row-50*px,100*px,40*px,tocolor(210,210,210))
			dxDrawText("Диски #"..k,wx+120*px*column-120*px,wy+50*px*row-50*px,wx+120*px*column-120*px+100*px,wy+50*px*row-50*px+40*px,tocolor(20,20,20),1,"default-bold","center","center")
			if getKeyState("mouse1") then
				currentWheels = k
				if k ~= 1 then
					addVehicleUpgrade(veh,v)
				else
					for key,val in pairs(vehAppWheels) do
						removeVehicleUpgrade(veh, val)
					end
				end
			end
		else
			dxDrawRectangle(wx+120*px*column-120*px,wy+50*px*row-50*px,100*px,40*px,tocolor(185,0,0,200))
			dxDrawText("Диски #"..k,wx+120*px*column-120*px,wy+50*px*row-50*px,wx+120*px*column-120*px+100*px,wy+50*px*row-50*px+40*px,tocolor(255,255,255),1,"default-bold","center","center")
		end
		column = column + 1
		if column == 4 then 
			row = row +1
			column = 1
		end
	end

	---возрат на drawVehicleShop
	if isCursorOverRectangle((posX+3)*px,posY+310*px,393*px,35*px) then
		dxDrawRectangle((posX+3)*px,posY+310*px,393*px,35*px,tocolor(255,255,255,255))
		dxDrawText("Выйти в главное меню",(posX)*px,posY+620*px,posX+sizeX,posY+30*px,tocolor(0,0,0),2*px,"default-bold","center","center")
		if getKeyState("mouse1") then
			addEventHandler("onClientRender",root,drawVehicleShop)
			removeEventHandler("onClientRender",root,drawVehicleApp)
		end
	else
		dxDrawRectangle((posX+3)*px,posY+310*px,393*px,35*px,tocolor(185,0,0,200))
		dxDrawText("Выйти в главное меню",(posX)*px,posY+620*px,posX+sizeX,posY+30*px,tocolor(240,240,240),2*px,"default-bold","center","center")
	end
end

function drawVehicleShop() 
	--showCursor(true)
	dxUpdateScreenSource( screen )
	dxDrawImage(0,0,scx,scy,screen)
	-- BG

	--UPPER BOX
	dxDrawRectangle((posX+4)*px,posY-150*px,sizeX,sizeY-150*px,tocolor(10,10,10,150))
	dxDrawRectangle((posX+1505)*px,posY-150*px,sizeX,sizeY-150*px,tocolor(10,10,10,150))
	dxDrawImage((posX-10)*px,posY-500*px,1920*px,1200*px,"files/cam.png", 0, 0, 0, tocolor(255, 255, 255, 255) )
	dxDrawImage((posX+1620)*px,posY+50*px,160*px,160*px,"logo/"..convertNumber(vehShopVehicles[currentShop][selection][6])..".png",0,0,0,tocolor(255,255,255))

	-- LOWER BOX
	dxDrawRectangle((posX+8)*px,posY-146*px,sizeX-8*px,sizeY-550*px, tocolor(255, 0, 0, 255), false)
	dxDrawRectangle((posX+1510)*px,posY-146*px,sizeX-8*px,sizeY-550*px, tocolor(255, 0, 0, 255), false)
	--dxDrawRectangle(posX,posY-100,sizeX,30,tocolor(255,0,0,180))
	--dxDrawRectangle(posX+1000,posY-100,sizeX,30,tocolor(255,0,0,180))
    
	--[[Colors
	local cbx = posX+20*px
	for k,v in pairs(vehShopColors) do
		local color = tocolor(v[1],v[2],v[3])
		if isCursorOverRectangle(cbx,posY-50*px,40*px,40*px) or currentColor == k then
			dxDrawRectangle(cbx-2*px,posY-48*px,44*px,44*px,color)
			if getKeyState("mouse1") then
				currentColor = k
				setVehicleColor(veh,v[1],v[2],v[3])
			end
		end
		dxDrawRectangle(cbx,posY-50*px,40*px,40*px,color)
		cbx = cbx + 45*px
	end
	dxDrawText("Выбери Цвет Автомобиля",posX,posY-170*px,posX+sizeX,posY+30*px,tocolor(240,240,240),2*px,"default-bold","center","center")
	
	-- WHEELS
	dxDrawText("Выберите диски Автомобиля",posX,posY-100*px,posX+sizeX,posY+150*px,tocolor(240,240,240),2*px,"default-bold","center","center")
	local wx,wy = posX+25*px, posY+50*px
	local row,column = 1,1
	for k,v in pairs(vehShopWheels) do
		if isCursorOverRectangle(wx+120*px*column-120*px,wy+50*px*row-50*px,100*px,40*px) or currentWheels == k  then
			dxDrawRectangle(wx+120*px*column-120*px,wy+50*px*row-50*px,100*px,40*px,tocolor(210,210,210))
			dxDrawText("Диски #"..k,wx+120*px*column-120*px,wy+50*px*row-50*px,wx+120*px*column-120*px+100*px,wy+50*px*row-50*px+40*px,tocolor(20,20,20),1,"default-bold","center","center")
			if getKeyState("mouse1") then
				currentWheels = k
				if k ~= 1 then
					addVehicleUpgrade(veh,v)
				else
					for key,val in pairs(vehShopWheels) do
						removeVehicleUpgrade(veh, val)
					end
				end
			end
		else
			dxDrawRectangle(wx+120*px*column-120*px,wy+50*px*row-50*px,100*px,40*px,tocolor(185,0,0,200))
			dxDrawText("Диски #"..k,wx+120*px*column-120*px,wy+50*px*row-50*px,wx+120*px*column-120*px+100*px,wy+50*px*row-50*px+40*px,tocolor(255,255,255),1,"default-bold","center","center")
		end
		column = column + 1
		if column == 4 then 
			row = row +1
			column = 1
		end
	end]]
	
	
	-- NAME AND ARROWS
	dxSetRenderTarget( rt,true )
	if scroll < 0 then scroll = 0
	elseif scroll >= scrollMax then scroll = scrollMax end
	local sy = 0
		for k,v in pairs(vehShopVehicles[currentShop]) do
			if k == selection then
				dxDrawRectangle(0,sy-scroll,500*px,29*px,tocolor(255,0,0,200))
			end
			dxDrawText(v[2],15*px,sy-scroll,500*px,sy-scroll+30*px,tocolor(255,255,255),1,"default-bold","left","center")
			sy = sy + 30*px
		end
	dxSetRenderTarget()
	dxDrawImage(posX,posY-20*px,380*px,300*px,rt)
	if sy >= 300*px then
		scrollMax = sy-300*px
		local size = 300*px/sy*300*px
		dxDrawRectangle(posX+380*px,posY-20*px,20*px,300*px,tocolor(0,0,0,200))
		dxDrawRectangle(posX+380*px,posY-20*px+scroll/scrollMax*(300*px-size),20*px,size,tocolor(255,0,0,200))
		
		dxDrawText("Выбор Автомобиля",(posX),posY-150*px,posX+sizeX,posY+30*px,tocolor(240,240,240),2*px,"default-bold","center","center")
		dxDrawText("Автосалон",(posX)*px,posY-270*px,posX+sizeX,posY+30*px,tocolor(240,240,240),2*px,"default-bold","center","center")
		dxDrawText("Информация",(posX+2002),posY-270*px,posX+sizeX,posY+30*px,tocolor(240,240,240),2*px,"default-bold","center","center")
	end

	local spy = 0
	for k,v in pairs(vehShopVehicles[currentShop]) do
		if isCursorOverRectangle(posX,posY-20*px,380*px,300*px) then
			if isCursorOverRectangle(posX,posY-20*px+spy-scroll,400*px,29*px) then
				if getKeyState("mouse1") and not clk then
					selection = k
					switchVehicleModel()
				end
			end
		end
		spy = spy + 30*px
	end
	-- PRICE
	dxDrawText("Стоимость:               "..(vehShopVehicles[currentShop][selection][3]).." руб",(posX+3000)*px,posY-540*px,posX+sizeX,posY+452*px,tocolor(255,255,255),2*px,"default-bold","center","center")
    dxDrawText("Макс.Скорость:   "..(vehShopVehicles[currentShop][selection][4]).."КМ/Ч",(posX+3000)*px,posY-460*px,posX+sizeX,posY+452*px,tocolor(255,255,255),2.2*px,"default-bold","center","center")
	dxDrawText(vehShopVehicles[currentShop][selection][2],(posX+3000)*px,posY-600*px,posX+sizeX,posY+452*px,tocolor(0,191,255),2*px,"default-bold","center","center")
	dxDrawText("Привод:                     "..vehShopVehicles[currentShop][selection][5],(posX+3000)*px,posY-400*px,posX+sizeX,posY+452*px,tocolor(255,255,255),2*px,"default-bold","center","center")

	-- BUTTONS
	if isCursorOverRectangle(posX+1502*px,posY+220*px,120*px,35*px) then
		dxDrawRectangle(posX+1502*px,posY+220*px,120*px,35*px,tocolor(200,200,200,255))
		dxDrawText("Купить",posX+3002*px,posY-110*px,posX+130*px,posY+585*px,tocolor(10,10,30),1,"default-bold","center","center")
		if getKeyState("mouse1") then
			if not press then
				press = true
				if getPlayerMoney(localPlayer) >= vehShopVehicles[currentShop][selection][3]+500 then
					initVehicleShopWindow()
					triggerServerEvent( "buyTheVehicle", localPlayer, localPlayer,currentShop,vehShopVehicles[currentShop][selection][1],currentColor,currentWheels)
				else
					outputChatBox("Недостаточно средств для покупки данного автомобиля.",200,20,20)
				end
			end
		else
			press = false
		end
	else
		dxDrawRectangle(posX+1502*px,posY+220*px,120*px,35*px,tocolor(185,0,0,200))
		dxDrawText("Купить",posX+3002*px,posY-110*px,posX+130*px,posY+585*px,tocolor(255,255,255),1,"default-bold","center","center")
	end
	
	if isCursorOverRectangle(posX+1778*px,posY+220*px,120*px,35*px) then
		dxDrawRectangle(posX+1778*px,posY+220*px,120*px,35*px,tocolor(200,200,200,255))
		dxDrawText("Тест-драйв",posX+3420*px,posY-110*px,posX+260*px,posY+585*px,tocolor(10,10,30),1,"default-bold","center","center")
		if getKeyState("mouse1") then
			triggerServerEvent("startTestDrive",localPlayer,vehShopVehicles[currentShop][selection][1])
			initVehicleShopWindow()
		end
	else
		dxDrawRectangle(posX+1778*px,posY+220*px,120*px,35*px,tocolor(185,0,0,200))
		dxDrawText("Тест-драйв",posX+3420*px,posY-110*px,posX+260*px,posY+585*px,tocolor(255,255,255),1,"default-bold","center","center")
	end

	if isCursorOverRectangle(posX+1502*px,posY+260*px,396*px,35*px) then
		dxDrawRectangle(posX+1502*px,posY+260*px,396*px,35*px,tocolor(200,200,200,255))
		dxDrawText("Выход из Автосалона",posX+3030*px,posY-20*px,posX+390*px,posY+585*px,tocolor(10,10,30),1,"default-bold","center","center")
		if getKeyState("mouse1") then
			initVehicleShopWindow()
		end
	else
		dxDrawRectangle(posX+1502*px,posY+260*px,396*px,35*px,tocolor(185,0,0,200))
		dxDrawText("Выход из Автосалона",posX+3030*px,posY-20*px,posX+390*px,posY+585*px,tocolor(255,255,255),1,"default-bold","center","center")
	end
	---------------------- Кнопка перехода в drawVehicleApp
    if isCursorOverRectangle(posX+1640*px,posY+220*px,120*px,35*px) then
		dxDrawRectangle(posX+1640*px,posY+220*px,120*px,35*px,tocolor(255,255,255,255))
		dxDrawText("Покраска",posX+3140*px,posY-110*px,posX+260*px,posY+585*px,tocolor(10,10,30),1,"default-bold","center","center")
		if getKeyState("mouse1") then
			addEventHandler("onClientRender",root,drawVehicleApp)
			removeEventHandler("onClientRender",root,drawVehicleShop)
		end
	else
		dxDrawRectangle(posX+1640*px,posY+220*px,120*px,35*px,tocolor(185,0,0,200))
		dxDrawText("Покраска",posX+3140*px,posY-110*px,posX+260*px,posY+585*px,tocolor(255,255,255),1,"default-bold","center","center")
	end
	--dxDrawText( string text, float left, float top [, float right=left, float bottom=top, int color=white,                   float scale=1, mixed font="default", string alignX="left", string alignY="top",                  bool clip=false, bool wordBreak=false, bool postGUI=false,                  bool colorCoded=false, bool subPixelPositioning=false,                   float fRotation=0, float fRotationCenterX=0, float fRotationCenterY=0 ] )
end
--addEventHandler("onClientRender",root,drawVehicleShop)

function initVehicleShopWindow()
	if shopWindow then
		shopWindow = false
		showCursor(false)
		fadeCamera( false, 1 )
		removeEventHandler("onClientRender",root,drawVehicleShop)
		setTimer(function()
				setElementAlpha(currentMarker,255)
				setCameraTarget(localPlayer)
				setElementFrozen(localPlayer,true)
				triggerServerEvent( "setPlayerDimension", localPlayer, 0 )
				destroyElement(veh)
				fadeCamera(true,2)
			end,1000,1)
		setTimer(setElementFrozen,2000,1,localPlayer,false)
	else
		shopWindow = true
		currentColor = 1
		currentWheels = 1
		selection = 1
		showCursor(true)
		fadeCamera( false, 1 )
		setElementFrozen(localPlayer,true)
		setTimer(function() 
			fadeCamera(true,1)
			addEventHandler("onClientRender",root,drawVehicleShop)
			showCursor(true)
			setCameraMatrix( -1955,270,36,-1953.009765625,264.6650390625,35.097522735596 )
			setElementDimension(localPlayer, 1)
			veh = createVehicle( vehShopVehicles[currentShop][1][1], -1951.009765625,260.6650390625,35.097522735596,0.406494140625,0.0054931640625,42.994995117188 )
			setVehicleColor(veh,200,200,200)
			setElementDimension(veh,1)
			setElementData(veh,"numberType","tr")
			setElementData(veh,"number:plate","oo000o12")
		end,1000,1)
	end
end
--initVehicleShopWindow("Wang Cars")

addEventHandler("onClientKey",root,function(key,press)
	if press then
		if not shopWindow then return end
		if key == "mouse_wheel_down" then
			scroll = scroll + 15*px
		elseif key == "mouse_wheel_up" then
			scroll = scroll - 15*px
		end
	end
end)

local managerWindow = nil
local vehmarker = nil
local vehblip = nil
local numberWindow = nil
local barterWindow = nil

function initVehicleManager(limits)
	if isElement(managerWindow) then
		destroyElement(managerWindow)
		showCursor(false)
	else
		if isElement(numberWindow) then return end
		if isElement(barterWindow) then return end
		local vehicles = getElementData(localPlayer,"vehicles")
		if vehicles and type(vehicles) == "table" then
			showCursor(true)
			managerWindow = guiCreateWindow(0.3,0.3,0.3,0.3,"Менеджер автомобилей",true)
			local gridlist = guiCreateGridList(0,0.1,0.7,0.7,true,managerWindow)
			local labelLimits = guiCreateLabel(0,0.92,0.7,0.1,"Парковочные места: "..limits,true,managerWindow)
			guiLabelSetHorizontalAlign(labelLimits,"center")
			guiSetFont(labelLimits,"default-bold-small")
			local btnSell = guiCreateButton(0.72,0.65,0.3,0.1,"Продать",true,managerWindow)
			local btnPlayerSell = guiCreateButton(0.72,0.76,0.3,0.1,"Игроку",true,managerWindow)
			local btnMark = guiCreateButton(0.72,0.43,0.3,0.1,"Метка",true,managerWindow)
			local btnOpen = guiCreateButton(0.72,0.54,0.3,0.1,"Откр/Закр",true,managerWindow)

			local btnSpawn = guiCreateButton(0.72,0.1,0.3,0.1,"Респавн",true,managerWindow)
			local btnDestroy = guiCreateButton(0.72,0.21,0.3,0.1,"Убрать",true,managerWindow)
			local btnBring = guiCreateButton(0.72,0.32,0.3,0.1,"Телепорт",true,managerWindow)
			local btnBlock = guiCreateButton(0.72,0.87,0.3,0.1,"Заблокировать",true,managerWindow)

			local btnNumber1 = guiCreateButton(0,0.82,0.3,0.08,"Установить номер",true,managerWindow)
			local btnNumber2 = guiCreateButton(0.4,0.82,0.3,0.08,"Снять номер",true,managerWindow)

			guiGridListAddColumn( gridlist, "Автомобиль", 0.6 )
			guiGridListAddColumn( gridlist, "Номер", 0.3 )
			for k,v in pairs(vehicles) do
				local data = getVehicleData(v["Model"])
				local row = guiGridListAddRow(gridlist)
				local num = fromJSON(v["Number"])
				guiGridListSetItemText( gridlist,row,1, data[2] , false,false )
				guiGridListSetItemText( gridlist,row,2, num[2] or "Не установлен", false,false )
				guiGridListSetItemData( gridlist,row,1, v["ID"] )
				if isDonateVehicle(v["Model"]) then
					guiGridListSetItemColor(gridlist,row,1,200,0,0)
					guiGridListSetItemColor(gridlist,row,2,200,0,0)
					guiSetFont(gridlist,"default-bold-small")
				end
			end
			if selection then guiGridListSetSelectedItem( gridlist, selection, 1 ) end
			addEventHandler("onClientGUIClick",managerWindow,function()
				local row = guiGridListGetSelectedItem( gridlist )
				if row >= 0 then
					local id = guiGridListGetItemData( gridlist, row,1 )
					if not id then return end
					selection = row
					if source == gridlist then
					elseif source == btnSell then
						if findVeh(id) then
							initVehicleManager()
							initSellConfirmationWindow(id)
						else
							outputChatBox("Автомобиль должен быть заспавнен!",200,50,50)
						end
					elseif source == btnPlayerSell then
						initTradeOffer(id)
						initVehicleManager()
						showCursor(true)
					elseif source == btnMark then
						local veh = findVeh(id)
						if veh then
							if isElement(vehmarker) then
								destroyElement(vehblip)
								destroyElement(vehmarker)
							else
								local x,y,z = getElementPosition(veh)
								vehmarker = createMarker( x,y,z )
								vehblip = createBlipAttachedTo(vehmarker)
							end
						end
					elseif source == btnOpen then
						triggerServerEvent( "lockVehicle", localPlayer, findVeh(id) )
					elseif source == btnSpawn then
						triggerServerEvent( "spawnTheVehicle", localPlayer, id )
					elseif source == btnDestroy then
						triggerServerEvent( "destroyTheVehicle", localPlayer, findVeh(id) )
					elseif source == btnBring then
						triggerServerEvent( "bringVehicle", localPlayer, findVeh(id) )
					elseif source == btnBlock then
						triggerServerEvent( "blockVehicle", localPlayer, localPlayer, findVeh(id))
						initVehicleManager()
					elseif source == btnNumber1 then
						local vehID = guiGridListGetItemData(gridlist,row,1) 
						numberWindow = guiCreateWindow(0.4,0.3,0.2,0.3,"Выберите номер",true)
						local list = guiCreateGridList(0,0.05,1,0.7,true,numberWindow)
						local numbers = getElementData(localPlayer,"numbers")
						guiGridListAddColumn(list,"Номер",0.8)
						for k,v in pairs(numbers) do
							--outputChatBox(k.." "..tostring(v))
							local row = guiGridListAddRow(list)
							guiGridListSetItemText(list,row,1,v[2],false,false)
							guiGridListSetItemData( list, row, 1, v )
						end
						local btnClose = guiCreateButton(0,0.8,0.3,0.1,"Отмена",true,numberWindow)
						local btnDelete = guiCreateButton(0.35,0.8,0.3,0.1,"Удалить",true,numberWindow)
						local btnInstall = guiCreateButton(0.7,0.8,0.3,0.1,"Установить",true,numberWindow)
						addEventHandler("onClientGUIClick",numberWindow,function()
							if source == btnClose then
								destroyElement(numberWindow)
								showCursor(false)
							elseif source == btnInstall then
								local num = guiGridListGetSelectedItem( list )
								if num >= 0 then
									triggerServerEvent( "switchVehicleNumberPlate", localPlayer, id, guiGridListGetItemData(list,num,1), false )
									destroyElement(numberWindow)
									showCursor(false)
								end
							elseif source == btnDelete then
								local num = guiGridListGetSelectedItem( list )
								if num >= 0 then
									triggerServerEvent( "switchVehicleNumberPlate", localPlayer, id, guiGridListGetItemData(list,num,1), true )
									destroyElement(numberWindow)
									showCursor(false)
								end
							end
						end)
						initVehicleManager()
						showCursor(true)
					elseif source == btnNumber2 then
						triggerServerEvent( "switchVehicleNumberPlate", localPlayer, id, {"c",""}, true )
						initVehicleManager()
					end
				end
			end)
		end
	end
end
addEvent("initVehicleManager",true)
addEventHandler("initVehicleManager",root,initVehicleManager)
--initVehicleManager()

local sellConfirmationWindow = nil

function initSellConfirmationWindow(id)
	if isElement(sellConfirmationWindow) then
		destroyElement(sellConfirmationWindow)
		showCursor(false)
	else
		sellConfirmationWindow = guiCreateWindow(0.3,0.4,0.3,0.15,"Утилизация",true)
		showCursor(true)
		local veh = findVeh(id)
		local data = getVehicleData(getElementModel(veh))
		local label = guiCreateLabel(0.1,0.3,0.8,0.1,"Вы действительно хотите продать "..data[2].." за $"..data[3]/2 .."?",true,sellConfirmationWindow)
		guiLabelSetHorizontalAlign( label, "center" )
		local btn1 = guiCreateButton(0.1,0.6,0.3,0.3,"Отмена",true,sellConfirmationWindow)
		local btn2 = guiCreateButton(0.6,0.6,0.3,0.3,"Подтвердить",true,sellConfirmationWindow)
		addEventHandler("onClientGUIClick",sellConfirmationWindow,function()
			if source == btn1 then
				initSellConfirmationWindow()
			elseif source == btn2 then
				triggerServerEvent("sellVehicle", localPlayer, veh)
				initSellConfirmationWindow()
			end
		end)
	end
end

addEventHandler("onClientMarkerHit", resourceRoot, function(player)
	local shop = getElementData(source, "shop")
	if shop then
		if getElementType(player) == "player" and player == localPlayer then
			if not getPedOccupiedVehicle( player ) then
				outputChatBox("Добро пожаловать в магазин "..shop)
				currentShop = shop
				initVehicleShopWindow()
				setElementAlpha(source,0)
				currentMarker = source
			end
		end
	end
	if source == vehmarker then
		destroyElement(vehblip)
		destroyElement(source)
	end

	if getElementData(source,"trade") then
		if getElementType(player) == "player" and player == localPlayer then
			if not isPedInVehicle(player) then
				barterMarkerWindow()
			end
		end
	end
end)

local tOffer = nil

function initTradeOffer(id)
	if isElement(tOffer) then
		showCursor(false)
		destroyElement(tOffer)
	else
		showCursor(true)
		tOffer = guiCreateWindow(0.4,0.3,0.15,0.3,"Продать автомобиль", true)
		local grid = guiCreateGridList(0,0.1,1,0.5,true,tOffer)
		guiGridListAddColumn(grid,"Выберите игрока",0.8)
		local players = getElementsByType("player")
		local x,y,z = getElementPosition(localPlayer)
		for k,v in pairs(players) do
			local x2,y2,z2 = getElementPosition(v)
			if getDistanceBetweenPoints3D( x,y,z,x2,y2,z2 ) <= 20 then
				local row = guiGridListAddRow(grid)
				guiGridListSetItemText(grid,row,1,getPlayerName(v),false,false)
				guiGridListSetItemData(grid,row,1,v)
			end
		end
		local edit = guiCreateEdit(0.1,0.65,0.8,0.1,"Введите сумму",true,tOffer)
		local btn1 = guiCreateButton(0.05,0.8,0.4,0.15,"Отмена",true,tOffer)
		local btn2 = guiCreateButton(0.55,0.8,0.4,0.15,"Продать",true,tOffer)
		addEventHandler("onClientGUIClick",tOffer,function()
			if source == btn1 then
				initTradeOffer()
			elseif source == btn2 then
				local row = guiGridListGetSelectedItem( grid )
				if row and row >= 0 then
					local sum = guiGetText(edit)
					if sum and tonumber(sum) and tonumber(sum) >= 1 then
						--outputChatBox(tostring(guiGridListGetItemData(grid,row,1)))
						triggerServerEvent( "sendTradeOffer", localPlayer, guiGridListGetItemData(grid,row,1), id, tonumber(sum) )
						initTradeOffer()
					else
						outputChatBox("Некорректная сумма!", 200,0,0)
					end
				else
					outputChatBox("Вы не выбрали игрока!", 200,0,0)
				end
			end
		end)
	end
end

local tAccept = nil

function cacheTradeOffer(id,vehname,cash)
	if not isElement(tAccept) then
		showCursor(true)
		local seller = source
		tAccept = guiCreateWindow(0.4,0.35,0.2,0.15,"Покупка автомобиля",true)
		local label = guiCreateLabel(0,0.2,1,0.2,getPlayerName(source).." предлагает вам купить "..vehname.." за $"..cash,true,tAccept)
		guiLabelSetHorizontalAlign( label,"center" )
		local btn1 = guiCreateButton(0.1,0.5,0.3,0.3,"Отклонить",true,tAccept)
		local btn2 = guiCreateButton(0.6,0.5,0.3,0.3,"Принять",true,tAccept)
		addEventHandler("onClientGUIClick",tAccept,function()
			if source == btn1 then
				destroyElement(tAccept)
				showCursor(false)
			elseif source == btn2 then
				triggerServerEvent( "tradeAccepted", localPlayer, seller, id, cash )
				destroyElement(tAccept)
				showCursor(false)
			end
		end)
	end
end
addEvent("cacheTradeOffer",true)
addEventHandler("cacheTradeOffer",root,cacheTradeOffer)

local markerWindow = nil

function initMarkerWindow()
	if isElement(markerWindow) then
		showCursor(false)
		destroyElement(markerWindow)
	else
		local veh = getPedOccupiedVehicle( localPlayer )
		local mdl = getElementModel(veh)
		local date = 1
		local withdraw = 2500
		setElementFrozen ( veh, false )
		local vehname = getVehicleData(getElementModel(veh))[2] or "Vehicle"
		showCursor(true)
		markerWindow = guiCreateWindow(0.3,0.3,0.3,0.3,"Продажа автомобиля",true)
		guiWindowSetSizable( markerWindow, false )
		local label1 = guiCreateLabel( 0.1,0.1,0.8,0.2,"Выставление на продажу автомобиля \n"..vehname,true,markerWindow )
		guiLabelSetColor(label1,200,200,0)
		guiLabelSetHorizontalAlign( label1,"center" )
		local label2 = guiCreateLabel( 0.1,0.3,0.8,0.2,"Укажите срок аренды парковочного места.",true,markerWindow )
		guiLabelSetColor(label2,100,200,0)
		--guiLabelSetHorizontalAlign( label2,"center" )
		local date1 = guiCreateRadioButton(0.1,0.4,0.6,0.05,"1 День - $2500",true,markerWindow)
		local date2 = guiCreateRadioButton(0.1,0.45,0.6,0.05,"3 Дня - $10000",true,markerWindow)
		local date3 = guiCreateRadioButton(0.1,0.5,0.6,0.05,"7 Дней - $15000",true,markerWindow)
		guiRadioButtonSetSelected( date1, true )
		local edit = guiCreateEdit(0.2,0.65,0.6,0.1,"Укажите цену",true, markerWindow)

		local btnCancel = guiCreateButton(0.05,0.85,0.3,0.12,"Отмена",true, markerWindow)
		local btnAccept = guiCreateButton(0.6,0.85,0.3,0.12,"Подтвердить",true, markerWindow)
		setElementFrozen ( veh, true )
		addEventHandler("onClientGUIClick",markerWindow,function()
			if source == btnCancel then
				initMarkerWindow()
				setElementFrozen ( veh, false )
			elseif source == edit then
				if guiGetText(edit) == "Укажите цену" then guiSetText(edit,"") end
			elseif source == btnAccept then
				local price = tonumber(guiGetText(edit))
				if price then
					if price <= getVehicleData(getElementModel(veh))[3]*0.9 then
						if date then
							triggerServerEvent( "placeVehicleOnMarket", localPlayer, veh, price, date, withdraw)
							initMarkerWindow()
							setElementFrozen ( veh, false )
						else
							outputChatBox("Вы не указали сроки продаж!")
						end
					else
						outputChatBox("Некорректная стоимость, максимальная цена для этой машины - "..math.floor(getVehicleData(getElementModel(veh))[3]*0.9).."!")
					end
				else
					outputChatBox("Некорректная стоимость!",200,0,0)
				end
			elseif source == date1 then
				date = 1
				withdraw = 2500
			elseif source == date2 then
				date = 3
				withdraw = 10000
			elseif source == date3 then
				date = 7
				withdraw = 15000
			end
		end)
	end
end
addEvent("initMarkerWindow",true)
addEventHandler("initMarkerWindow",root,initMarkerWindow)
--initMarkerWindow()

local marketVehicleWindow = nil

function initMarketVehicleWindow(owner)
	if isElement(marketVehicleWindow) then
		showCursor(false)
		destroyElement( marketVehicleWindow )
	else
		local veh = getPedOccupiedVehicle( localPlayer )
		if veh then
			local vehname = getVehicleData(getElementModel(veh))[2]
			local price = getElementData(veh,"Price")
			if owner then price = 0 end
			showCursor(true)
			marketVehicleWindow = guiCreateWindow(0.35,0.4,0.3,0.15,"Покупка автомобиля",true)
			local label = guiCreateLabel(0,0.25,1,0.1,"Купить "..vehname.." за $"..price.." ?",true,marketVehicleWindow)
			guiLabelSetHorizontalAlign( label, "center" )
			local btnCancel = guiCreateButton(0.1,0.6,0.3,0.3,"Отмена",true,marketVehicleWindow)
			local btnAccept = guiCreateButton(0.6,0.6,0.3,0.3,"Купить",true,marketVehicleWindow)
			addEventHandler("onClientGUIClick",marketVehicleWindow,function()
				if source == btnCancel then
					initMarketVehicleWindow()
					removePedFromVehicle(localPlayer)
				elseif source == btnAccept then
					triggerServerEvent( "buyVehicleFromMarket", localPlayer, veh )
					initMarketVehicleWindow()
				end
			end)
		end
	end
end
addEvent("initMarketVehicleWindow",true)
addEventHandler("initMarketVehicleWindow",root,initMarketVehicleWindow)
--initMarketVehicleWindow()

function drawMarketVehicleInfo()
	for k,v in pairs(getElementsByType("vehicle")) do
		if getElementData(v,"owner") == "Market" then
			local x,y,z = getElementPosition(localPlayer)
			local x2,y2,z2 = getElementPosition(v)
			if getDistanceBetweenPoints3D( x,y,z,x2,y2,z2 ) <= 10 then
				local sx,sy = getScreenFromWorldPosition( x2, y2, z+1 )
				if sx and sy then
					dxDrawText( getVehicleData(getElementModel(v))[2].."\n#00dd00$"..getElementData(v,"Price"),sx,sy,sx,sy,tocolor(255,255,255),2,"default-bold","center","center",false,false,false,true)
				end
			end
		end
	end
end
addEventHandler("onClientRender",root,drawMarketVehicleInfo)

bindKey("g","down",function() triggerServerEvent("toggleEngine",localPlayer) end)
bindKey("f3","down",function() triggerServerEvent("switchVehicleManager",localPlayer) end)


-- TEST DRIVE

local demoTimer = nil
local timerShown = false

function initDemoTimer()
	if timerShown then
		removeEventHandler( "onClientRender", root, drawDemoTimer)
		if isTimer(demoTimer) then killTimer(demoTimer) end
		demoTimer = nil
	else
		demoTimer = setTimer(function() end, 60000, 1)
		addEventHandler( "onClientRender", root, drawDemoTimer)
	end
	timerShown = not timerShown
end
addEvent("initDemoTimer",true)
addEventHandler("initDemoTimer",root,initDemoTimer)

function drawDemoTimer()
	if isTimer(demoTimer) then
		local details = getTimerDetails( demoTimer )
		dxDrawRectangle(scx/2-50*px,0,100*px,50*px,tocolor(0,0,0,150))
		dxDrawText("0:"..math.floor(details/1000),scx/2,30,scx/2,30,tocolor(255,255,255),2*px,"default-bold","center","center")
	end
end