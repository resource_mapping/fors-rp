﻿GUIEditor_Window = {}
GUIEditor_Button = {}
GUIEditor_Label = {}
GUIEditor_Image = {}

GUIEditor_Window[1] = guiCreateWindow(0.3431,0.3922,0.3118,0.1811,"Работа :  Дальнабойщик",true)
guiSetAlpha(GUIEditor_Window[1],0.7)
guiSetVisible(GUIEditor_Window[1],false)
GUIEditor_Button[1] = guiCreateButton(0.02,0.7853,0.4343,0.1534,"Да, я хочу работать",true,GUIEditor_Window[1])
GUIEditor_Label[1] = guiCreateLabel(0.1737,0.2699,0.6548,0.1227,"Ты хочешь работать дальнобойщиком? ?",true,GUIEditor_Window[1])
guiLabelSetColor(GUIEditor_Label[1],255,255,0,255)
guiSetFont(GUIEditor_Label[1],"clear-normal")
GUIEditor_Label[2] = guiCreateLabel(0.1737,0.3067,0.6437,0.1104,"__________________________________________",true,GUIEditor_Window[1])
guiSetFont(GUIEditor_Label[2],"default-bold-small")
GUIEditor_Button[2] = guiCreateButton(0.5479,0.7853,0.4321,0.1534,"Нет, я передумал",true,GUIEditor_Window[1])
guiSetProperty( GUIEditor_Button[2], 'NormalTextColour', 'FFFF8000' )
guiSetProperty( GUIEditor_Button[2], 'HoverTextColour', 'FFFF8000' )
GUIEditor_Image[1] = guiCreateStaticImage(0.029,0.1656,0.1136,0.362,"al.png",true,GUIEditor_Window[1])

function showJOBGUI()
guiSetVisible (GUIEditor_Window[1],true)
showCursor(true)
end
addEvent("showJOBGUI",true)
addEventHandler("showJOBGUI",getRootElement(),showJOBGUI)

addEventHandler("onClientGUIClick",getRootElement(),
function (player)
   if (source == GUIEditor_Button[2]) then
   guiSetVisible (GUIEditor_Window[1],false)
   showCursor (false)
   end
end)

addEventHandler("onClientGUIClick",getRootElement(),
function (player)
   if (source == GUIEditor_Button[1]) then
   triggerServerEvent ("da", getLocalPlayer())
   guiSetVisible (GUIEditor_Window[1],false)
   showCursor (false)
   end
end)

--=============================================================================================================--

GUIEditor_Window[2] = guiCreateWindow(0.4056,0.3456,0.1917,0.2067,"Выбор груза",true)
guiSetAlpha(GUIEditor_Window[2],0.7)
guiSetVisible(GUIEditor_Window[2],false)
GUIEditor_Label[5] = guiCreateLabel(0.0543,0.3226,0.1196,0.1075,"Груз:",true,GUIEditor_Window[2])
guiLabelSetColor(GUIEditor_Label[5],255,0,0,255)
guiSetFont(GUIEditor_Label[5],"default-bold-small")
GUIEditor_Label[6] = guiCreateLabel(0.0543,0.4839,0.1196,0.1075,"Груз:",true,GUIEditor_Window[2])
guiLabelSetColor(GUIEditor_Label[6],255,0,0)
guiSetFont(GUIEditor_Label[6],"default-bold-small")
GUIEditor_Label[7] = guiCreateLabel(0.0543,0.8065,0.1196,0.1075,"Груз:",true,GUIEditor_Window[2])
guiLabelSetColor(GUIEditor_Label[7],255,0,0)
guiSetFont(GUIEditor_Label[7],"default-bold-small")
GUIEditor_Label[8] = guiCreateLabel(0.0543,0.6452,0.1196,0.1075,"Груз:",true,GUIEditor_Window[2])
guiLabelSetColor(GUIEditor_Label[8],255,0,0)
guiSetFont(GUIEditor_Label[8],"default-bold-small")
GUIEditor_Label[9] = guiCreateLabel(0.1739,0.3226,0.4746,0.1075,"Одежда, продукты",true,GUIEditor_Window[2])
guiLabelSetColor(GUIEditor_Label[9],0,255,0,255)
guiSetFont(GUIEditor_Label[9],"clear-normal")
GUIEditor_Label[10] = guiCreateLabel(0.1739,0.4839,0.4746,0.1075,"Бытовая химия",true,GUIEditor_Window[2])
guiLabelSetColor(GUIEditor_Label[10],0,255,0)
guiSetFont(GUIEditor_Label[10],"clear-normal")
GUIEditor_Label[11] = guiCreateLabel(0.1739,0.8065,0.4746,0.1075,"Щебень, песок",true,GUIEditor_Window[2])
guiLabelSetColor(GUIEditor_Label[11],0,255,0)
guiSetFont(GUIEditor_Label[11],"clear-normal")
GUIEditor_Label[12] = guiCreateLabel(0.1739,0.6452,0.4746,0.1075,"Топливо",true,GUIEditor_Window[2])
guiLabelSetColor(GUIEditor_Label[12],0,255,0)
guiSetFont(GUIEditor_Label[12],"clear-normal")
GUIEditor_Button[3] = guiCreateButton(0.6449,0.328,0.2717,0.0968,"Доставить!",true,GUIEditor_Window[2])
GUIEditor_Button[4] = guiCreateButton(0.6449,0.8172,0.2717,0.0968,"Доставить!",true,GUIEditor_Window[2])
GUIEditor_Button[5] = guiCreateButton(0.6449,0.6559,0.2717,0.0968,"Доставить!",true,GUIEditor_Window[2])
GUIEditor_Button[6] = guiCreateButton(0.6449,0.4946,0.2717,0.0968,"Доставить!",true,GUIEditor_Window[2])
GUIEditor_Button[7] = guiCreateButton(0.8949,0.1344,0.0652,0.086,"x",true,GUIEditor_Window[2])
guiSetFont(GUIEditor_Button[7],"default-bold-small")

function showGUI()
guiSetVisible (GUIEditor_Window[2],true)
showCursor(true)
end
addEvent("showGUI",true)
addEventHandler("showGUI",getRootElement(),showGUI)

addEventHandler("onClientGUIClick",getRootElement(),
function (player)
   if (source == GUIEditor_Button[7]) then
   guiSetVisible (GUIEditor_Window[2],false)
   showCursor (false)
   elseif (source == GUIEditor_Button[3]) then
   triggerServerEvent ("but1", getLocalPlayer())
   guiSetVisible (GUIEditor_Window[2],false)
   showCursor (false)
   elseif (source == GUIEditor_Button[4]) then
   triggerServerEvent ("but2", getLocalPlayer())
   guiSetVisible (GUIEditor_Window[2],false)
   showCursor (false)
   elseif (source == GUIEditor_Button[5]) then
   triggerServerEvent ("but3", getLocalPlayer())
   guiSetVisible (GUIEditor_Window[2],false)
   showCursor (false)  
   elseif (source == GUIEditor_Button[6]) then
   triggerServerEvent ("but4", getLocalPlayer())
   guiSetVisible (GUIEditor_Window[2],false)
   showCursor (false)
   end
  end)


function onResourceStart()
	local sound = playSound3D("1.mp3", 104.415, -291.4658, 3, true) 
        setSoundMaxDistance( Sound, 80 )
end
addEventHandler("onClientResourceStart", getResourceRootElement(getThisResource()), onResourceStart)


