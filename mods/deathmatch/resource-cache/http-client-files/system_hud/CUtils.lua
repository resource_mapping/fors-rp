-- ==== Параметры экрана ==== --
local screenW, screenH = guiGetScreenSize()
local px, py = screenW/1920, screenH/1080

function isCursorOverRectangle(x,y,w,h)
	if isCursorShowing() then
		local mx,my = getCursorPosition()
		local cursorx,cursory = mx*screenW,my*screenH
		if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
			return true
		end
	end
return false
end

function dxCreateText(x, y, w, h, text, fontText, colorText, posX, posY)
    local posX = posX or "center"
	local posY = posY or "center"
    local colorText = colorText or tocolor(255, 255, 255, 255)
	local fontText = fontText or "default"
	dxDrawText(text, x, y, w + x, h + y, colorText, 1*px, fontText, posX, posY, true, false, false, true)
end

function convertNumber(number)  
	local formatted = number  
	while true do      
		formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1 %2')    
		if ( k==0 ) then      
			break   
		end  
	end  
	return formatted
end

function getFormatSpeed(unit)
    if unit < 10 then
        unit = "#beb5ae00#ffffff" .. unit
    elseif unit < 100 then
        unit = "#beb5ae0#ffffff" .. unit
    elseif unit >= 1000 then
        unit = "#ffffff999"
    end
    return unit
end

function math.round(number, decimals, method)
    decimals = decimals or 0
    local factor = 10 ^ decimals
    if (method == "ceil" or method == "floor") then return math[method](number * factor) / factor
    else return tonumber(("%."..decimals.."f"):format(number)) end
end

function getElementSpeed(element,unit)
    if (unit == nil) then unit = 0 end
    if (isElement(element)) then
        local x,y,z = getElementVelocity(element)
        if (unit=="mph" or unit==1 or unit =='1') then
            return math.floor((x^2 + y^2 + z^2) ^ 0.5 * 100)
        else
            return math.floor((x^2 + y^2 + z^2) ^ 0.5 * 100 * 1.609344)
        end
    else
        return false
    end
end

function angle(vehicle)
	local vx,vy,vz = getElementVelocity(vehicle)
	local modV = math.sqrt(vx*vx + vy*vy)
	
	if not isVehicleOnGround(vehicle) then return 0,modV end
	
	local rx,ry,rz = getElementRotation(vehicle)
	local sn,cs = -math.sin(math.rad(rz)), math.cos(math.rad(rz))
	
	local cosX = (sn*vx + cs*vy)/modV
	return math.deg(math.acos(cosX))*0.5, modV
end

function setElementSpeed(element, unit, speed)
	if (unit == nil) then unit = 0 end
	if (speed == nil) then speed = 0 end
	speed = tonumber(speed)
	local acSpeed = getElementSpeed(element, unit)
	if (acSpeed~=false) then 
		local diff = speed/acSpeed
		local x,y,z = getElementVelocity(element)
		setElementVelocity(element,x*diff,y*diff,z*diff)
		return true
	else
		return false
	end
end

function round2(num, idp)
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

lp = getLocalPlayer()
ccEnabled = false

myveh = false
targetSpeed = 1
multiplier = 1

function cc()
	if (not isElement(myveh)) then
		removeEventHandler("onClientRender", getRootElement(), cc)
		ccEnabled=false
		return false
	end
	if getVehicleEngineState(myveh) == false then
		removeEventHandler("onClientRender", getRootElement(), cc)
		ccEnabled=false
		return false
	end
	local x,y = angle(myveh)
	if (x<15) then
		local speed = getElementSpeed(myveh)
		local targetSpeedTmp = speed + multiplier
		if (targetSpeedTmp > targetSpeed) then
			targetSpeedTmp = targetSpeed
		end
		if (targetSpeedTmp > 3) then
			setElementSpeed(myveh, "k", targetSpeedTmp)
		end
	end
end

bindKey("lshift", "up", function()
	local veh = getPedOccupiedVehicle(lp)
	if (veh) then
		if (lp==getVehicleOccupant(veh)) then
			myveh = veh
			if (ccEnabled) then
				removeEventHandler("onClientRender", getRootElement(), cc)
				ccEnabled=false
			else
				targetSpeed = getElementSpeed(veh)
				if targetSpeed > 4 then
					if (limit) then
						if in_array(getVehicleType(veh), allowedTypes) then
							targetSpeed = round2(targetSpeed)
							addEventHandler("onClientRender", getRootElement(), cc)
							ccEnabled=true				
						end
					else
						targetSpeed = round2(targetSpeed)
						addEventHandler("onClientRender", getRootElement(), cc)
						ccEnabled=true
					end
				end
			end
		end
	end
end)

bindKey("brake_reverse","down",function()
	if ccEnabled then
		removeEventHandler("onClientRender", getRootElement(), cc)
		ccEnabled=false
	end
end)

bindKey("handbrake","down",function()
	if ccEnabled then
		removeEventHandler("onClientRender", getRootElement(), cc)
		ccEnabled=false
	end
end)

addEventHandler("onClientPlayerVehicleExit", getLocalPlayer(), function(veh, seat)
	if (seat==0) then
		if (ccEnabled) then
			removeEventHandler("onClientRender", getRootElement(), cc)
			ccEnabled=false
		end
	end
end)
