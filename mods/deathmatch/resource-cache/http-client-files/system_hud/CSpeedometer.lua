---- ==== Парметры худа ==== ----
local screenW, screenH = guiGetScreenSize()

local napFont = exports.system_fonts:createFont("OpenSans/OpenSans-Italic.ttf", 11, "antialiased")
local speedFont = exports.system_fonts:createFont("OpenSans/OpenSans-Italic.ttf", 42, "antialiased")
local kmhFont = exports.system_fonts:createFont("OpenSans/OpenSans-BoldItalic.ttf", 11, "antialiased")

local healthVehicleTexture = dxCreateTexture( "img/speed_engine.png" )
local healthVehicleShader = dxCreateShader( "fx/circle.fx" )
local fuelVehicleTexture= dxCreateTexture( "img/speed_fuel.png" )
local fuelVehicleShader = dxCreateShader( "fx/circle.fx" )

function drawSpeed()
	local currentVehicle = getPedOccupiedVehicle(localPlayer)
	if getVehicleController(currentVehicle) ~= localPlayer then return end
	-- Фон жизней
	dxDrawImage(screenW - 260, screenH - 220, 190, 190, healthVehicleTexture, 0,0,0, tocolor(255,255,255,200))	
	-- Жизни машины	
	dxSetShaderValue(healthVehicleShader, "tex", healthVehicleTexture)
	dxSetShaderValue(healthVehicleShader, "angle", -1 )
	local procent_exp = 0.5 + ( 0.75 * ( 1 - ( getElementHealth(currentVehicle) - 300 ) / 700 ) )
	dxSetShaderValue(healthVehicleShader, "dg", procent_exp )
	dxDrawImage(screenW - 260, screenH - 220, 190, 190, healthVehicleShader, 0,0,0, tocolor(255,255,255,255))
			
	-- Фон топлива
	dxDrawImage(screenW - 275, screenH - 235, 220, 220, fuelVehicleTexture, 0,0,0, tocolor(255,255,255,200))
	-- Топливо
	dxSetShaderValue(fuelVehicleShader, "tex", fuelVehicleTexture)
	dxSetShaderValue(fuelVehicleShader, "angle", -1 )
	local fuel, max_fuel = (getElementData(currentVehicle, "fuel") or 0), (getElementData(currentVehicle, "maxFuel") or 100) -- кол-во бензина / максимальное кол-во бензина
	local fuel_erc = fuel / max_fuel
	local procent_exp = 0.5 + ( 0.5 * ( 1 - fuel_erc ) )
	dxSetShaderValue(fuelVehicleShader, "dg", procent_exp )
    dxDrawImage(screenW - 275, screenH - 235, 220, 220, fuelVehicleShader, 0,0,0, tocolor(255,255,255,255))
	
	-- Напоминалка
	local need_repair = getElementHealth(currentVehicle) <= 400
	local text = fuel == 0 and ( "Пора за канистрой".. ( need_repair and "\nи починиться" or "" ) ) or ( fuel < 0.15 and "Пора бы заправиться" ..( need_repair and "\nи починиться" or "" ) or ( need_repair and "Пора в автосервис" or "" ) )
	dxCreateText(screenW-185, screenH - 90, 0, 28, text, napFont, tocolor(255,255,255,100 + math.sin( getTickCount( ) / 250 ) * 150  ), "center", "center")
	
	-- Скорость
	local speed = getElementSpeed(currentVehicle)
	dxCreateText(screenW - 260, screenH - 225, 190, 190, getFormatSpeed(speed), speedFont, tocolor(255,255,255,255), "center", "center")
	dxCreateText(screenW - 130, screenH - 110, 0, 20, "км/ч", kmhFont, tocolor(255,255,255,200), "center", "center")
	
	-- Пробег
	if not driveDistance then lastTick = getTickCount() driveDistance = getElementData (currentVehicle, "driveDistance" ) or 0 end
		local neux, neuy, neuz = getElementPosition(currentVehicle)
	if not altx or not alty or not altz then
		altx, alty, altz=getElementPosition(currentVehicle)
	end
	local driveTotal = getDistanceBetweenPoints3D(neux,neuy,neuz,altx,alty,altz)
	driveTotal = driveTotal/1000
	altx,alty,altz=neux,neuy,neuz
	driveDistance = tonumber(("%.1f"):format(driveDistance+driveTotal))
	if lastTick+5000 < getTickCount() then
		lastTick = getTickCount()
		setElementData (currentVehicle, "driveDistance", driveDistance )
	end
	dxCreateText(screenW - 95, screenH - 50, 0, 20, driveDistance.." км", kmhFont, tocolor(255,255,255,200), "left", "center")
	
	-- Поворотники
		-- > Правый
	if getElementData(currentVehicle, "signal") == 1 or getElementData(currentVehicle, "signal") == 3 then
		if ( getTickCount () % 1400 >= 400 ) then
			dxDrawImage(screenW - 220, screenH - 50, 25, 18, "img/signal.png", -180,0,0, tocolor(255,255,255,250))
		else
			dxDrawImage(screenW - 220, screenH - 50, 25, 18, "img/signal_outline.png", -180,0,0, tocolor(255,255,255,200))
		end
	else
		dxDrawImage(screenW - 220, screenH - 50, 25, 18, "img/signal_outline.png", -180,0,0, tocolor(255,255,255,200))
	end
		-- > Левый
	if getElementData(currentVehicle, "signal") == 2 or getElementData(currentVehicle, "signal") == 3 then
		if ( getTickCount () % 1400 >= 400 ) then
			dxDrawImage(screenW - 185, screenH - 50, 25, 18, "img/signal.png", 0,0,0, tocolor(255,255,255,250))
		else
			dxDrawImage(screenW - 185, screenH - 50, 25, 18, "img/signal_outline.png", 0,0,0, tocolor(255,255,255,200))
		end
	else
		dxDrawImage(screenW - 185, screenH - 50, 25, 18, "img/signal_outline.png", 0,0,0, tocolor(255,255,255,200))
	end
	
	-- Иконки
	dxDrawImage(screenW - 65, screenH - 120, 18, 18, "img/fuel_icon.png", 0,0,0, tocolor(255,255,255,255))
	dxDrawImage(screenW - 140, screenH - 50, 26, 18, "img/engine_icon.png", 0,0,0, tocolor(255,255,255,255))
	
	-- Круиз
	if ccEnabled == true then
		dxDrawImage(screenW - 271, screenH - 55, 31, 31, "img/cruise_enabled.png", 0,0,0, tocolor(255,255,255,255))
	else
		dxDrawImage(screenW - 271, screenH - 55, 31, 31, "img/cruise_disabled.png", 0,0,0, tocolor(255,255,255,255))
	end
end

function ClientVehicleEnter(vehicle, seat )
    if seat == 0 then
        addEventHandler("onClientRender", root, drawSpeed)
    end
end
addEventHandler( "onClientPlayerVehicleEnter", localPlayer, ClientVehicleEnter )

function ClientVehicleExit( vehicle, seat )
	removeEventHandler("onClientRender", root, drawSpeed)
end
addEventHandler( "onClientPlayerVehicleExit", localPlayer, ClientVehicleExit )