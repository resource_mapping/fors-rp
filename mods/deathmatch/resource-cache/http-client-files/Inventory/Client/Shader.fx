texture Tex0;

float4x4 World;
float4x4 View;
float4x4 Projection;
float4x4 WorldViewProjection;
int Width, Height;
float Time;

sampler Sampler0 = sampler_state
{
    Texture = (Tex0);
};

struct VSInput
{
    float3 Position : POSITION;
    float3 Normal : NORMAL0;
    float4 Diffuse  : COLOR0;
    float2 TexCoord : TEXCOORD0;
};

struct PSInput
{
  float4 Position : POSITION0;
  float4 Diffuse  : COLOR0;
  float2 TexCoord : TEXCOORD0;
};

float invertColor(float color)
{
    if (color >= 0.5) return 1.0f;
    return 0.0f;
}

PSInput VertexShaderExample(VSInput VS)
{
    PSInput PS = (PSInput)0;
    PS.Position = mul(float4(VS.Position, 1), WorldViewProjection);

    //float Multx = 0.45*sin(Time) + 0.52;
    //float Multy = 0.45*cos(Time) + 0.52;
    //VS.TexCoord.x = Multx * VS.TexCoord.x + 0.5 - 0.5 * Multx;
    //VS.TexCoord.y = Multy * VS.TexCoord.y + 0.5 - 0.5 * Multy;
    

    PS.Diffuse = VS.Diffuse;
    PS.TexCoord = VS.TexCoord;
    return PS;
}

float4 PixelShaderExample(PSInput PS) : COLOR0
{
    //PS.TexCoord.x += sin(PS.TexCoord.x * 50 + Time * 16) * 0.05;
    PS.TexCoord.y += sin(PS.TexCoord.y * 50 + Time * 16) * 0.03;

    //PS.TexCoord.x = -PS.TexCoord.x;

	float4 StartColor = tex2D(Sampler0, PS.TexCoord); // 007FFF 0095B6
	float4 finalColor = float4(
	0.0 + 0.7 * abs(3 * sin(PS.TexCoord.x + Time)),
    0.58 + 0.42 * abs(3 * sin(PS.TexCoord.x + Time)),
	0.71 + 0.29 * abs(3 * sin(PS.TexCoord.x + Time)),
    0.14);
	return finalColor;
}

technique complercated
{
    pass P0
    {
        VertexShader = compile vs_2_0 VertexShaderExample();
        PixelShader  = compile ps_2_0 PixelShaderExample();
    }
}