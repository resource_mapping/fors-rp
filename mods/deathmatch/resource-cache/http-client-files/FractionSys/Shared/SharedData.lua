-- TrollingCont. 24.02.2020

FRACTION_ID_KEY = "tcfs.fraction"
FRACTION_DATA_KEY = "tcfs.fractionData"
ACCOUNT_NAME_KEY = "tcfs.accountName"
WANTED_STATUS_KEY = "tcfs.wantedStatus"

defaultSkinId = 0

fractionIds = {
    "hwpatrol"
}

-- Фракция ДПС
HWPATROL_FRACTION_ID = fractionIds[1]

fractionTeamParams = {
    [HWPATROL_FRACTION_ID] = { name="ДПС", r=31, g=79, b=237 }
}

hwPatrolRanks = {
    ["r"] = "Рядовой",
    ["m_ser"] = "Младший сержант",
    ["ser"] = "Сержант",
    ["s_ser"] = "Старший сержант",
    ["sta"] = "Старшина",
    ["pra"] = "Прапорщик полиции",
    ["s_pra"] = "Старший прапорщик",
    ["m_lei"] = "Младший лейтенант",
    ["lei"] = "Лейтенант",
    ["s_lei"] = "Старший лейтенант",
    ["cap"] = "Капитан",
    ["may"] = "Майор",
    ["ppol"] = "Подполковник",
    ["pol"] = "Полковник",
    ["gen"] = "Генерал"
}

hwPatrolRights = {
    ["ppol"] = { viewWorkers = true },
    ["pol"] = { viewWorkers = true, viewLogs = true, editPlayers = true },
    ["gen"] = { viewWorkers = true, viewLogs = true, editPlayers = true }
}

hwPatrolSkins = {
    ["r"] = 66,
    ["m_ser"] = 281,
    ["ser"] = 281,
    ["s_ser"] = 281,
    ["sta"] = 144,
    ["pra"] = 233,
    ["s_pra"] = 233,
    ["m_lei"] = 233,
    ["lei"] = 233,
    ["s_lei"] = 233,
    ["cap"] = 169,
    ["may"] = 283,
    ["ppol"] = 283,
    ["pol"] = 288,
    ["gen"] = 254
}

fracSysWindows = {
    [HWPATROL_FRACTION_ID] = {}
}

wantedReasons = {
    [1] = { name="1.1 УК РФ" },
    [2] = { name="1.2 УК РФ" }
}