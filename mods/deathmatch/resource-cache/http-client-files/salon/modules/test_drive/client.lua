local demoTimer = nil
local timerShown = false

function initDemoTimer()
	if timerShown then
		removeEventHandler( "onClientRender", root, drawDemoTimer)
		if isTimer(demoTimer) then killTimer(demoTimer) end
		demoTimer = nil
	else
		demoTimer = setTimer(function() end, 60000, 1)
		addEventHandler( "onClientRender", root, drawDemoTimer)
	end
	timerShown = not timerShown
end
addEvent("initDemoTimer",true)
addEventHandler("initDemoTimer",root,initDemoTimer)

function drawDemoTimer()
	if isTimer(demoTimer) then
		local details = getTimerDetails( demoTimer )
		dxDrawRectangle(scx/2-50*px,0,100*px,50*px,tocolor(0,0,0,150))
		dxDrawText("0:"..math.floor(details/1000),scx/2,30,scx/2,30,tocolor(255,255,255),2*px,"default-bold","center","center")
	end
end