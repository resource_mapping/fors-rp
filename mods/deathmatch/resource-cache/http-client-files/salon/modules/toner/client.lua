local markers = {}

for k,v in pairs(tonerMarkers) do
	local mrk = createMarker(v[1],v[2],v[3],"cylinder",4,100,100,100)
	setElementData(mrk,"toner.marker",true)
	if blipData[1] then
		local blp = createBlipAttachedTo(mrk,blipData[2])
		setBlipVisibleDistance(blp,blipData[3])
	end
	table.insert(markers,mrk)
end

function isValidVehicle(veh)
	if veh then
		local id = getElementModel(veh)
		if vehicleAdaptation[id] then
			return vehicleAdaptation[id]
		end
	end
end

local tonerShown = false
local currentType = 1
local colorPositions = {0.5,0.5,0.5,0.5}
local selection = 1
local oldToner = {}
local shader = {}
local vehicle = nil


function initTonerWindow()
	if tonerShown then
		showCursor(false)
		removeEventHandler("onClientRender",root,drawTonerWindow)
		removeEventHandler("onClientKey", root, blockControls)
		showChat(true)
		setElementData(vehicle,"toner",oldToner)
		for k,v in pairs(markers) do
			setElementAlpha(v,200)
		end
		tonerShown = false
	else
		local veh = getPedOccupiedVehicle( localPlayer )
		if veh then
			if isValidVehicle(veh) then
				if getElementData(veh,"parent") == localPlayer then
					showCursor(true)
					showChat(false)
					addEventHandler("onClientRender",root,drawTonerWindow)
					addEventHandler("onClientKey", root, blockControls)
					currentType = isValidVehicle(veh)
					applyToner(veh)
					oldToner = getElementData(veh,"toner") or {}
					for k,v in pairs(tonerTable[currentType]) do
						if not oldToner[k] then
							oldToner[k] = {0,0,0,0.3}
						end
					end
					setElementData(veh,"toner",oldToner)
					vehicle = veh

					for k,v in pairs(markers) do
						setElementAlpha(v,0)
					end
					tonerShown = true
				end
			else
				outputChatBox("Этот автомобиль не поддерживает тонировку!",200,50,50)
			end
		end
	end
end

local scx,scy = guiGetScreenSize()

local function isCursorOverRectangle(x,y,w,h)
	if isCursorShowing() then
		local mx,my = getCursorPosition() -- relative
		local cursorx,cursory = mx*scx,my*scy
		if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
			return true
		end
	end
return false
end

local sizeX,sizeY = 600,500
local posX,posY = 10,scy/2-sizeY/2

local r,g,b = 255,255,255
local r2,g2,b2 = 255,255,255
local fr,fg,fb,fa = 255,255,255,255/2

local f = dxCreateTexture("modules/toner/files/palette.png")
local pixels = dxGetTexturePixels(f,0,0,256,256)

local colorRT = dxCreateRenderTarget(20,200,true)

local clk = false

function drawTonerWindow()
	dxDrawRectangle(posX,posY,sizeX,sizeY,tocolor(20,20,20,230))
	local sx,sy = posX+20,posY+20
	--dxDrawRectangle(sx,sy,200,35*#tonerTable[currentType])
	for k,v in pairs(tonerTable[currentType]) do
		if selection == k then
			dxDrawRectangle(sx,sy,200,30,tocolor(80,190,80,255))
		else
			dxDrawRectangle(sx,sy,200,30,tocolor(100,100,100,230))
		end
		if isCursorOverRectangle(sx,sy,200,30) then
			if getKeyState( "mouse1" ) then
				selection = k
			end
		end
		dxDrawText(v[2],sx,sy,sx+200,sy+30,tocolor(255,255,255),1,"default-bold","center","center")
		--dxDrawText(v[2],sx,sy,sx+200,sy+30,tocolor(0,0,0),1,"default-bold","center","center")
		sy=sy+35
	end
	if selection == 0 then
		dxDrawRectangle(sx,sy,200,30,tocolor(80,190,80,255))
	else
		dxDrawRectangle(sx,sy,200,30,tocolor(100,100,100,230))
	end
	if isCursorOverRectangle(sx,sy,200,30) then
		if getKeyState( "mouse1" ) then
			selection = 0
		end
	end
	dxDrawText("Все",sx,sy,sx+200,sy+30,tocolor(255,255,255),1,"default-bold","center","center")

	dxSetRenderTarget( colorRT, true )
		dxDrawImage(0,0,20,200,"modules/toner/files/alpha.png",0,0,0,tocolor(r2,g2,b2,255))
	dxSetRenderTarget()

		local pixels2 = dxGetTexturePixels( colorRT )

		dxDrawImage(posX+240,posY+20,300,200,"modules/toner/files/palette.png")
		dxDrawRectangle(posX+240+300*colorPositions[1]-3,posY+20+200*colorPositions[2]-3,6,6,tocolor(0,0,0))

		dxDrawImage(posX+550,posY+20,20,200,colorRT)

		if isCursorOverRectangle(posX+240,posY+20,300,200) then
			if getKeyState("mouse1") then
				local mx,my = getCursorPosition()
				local cx,cy = mx*scx,my*scy
				local relX,relY = cx-(posX+240), cy - (posY+20)
				colorPositions[1] = relX/300
				colorPositions[2] = relY/200
				r2,g2,b2 = dxGetPixelColor(pixels,colorPositions[1]*256,colorPositions[2]*256)
				fr,fg,fb = dxGetPixelColor(pixels2,10,colorPositions[3]*200)

				if selection ~= 0 then
					local d = getElementData(vehicle,"toner")
					d[selection][1] = fr/255
					d[selection][2] = fg/255
					d[selection][3] = fb/255
					d[selection][4] = fa/255
					setElementData(vehicle,"toner",d,false)
				else
					local d = getElementData(vehicle,"toner")
					for k,v in pairs(tonerTable[currentType]) do
						d[k][1] = fr/255
						d[k][2] = fg/255
						d[k][3] = fb/255
						d[k][4] = fa/255
					end
					setElementData(vehicle,"toner",d,false)
				end

			end
		end


		dxDrawRectangle(posX+548,posY+20+200*colorPositions[3],24,3,tocolor(255,255,255))
		if isCursorOverRectangle(posX+550,posY+20,20,200) then
			if getKeyState("mouse1") then
				local mx,my = getCursorPosition()
				local cx,cy = mx*scx,my*scy
				local relX,relY = cx-(posX+550), cy - (posY+20)
				colorPositions[3] = relY/200
				r,g,b = dxGetPixelColor(pixels2,relX,relY)
				fr,fg,fb = dxGetPixelColor(pixels2,10,colorPositions[3]*200)

				if selection ~= 0 then
					local d = getElementData(vehicle,"toner")
					d[selection][1] = fr/255
					d[selection][2] = fg/255
					d[selection][3] = fb/255
					d[selection][4] = fa/255
					setElementData(vehicle,"toner",d,false)
				else
					local d = getElementData(vehicle,"toner")
					for k,v in pairs(tonerTable[currentType]) do
						d[k][1] = fr/255
						d[k][2] = fg/255
						d[k][3] = fb/255
						d[k][4] = fa/255
					end
					setElementData(vehicle,"toner",d,false)
				end

			end
		end
		if fa <= 80 then fa = 80 end

		dxDrawRectangle(posX+240,posY+240,300,20,tocolor(150,150,150))
		dxDrawText("Прозрачность (".. 100-math.floor(fa/255*100) .."%)",posX+240,posY+240,posX+540,posY+260,tocolor(50,50,180),1,"default-bold","center","center")
		dxDrawRectangle(posX+240+colorPositions[4]*300-2,posY+237,4,26,tocolor(255,255,255))

		if isCursorOverRectangle(posX+240,posY+240,300,20) then
			if getKeyState("mouse1") then
				local mx,my = getCursorPosition()
				local cx,cy = mx*scx,my*scy
				local relX = cx-(posX+240)
				colorPositions[4] = relX/300
				fa = 80+175*colorPositions[4]

				if selection ~= 0 then
					local d = getElementData(vehicle,"toner")
					d[selection][4] = fa/255
					setElementData(vehicle,"toner",d,false)
				else
					local d = getElementData(vehicle,"toner")
					for k,v in pairs(tonerTable[currentType]) do
						d[k][4] = fa/255
					end
					setElementData(vehicle,"toner",d,false)
				end
			end
		end

	if getKeyState("mouse2") then
		showCursor(false)
	else
		showCursor(true)
	end

	local cart = {}
	local d = getElementData(vehicle,"toner")
	for k,v in pairs(tonerTable[currentType]) do
		if d[k][1] ~= oldToner[k][1] or d[k][2] ~= oldToner[k][2] or d[k][3] ~= oldToner[k][3] or d[k][4] ~= oldToner[k][4] then
			table.insert(cart,{"Изменение цвета ("..v[2]..")",v[3]})
		end
	end
	local tx,ty = posX+240, posY+300
	local total = 0
	for k,v in pairs(cart) do
		dxDrawText(v[1].." - "..convertNumber(v[2]),tx,ty,tx+300,ty+20,tocolor(255,255,255),1,"default-bold","center","center")
		ty = ty+20
		total = total + v[2]
	end


	if isCursorOverRectangle(posX+20,posY+450,200,40) then
		if getKeyState("mouse1") then
			initTonerWindow()
		end
		dxDrawRectangle(posX+20,posY+450,200,40,tocolor(200,40,40))
	else
		dxDrawRectangle(posX+20,posY+450,200,40,tocolor(150,40,40))
	end
	dxDrawText("Выйти",posX+20,posY+450,posX+220,posY+490,tocolor(255,255,255),1,"default-bold","center","center")

	if isCursorOverRectangle(posX+380,posY+450,200,40) then
		if getKeyState("mouse1") and not clk then
			if getPlayerMoney() >= total then
				triggerServerEvent("buyToner",localPlayer,getElementData(vehicle,"toner"),total)
				oldToner = getElementData(vehicle,"toner")
				initTonerWindow()
			else
				outputChatBox("Недостаточно денег!",200,50,50)
			end
		end
		dxDrawRectangle(posX+380,posY+450,200,40,tocolor(40,200,40))
	else
		dxDrawRectangle(posX+380,posY+450,200,40,tocolor(40,150,40))
	end
	dxDrawText("Оплатить\n"..convertNumber(total),posX+380,posY+450,posX+580,posY+490,tocolor(255,255,255),1,"default-bold","center","center")

	if getKeyState("mouse1") then clk = true else clk = false end
end

function applyToner(veh)
	local form = isValidVehicle(veh)
	if not form then return end

	if not shader[veh] then
		shader[veh] = {}
	end

	local data = getElementData(veh,"toner") or {}

	for k,v in pairs(tonerTable[form]) do
		if not shader[veh][k] then
			shader[veh][k] = dxCreateShader("modules/toner/files/shader.fx")
		end
		if not data[k] then
			data[k] = {0,0,0,0.5}
		end
		dxSetShaderValue(shader[veh][k], "gTexture", "modules/toner/files/texture.png")
		dxSetShaderValue(shader[veh][k], "gAlpha", data[k][4])
		dxSetShaderValue(shader[veh][k], "gRedColor", data[k][1])
		dxSetShaderValue(shader[veh][k], "gGrnColor", data[k][2])
		dxSetShaderValue(shader[veh][k], "gBluColor", data[k][3])
		engineApplyShaderToWorldTexture( shader[veh][k], v[1], veh )
	end
end

addEventHandler ( "onClientElementDataChange", getRootElement(),
function ( dataName )
	if getElementType ( source ) == "vehicle" and dataName == "toner" then
		if getElementData(source,"toner") then
			applyToner(source)
		end
	end
end)

addEventHandler ( "onClientMarkerHit", getRootElement(), function(ply) 
	if ply == localPlayer then
		local veh = getPedOccupiedVehicle( localPlayer )
		if veh then
			if localPlayer == getVehicleController(veh) then
				for k,v in pairs(markers) do
					if v == source then
						initTonerWindow()
					end
				end
			end
		end
	end
end)