local fuelWindow = nil

for k,v in pairs(fuelMarkers) do
	local marker = createMarker(v[1],v[2],v[3],"corona",3,200,100,0,150)
	local blip = createBlipAttachedTo(marker,blipID)
	setBlipVisibleDistance( blip, 200 )
	setElementData(marker,"fuelstation",true)
end

addEventHandler ( "onClientMarkerHit", getRootElement(), function(ply)
	if ply == localPlayer then
		if getElementData(source,"fuelstation") then
			local veh = getPedOccupiedVehicle(ply)
			if veh and getVehicleController(veh) == ply	then
				initFuelWindow()
			end
		end
	end
end)

local scx,scy = guiGetScreenSize()
local px = scx/1920
if px <= 0.8 then px = 0.8 end

local sizeX,sizeY = 700*px,600*px
local posX,posY = scx/2-sizeX/2,scy/2-sizeY/2

local function isCursorOverRectangle(x,y,w,h)
	if isCursorShowing() then
		local mx,my = getCursorPosition() -- relative
		local cursorx,cursory = mx*scx,my*scy
		if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
			return true
		end
	end
return false
end

local fuelWindowShown = false
local selection = 1
local font1 = dxCreateFont("files/font.otf",14*px)
local font2 = dxCreateFont("files/font.otf",12*px)
local font3 = dxCreateFont("files/font.otf",25*px)
local scrollX = 0
local fuelAmount = 0
local click = false
local fillingNow = false

function initFuelWindow()
	local veh = getPedOccupiedVehicle( localPlayer )
	if fuelWindowShown then
		removeEventHandler("onClientRender",root,drawFuelWindow)
		showCursor(false)
		setElementFrozen(veh,false)
	else
		addEventHandler("onClientRender",root,drawFuelWindow)
		showCursor(true)
		setElementFrozen(veh,true)
	end
	fuelWindowShown = not fuelWindowShown
end
addEvent("initFuelWindow",true)
addEventHandler("initFuelWindow",root,initFuelWindow)

function drawFuelWindow()
	--showCursor(true)
	local veh = getPedOccupiedVehicle( localPlayer )
	local current = math.floor(getElementData(veh,"fuel"))
	local max = getElementData(veh,"maxFuel") or 100
	local fType = getElementData(veh,"fuelType") or 1

	if selection ~= fType then
		current = 0
	end

	dxDrawRectangle(posX-2*px,posY-32*px,sizeX+4*px,sizeY+34*px,tocolor(gColor[1],gColor[2],gColor[3],210))


	if isCursorOverRectangle(posX+sizeX-32*px,posY-32*px,34*px,34*px) then
		dxDrawRectangle(posX+sizeX-32*px,posY-32*px,34*px,34*px,tocolor(200,50,50,230))
		dxDrawText("X",posX+sizeX-32*px,posY-32*px,posX+sizeX+2*px,posY,tocolor(255,255,255),1,font1,"center","center")
		if getKeyState("mouse1") and not click then
			initFuelWindow()
		end
	else
		dxDrawRectangle(posX+sizeX-32*px,posY-32*px,34*px,34*px,tocolor(150,50,50,230))
		dxDrawText("X",posX+sizeX-32*px,posY-32*px,posX+sizeX+2*px,posY,tocolor(200,200,200),1,font1,"center","center")
	end


	--dxDrawImage(posX,posY,sizeX,sizeY,"files/background.png")
	dxDrawRectangle(posX,posY,sizeX,sizeY,tocolor(0,0,0,250))

	dxDrawRectangle(posX+98*px,posY+148*px,504*px,254*px,tocolor(200,200,200,200))
	dxDrawRectangle(posX+100*px,posY+150*px,500*px,250*px,tocolor(gColor[1]/3,gColor[2]/3,gColor[3]/3,255))

	dxDrawRectangle(posX+150*px,posY+100*px,400*px,30*px,tocolor(100,100,100,255))
	dxDrawRectangle(posX+150*px,posY+100*px,current/max*400*px,30*px,tocolor(100,200,100,255))

	dxDrawText(current.."/"..max.." л  ("..fuelTypes[fType][1]..")",posX+150*px,posY+100*px,posX+550*px,posY+130*px,tocolor(255,255,255),1,font1,"center","center")

	dxDrawText("Выберите топливо",posX+100*px,posY+150*px,posX+600*px,posY+190*px,tocolor(220,200,255),1,font1,"center","center")

	--dxDrawText("АЗС",posX+100*px,posY,posX+600*px,posY+100*px,tocolor(240,240,255),1,font3,"center","center")

	local lx,ly = posX+120*px,posY+200*px
	local side = "left"
	for k,v in pairs(fuelTypes) do
		if isCorrectFuel(getElementModel(veh),k) then
			dxDrawImage(lx,ly,31*px,31*px,"files/rbUnactive.png")
		else
			dxDrawImage(lx,ly,31*px,31*px,"files/rbUnactive.png",0,0,0,tocolor(150,150,150))
		end
		if selection == k then
			dxDrawImage(lx,ly,31*px,31*px,"files/rbActive.png")
		end
		if side == "left" then
			dxDrawText(v[1],lx+35*px,ly,lx,ly+31*px,tocolor(255,255,255),1,font1,side,"center")
			dxDrawText(convertNumber(v[2]),lx+120*px,ly,lx,ly+31*px,tocolor(55,200,55),1,font2,side,"center")

		else
			dxDrawText(v[1],lx,ly,lx-4*px,ly+31*px,tocolor(255,255,255),1,font1,side,"center")
			dxDrawText(convertNumber(v[2]),lx+35*px,ly,lx-100*px,ly+31*px,tocolor(55,200,55),1,font2,side,"center")
		end

		if isCursorOverRectangle(lx-20*px,ly,100*px,31*px) then
			if getKeyState("mouse1") and not click then
				if isCorrectFuel(getElementModel(veh),k) then
					selection = k
					scrollX = 0
					fuelAmount = 0
				end
			end
		end

		ly = ly + 50*px
		if k == 4 then
			side = "right"
			lx = posX+550*px
			ly = posY+200*px
		end
	end

	dxDrawRectangle(posX+70*px,posY+520*px,300*px,10*px,tocolor(200,200,200))

	if isCursorOverRectangle(posX+60*px,posY+510*px,320*px,30*px) then
		if getKeyState("mouse1") and not fillingNow then
			local cx,cy = getCursorPosition()
			local mx,my = cx*scx,cy*scy
			scrollX = mx-posX-72.5*px
			--outputChatBox(scrollX)
			if scrollX >= 295*px then scrollX = 295*px
			elseif scrollX <= 0 then scrollX = 0 end
			fuelAmount = math.floor(scrollX/(295*px)*(max-current))
			if fuelAmount <= 0 then fuelAmount = 0 end
		end
	end

	dxDrawRectangle(posX+70*px+scrollX,posY+515*px,5*px,20*px,tocolor(gColor[1],gColor[2],gColor[3]))

	dxDrawText(fuelAmount.." л",posX+70*px,posY+520*px,posX+370*px,posY+580*px,tocolor(255,255,255),1,font1,"center","center")

	sum = math.floor(fuelAmount*fuelTypes[selection][2])

	if isCursorOverRectangle(posX+450*px,posY+520*px,200*px,50*px) then
		dxDrawRectangle(posX+450*px,posY+520*px,200*px,50*px,tocolor(gColor[1]/2,gColor[2]/2,gColor[3]/2))
		dxDrawText("Оплатить\n("..convertNumber(sum)..")",posX+450*px,posY+520*px,posX+650*px,posY+570*px,tocolor(255,255,255),1,font1,"center","center")
		if getKeyState("mouse1") and not click then
			if getPlayerMoney(localPlayer) >= sum then
				if isCorrectFuel(getElementModel(veh),selection) then
					if fType ~= selection then
						setElementData(veh,"fuel",0,false)
					end
					setElementData(veh,"fuelType",selection,false)
					fillVeh(fuelAmount)
				else
					outputChatBox("Выбран некорректный вид топлива!",200,50,50)
				end
			else
				outputChatBox("Недостаточно денег!",200,50,50)
			end
		end
	else
		dxDrawRectangle(posX+450*px,posY+520*px,200*px,50*px,tocolor(gColor[1]/3,gColor[2]/3,gColor[3]/3))
		dxDrawText("Оплатить\n("..convertNumber(sum)..")",posX+450*px,posY+520*px,posX+650*px,posY+570*px,tocolor(200,200,200),1,font1,"center","center")
	end

	if getKeyState("mouse1") or fillingNow then click = true else click = false end

end
--initFuelWindow()

local toFill = 0

function fillVeh(amount)
	local amount = tonumber(amount) or 0
	local veh = getPedOccupiedVehicle(localPlayer)
	toFill = getElementData(veh,"fuel")+amount
	addEventHandler("onClientPreRender",root,filling)
	fillingNow = true
end

function filling()
	local veh = getPedOccupiedVehicle(localPlayer)
	local current = getElementData(veh,"fuel")
	if current < toFill then
		setElementData(veh,"fuel",current+0.5,false)
	else
		triggerServerEvent("fillVeh2",localPlayer,veh,selection,fuelAmount)
		removeEventHandler("onClientPreRender",root,filling)
		toFill = 0
		fillingNow = false
		fuelAmount = 0
	end
end

function checkFuel()
	local veh = getPedOccupiedVehicle(localPlayer)
	if veh and getVehicleController(veh) == localPlayer then
		if not getElementData(veh,"fuel") then return end
		if getElementData(veh,"fuel") <= 1 then
			setVehicleEngineState(veh,false)
		end
	end
end
addEventHandler('onClientPreRender',root,checkFuel)