local scx,scy = guiGetScreenSize()
local px = scx/1920
if px <= 0.8 then px = 0.8 end
local sizeX, sizeY = dxElements["background"][3]*px,dxElements["background"][4]*px
local posX,posY = scx/2-sizeX/2,scy/2-sizeY/2

local fps = 60
local casinoShown = false
local rt = dxCreateRenderTarget( dxElements["slots"][3]*px,dxElements["slots"][4]*px,true )
local generatedSlots = {}
local bias = 0
local slot = 1
local isSpinning = false
local speed,speed0 = 0,0
local boxSize = dxElements["slotSize"]*px
local point = 0
local clk = false

function math.percentChance (percent,repeatTime)
	local hits = 0
	for i = 1, repeatTime do
	local number = math.random(0,1000)/10
		if number <= percent then
			hits = hits+1
		end
	end
	return hits
end

function updateFPS(msSinceLastFrame)
    fps = (1 / msSinceLastFrame) * 1000
end
addEventHandler("onClientPreRender", root, updateFPS)

local function isCursorOverRectangle(x,y,w,h)
	if isCursorShowing() then
		local mx,my = getCursorPosition() -- relative
		local cursorx,cursory = mx*scx,my*scy
		if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
			return true
		end
	end
return false
end

function draw()
	dxDrawImage(posX,posY,sizeX,sizeY,"files/casino/background.png")
	dxDrawImage(posX+dxElements["slots"][1]*px,posY+dxElements["slots"][2]*px,dxElements["slots"][3]*px,dxElements["slots"][4]*px,"files/casino/slots.png")
	if isCursorOverRectangle(posX+dxElements["close"][1]*px,posY+dxElements["close"][2]*px,dxElements["close"][3],dxElements["close"][4]) then
		if getKeyState("mouse1") and not isSpinning and not clk then
			initCasino()
		end
		dxDrawImage(posX+dxElements["close"][1]*px,posY+dxElements["close"][2]*px,dxElements["close"][3],dxElements["close"][4],"files/casino/closeA.png")
	else
		dxDrawImage(posX+dxElements["close"][1]*px,posY+dxElements["close"][2]*px,dxElements["close"][3],dxElements["close"][4],"files/casino/close.png")
	end

	if isCursorOverRectangle(posX+dxElements["spinA"][1]*px,posY+dxElements["spinA"][2]*px,dxElements["spinA"][3]*px,dxElements["spinA"][4]*px) then
		if getKeyState("mouse1") and not isSpinning and not clk then
			triggerServerEvent("spinRequest",localPlayer)
		end
		dxDrawImage(posX+dxElements["spinA"][1]*px,posY+dxElements["spinA"][2]*px,dxElements["spinA"][3]*px,dxElements["spinA"][4]*px,"files/casino/spinA.png")
	else
		dxDrawImage(posX+dxElements["spin"][1]*px,posY+dxElements["spin"][2]*px,dxElements["spin"][3]*px,dxElements["spin"][4]*px,"files/casino/spin.png")
	end

	local sx,sy = -boxSize*1.2,(dxElements["slots"][4]*px-boxSize)/2
	dxSetRenderTarget(rt,true)
		for k,v in pairs(generatedSlots) do
			sx = sx + boxSize*1.2
			dxDrawImage(sx+bias,sy,boxSize,boxSize,"files/casino/"..slotContent[v][1])
			--[[if k == #slotContent*5+slot then
				dxDrawRectangle(sx+bias,sy,boxSize,boxSize,tocolor(150,50,50,200))
			end]]
			dxDrawRectangle(dxElements["slots"][3]*px/2,0,2,dxElements["slots"][4]*px,tocolor(150,50,50))
		end
	dxSetRenderTarget()

	dxDrawImage(posX+dxElements["slots"][1]*px,posY+dxElements["slots"][2]*px,dxElements["slots"][3]*px,dxElements["slots"][4]*px,rt)

	if getKeyState("mouse1") then
		clk = true
	else
		clk = false
	end
end

function generateSlots(seed)
	local int = #slotContent
	local blocks = 7
	generatedSlots = {}
	for i=1,blocks do
		for i=1,int do
			local rand = math.random(0,1000)/10
			local item = math.random(1,#slotContent)
			for k,v in ipairs(slotContent) do
				local visChance = v[4]*3
				if visChance >= 50 then visChance = 50 end
				--outputChatBox(k.." - "..visChance)
				if math.percentChance(visChance,1) > 0 then
					item = k
					break
				end
			end
			table.insert(generatedSlots,item)
		end
	end

	generatedSlots[int*5+seed] = slot
end
generateSlots(1)

function initCasino()
	if casinoShown then
		speed = 0
		isSpinning = false
		showCursor(false)
		removeEventHandler("onClientRender",root,draw)
		setElementFrozen(localPlayer,false)
	else
		showCursor(true)
		addEventHandler("onClientRender",root,draw)
		setElementFrozen(localPlayer,true)
	end
	casinoShown = not casinoShown
end
addEvent("initCasino",true)
addEventHandler("initCasino",root,initCasino)
--initCasino()

function spin()
	isSpinning = true
	bias = 0
	local item = 1
	local rand = math.random(0,10000)/100
	for k,v in ipairs(slotContent) do
		local hit = math.percentChance (v[4],1)
		if hit and hit > 0 then 
			item = k
			break
		end
	end
	slot = item
	generateSlots(slot)
	point = -(boxSize*1.2*slot+(boxSize*1.2)*#slotContent*5-dxElements["slots"][3]*px/2)+boxSize*1.2/2+math.random(-30*px,30*px) -- -((boxSize+2)*slot+boxSize*#slotContent*6-dxElements["slots"][3]*px/2)--+math.random(-30*px,30*px)
	speed = (30 + 0.2*#slotContent)*px
	speed0 = (30 + 0.2*#slotContent)*px
	addEventHandler("onClientRender",root,spinning)
	--outputChatBox(point.." "..slot.." "..rand)
end
addEvent("casinoSpin",true)
addEventHandler("casinoSpin",root,spin)

addCommandHandler("ctest",function()
	local results = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
	for i=1,1000 do
		local item = 1
		local rand = math.random(0,10000)/100
		for k,v in ipairs(slotContent) do
			local hit = math.percentChance (v[4],1)
			if hit and hit > 0 then 
				item = k
				break
			end
		end
		results[item] = results[item] + 1
	end
	for k,v in pairs(results) do
		if slotContent[k] then
			outputConsole(slotContent[k][1].." - "..v)
		end
	end
end)

function spinning()
	bias = bias - speed * 30/fps
	if speed >= 5 then
		speed = speed0 * (1.1-(bias/point)*1)
	end
	if bias <= point then
		speed = 0.25
		giveReward()
	end
end

function giveReward()
	speed = 0
	removeEventHandler("onClientRender",root,spinning)
	isSpinning = false
	triggerServerEvent( "receiveReward", localPlayer, slot )
end