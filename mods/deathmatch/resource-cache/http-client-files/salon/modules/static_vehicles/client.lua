function drawDemoVehicles()
	local x,y,z = getElementPosition(localPlayer)
	for k,v in pairs(demoVehiclesTable) do
		if getDistanceBetweenPoints3D(x,y,z,v[2][1],v[2][2],v[2][3]) <= 10 then
			local sx,sy = getScreenFromWorldPosition( v[2][1],v[2][2],v[2][3]+1.3 )
			if sx and sy then
				local data = getVehicleData(v[1])
				dxDrawText(data[2].."\n"..convertNumber(data[3]),sx+1,sy+1,sx,sy,tocolor(0,0,0),1.5,"default-bold","center","center",false,false,false,true)
				dxDrawText(data[2].."\n"..convertNumber(data[3]),sx,sy,sx,sy,tocolor(255,255,255),1.5,"default-bold","center","center")
			end
		end
	end
end
addEventHandler("onClientRender",root,drawDemoVehicles)