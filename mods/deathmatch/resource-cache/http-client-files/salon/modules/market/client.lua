
if marketType == 1 then

local markerWindow = nil

function initMarkerWindow()
	if isElement(markerWindow) then
		showCursor(false)
		destroyElement(markerWindow)
	else
		local veh = getPedOccupiedVehicle( localPlayer )
		local mdl = getElementModel(veh)
		local date = 1
		local withdraw = 100
		setElementFrozen ( veh, false )
		local vehname = getVehicleData(getElementModel(veh))[2] or "Vehicle"
		showCursor(true)
		markerWindow = guiCreateWindow(0.3,0.3,0.3,0.3,"Продажа автомобиля",true)
		guiWindowSetSizable( markerWindow, false )
		local label1 = guiCreateLabel( 0.1,0.1,0.8,0.2,"Выставление на продажу автомобиля \n"..vehname,true,markerWindow )
		guiLabelSetColor(label1,200,200,0)
		guiLabelSetHorizontalAlign( label1,"center" )
		local label2 = guiCreateLabel( 0.1,0.3,0.8,0.2,"Укажите срок аренды парковочного места.",true,markerWindow )
		guiLabelSetColor(label2,100,200,0)
		--guiLabelSetHorizontalAlign( label2,"center" )
		local date1 = guiCreateRadioButton(0.1,0.4,0.6,0.05,"1 День - $100",true,markerWindow)
		local date2 = guiCreateRadioButton(0.1,0.45,0.6,0.05,"3 Дня - $250",true,markerWindow)
		local date3 = guiCreateRadioButton(0.1,0.5,0.6,0.05,"7 Дней - $800",true,markerWindow)
		guiRadioButtonSetSelected( date1, true )
		local edit = guiCreateEdit(0.2,0.65,0.6,0.1,"Укажите цену",true, markerWindow)

		local btnCancel = guiCreateButton(0.05,0.85,0.3,0.12,"Отмена",true, markerWindow)
		local btnAccept = guiCreateButton(0.6,0.85,0.3,0.12,"Подтвердить",true, markerWindow)
		setElementFrozen ( veh, true )
		addEventHandler("onClientGUIClick",markerWindow,function()
			if source == btnCancel then
				initMarkerWindow()
				setElementFrozen ( veh, false )
			elseif source == edit then
				if guiGetText(edit) == "Укажите цену" then guiSetText(edit,"") end
			elseif source == btnAccept then
				local price = tonumber(guiGetText(edit))
				if price then
					if price > 0 and price <= getVehicleData(getElementModel(veh))[3]*marketHigherLimit then
						if date then
							triggerServerEvent( "placeVehicleOnMarket", localPlayer, veh, price, date, withdraw)
							initMarkerWindow()
							setElementFrozen ( veh, false )
						else
							outputChatBox("Вы не указали сроки продаж!")
						end
					else
						outputChatBox("Некорректная стоимость, максимальная цена для этой машины - "..math.floor(getVehicleData(getElementModel(veh))[3]*marketHigherLimit).."!")
					end
				else
					outputChatBox("Некорректная стоимость!",200,0,0)
				end
			elseif source == date1 then
				date = 1
				withdraw = 100
			elseif source == date2 then
				date = 3
				withdraw = 250
			elseif source == date3 then
				date = 7
				withdraw = 800
			end
		end)
	end
end
addEvent("initMarkerWindow",true)
addEventHandler("initMarkerWindow",root,initMarkerWindow)
--initMarkerWindow()

local marketVehicleWindow = nil

function initMarketVehicleWindow(owner)
	if isElement(marketVehicleWindow) then
		showCursor(false)
		destroyElement( marketVehicleWindow )
	else
		local veh = getPedOccupiedVehicle( localPlayer )
		if veh then
			local vehname = getVehicleData(getElementModel(veh))[2]
			local price = getElementData(veh,"Price")
			if owner then price = 0 end
			showCursor(true)
			marketVehicleWindow = guiCreateWindow(0.35,0.4,0.3,0.15,"Покупка автомобиля",true)
			local label = guiCreateLabel(0,0.25,1,0.1,"Купить "..vehname.." за "..convertNumber(price).." ?",true,marketVehicleWindow)
			guiLabelSetHorizontalAlign( label, "center" )
			local btnCancel = guiCreateButton(0.1,0.6,0.3,0.3,"Отмена",true,marketVehicleWindow)
			local btnAccept = guiCreateButton(0.6,0.6,0.3,0.3,"Купить",true,marketVehicleWindow)
			addEventHandler("onClientGUIClick",marketVehicleWindow,function()
				if source == btnCancel then
					initMarketVehicleWindow()
					removePedFromVehicle(localPlayer)
				elseif source == btnAccept then
					triggerServerEvent( "buyVehicleFromMarket", localPlayer, veh )
					initMarketVehicleWindow()
				end
			end)
		end
	end
end
addEvent("initMarketVehicleWindow",true)
addEventHandler("initMarketVehicleWindow",root,initMarketVehicleWindow)
--initMarketVehicleWindow()

elseif marketType == 2 then

local markerWindow = nil

function initMarkerWindow()
	if isElement(markerWindow) then
		showCursor(false)
		destroyElement(markerWindow)
	else
		local veh = getPedOccupiedVehicle( localPlayer )
		local mdl = getElementModel(veh)
		local date = 1
		local withdraw = 100
		setElementFrozen ( veh, false )
		local vehname = getVehicleData(getElementModel(veh))[2] or "Vehicle"
		showCursor(true)
		markerWindow = guiCreateWindow(0.3,0.3,0.3,0.3,"Продажа автомобиля",true)
		guiWindowSetSizable( markerWindow, false )
		local label1 = guiCreateLabel( 0.1,0.1,0.8,0.2,"Выставление на продажу автомобиля \n"..vehname,true,markerWindow )
		guiLabelSetColor(label1,200,200,0)
		guiLabelSetHorizontalAlign( label1,"center" )
		local label2 = guiCreateLabel( 0.1,0.3,0.8,0.2,"Минимальная стоимость: "..convertNumber(math.floor(getVehicleData(getElementModel(veh))[3]*0.2)).."\nМаксимальная стоимость: "..convertNumber(math.floor(getVehicleData(getElementModel(veh))[3]*marketHigherLimit)),true,markerWindow )
		guiLabelSetColor(label2,100,200,0)
		--guiLabelSetHorizontalAlign( label2,"center" )
		--local date1 = guiCreateRadioButton(0.1,0.4,0.6,0.05,"1 День - $100",true,markerWindow)
		--local date2 = guiCreateRadioButton(0.1,0.45,0.6,0.05,"3 Дня - $250",true,markerWindow)
		--local date3 = guiCreateRadioButton(0.1,0.5,0.6,0.05,"7 Дней - $800",true,markerWindow)
		--guiRadioButtonSetSelected( date1, true )
		local edit = guiCreateEdit(0.2,0.65,0.6,0.1,"Укажите цену",true, markerWindow)

		local btnCancel = guiCreateButton(0.05,0.85,0.3,0.12,"Отмена",true, markerWindow)
		local btnAccept = guiCreateButton(0.6,0.85,0.3,0.12,"Подтвердить",true, markerWindow)
		setElementFrozen ( veh, true )
		addEventHandler("onClientGUIClick",markerWindow,function()
			if source == btnCancel then
				initMarkerWindow()
				setElementFrozen ( veh, false )
			elseif source == edit then
				if guiGetText(edit) == "Укажите цену" then guiSetText(edit,"") end
			elseif source == btnAccept then
				local price = tonumber(guiGetText(edit))
				if price then
					if price > math.floor(getVehicleData(getElementModel(veh))[3]*0.2) and price <= getVehicleData(getElementModel(veh))[3]*marketHigherLimit then
						if date then
							triggerServerEvent( "placeVehicleOnMarket", localPlayer, veh, price, date, withdraw)
							initMarkerWindow()
							setElementFrozen ( veh, false )
						else
							outputChatBox("Вы не указали сроки продаж!")
						end
					else
						outputChatBox("Некорректная стоимость, максимальная цена для этой машины - "..convertNumber(math.floor(getVehicleData(getElementModel(veh))[3]*marketHigherLimit)).."!",200,50,50)
					end
				else
					outputChatBox("Некорректная стоимость!",200,0,0)
				end
			end
		end)
	end
end
addEvent("initMarkerWindow",true)
addEventHandler("initMarkerWindow",root,initMarkerWindow)
--initMarkerWindow()

local marketVehicleWindow = nil

function initMarketVehicleWindow(owner)
	if isElement(marketVehicleWindow) then
		showCursor(false)
		destroyElement( marketVehicleWindow )
	else
		local veh = getPedOccupiedVehicle( localPlayer )
		if veh then
			local vehname = getVehicleData(getElementModel(veh))[2]
			local price = getElementData(veh,"Price")
			--if owner then price = 0 end
			showCursor(true)
			marketVehicleWindow = guiCreateWindow(0.35,0.4,0.3,0.15,"Покупка автомобиля",true)
			local label = guiCreateLabel(0,0.25,1,0.1,"Купить "..vehname.." за "..convertNumber(price).."?",true,marketVehicleWindow)
			guiLabelSetHorizontalAlign( label, "center" )
			local btnCancel = guiCreateButton(0.1,0.6,0.3,0.3,"Отмена",true,marketVehicleWindow)
			local btnAccept = guiCreateButton(0.6,0.6,0.3,0.3,"Купить",true,marketVehicleWindow)
			addEventHandler("onClientGUIClick",marketVehicleWindow,function()
				if source == btnCancel then
					initMarketVehicleWindow()
					removePedFromVehicle(localPlayer)
				elseif source == btnAccept then
					triggerServerEvent( "buyVehicleFromMarket", localPlayer, veh )
					initMarketVehicleWindow()
				end
			end)
		end
	end
end
addEvent("initMarketVehicleWindow",true)
addEventHandler("initMarketVehicleWindow",root,initMarketVehicleWindow)

end

function drawMarketVehicleInfo()
	for k,v in pairs(getElementsByType("vehicle")) do
		if getElementData(v,"owner") == "Market" then
			local x,y,z = getElementPosition(localPlayer)
			local x2,y2,z2 = getElementPosition(v)
			if getElementDimension(v) == getElementDimension(localPlayer) then
				if getDistanceBetweenPoints3D( x,y,z,x2,y2,z2 ) <= 10 then
					local sx,sy = getScreenFromWorldPosition( x2, y2, z+1 )
					if sx and sy then
						dxDrawText( getVehicleData(getElementModel(v))[2].."\n#00dd00"..convertNumber(getElementData(v,"Price")),sx,sy,sx,sy,tocolor(255,255,255),2,"default-bold","center","center",false,false,false,true)
					end
				end
			end
		end
	end
end
addEventHandler("onClientRender",root,drawMarketVehicleInfo)


local elevatorShown = false

function initElevatorWindow()
	if elevatorShown then
		showCursor(false)
		removeEventHandler("onClientRender",root,drawElevatorWindow)
	else
		addEventHandler("onClientRender",root,drawElevatorWindow)
		showCursor(true)
	end
	elevatorShown = not elevatorShown
end

local sizeX,sizeY = 500,500
local posX,posY = scx/2-sizeX/2,scy/2-sizeY/2

function drawElevatorWindow()
	dxDrawImage(posX,posY,sizeX,sizeY,"files/info.png")
	dxDrawText("Лифт",posX,posY,posX+sizeX,posY+80,GRAY,1,font1,"center","center")
	local sx,sy = posX+20,posY+100
	for i=0,35 do
		if isCursorOverRectangle(sx,sy,100,30) then
			if getKeyState("mouse1") then
				triggerServerEvent( "setPlayerDimension", localPlayer, i )
				initElevatorWindow()
			end
			dxDrawRectangle(sx,sy,100,30,tocolor(50,100,150))
		else
			dxDrawRectangle(sx,sy,100,30,GRAY)
		end
		dxDrawText(i,sx,sy,sx+100,sy+30,tocolor(0,0,0),1,font3,"center",'center')
		sx = sx + 120
		if sx >= posX+450 then
			sx = posX+20
			sy = sy+40
		end
	end
end

for i=0,35 do
	local mrk = createMarker(elevatorPos[1],elevatorPos[2],elevatorPos[3],"cylinder",2,50,150,150)
	setElementDimension(mrk,i)
	setElementData(mrk,"elevator",true,false)
end