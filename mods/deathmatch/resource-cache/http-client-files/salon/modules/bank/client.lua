﻿local sW,sH = guiGetScreenSize()
local directes = {}
directes.window = false
directes.put_money = false
directes.take_money = false
directes.transfer_money = false
directes.chars = "0"
directes.players = false

gridlist = guiCreateGridList((sW - 300)/2,(sH - 300)/2, 300, 300, false)
pColumn = guiGridListAddColumn(gridlist,"Игрок",0.9)
guiSetVisible(gridlist,false)

addEventHandler("onClientResourceStart",resourceRoot,
function()
	for i, bank in ipairs(Banks) do
		local marker = createMarker(bank[1],bank[2],bank[3] - 1,"cylinder",1.2,100,100,150,200)
		local blip = createBlipAttachedTo(marker,52)
		setBlipVisibleDistance( blip, 200 )
		setElementData(marker,"bank.marker",true,false)
	end
	
	addEventHandler("onClientGUIDoubleClick",gridlist,
	function()
		local row,column = guiGridListGetSelectedItem(gridlist)
		if row and column then
			local string_row = guiGridListGetItemText(gridlist,row,column) 
			if string_row == "-> НАЗАД" then
				guiSetVisible(gridlist,false)
				return true
			end
			local data_row = guiGridListGetItemData(gridlist,row,column)
			if isElement(data_row) then
				directes.players = data_row;
				guiSetVisible(gridlist,false)
			else
				outputChatBox("#ff0000Ошибка:#ffffff Этого игрока нет в сети!",255,255,255,true)
			end
		end
	end,false)
end)

addEventHandler("onClientMarkerHit",getRootElement(),
function(player)
	if player ~= localPlayer or not getElementData(source,"bank.marker") then
		return false
	end
	if isPedInVehicle(player) then
		outputChatBox("Выйдите из машины!",200,50,50)
		return false
	end
	directes.window = true
	showCursor(true)
end)

addEventHandler("onClientMarkerLeave",getRootElement(),
function(player)
	if player ~= localPlayer or not getElementData(source,"bank.marker") then
		return false
	end
	directes.window = false
	showCursor(false)
	guiSetVisible(gridlist,false)
end)

addEventHandler("onClientRender",getRootElement(),
function()
	if directes.window then
		dxDrawRectangle(sW/2-200,sH/2-120,400,350,tocolor(0,0,0,240))
		dxDrawRectangle(sW/2-200,sH/2-120,400,60,tocolor(gColor[1],gColor[2],gColor[3],200))
		--dxDrawImage(0,0,sW,sH,"image.png")

		dxDrawText("в банке "..convertNumber((getElementData(localPlayer,"bank.money") or 0)),(sW - 249) / 2,(sH - 200)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 50,tocolor(255,255,255,255),2,"default-bold","center","top",false,false,true,false,false)
		local put = isMouseInPosition((sW - 250) / 2,(sH - 50)/2,250,50)
		dxDrawRectangle((sW - 250) / 2,(sH - 50)/2,250,50,put and tocolor(65,65,65,200) or tocolor(65,65,65,150))
		dxDrawText("Внести наличные",(sW - 249) / 2,(sH - 51)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Внести наличные",(sW - 250) / 2,(sH - 50)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
		
		local take = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 60,250,50)
		dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 60,250,50,take and tocolor(65,65,65,200) or tocolor(65,65,65,150))
		dxDrawText("Снять наличные",(sW - 249) / 2,((sH - 51)/2) + 60,((sW - 250) / 2) + 250,((sH - 50)/2) + 60 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Снять наличные",(sW - 250) / 2,((sH - 50)/2) + 60,((sW - 250) / 2) + 250,((sH - 50)/2) + 60 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
		
		local transfer = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 120,250,50)
		dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 120,250,50,transfer and tocolor(65,65,65,200) or tocolor(65,65,65,150))
		dxDrawText("Перевести игроку",(sW - 249) / 2,((sH - 51)/2) + 120,((sW - 250) / 2) + 250,((sH - 50)/2) + 120 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Перевести игроку",(sW - 250) / 2,((sH - 50)/2) + 120,((sW - 250) / 2) + 250,((sH - 50)/2) + 120 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
		
		local close = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 180,250,50)
		dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 180,250,50,close and tocolor(65,65,65,200) or tocolor(65,65,65,150))
		dxDrawText("Закрыть",(sW - 249) / 2,((sH - 51)/2) + 180,((sW - 250) / 2) + 250,((sH - 50)/2) + 180 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Закрыть",(sW - 250) / 2,((sH - 50)/2) + 180,((sW - 250) / 2) + 250,((sH - 50)/2) + 180 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
	elseif directes.put_money then
		dxDrawRectangle(sW/2-200,sH/2-120,400,350,tocolor(0,0,0,240))
		--dxDrawRectangle(sW/2-200,sH/2-120,400,60,tocolor(40,40,80,200))
		--dxDrawImage(0,0,sW,sH,"image.png")
		-- окно внесение наличных
		dxDrawRectangle((sW - 250) / 2,(sH - 50)/2,250,30,tocolor(gColor[1],gColor[2],gColor[3],150))
		dxDrawText(directes.chars,(sW - 249) / 2,(sH - 50)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 30,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText(directes.chars,(sW - 250) / 2,(sH - 50)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 30,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
		
		dxDrawText("Введите сумму (на руках "..convertNumber(getPlayerMoney()).."):",(sW - 249) / 2,(sH - 221)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 50,tocolor(0,0,0,255),1.2,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Введите сумму (на руках "..convertNumber(getPlayerMoney()).."):",(sW - 250) / 2,(sH - 220)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 50,tocolor(255,255,255,255),1.2,"default-bold","center","center",false,false,true,false,false)

		local transfer = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 40,250,50)
		dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 40,250,50,transfer and tocolor(65,65,65,200) or tocolor(65,65,65,150))
		dxDrawText("Внести",(sW - 249) / 2,((sH - 51)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Внести",(sW - 250) / 2,((sH - 50)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
		
		local close = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 100,250,50)
		dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 100,250,50,close and tocolor(65,65,65,200) or tocolor(65,65,65,150))
		dxDrawText("Назад",(sW - 249) / 2,((sH - 51)/2) + 100,((sW - 250) / 2) + 250,((sH - 50)/2) + 100 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Назад",(sW - 250) / 2,((sH - 50)/2) + 100,((sW - 250) / 2) + 250,((sH - 50)/2) + 100 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
	elseif directes.take_money then
		dxDrawRectangle(sW/2-200,sH/2-120,400,350,tocolor(0,0,0,240))
		--dxDrawRectangle(sW/2-200,sH/2-120,400,60,tocolor(40,40,80,200))
		--dxDrawImage(0,0,sW,sH,"image.png")
		-- окно снятия
		dxDrawRectangle((sW - 250) / 2,(sH - 50)/2,250,30,tocolor(gColor[1],gColor[2],gColor[3],150))
		dxDrawText(directes.chars,(sW - 249) / 2,(sH - 50)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 30,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText(directes.chars,(sW - 250) / 2,(sH - 50)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 30,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
		
		dxDrawText("Введите сумму (в банке "..convertNumber((getElementData(localPlayer,"bank.money") or 0)).."):",(sW - 249) / 2,(sH - 221)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 50,tocolor(0,0,0,255),1.2,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Введите сумму (в банке "..convertNumber((getElementData(localPlayer,"bank.money") or 0)).."):",(sW - 250) / 2,(sH - 220)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 50,tocolor(255,255,255,255),1.2,"default-bold","center","center",false,false,true,false,false)

		local take = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 40,250,50)
		dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 40,250,50,take and tocolor(65,65,65,200) or tocolor(65,65,65,150))
		dxDrawText("Снять",(sW - 249) / 2,((sH - 51)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Снять",(sW - 250) / 2,((sH - 50)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
		
		local close = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 100,250,50)
		dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 100,250,50,close and tocolor(65,65,65,200) or tocolor(65,65,65,150))
		dxDrawText("Назад",(sW - 249) / 2,((sH - 51)/2) + 100,((sW - 250) / 2) + 250,((sH - 50)/2) + 100 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
		dxDrawText("Назад",(sW - 250) / 2,((sH - 50)/2) + 100,((sW - 250) / 2) + 250,((sH - 50)/2) + 100 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
	elseif directes.transfer_money then
		--dxDrawImage(0,0,sW,sH,"image.png")
		-- окно перевода
		if not guiGetVisible(gridlist) then
			dxDrawRectangle(sW/2-200,sH/2-120,400,350,tocolor(0,0,0,240))
			dxDrawRectangle((sW - 250) / 2,(sH - 50)/2,250,30,tocolor(65,65,65,150))
			dxDrawText(directes.chars,(sW - 249) / 2,(sH - 50)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 30,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
			dxDrawText(directes.chars,(sW - 250) / 2,(sH - 50)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 30,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
			
			dxDrawText("Введите сумму (доступно "..convertNumber((getElementData(localPlayer,"bank.money") or 0)).."):",(sW - 249) / 2,(sH - 221)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 50,tocolor(0,0,0,255),1.2,"default-bold","center","center",false,false,true,false,false)
			dxDrawText("Введите сумму (доступно "..convertNumber((getElementData(localPlayer,"bank.money") or 0)).."):",(sW - 250) / 2,(sH - 220)/2,((sW - 250) / 2) + 250,((sH - 50)/2) + 50,tocolor(255,255,255,255),1.2,"default-bold","center","center",false,false,true,false,false)
			
			local player = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 40,250,50,tocolor(65,65,65,150))
			dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 40,250,50,player and tocolor(65,65,65,200) or tocolor(65,65,65,150))
			if directes.players and isElement(directes.players) then
				local name = string.gsub(getPlayerName(directes.players),"#%x%x%x%x%x%x","")
				dxDrawText(name,(sW - 249) / 2,((sH - 51)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
				dxDrawText(name,(sW - 250) / 2,((sH - 50)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
			else
				dxDrawText("Выбрать игрока",(sW - 249) / 2,((sH - 51)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
				dxDrawText("Выбрать игрока",(sW - 250) / 2,((sH - 50)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
			end
			local transfer = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 100,250,50,tocolor(65,65,65,150))
			dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 100,250,50,transfer and tocolor(65,65,65,200) or tocolor(65,65,65,150))
			dxDrawText("Перевести",(sW - 249) / 2,((sH - 51)/2) + 100,((sW - 250) / 2) + 250,((sH - 50)/2) + 100 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
			dxDrawText("Перевести",(sW - 250) / 2,((sH - 50)/2) + 100,((sW - 250) / 2) + 250,((sH - 50)/2) + 100 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
			
			local close = isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 160,250,50)
			dxDrawRectangle((sW - 250) / 2,((sH - 50)/2) + 160,250,50,close and tocolor(65,65,65,200) or tocolor(65,65,65,150))
			dxDrawText("Назад",(sW - 249) / 2,((sH - 51)/2) + 160,((sW - 250) / 2) + 250,((sH - 50)/2) + 160 + 50,tocolor(0,0,0,255),1.4,"default-bold","center","center",false,false,true,false,false)
			dxDrawText("Назад",(sW - 250) / 2,((sH - 50)/2) + 160,((sW - 250) / 2) + 250,((sH - 50)/2) + 160 + 50,tocolor(255,255,255,255),1.4,"default-bold","center","center",false,false,true,false,false)
		end
	end
end)

addEventHandler("onClientKey",root,
function(button,press)
	if directes.put_money or directes.take_money or directes.transfer_money then
		if button == "backspace" and press then
			if string.len(directes.chars) > 0 then
				directes.chars = string.sub(directes.chars,1,string.len(directes.chars) - 1)
			end
			if string.len(directes.chars) <= 0 then
				directes.chars = "0"
			end
			return true
		end
		for i = 0, 9 do
			if button == tostring(i) and press or button == "num_"..tostring(i) and press then
				if string.len(directes.chars) == 1 and directes.chars == "0" and button ~= 0 then
					directes.chars = tostring(i)
					return false
				end
				directes.chars = directes.chars..tostring(i)
				break
			end
		end
	end
	if (button == "mouse1" and press) then
		if directes.window then
			if isMouseInPosition((sW - 250) / 2,(sH - 50)/2,250,50) then -- put money
				directes.window = false
				directes.put_money = true
			elseif isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 60,250,50) then -- take money
				directes.take_money = true
				directes.window = false
			elseif isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 180,250,50) then -- закрытие окна (родителя)
				directes.window = false
				setTimer(showCursor,50,1,false)
			elseif isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 120,250,50) then -- transfer
				directes.window = false
				directes.transfer_money = true
			end
		elseif directes.put_money then
			if isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 100,250,50) then -- закрыть окно
				directes.window = true
				directes.put_money = false
			elseif isMouseInPosition((sW - 249) / 2,((sH - 51)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50) then -- положить деньги
				if string.len(directes.chars) > 0 then
					local input_money = tonumber(directes.chars)
					if input_money then
						triggerServerEvent("player:moneyPut",localPlayer,input_money)
						directes.chars = "0"
						directes.window = true
						directes.put_money = false
					end
				end
			end
		elseif directes.take_money then
			if isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 100,250,50) then -- закрыть окно
				directes.window = true
				directes.take_money = false
			elseif isMouseInPosition((sW - 249) / 2,((sH - 51)/2) + 40,((sW - 250) / 2) + 250,((sH - 50)/2) + 40 + 50) then -- положить деньги
				if string.len(directes.chars) > 0 then
					local input_money = tonumber(directes.chars)
					if input_money then
						triggerServerEvent("player:moneyTake",localPlayer,input_money)
						directes.chars = "0"
						directes.window = true
						directes.take_money = false
					end
				end
			end
		elseif directes.transfer_money and not guiGetVisible(gridlist) then
			if isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 160,250,50) then
				directes.window = true
				directes.transfer_money = false
				guiSetVisible(gridlist,false)
			elseif isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 40,250,50,tocolor(65,65,65,150)) then
				guiSetVisible(gridlist,true)
				local player_row = guiGridListGetSelectedItem(gridlist)
				local players = getElementsByType("player")
				guiGridListClear(gridlist)
				guiGridListSetItemText(gridlist,guiGridListAddRow(gridlist),pColumn,"-> НАЗАД",false,false)
				for i, player in ipairs(players) do
					if player ~= localPlayer then
						local row = guiGridListAddRow(gridlist)
						guiGridListSetItemText(gridlist,row,pColumn,string.gsub(getPlayerName(player),"#%x%x%x%x%x%x",""),false,false)
						guiGridListSetItemData(gridlist,row,pColumn,player)
					end
				end
				guiGridListSetSelectedItem(gridlist,player_row,1)
			elseif isMouseInPosition((sW - 250) / 2,((sH - 50)/2) + 100,250,50,tocolor(65,65,65,150)) then
				local input_money = tonumber(directes.chars)
				if input_money then
					if not directes.players then return false end
					if not isElement(directes.players) then 
						directes.players = false
						outputChatBox("#ff0000Ошибка:#ffffff Этого игрока нет в сети!",255,255,255,true)
						return false 
					end
					triggerServerEvent("player:moneyTransfer",localPlayer,input_money,directes.players,string.gsub(getPlayerName(directes.players),"#%x%x%x%x%x%x",""))
				end
			end
		end
	end
end)

function isMouseInPosition(x,y,width,height)
	if not isCursorShowing() then
		return false
	end
    local cx,cy = getCursorPosition()
    local cx,cy = (cx * sW),(cy * sH)
	if cx and cy then
		if (cx >= x and cx <= x + width) and (cy >= y and cy <= y + height) then
			return true
		else
			return false
		end
	end
end