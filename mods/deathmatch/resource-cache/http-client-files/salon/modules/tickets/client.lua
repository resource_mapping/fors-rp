local scx,scy = guiGetScreenSize()

local rt = dxCreateRenderTarget( 300,300,true )
local ss = dxCreateScreenSource( 300,300 )

local function isCursorOverRectangle(x,y,w,h)
	if isCursorShowing() then
		local mx,my = getCursorPosition() -- relative
		local cursorx,cursory = mx*scx,my*scy
		if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
			return true
		end
	end
return false
end

function getPositionFromElementOffset(element,offX,offY,offZ)
    local m = getElementMatrix ( element )  -- Get the matrix
    local x = offX * m[1][1] + offY * m[2][1] + offZ * m[3][1] + m[4][1]  -- Apply transform
    local y = offX * m[1][2] + offY * m[2][2] + offZ * m[3][2] + m[4][2]
    local z = offX * m[1][3] + offY * m[2][3] + offZ * m[3][3] + m[4][3]
    return x, y, z                               -- Return the transformed point
end

local alpha = 255
local speed = 120
local speedlimit = 60
local cash = 1000
local pixels = ""
local rand = 0
local number = "Не опознан"


function getSpeedCost(spd)
	for i=1,#speedCost-1 do
		if spd >= speedCost[i][1] and spd < speedCost[i+1][1] then
			return speedCost[i][2]
		end 
	end
	return 100
end

function getCurrentSpeed()
	local veh = getPedOccupiedVehicle( localPlayer )
	if veh then
		local speedx, speedy, speedz = getElementVelocity ( veh )
		local actualspeed = (speedx^2 + speedy^2 + speedz^2)^(0.5)
		local kmh = math.floor(actualspeed * 180)
		return kmh
	end
end

function takePhoto()
	local veh = getPedOccupiedVehicle( localPlayer )
	if veh then
		speed = getCurrentSpeed()
		if speed <= speedlimit then return end
		cash = getSpeedCost(speed-speedlimit)
		local x,y,z = getPositionFromElementOffset(veh,math.random(-4,4),8*getCurrentSpeed()/100,4)
		local vx,vy,vz = getElementPosition(veh)
		setCameraMatrix(x,y,z,vx,vy,vz)
		setTimer(dxUpdateScreenSource,50,1,ss)
		setTimer(setCameraTarget,200,1,localPlayer)
		setTimer(updateRenderTarget,100,1)
		alpha = 255
		addEventHandler("onClientRender",root,drawPenalty)
		setTimer(function()
			removeEventHandler("onClientRender",root,drawPenalty)
			triggerServerEvent("createTicket",localPlayer,localPlayer,speed,speedlimit,cash,rand)
			outputChatBox(pixels)
		end,5000,1)
		if getElementData(veh,"number:plate") then
			number = getElementData(veh,"number:plate")
		else
			number = "Не опознан"
		end
	end
end

function saveImage()
	rand = math.random(1,1000)
	local file = fileCreate( "files/photo"..rand..".png" )
	fileWrite(file, dxConvertPixels(pixels, 'png') )
	fileClose(file)
end

function updateRenderTarget()
	dxSetRenderTarget(rt,true)
		pixels = dxGetTexturePixels( ss )
		for x=0,299 do
			for y=0,299 do
				local r,g,b = dxGetPixelColor(pixels,x,y)
				dxSetPixelColor(pixels,x,y,(r+g+b)/3,(r+g+b)/3,(r+g+b)/3)
			end
		end
		local tex = dxCreateTexture(pixels)
		saveImage()
		--dxCreateTexture( string filepath [, string textureFormat = "argb", bool mipmaps = true, string textureEdge = "wrap" ] )element dxCreateTexture ( string pixels [, string textureFormat = "argb", bool mipmaps = true, string textureEdge = "wrap" ] )element dxCreateTexture ( int width, int height [, string textureFormat = "argb", string textureEdge = "wrap", string textureType = "2d", int depth = 1 ] )
		dxDrawImage(0,0,300,300,tex)
	dxSetRenderTarget()
end

--bindKey("x","down",takePhoto)

function drawPenalty()
	dxDrawRectangle(10,scy/2-200,310,400,tocolor(10,10,10,200))
	dxDrawText("Лимит скорости на этом участке: #dd2222"..speedlimit.."км/ч\n#ffffffВаша скорость: #dd2222"..speed.."км/ч\n#ffffffШтраф: #22dd22"..convertNumber(cash).."\n\n#ffffffНомер: #dd2222"..number,40,scy/2+105,320,scy/2+200,tocolor(255,255,255),1,"default-bold","left","center",false,false,false,true)
	dxDrawImage(15,scy/2-195,300,300,rt)
	if alpha >= 10 then
		alpha = alpha-2
		dxDrawRectangle(0,0,scx,scy,tocolor(255,255,255,alpha))
	end
end

local ticketData = {}

local tmShown = false

function initTicketManager(data)
	if tmShown then
		removeEventHandler("onClientRender",root,drawTicketManager)
		removeEventHandler("onClientKey", root, scrollHandler)
		showCursor(false)
	else
		ticketData = data
		addEventHandler("onClientRender",root,drawTicketManager)
		addEventHandler("onClientKey", root, scrollHandler)
		showCursor(true)
	end
	tmShown = not tmShown
end
addEvent("initTicketManager",true)
addEventHandler("initTicketManager",root,initTicketManager)

local sizeX,sizeY = 600,600
local posX,posY = scx/2-sizeX/2,scy/2-sizeY/2

local ticketRT = dxCreateRenderTarget(500,400,true)

local scroll = 0

local clk = false

function drawTicketManager()
	dxDrawRectangle(posX,posY,sizeX,sizeY,tocolor(20,20,20,230))
	dxDrawText("Ваши штрафы",posX,posY,posX+sizeX,posY+50,tocolor(255,255,255),2,"default-bold","center","center")
	dxSetRenderTarget( ticketRT, true )
		local sx,sy = 0,0
		for k,v in pairs(ticketData) do
			dxDrawRectangle(sx,sy+scroll,500,100,tocolor(200,200,200))
			dxDrawImage(sx,sy+scroll,100,100,"files/photo"..v["Photo"]..".png")
			dxDrawText("Зафиксированная скорость: "..v["Speed"].."км/ч",sx+120,sy+20+scroll,sx,sy,tocolor(10,10,10),1.2,"default-bold")
			dxDrawText("Ограничение на участке: "..v["SpeedLimit"].."км/ч",sx+120,sy+40+scroll,sx,sy,tocolor(10,10,10),1.2,"default-bold")
			dxDrawText("Сумма штрафа: "..convertNumber(v["Cash"]),sx+120,sy+60+scroll,sx,sy,tocolor(150,50,50),1.2,"default-bold")

			if isCursorOverRectangle(posX+50+sx+440,posY+50+sy+25+scroll,50,50) then
				if getKeyState("mouse1") and not clk then
					if getPlayerMoney() >= v["Cash"] then
						triggerServerEvent("payTicket",localPlayer,v["ID"],v["Cash"],v["Photo"])
						ticketData[k] = nil
					else
						outputChatBox("Недостаточно денег",200,50,50)
					end
				end
				dxDrawRectangle(sx+440,sy+25+scroll,50,50,tocolor(200,50,50))
			else
				dxDrawRectangle(sx+440,sy+25+scroll,50,50,tocolor(150,50,50))
			end
			dxDrawText("$",sx+440,sy+25+scroll,sx+490,sy+75+scroll,tocolor(255,255,255),2,"default-bold","center","center")
			sy = sy + 110
		end
	dxSetRenderTarget()
	dxDrawRectangle(posX,posY+48,sizeX,404,tocolor(30,20,20,230))
	dxDrawImage(posX+50,posY+50,500,400,ticketRT)

	dxDrawText("Всего штрафов: "..#ticketData,posX+50,posY+470,posX+50,posY+470,tocolor(255,255,255),1,"default-bold")
	local total = 0
	for k,v in pairs(ticketData) do
		total = total+v["Cash"]
	end
	dxDrawText("Общая сумма штрафов: "..convertNumber(total),posX+50,posY+490,posX+50,posY+470,tocolor(255,255,255),1,"default-bold")
	if isCursorOverRectangle(posX+50,posY+530,200,50) then
		if getKeyState("mouse1") then
			initTicketManager()
		end
		dxDrawRectangle(posX+50,posY+530,200,50,tocolor(150,50,50))
	else
		dxDrawRectangle(posX+50,posY+530,200,50,tocolor(100,50,50))
	end
	dxDrawText("Выйти",posX+50,posY+530,posX+250,posY+580,tocolor(255,255,255),1,"default-bold","center","center")

	if isCursorOverRectangle(posX+320,posY+530,200,50) then
		if getKeyState("mouse1") then
			for k,v in pairs(ticketData) do
				triggerServerEvent("payTicket",localPlayer,v["ID"],v["Cash"],v["Photo"])
			end
			initTicketManager()
		end
		dxDrawRectangle(posX+320,posY+530,230,50,tocolor(50,150,50))
	else
		dxDrawRectangle(posX+320,posY+530,230,50,tocolor(50,100,50))
	end
	dxDrawText("Оплатить все штрафы\n("..convertNumber(total)..")",posX+320,posY+530,posX+570,posY+580,tocolor(255,255,255),1,"default-bold","center","center")

	if getKeyState("mouse1") then
		clk = true
	else
		clk = false
	end
end

--initTicketManager()

function scrollHandler(key,prs)
	if prs then
		if key == "mouse_wheel_up" then
			scroll = scroll + 30
			if scroll >= 0 then
				scroll = 0
			end
		elseif key == "mouse_wheel_down" then
			local scrollMax = #ticketData*110-400
			scroll = scroll - 30
			if scroll <= -scrollMax then
				scroll = -scrollMax
			end
		end
	end
	cancelEvent()
end


function ticketPaid(photo)
	fileDelete( "files/photo"..photo..".png" )
end
addEvent("ticketPaid",true)
addEventHandler("ticketPaid",root,ticketPaid)

for k,v in pairs(radars) do
	local col = createColSphere(v[1],v[2],v[3],v[4])
	setElementData(col,"speedlimit",v[5])
	if camData[1] then
		local blp = createBlipAttachedTo(col,camData[2])
		setBlipVisibleDistance( blp, camData[3] )
	end
end

setDevelopmentMode( true )

addEventHandler("onClientColShapeHit",getRootElement(),function(element)
	local lim = getElementData(source,"speedlimit")
	if not lim then return end
	if element == localPlayer then
		local veh = getPedOccupiedVehicle( localPlayer )
		if veh then
			if getVehicleController(veh) == localPlayer then
				speedlimit = lim
				takePhoto()
			end
		end
	end
end)