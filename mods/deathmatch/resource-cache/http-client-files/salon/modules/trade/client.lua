function barterMarkerWindow()
	local window = guiCreateWindow(0.4,0.3,0.2,0.3,"Предложить обмен",true)
	local list = guiCreateGridList(0.1,0.1,0.8,0.7,true,window)
	showCursor(true)
	guiGridListAddColumn( list, "Игроки",0.9 )
	for k,v in pairs(getElementsByType("player")) do
		if localPlayer ~= v then
			local x,y,z = getElementPosition(localPlayer)
			local x2,y2,z2 = getElementPosition(v)
			if getDistanceBetweenPoints3D( x,y,z,x2,y2,z2 ) <= 15 then
				local row = guiGridListAddRow(list)
				guiGridListSetItemText( list,row,1,getPlayerName(v),false,false )
				guiGridListSetItemData( list,row,1,v)
			end
		end
	end
	local btn1 = guiCreateButton(0.1,0.85,0.3,0.1,"Отмена",true,window)
	local btn2 = guiCreateButton(0.6,0.85,0.3,0.1,"Предложить",true,window)
	guiSetEnabled(btn2,false)
	addEventHandler("onClientGUIClick",window,function()
		if source == list then
			local itemI = guiGridListGetSelectedItem( list )
			if itemI and itemI >= 0 then
				guiSetEnabled(btn2,true)
			else
				guiSetEnabled(btn2,false)
			end
		elseif source == btn1 then
			destroyElement(window)
			showCursor(false)
		elseif source == btn2 then
			triggerServerEvent( "sendBarterOffer", localPlayer, guiGridListGetItemData(list,guiGridListGetSelectedItem(list)) )
			destroyElement(window)
			showCursor(false)
		end
	end)
end
--barterMarkerWindow()

local cached = false

function cacheBarterOffer()
	if cached then return end
	showCursor(true)
	local ply = source
	local window = guiCreateWindow(0.3,0.4,0.3,0.2,"Предложение обмена",true)
	cached = true
	local label = guiCreateLabel(0.1,0.4,0.8,0.1,getPlayerName(source).." предлагает вам обмен.",true,window)
	guiLabelSetHorizontalAlign( label,"center" )
	local btn1 = guiCreateButton(0.1,0.75,0.3,0.15,"Отмена",true,window)
	local btn2 = guiCreateButton(0.6,0.75,0.3,0.15,"Подтвердить",true,window)
	addEventHandler("onClientGUIClick",window,function()
		if source == btn1 then
			destroyElement(window)
			showCursor(false)
			triggerServerEvent( "abortBarter", localPlayer, false, ply )
			cached = false
		elseif source == btn2 then
			triggerServerEvent( "offerAccepted", localPlayer, ply )
			destroyElement(window)
			showCursor(false)
			cached = false
		end
	end)
end
addEvent("cacheBarterOffer",true)
addEventHandler("cacheBarterOffer",root,cacheBarterOffer)

local buttonTrade = nil
local bReady = {}
local bTrade = {}
local items1 = {} -- MY
local items2 = {} -- OFFERED
local commision = 0
local id = 0
local clk = false
local otherPlayer = nil
local counterN = 0

local selectorItems = {}

local selectorShown = false
local onlyNumbers = false
local offeredNumber = ""

local barterWindowShown = false

local sizeX,sizeY = 700,600
local posX,posY = scx/2-sizeX/2,scy/2-sizeY/2

local rt1 = dxCreateRenderTarget(450,200,true)
local rt2 = dxCreateRenderTarget(450,200,true)
local rt3 = dxCreateRenderTarget(420,310,true)

local counterShown = false

local selectorMax = 0

local selectorScroll = 40

function initBarterWindow(n)
	if barterWindowShown then
		removeEventHandler("onClientRender",root,drawBarterWindow)
		showCursor(false)
		setElementData(localPlayer,"barterWindowShown",false)
		removeEventHandler("onClientKey", root, keyHandler)
	else
		setElementData(localPlayer,"barterWindowShown",true)
		local addOpened = false
		showCursor(true)
		addEventHandler("onClientRender",root,drawBarterWindow)
		id = n
		otherPlayer = source
		selectorShown = false
		onlyNumbers = false
		offeredNumber = ""
		items1 = {} -- MY
		items2 = {} -- OFFERED
		bReady = {[localPlayer] = false, [otherPlayer] = false}
		bTrade = {[localPlayer] = false, [otherPlayer] = false}
		addEventHandler("onClientKey", root, keyHandler)
	end
	barterWindowShown = not barterWindowShown
end
addEvent("initBarterWindow",true)
addEventHandler("initBarterWindow",root,initBarterWindow)
--initBarterWindow()

local buttons = {"Добавить","Готов","Обмен"}

function showItemSelector(items)
	if not selectorShown then
		selectorShown = true
		if onlyNumbers then
			for k,v in pairs(items) do
				if v[1] ~= "number" then
					items[k] = nil
				end
			end
		end
		selectorItems = items
	end
end
addEvent("showItemSelector",true)
addEventHandler("showItemSelector",root,showItemSelector)

function drawBarterWindow()
	showCursor(true)
	dxDrawImage(posX,posY,sizeX,sizeY,"files/info.png")
	dxDrawText("Обмен с "..getPlayerName(otherPlayer),posX,posY,posX+sizeX,posY+100,GRAY,1,font1,"center","center")


	if isCursorOverRectangle(posX+sizeX-70,posY+30,32,32) and not selectorShown and not counterShown then
		if getKeyState("mouse1") and not clk then
			triggerServerEvent( "abortBarter", localPlayer, id, otherPlayer, true )
			initBarterWindow()
		end
		dxDrawImage(posX+sizeX-70,posY+30,32,32,"files/closeH.png")
	else
		dxDrawImage(posX+sizeX-70,posY+30,32,32,"files/close.png")
	end
	
	local comission = 0

	dxSetRenderTarget(rt1,true)
		dxDrawRectangle(0,0,500,200,GRAY)
		local sx,sy = 0,1
		for k,v in pairs(items1) do
			if isCursorOverRectangle(posX+50,posY+110+sy,450,30) and not selectorShown and not counterShown then
				if getKeyState("mouse1") and not clk then
					triggerServerEvent( "addItem", localPlayer, id, v, true, k )
				end
				dxDrawRectangle(sx,sy,450,30,tocolor(100,100,150))
			else
				dxDrawRectangle(sx,sy,450,30,tocolor(100,100,120))
			end

			dxDrawText(v[3],sx+50,sy,sx,sy+30,GRAY,1,font5,"left","center")

			if v[1] == "vehicle" then
				dxDrawImage(sx+10,sy,30,30,"files/icon_car.png")
			elseif v[1] == "house" then
				dxDrawImage(sx+10,sy,30,30,"files/icon_house.png")
			elseif v[1] == "money" then
				dxDrawImage(sx+10,sy,30,30,"files/icon_cash.png")
			elseif v[1] == "number" then
				dxDrawImage(sx+10,sy+4,30,22,"files/icon_number.png")
			end
			comission = comission + v[4]
			sy = sy+31
		end
	dxSetRenderTarget()
	dxDrawImage(posX+50,posY+110,450,200,rt1)
	
	if tradeCommisions[1] then
		dxDrawText("Коммиссия: $"..comission,posX+510,posY+250,posX+670,posY+250,GRAY,1,font5,"center","center")
	end

	dxSetRenderTarget(rt2,true)
		dxDrawRectangle(0,0,500,200,GRAY)
		local sx,sy = 0,0
		for k,v in pairs(items2) do
			if isCursorOverRectangle(posX+50,posY+350+sy,450,30) and not selectorShown and not counterShown then
				dxDrawRectangle(sx,sy,450,30,tocolor(100,100,150))
			else
				dxDrawRectangle(sx,sy,450,30,tocolor(100,100,120))
			end
			dxDrawText(v[3],sx+50,sy,sx,sy+30,GRAY,1,font5,"left","center")
			if v[1] == "vehicle" then
				dxDrawImage(sx+10,sy,30,30,"files/icon_car.png")
			elseif v[1] == "house" then
				dxDrawImage(sx+10,sy,30,30,"files/icon_house.png")
			elseif v[1] == "money" then
				dxDrawImage(sx+10,sy,30,30,"files/icon_cash.png")
			elseif v[1] == "number" then
				dxDrawImage(sx+10,sy+4,30,22,"files/icon_number.png")
			end
			sy = sy+31
		end
	dxSetRenderTarget()
	dxDrawImage(posX+50,posY+350,450,200,rt2)

	local sx,sy = posX+510,posY+150
	for k,v in pairs(buttons) do
		if isCursorOverRectangle(sx,sy,160,40) then
			if getKeyState("mouse1") and not clk and not selectorShown and not counterShown then
				if k == 1 then
					triggerServerEvent("initItemSelector",localPlayer,id)
					onlyNumbers = false
				elseif k == 2 then
					triggerServerEvent("updateBarterStatus",localPlayer,id,true,false)
				elseif k == 3 then
					if bReady[localPlayer] and bReady[otherPlayer] then
						triggerServerEvent("updateBarterStatus",localPlayer,id,false,true)
					end
				end
			end
			dxDrawRectangle(sx,sy,160,40,tocolor(150,150,250))
		else
			dxDrawRectangle(sx,sy,160,40,tocolor(150,150,200))
		end
		dxDrawText(v,sx,sy,sx+160,sy+40,GRAY,1,font3,"center","center")
		sy=sy+50
		if k == 1 then
			sy = sy + 150
		end
	end
	
	sx = sx + 30

	if bReady[localPlayer] then
		dxDrawImage(sx,sy,40,40,"files/ok.png")
	else
		dxDrawImage(sx,sy,40,40,"files/notok.png")
	end

	if bReady[otherPlayer] then
		dxDrawImage(sx+60,sy,40,40,"files/ok.png")
	else
		dxDrawImage(sx+60,sy,40,40,"files/notok.png")
	end

	sy = sy + 60

	if bTrade[localPlayer] then
		dxDrawImage(sx,sy,40,40,"files/ok.png")
	else
		dxDrawImage(sx,sy,40,40,"files/notok.png")
	end

	if bTrade[otherPlayer] then
		dxDrawImage(sx+60,sy,40,40,"files/ok.png")
	else
		dxDrawImage(sx+60,sy,40,40,"files/notok.png")
	end

	if selectorShown then
		dxSetRenderTarget(rt3,true)
		dxDrawRectangle(0,0,420,310,tocolor(50,50,50))
			local rsx,rsy = 10,5+selectorScroll
			--dxDrawRectangle(rsx,rsy,400,(#selectorItems*31)+50,tocolor(50,50,50))
			selectorMax = -(#selectorItems*31-255)
			local sx,sy = rsx,rsy+51
			for k,v in pairs(selectorItems) do
				if isCursorOverRectangle(scx/2-200+sx,scy/2-150+sy,400,30) and isCursorOverRectangle(scx/2-200,scy/2-100,400,260)  then
					if getKeyState("mouse1") and not clk then
						if onlyNumbers then
							selectorShown = false
							onlyNumbers = false
							local ritem = {"numbers",{offeredNumber,v}}
							triggerServerEvent( "addItem",localPlayer, id, ritem, false )
						else
							if v[1] == "money" then
								selectorShown = false
								counterShown = true
							else
								selectorShown = false
								triggerServerEvent( "addItem",localPlayer, id, v, false )
							end
						end
					end
					dxDrawRectangle(sx,sy,400,30,tocolor(100,100,180))
				else
					dxDrawRectangle(sx,sy,400,30,tocolor(100,100,120))
				end
					if v[1] == "vehicle" then
						dxDrawImage(sx+10,sy,30,30,"files/icon_car.png")
					elseif v[1] == "house" then
						dxDrawImage(sx+10,sy,30,30,"files/icon_house.png")
					elseif v[1] == "money" then
						dxDrawImage(sx+10,sy,30,30,"files/icon_cash.png")
					elseif v[1] == "number" then
						dxDrawImage(sx+10,sy+4,30,22,"files/icon_number.png")
					end
					dxDrawText(v[3],sx,sy,sx+400,sy+30,GRAY,1,font5,"center","center")
					sy=sy+31
			end
			if onlyNumbers then
				dxDrawRectangle(0,0,420,50,tocolor(50,50,50))
				dxDrawText("Вам предлагают: ".. offeredNumber[2][2] .."\nВыберите номер",0,0,420,50,GRAY,1,font1,"center",'center')
			else
				dxDrawRectangle(0,0,420,50,tocolor(50,50,50))
				dxDrawText("Выберите лот",0,0,420,50,GRAY,1,font1,"center",'center')
			end
			--dxDrawText(selectorMax.." "..selectorScroll,10,10)
		dxSetRenderTarget()
		dxDrawImage(scx/2-200,scy/2-150,400,310,rt3)
	end

	if counterShown then
		dxDrawRectangle(scx/2-100,scy/2-75,200,150,tocolor(50,50,50))
		dxDrawText("Сумма",scx/2,scy/2-50,scx/2,scy/2-50,GRAY,1,font1,"center","center")
		dxDrawRectangle(scx/2-80,scy/2-10,160,30,tocolor(200,200,200))
		dxDrawText(counterN,scx/2-80,scy/2-10,scx/2+80,scy/2+20,tocolor(10,10,10),1,font1,"center","center")
		if isCursorOverRectangle(scx/2-50,scy/2+30,100,30) then
			if getKeyState("mouse1") and not clk then
				counterShown = false
				triggerServerEvent( "addItem",localPlayer, id, {"money",counterN,false})
			end
			dxDrawRectangle(scx/2-50,scy/2+30,100,30,tocolor(100,100,160))
		else
			dxDrawRectangle(scx/2-50,scy/2+30,100,30,tocolor(100,100,120))
		end
		dxDrawText("OK",scx/2-50,scy/2+30,scx/2+50,scy/2+60,tocolor(255,255,255),1,font1,"center","center")
	end

	if getKeyState('mouse1') then
		clk = true
	else
		clk = false
	end
end
--addEventHandler("onClientRender",root,drawBarterWindow)

function keyHandler(key,press)
	if press then
		if counterShown then
			if tonumber(key) then
				local str = tostring(counterN)
				if tonumber(str..key) >= 0 then
					counterN = tonumber(str..key)
				else
					counterN = 0
				end
			end
			if key == "backspace" then
				local str = tostring(counterN)
				counterN = tonumber(string.sub(str,1,#str-1)) or 0
			end
		end
		if selectorShown then
			if key == "mouse_wheel_up" then
				selectorScroll = selectorScroll + 25
			elseif key == "mouse_wheel_down" then
				selectorScroll = selectorScroll - 25
			end
			if selectorScroll <= selectorMax then
				if selectorMax < 0 then
					selectorScroll = selectorMax
				else
					selectorScroll = 0
				end
			end
			if selectorScroll > 0 then
				selectorScroll = 0
			end
		end
	end
	cancelEvent()
end
--addEventHandler("onClientKey", root, keyHandler)

function updateBarterStatus(items,ready,trade)
	if items then
		items1 = items[localPlayer]
		items2 = items[otherPlayer]
	end

	if ready then
		bReady = ready
	end

	if trade then
		bTrade = trade
	end
end
addEvent("updateBarterStatus",true)
addEventHandler("updateBarterStatus",root,updateBarterStatus)

function cacheNumbersOffer(num)
	offeredNumber = num
	selectorShown = false
	onlyNumbers = true
	triggerServerEvent("initItemSelector",localPlayer,id)
	--selectorShown = true
end
addEvent("cacheNumbersOffer",true)
addEventHandler("cacheNumbersOffer",root,cacheNumbersOffer)