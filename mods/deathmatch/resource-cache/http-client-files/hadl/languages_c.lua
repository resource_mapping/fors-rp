﻿--[[
    -------------
    Russian ingame vehicle handling editor, for MTASA v1.2 and higher
    ---------

    Updates, bugs, new features:
        http://multi-theft-auto-ru.googlecode.com/

    Developers of current file:
        MX_Master ( http://multi-theft-auto.ru/ )

    Variables names legend:
        b   boolean
        n   number
        s   string
        t   table
        f   function
        u   userdata
        th  thread
]]

-- Default language id, based on tLanguages indexes!
-- Needs for the first resource launch, while next launches will
-- be set saved language id.
-- Indexes starts with 1 (1,2,3,..)
-- DEFAULT VALUE: 1
nDefaultLanguage = 1

-- Table with languages strings.
-- You can add new sub-table with your favorite language strings.
-- Just copy any sub-table and translate string values at the right side.
tLanguages = {

    {   -- RUSSIAN ------------------------------------------------------------------------------------
        -- language name
        ['langName'] = 'русский',

        -- windows titles
        ['mainWindowTitle'] =   'редактор настроек транспорта   :   CTRL + H',
        ['reportWindowTitle'] = 'отчёт',

        -- tab titles
        ['tabEngine'] =         'Движок',
        ['tabBody'] =           'Корпус',
        ['tabSuspension'] =     'Подвеска',
        ['tabWheels'] =         'Колёса',
        ['tabPEDs'] =           'Персонаж',
        ['tabModelFlags'] =     'Модель',
        ['tabHandlingFlags'] =  'Разное',

        -- values sizes
        ['kph'] =           'км / ч',
        ['m/sec2'] =        'м / сек²',
        ['kg*m2'] =         'кг · м²',
        ['m'] =             'м',
        ['kg'] =            'кг',
        ['degrees'] =       '°',
        ['percents/100'] =  '% / 100',
        ['percents'] =      '%',
        ['dollars'] =       '$',
        ['#'] =             '#',

        -- handling parameters
        ['numberOfGears'] =                 'Количество скоростей',
        ['collisionDamageMultiplier'] =     '% повреждений от столкновений',
        ['steeringLock'] =                  'Максимальный угол поворота руля',
        ['engineType'] =                    'Тип двигателя',
            ['petrol'] =                        'бензин',
            ['diesel'] =                        'дизель',
            ['electric'] =                      'электро',
        ['driveType'] =                     'Привод',
            ['fwd'] =                           'передний',
            ['rwd'] =                           'задний',
            ['awd'] =                           'полный',
        ['engineInertia'] =                 'Инерция',
        ['engineAcceleration'] =            'Ускорение',
        ['maxVelocity'] =                   'Максимальная скорость',
        ['mass'] =                          'Масса',
        ['percentSubmerged'] =              '% погружения в воду',
        ['centerOfMass'] =                  'Центр массы - x, y, z',
        ['dragCoeff'] =                     'Сила рывка',
        ['turnMass'] =                      'Масса толчка',
        ['tailLight'] =                     'Задние фары',
        ['headLight'] =                     'Передние фары',
        ['monetary'] =                      'Примерная стоимость транспорта',
        ['suspensionFrontRearBias'] =       'Смещение подвески спереди/сзади',
        ['suspensionAntiDiveMultiplier'] =  'Шатание корпуса при газе/тормозе',
        ['suspensionLowerLimit'] =          'Нижняя граница подвески',
        ['suspensionUpperLimit'] =          'Верхняя граница подвески',
        ['suspensionHighSpeedDamping'] =    'Колебания подвески на скорости',
        ['suspensionDamping'] =             'Колебания подвески без скорости',
        ['suspensionForceLevel'] =          'Сила подвески',
        ['tractionMultiplier'] =            'Сила сцепления с дорогой',
        ['brakeBias'] =                     'Смещение при торможении',
        ['brakeDeceleration'] =             'Замедление при торможении',
        ['tractionBias'] =                  'Смещение сцепления с дорогой',
        ['tractionLoss'] =                  'Потеря сцепления с дорогой',
        ['ABS'] =                           'ABS',
            ['no'] =                            'нет',
            ['yes'] =                           'да',
        ['animGroup'] =                     'Группа анимаций персонажа',
        ['seatOffsetDistance'] =            'Смещение дистанции до сиденья',

        -- column title
        ['flagsColumnTitle'] =      'CTRL + CLICK   :   CTRL + КЛИК',

        -- model flags
        ['IS_VAN'] =                'задние двери открываются как у фургона',
        ['IS_BUS'] =                'стоит на остановках как автобус',
        ['IS_LOW'] =                'пассажиры пригибаются как в низком транспорте',
        ['IS_BIG'] =                'управляется ботами как большой транспорт',
            ['REVERSE_BONNET'] =        'капот и багажник открываются наоборот',
            ['HANGING_BOOT'] =          'багажник открывается с верхнего края',
            ['TAILGATE_BOOT'] =         'багажник открывается с нижнего края',
            ['NOSWING_BOOT'] =          'багажник не открывается',
        ['NO_DOORS'] =              'анимация входа: как будто нет дверей',
        ['TANDEM_SEATS'] =          'анимация входа: сдвоенные сиденья',
        ['SIT_IN_BOAT'] =           'анимация входа: стоя как в лодке',
        ['CONVERTIBLE'] =           'анимация входа: прыжок в кабриолет',
            ['NO_EXHAUST'] =            'без выхлопной трубы',
            ['DBL_EXHAUST'] =           'две выхлопные трубы',
            ['NO1FPS_LOOK_BEHIND'] =    'скрывать модель при включении вида сзади',
            ['FORCE_DOOR_CHECK'] =      'проверка дверей',
        ['AXLE_F_NOTILT'] =         'передние колеса: всегда вертикальные',
        ['AXLE_F_SOLID'] =          'передние колеса: параллельны друг другу',
        ['AXLE_F_MCPHERSON'] =      'передние колеса: могут наклоняться вниз',
        ['AXLE_F_REVERSE'] =        'передние колеса: могут наклоняться вверх + флаг 19',
            ['AXLE_R_NOTILT'] =         'задние колеса: всегда вертикальные',
            ['AXLE_R_SOLID'] =          'задние колеса: параллельны друг другу',
            ['AXLE_R_MCPHERSON'] =      'задние колеса: могут наклоняться вниз',
            ['AXLE_R_REVERSE'] =        'задние колеса: могут наклоняться вверх + флаг 23',
        ['IS_BIKE'] =               'вид транспорта: байк',
        ['IS_HELI'] =               'вид транспорта: вертолет',
        ['IS_PLANE'] =              'вид транспорта: самолет',
        ['IS_BOAT'] =               'вид транспорта: лодка',
            ['BOUNCE_PANELS'] =          'установлены упругие панели',
            ['DOUBLE_RWHEELS'] =         'двойные задние колеса как у фуры',
            ['FORCE_GROUND_CLEARANCE'] = 'на скорости прижимается к земле',
            ['IS_HATCHBACK'] =           'тип кузова: хэтчбэк',

        -- handling flags
        ['1G_BOOST'] =              'ускорение лучше при старте',
        ['2G_BOOST'] =              'ускорение лучше на скорости',
        ['NPC_ANTI_ROLL'] =         'для ботов: корпус не качается',
        ['NPC_NEUTRAL_HANDL'] =     'для ботов: проще управление',
            ['NO_HANDBRAKE'] =          'нет ручного тормоза',
            ['STEER_REARWHEELS'] =      'рулит задними колесами',
            ['HB_REARWHEEL_STEER'] =    'рулит всеми колесами',
            ['ALT_STEER_OPT'] =         'альтерантивное управление',
        ['WHEEL_F_NARROW2'] =       'передние колеса: очень узкие',
        ['WHEEL_F_NARROW'] =        'передние колеса: узкие',
        ['WHEEL_F_WIDE'] =          'передние колеса: широкие',
        ['WHEEL_F_WIDE2'] =         'передние колеса: очень широкие',
            ['WHEEL_R_NARROW2'] =       'задние колеса: очень узкие',
            ['WHEEL_R_NARROW'] =        'задние колеса: узкие',
            ['WHEEL_R_WIDE'] =          'задние колеса: широкие',
            ['WHEEL_R_WIDE2'] =         'задние колеса: очень широкие',
        ['HYDRAULIC_GEOM'] =        'особая гидравлика',
        ['HYDRAULIC_INST'] =        'спавнится уже с гидравликой',
        ['HYDRAULIC_NONE'] =        'нельзя ставить гидравлику',
        ['NOS_INST'] =              'спавнится уже с NOS',
            ['OFFROAD_ABILITY'] =       'хорошо ездит по грязи',
            ['OFFROAD_ABILITY2'] =      'хорошо ездит по песку',
            ['HALOGEN_LIGHTS'] =        'галогенные фары',
            ['PROC_REARWHEEL_1ST'] =    'сцепление с дорогой задних колес лучше',
        ['USE_MAXSP_LIMIT'] =       'жесткий лимит максимальной скорости',
        ['LOW_RIDER'] =             'может тюнится как лоу райдер',
        ['STREET_RACER'] =          'может тюнится как стрит рейсер',
        ['UNDEFINED1'] =            '? неизвестный параметр 1 ?',
            ['SWINGING_CHASSIS'] =      'кузов шатается на подвеске как у лоу райдера',
            ['UNDEFINED2'] =            '? неизвестный параметр 2 ?',
            ['UNDEFINED3'] =            '? неизвестный параметр 3 ?',
            ['UNDEFINED4'] =            '? неизвестный параметр 4 ?',

        -- buttons
        ['btnDefaults'] =   'По умолчанию',
        ['btnClose'] =      'Закрыть',
        ['btnApply'] =      'Применить',

        -- header model and vehicle info
        ['modelID'] =       'ID модели:',
        ['modelName'] =     'Имя модели:',
        ['vehicleName'] =   'Имя транспорта:',

        -- report messages
        ['msgApplied'] =            'Настройки применены к вашему транспорту',
        ['msgResetedToDefaults'] =  'В редакторе выставлены настройки по умолчанию, примените их сами'
    },

    {   -- ENGLISH ------------------------------------------------------------------------------------
        -- language name
        ['langName'] = 'english',

        -- windows titles
        ['mainWindowTitle'] =   'vehicle handling editor   :   CTRL + H',
        ['reportWindowTitle'] = 'report',

        -- tab titles
        ['tabEngine'] =         'Engine',
        ['tabBody'] =           'Body',
        ['tabSuspension'] =     'Suspension',
        ['tabWheels'] =         'Wheels',
        ['tabPEDs'] =           'PEDs',
        ['tabModelFlags'] =     'Model flags',
        ['tabHandlingFlags'] =  'Other flags',

        -- value size names
        ['kph'] =           'kph',
        ['m/sec2'] =        'm / sec²',
        ['kg*m2'] =         'kg · m²',
        ['m'] =             'm',
        ['kg'] =            'kg',
        ['degrees'] =       '°',
        ['percents/100'] =  '% / 100',
        ['percents'] =      '%',
        ['dollars'] =       '$',
        ['#'] =             '#',

        -- handling parameters
        ['numberOfGears'] =                 'Number of gears',
        ['collisionDamageMultiplier'] =     'Collision damage multiplier',
        ['steeringLock'] =                  'Steering lock',
        ['engineType'] =                    'Engine type',
            ['petrol'] =                        'petrol',
            ['diesel'] =                        'diesel',
            ['electric'] =                      'electric',
        ['driveType'] =                     'Drive type',
            ['fwd'] =                           'front',
            ['rwd'] =                           'rear',
            ['awd'] =                           'full',
        ['engineInertia'] =                 'Engine inertia',
        ['engineAcceleration'] =            'Engine acceleration',
        ['maxVelocity'] =                   'Max velocity',
        ['mass'] =                          'Mass',
        ['percentSubmerged'] =              'Percent submerged',
        ['centerOfMass'] =                  'Center of mass - x, y, z',
        ['dragCoeff'] =                     'Drag coefficient',
        ['turnMass'] =                      'Turn mass',
        ['tailLight'] =                     'Tail light',
        ['headLight'] =                     'Head light',
        ['monetary'] =                      'Monetary',
        ['suspensionFrontRearBias'] =       'Suspension front rear bias',
        ['suspensionAntiDiveMultiplier'] =  'Suspension anti dive multiplier',
        ['suspensionLowerLimit'] =          'Suspension lower limit',
        ['suspensionUpperLimit'] =          'Suspension upper limit',
        ['suspensionHighSpeedDamping'] =    'Suspension high speed damping',
        ['suspensionDamping'] =             'Suspension damping',
        ['suspensionForceLevel'] =          'Suspension force level',
        ['tractionMultiplier'] =            'Traction multiplier',
        ['brakeBias'] =                     'Brake bias',
        ['brakeDeceleration'] =             'Brake deceleration',
        ['tractionBias'] =                  'Traction bias',
        ['tractionLoss'] =                  'Traction loss',
        ['ABS'] =                           'ABS',
            ['no'] =                            'no',
            ['yes'] =                           'yes',
        ['animGroup'] =                     'Animation group',
        ['seatOffsetDistance'] =            'Seat offset distance',

        -- column title
        ['flagsColumnTitle'] =      'CTRL + CLICK   :   CTRL + КЛИК',

        -- model flags
        ['IS_VAN'] =                'allows double doors for the rear animation',
        ['IS_BUS'] =                'vehicle uses bus stops, trying to take on passengers',
        ['IS_LOW'] =                'drivers and passengers sit lower and lean back',
        ['IS_BIG'] =                'changes the way that the AI drives around corners',
            ['REVERSE_BONNET'] =        'bonnet and boot open in opposite direction from normal',
            ['HANGING_BOOT'] =          'boot opens from top edge',
            ['TAILGATE_BOOT'] =         'boot opens from bottom edge',
            ['NOSWING_BOOT'] =          'boot does not open',
        ['NO_DOORS'] =              'door open and close animations are skipped',
        ['TANDEM_SEATS'] =          'two people will use the front passenger seat',
        ['SIT_IN_BOAT'] =           'uses seated boat animation instead of standing',
        ['CONVERTIBLE'] =           'changes how hookers operate and other small effects',
            ['NO_EXHAUST'] =            'removes all exhaust particles',
            ['DBL_EXHAUST'] =           'adds a second exhaust particle on opposite side to first',
            ['NO1FPS_LOOK_BEHIND'] =    'prevents player using rear view when in 1st-person mode',
            ['FORCE_DOOR_CHECK'] =      'force door check',
        ['AXLE_F_NOTILT'] =         'front wheels stay vertical to the car like GTA3',
        ['AXLE_F_SOLID'] =          'front wheels stay parallel to each other',
        ['AXLE_F_MCPHERSON'] =      'front wheels tilt like GTA Vice City',
        ['AXLE_F_REVERSE'] =        'reverses the tilting of wheels when using flag 19',
            ['AXLE_R_NOTILT'] =         'rear wheels stay vertical to the car like GTA3',
            ['AXLE_R_SOLID'] =          'rear wheels stay parallel to each other',
            ['AXLE_R_MCPHERSON'] =      'rear wheels tilt like GTA Vice City',
            ['AXLE_R_REVERSE'] =        'reverses the tilting of wheels when using flag 23',
        ['IS_BIKE'] =               'vehicle type: bike',
        ['IS_HELI'] =               'vehicle type: helicopter',
        ['IS_PLANE'] =              'vehicle type: plane',
        ['IS_BOAT'] =               'vehicle type: boat',
            ['BOUNCE_PANELS'] =          'bounce panels',
            ['DOUBLE_RWHEELS'] =         'double rear wheels',
            ['FORCE_GROUND_CLEARANCE'] = 'force ground clearance',
            ['IS_HATCHBACK'] =           'is hatchback',

        -- handling flags
        ['1G_BOOST'] =              'gives more engine power for standing starts',
        ['2G_BOOST'] =              'gives more engine power at slightly higher speeds',
        ['NPC_ANTI_ROLL'] =         'no body roll when driven by AI characters',
        ['NPC_NEUTRAL_HANDL'] =     'less likely to spin out when driven by AI characters',
            ['NO_HANDBRAKE'] =          'disables the handbrake effect',
            ['STEER_REARWHEELS'] =      'rear wheels steer instead of front, like a forklift truck',
            ['HB_REARWHEEL_STEER'] =    'handbrake makes the rear wheels steer like the monster truck',
            ['ALT_STEER_OPT'] =         'alternative steering',
        ['WHEEL_F_NARROW2'] =       'very narrow front wheels',
        ['WHEEL_F_NARROW'] =        'narrow front wheels',
        ['WHEEL_F_WIDE'] =          'wide front wheels',
        ['WHEEL_F_WIDE2'] =         'very wide front wheels',
            ['WHEEL_R_NARROW2'] =       'very narrow rear wheels',
            ['WHEEL_R_NARROW'] =        'narrow rear wheels',
            ['WHEEL_R_WIDE'] =          'wide rear wheels',
            ['WHEEL_R_WIDE2'] =         'very wide rear wheels',
        ['HYDRAULIC_GEOM'] =        'special hydraulics',
        ['HYDRAULIC_INST'] =        'will spawn with hydraulics installed',
        ['HYDRAULIC_NONE'] =        'hydraulics cannot be installed',
        ['NOS_INST'] =              'will spawn with NOS installed',
            ['OFFROAD_ABILITY'] =       'vehicle will perform better on loose surfaces like dirt',
            ['OFFROAD_ABILITY2'] =      'vehicle will perform better on soft surfaces like sand',
            ['HALOGEN_LIGHTS'] =        'makes headlights brighter and more blue',
            ['PROC_REARWHEEL_1ST'] =    'rear wheels traction better than front wheels traction',
        ['USE_MAXSP_LIMIT'] =       'prevents vehicle going faster than maximum speed value',
        ['LOW_RIDER'] =             'allows vehicle to be modified at Loco Low Co',
        ['STREET_RACER'] =          'allows vehicle to be modified at Wheel Arch Angels',
        ['UNDEFINED1'] =            '? unknown flag 1 ?',
            ['SWINGING_CHASSIS'] =      'lets the car body move from side to side on the suspension',
            ['UNDEFINED2'] =            '? unknown flag 2 ?',
            ['UNDEFINED3'] =            '? unknown flag 3 ?',
            ['UNDEFINED4'] =            '? unknown flag 4 ?',

        -- buttons
        ['btnDefaults'] =   'Defaults',
        ['btnClose'] =      'Close',
        ['btnApply'] =      'Apply',

        -- header model and vehicle info
        ['modelID'] =       'Model ID:',
        ['modelName'] =     'Model name:',
        ['vehicleName'] =   'Vehicle name:',

        -- report messages
        ['msgApplied'] =            'All settings successfully applied to your vehicle',
        ['msgResetedToDefaults'] =  "All fields reseted to defaults, apply they by yourself"
    }

}
