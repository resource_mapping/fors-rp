﻿--[[
    -------------
    Русский игровой редактор настроек транспорта для MTASA v1.2 и выше
    ---------

    Новые версии, баги, запросы:
        http://multi-theft-auto-ru.googlecode.com/

    Авторы текущего файла:
        MX_Master ( http://multi-theft-auto.ru/ )

    Легенда по префиксам в названиях переменных:
        b   boolean, булевое
        n   number, число
        s   string, строка
        t   table, таблица
        f   function, функция
        u   userdata, в MTASA этот тип юзают элементы
        th  thread, поток
]]

---
-- ЯЗЫКОВЫЕ ДАННЫЕ --
---

-- текущий язык (ид)
nCurrentLanguage = nDefaultLanguage or 1

-- Автогенерируемая копия таблицы с языковыми строками,
-- реверсированная по полям подтаблиц.
-- Если у подтаблиц tLanguages такой порядок: ['имя'] = 'строка',
-- то у tLanguagesReversed такой: ['строка'] = 'имя'
-- Используется для быстрого поиска имени языковой строки по содержимому.
local tLanguagesReversed = {}

for nID, tStrings in ipairs(tLanguages or {}) do
    tLanguagesReversed[nID] = {}
    for sName, sString in pairs(tStrings or {}) do
        tLanguagesReversed[nID][sString] = sName
    end
end




---
-- ИНСТРУМЕНТЫ --
---

-- возвращает языковую строку в зависимости от текущего языка
function fLangStr ( sName )
    if type(sName) ~= 'string' then
        sName = tostring(sName)
    end

    if tLanguages[nCurrentLanguage] then
        return tLanguages[nCurrentLanguage][sName] or sName
    else
        return sName
    end
end

-- меняет язык интерфейса на новый
function fChangeLanguage ( nNewID )
    if nNewID == nCurrentLanguage or not tLanguages[nNewID] then return end

    local nOldID = nCurrentLanguage
    nCurrentLanguage = nNewID


    -- меняем строки интерфейса
    local sText, sName, nItems, nRows, nCols = '', '', 0, 0, 0

    -- заголовки окон
    for _, uElement in ipairs( getElementsByType( 'gui-window', resourceRoot ) or {} ) do
        sText = guiGetText(uElement)
        sName = tLanguagesReversed[nOldID][sText]
        if sName then guiSetText( uElement, tLanguages[nNewID][sName] ) end
    end

    -- лэйблы
    for _, uElement in ipairs( getElementsByType( 'gui-label', resourceRoot ) or {} ) do
        sText = guiGetText(uElement)
        sName = tLanguagesReversed[nOldID][sText]
        if sName then guiSetText( uElement, tLanguages[nNewID][sName] ) end
    end

    -- кнопки
    for _, uElement in ipairs( getElementsByType( 'gui-button', resourceRoot ) or {} ) do
        sText = guiGetText(uElement)
        sName = tLanguagesReversed[nOldID][sText]
        if sName then guiSetText( uElement, tLanguages[nNewID][sName] ) end
    end

    -- закладки
    for _, uElement in ipairs( getElementsByType( 'gui-tab', resourceRoot ) or {} ) do
        sText = guiGetText(uElement)
        sName = tLanguagesReversed[nOldID][sText]
        if sName then guiSetText( uElement, tLanguages[nNewID][sName] ) end
    end

    -- комбобоксы
    for _, uElement in ipairs( getElementsByType( 'gui-combobox', resourceRoot ) or {} ) do
        sText = guiGetProperty( uElement, 'Text' )
        sName = tLanguagesReversed[nOldID][sText]
        if sName then
            guiSetProperty( uElement, 'Text', tLanguages[nNewID][sName] )
        end

        nItems = getElementData( uElement, 'itemsCount' )
        if type(nItems) == 'number' then
            for n = 0, nItems - 1 do
                sText = guiComboBoxGetItemText( uElement, n )
                sName = tLanguagesReversed[nOldID][sText]
                if sName then
                    guiComboBoxSetItemText( uElement, n, tLanguages[nNewID][sName] )
                end
            end
        end
    end

    -- списки
    for _, uElement in ipairs( getElementsByType( 'gui-gridlist', resourceRoot ) or {} ) do
        nRows = guiGridListGetRowCount(uElement) or 0
        nCols = guiGridListGetColumnCount(uElement) or 0
        for nRow = 0, nRows - 1 do
            for nCol = 1, nCols do
                sText = guiGridListGetItemText( uElement, nRow, nCol )
                sName = tLanguagesReversed[nOldID][sText]
                if sName then
                    guiGridListSetItemText( uElement, nRow, nCol, tLanguages[nNewID][sName], false, false )
                end
            end
        end
    end
end
