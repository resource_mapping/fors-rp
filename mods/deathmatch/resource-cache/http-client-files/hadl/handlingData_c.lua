﻿--[[
    -------------
    Русский игровой редактор настроек транспорта для MTASA v1.2 и выше
    ---------

    Новые версии, баги, запросы:
        http://multi-theft-auto-ru.googlecode.com/

    Авторы текущего файла:
        MX_Master ( http://multi-theft-auto.ru/ )

    Легенда по префиксам в названиях переменных:
        b   boolean, булевое
        n   number, число
        s   string, строка
        t   table, таблица
        f   function, функция
        u   userdata, в MTASA этот тип юзают элементы
        th  thread, поток
]]

---
-- НАСТРОЙКИ И ДАННЫЕ ДЛЯ ПРАВКИ ХЭНДЛИНГА --
---

-- лимиты значений
-- источник - http://code.google.com/p/mtasa-blue/source/browse/trunk/MTA10_Server/mods/deathmatch/logic/CStaticFunctionDefinitions.cpp
-- все поля это функции, которые на входе получают значение,
-- а на выходе отдают значение, удовлетворяющее лимиту
tHandlingParamLimits = {
    ['mass'] = function ( value )
        value = tonumber(value) or 0
        if     value <= 0 then value = 0.001
        elseif value > 100000 then value = 100000 end
        return value
    end,
    ['turnMass'] = function ( value )
        value = tonumber(value) or 0
        if     value <= 0 then value = 0.001
        elseif value > 10000000 then value = 10000000 end
        return value
    end,
    ['dragCoeff'] = function ( value )
        value = tonumber(value) or 0
        if     value < -200 then value = -200
        elseif value > 200 then value = 200 end
        return value
    end,
    ['centerOfMass'] = function ( value )
        if type(value) ~= 'table' then return {0,0,0} end
        for n = 1, 3 do
            if not value[n] then
                value[n] = 0
            else
                value[n] = tonumber(value[n])
                if     value[n] < -10 then value[n] = -10
                elseif value[n] > 10 then value[n] = 10 end
            end
        end
        return value
    end,
    ['percentSubmerged'] = function ( value )
        value = math.floor( tonumber(value) or 0 )
        if     value <= 0 then value = 1
        elseif value > 200 then value = 200 end
        return value
    end,
    ['tractionMultiplier'] = function ( value )
        value = tonumber(value) or 0
        if     value < -100000 then value = -100000
        elseif value > 100000 then value = 100000 end
        return value
    end,
    ['tractionLoss'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 100 then value = 100 end
        return value
    end,
    ['tractionBias'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 1 then value = 1 end
        return value
    end,
    ['numberOfGears'] = function ( value )
        value = math.floor( tonumber(value) or 0 )
        if     value < 1 then value = 1
        elseif value > 5 then value = 5 end
        return value
    end,
    ['maxVelocity'] = function ( value )
        value = tonumber(value) or 0
        if     value <= 0 then value = 0.001
        elseif value > 200000 then value = 200000 end
        return value
    end,
    ['engineAcceleration'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 100000 then value = 100000 end
        return value
    end,
    ['engineInertia'] = function ( value )
        value = tonumber(value) or 0
        if     value < -1000 then value = -1000
        elseif value > 1000 then value = 1000
        elseif value == 0.0 then value = 0.001 end
        return value
    end,
    ['driveType'] = function ( value )
        local tValues = {['fwd']=1,['rwd']=2,['awd']=3}
        value = tostring(value)
        if not tValues[value] then value = 'fwd' end
        return value
    end,
    ['engineType'] = function ( value )
        local tValues = {['petrol']=1,['diesel']=2,['electric']=3}
        value = tostring(value)
        if not tValues[value] then value = 'petrol' end
        return value
    end,
    ['brakeDeceleration'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 100000 then value = 100000 end
        return value
    end,
    ['brakeBias'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 1 then value = 1 end
        return value
    end,
    ['ABS'] = function ( value )
        if value then value = true
        else          value = false end
        return value
    end,
    ['steeringLock'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 360 then value = 360 end
        return value
    end,
    ['suspensionForceLevel'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 100 then value = 100 end
        return value
    end,
    ['suspensionDamping'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 100 then value = 100 end
        return value
    end,
    ['suspensionHighSpeedDamping'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 600 then value = 600 end
        return value
    end,
    ['suspensionUpperLimit'] = function ( value, lower )
        value = tonumber(value) or 0
        lower = tonumber(lower) or 0
        if     value < -50 then value = -50
        elseif value > 50 then value = 50 end
        if not (value <= lower - 0.1 or value >= lower + 0.1) then
            if     value > 50-0.1 then  value = lower - 0.1
            elseif value < -50+0.1 then value = lower + 0.1
            else                        value = lower + 0.1 end
        end
        return value
    end,
    ['suspensionLowerLimit'] = function ( value, upper )
        value = tonumber(value) or 0
        upper = tonumber(upper) or 0
        if     value < -50 then value = -50
        elseif value > 50 then value = 50 end
        if not (value <= upper - 0.1 or value >= upper + 0.1) then
            if     value > 50-0.1 then  value = upper - 0.1
            elseif value < -50+0.1 then value = upper + 0.1
            else                        value = upper + 0.1 end
        end
        return value
    end,
    ['suspensionFrontRearBias'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 3 then value = 3 end
        return value
    end,
    ['suspensionAntiDiveMultiplier'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 30 then value = 30 end
        return value
    end,
    ['seatOffsetDistance'] = function ( value )
        value = tonumber(value) or 0
        if     value < -20 then value = -20
        elseif value > 20 then value = 20 end
        return value
    end,
    ['collisionDamageMultiplier'] = function ( value )
        value = tonumber(value) or 0
        if     value < 0 then value = 0
        elseif value > 100 then value = 100  end
        return value
    end,
    ['monetary'] = function ( value )
        value = math.floor( tonumber(value) or 0 )
        if     value < 0 then value = 0
        elseif value > 0xFFFFFFFF then value = 0xFFFFFFFF end
        return value
    end,
    ['modelFlags'] = function ( value )
        value = math.floor( tonumber(value) or 0 )
        if     value < 0 then value = 0
        elseif value > 0xFFFFFFFF then value = 0xFFFFFFFF end
        return value
    end,
    ['handlingFlags'] = function ( value )
        value = math.floor( tonumber(value) or 0 )
        if     value < 0 then value = 0
        elseif value > 0xFFFFFFFF then value = 0xFFFFFFFF end
        return value
    end,
    ['headLight'] = function ( value )
        local tValues = {['small']=1,['long']=2,['big']=3,['tall']=4}
        value = tostring(value)
        if not tValues[value] then value = 'small' end
        return value
    end,
    ['tailLight'] = function ( value )
        local tValues = {['small']=1,['long']=2,['big']=3,['tall']=4}
        value = tostring(value)
        if not tValues[value] then value = 'small' end
        return value
    end,
    ['animGroup'] = function ( value )
        value = math.floor( tonumber(value) or 0 )
        if     value < 0 then value = 0
        elseif value > 29 then value = 29
        elseif value == 3 then value = 4
        elseif value == 8 then value = 9
        elseif value == 17 then value = 18
        elseif value == 23 then value = 24 end
        return value
    end
}




-- короткие названия моделей транспорта
tModelToModelName = { -- кол-во = 212
--  ['ID'] = 'modelname',
    [400] = 'landstal',
    [401] = 'bravura',
    [402] = 'buffalo',
    [403] = 'linerun',
    [404] = 'peren',
    [405] = 'sentinel',
    [406] = 'dumper',
    [407] = 'firetruk',
    [408] = 'trash',
    [409] = 'stretch',
    [410] = 'manana',
    [411] = 'infernus',
    [412] = 'voodoo',
    [413] = 'pony',
    [414] = 'mule',
    [415] = 'cheetah',
    [416] = 'ambulan',
    [417] = 'leviathn',
    [418] = 'moonbeam',
    [419] = 'esperant',
    [420] = 'taxi',
    [421] = 'washing',
    [422] = 'bobcat',
    [423] = 'mrwhoop',
    [424] = 'bfinject',
    [425] = 'hunter',
    [426] = 'premier',
    [427] = 'enforcer',
    [428] = 'securica',
    [429] = 'banshee',
    [430] = 'predator',
    [431] = 'bus',
    [432] = 'rhino',
    [433] = 'barracks',
    [434] = 'hotknife',
    [435] = 'artict1',
    [436] = 'previon',
    [437] = 'coach',
    [438] = 'cabbie',
    [439] = 'stallion',
    [440] = 'rumpo',
    [441] = 'rcbandit',
    [442] = 'romero',
    [443] = 'packer',
    [444] = 'monster',
    [445] = 'admiral',
    [446] = 'squalo',
    [447] = 'seaspar',
    [448] = 'pizzaboy',
    [449] = 'tram',
    [450] = 'artict2',
    [451] = 'turismo',
    [452] = 'speeder',
    [453] = 'reefer',
    [454] = 'tropic',
    [455] = 'flatbed',
    [456] = 'yankee',
    [457] = 'caddy',
    [458] = 'solair',
    [459] = 'topfun',
    [460] = 'skimmer',
    [461] = 'pcj600',
    [462] = 'faggio',
    [463] = 'freeway',
    [464] = 'rcbaron',
    [465] = 'rcraider',
    [466] = 'glendale',
    [467] = 'oceanic',
    [468] = 'sanchez',
    [469] = 'sparrow',
    [470] = 'patriot',
    [471] = 'quad',
    [472] = 'coastg',
    [473] = 'dinghy',
    [474] = 'hermes',
    [475] = 'sabre',
    [476] = 'rustler',
    [477] = 'zr350',
    [478] = 'walton',
    [479] = 'regina',
    [480] = 'comet',
    [481] = 'bmx',
    [482] = 'burrito',
    [483] = 'camper',
    [484] = 'marquis',
    [485] = 'baggage',
    [486] = 'dozer',
    [487] = 'maverick',
    [488] = 'vcnmav',
    [489] = 'rancher',
    [490] = 'fbiranch',
    [491] = 'virgo',
    [492] = 'greenwoo',
    [493] = 'jetmax',
    [494] = 'hotring',
    [495] = 'sandking',
    [496] = 'blistac',
    [497] = 'polmav',
    [498] = 'boxville',
    [499] = 'benson',
    [500] = 'mesa',
    [501] = 'rcgoblin',
    [502] = 'hotrina',
    [503] = 'hotrinb',
    [504] = 'bloodra',
    [505] = 'rnchlure',
    [506] = 'supergt',
    [507] = 'elegant',
    [508] = 'journey',
    [509] = 'bike',
    [510] = 'mtbike',
    [511] = 'beagle',
    [512] = 'cropdust',
    [513] = 'stunt',
    [514] = 'petro',
    [515] = 'rdtrain',
    [516] = 'nebula',
    [517] = 'majestic',
    [518] = 'buccanee',
    [519] = 'shamal',
    [520] = 'hydra',
    [521] = 'fcr900',
    [522] = 'nrg500',
    [523] = 'copbike',
    [524] = 'cement',
    [525] = 'towtruck',
    [526] = 'fortune',
    [527] = 'cadrona',
    [528] = 'fbitruck',
    [529] = 'willard',
    [530] = 'forklift',
    [531] = 'tractor',
    [532] = 'combine',
    [533] = 'feltzer',
    [534] = 'remingtn',
    [535] = 'slamvan',
    [536] = 'blade',
    [537] = 'freight',
    [538] = 'streak',
    [539] = 'vortex',
    [540] = 'vincent',
    [541] = 'bullet',
    [542] = 'clover',
    [543] = 'sadler',
    [544] = 'firela',
    [545] = 'hustler',
    [546] = 'intruder',
    [547] = 'primo',
    [548] = 'cargobob',
    [549] = 'tampa',
    [550] = 'sunrise',
    [551] = 'merit',
    [552] = 'utility',
    [553] = 'nevada',
    [554] = 'yosemite',
    [555] = 'windsor',
    [556] = 'monstera',
    [557] = 'monsterb',
    [558] = 'uranus',
    [559] = 'jester',
    [560] = 'sultan',
    [561] = 'stratum',
    [562] = 'elegy',
    [563] = 'raindanc',
    [564] = 'rctiger',
    [565] = 'flash',
    [566] = 'tahoma',
    [567] = 'savanna',
    [568] = 'bandito',
    [569] = 'freiflat',
    [570] = 'streakc',
    [571] = 'kart',
    [572] = 'mower',
    [573] = 'duneride',
    [574] = 'sweeper',
    [575] = 'broadway',
    [576] = 'tornado',
    [577] = 'at400',
    [578] = 'dft30',
    [579] = 'huntley',
    [580] = 'stafford',
    [581] = 'bf400',
    [582] = 'newsvan',
    [583] = 'tug',
    [584] = 'petrotr',
    [585] = 'emperor',
    [586] = 'wayfarer',
    [587] = 'euros',
    [588] = 'hotdog',
    [589] = 'club',
    [590] = 'freibox',
    [591] = 'artict3',
    [592] = 'androm',
    [593] = 'dodo',
    [594] = 'rccam',
    [595] = 'launch',
    [596] = 'copcarla',
    [597] = 'copcarsf',
    [598] = 'copcarvg',
    [599] = 'copcarru',
    [600] = 'picador',
    [601] = 'swatvan',
    [602] = 'alpha',
    [603] = 'phoenix',
    [604] = 'glenshit',
    [605] = 'sadlshit',
    [606] = 'bagboxa',
    [607] = 'bagboxb',
    [608] = 'tugstair',
    [609] = 'boxburg',
    [610] = 'farmtr1',
    [611] = 'utiltr1'
}
