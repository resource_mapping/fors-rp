﻿local sw,sh = guiGetScreenSize()
local UniSans = dxCreateFont("UniSans.otf",10.5)

local function getPlayerCustomName(player) -- ИМЯ ФАМИЛИЯ
	local FIRST_NAME = getElementData(player, "Name") --свою переменную 
	local LAST_NAME = getElementData(player, "Familia")--свою переменную 
	
	if not FIRST_NAME or not LAST_NAME then
		return getPlayerName(player)
	end
	
	return tostring(FIRST_NAME.." "..LAST_NAME)
end

addEventHandler( "onClientRender", root, function (  )
    for k,player in ipairs(getElementsByType("player")) do
        if player ~= getLocalPlayer() and getElementDimension(getLocalPlayer()) == getElementDimension(player) then
            local x, y, z = getElementPosition(player)
            z = z + 0.95
            local r,g,b = 255,255,255
            local Mx, My, Mz = getCameraMatrix()
            local distance = getDistanceBetweenPoints3D( x, y, z, Mx, My, Mz )
            local size = 1
            if ( distance <= 30 ) then
            local sx,sy = getScreenFromWorldPosition( x, y, z, 0 )
                if ( sx and sy ) then
                    local hp = getElementHealth(player)
                    if hp > 100 then hp = 100 end
                    if hp > 0 then
                        local maxhp = 64
                        local pw,ph = hp*(maxhp/100),5
                        local r2,g2,b2 = 0,0,0
                        local r22,g22,b22 = 255,50,50
                        dxDrawImage ( sx-maxhp/2-2, sy-ph/2-2+5, maxhp+4,ph+4, "white.jpg",0,0,0,tocolor(r2,g2,b2,100) )
                        dxDrawImage ( sx-maxhp/2, sy-ph/2+5, pw,ph, "white.jpg",0,0,0,tocolor(r22,g22,b22,150) )
                    end
                    local name = getPlayerName(player)
                    if hp > 0 then
                        dxDrawText(getElementData ( player, "accName" ), sx, sy, sx+size, sy+size, tocolor(0,0,0,255), size, UniSans, "center", "bottom", false, false, false ,true) 
                        dxDrawText(getElementData ( player, "accName" ), sx, sy, sx-size, sy-size, tocolor(0,0,0,255), size, UniSans, "center", "bottom", false, false, false,true ) 
                        dxDrawText(getElementData ( player, "accName" ), sx, sy, sx-size, sy+size, tocolor(0,0,0,255), size, UniSans, "center", "bottom", false, false, false,true) 
                        dxDrawText(getElementData ( player, "accName" ), sx, sy, sx+size, sy-size, tocolor(0,0,0,255), size, UniSans, "center", "bottom", false, false, false,true ) 
                        dxDrawText(getElementData ( player, "accName" ), sx, sy, sx, sy, tocolor(r,g,b,255), size, UniSans, "center", "bottom", false, false, false,true ) 
                    end
                end
            end
        end
    end 
end)