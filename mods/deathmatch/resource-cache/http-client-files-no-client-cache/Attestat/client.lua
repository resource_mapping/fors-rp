﻿function centerWindow ( center_window )
	local sx, sy = guiGetScreenSize ( )
	local windowW, windowH = guiGetSize ( center_window, false )
	local x, y = ( sx - windowW ) / 2, ( sy - windowH ) / 2
	guiSetPosition ( center_window, x, y, false )
end

if not getElementData(localPlayer,"Education") then
    setElementData(localPlayer, "Education", false)
end

if not getElementData(localPlayer,"CertificateProcess") then
    setElementData(localPlayer, "CertificateProcess", 0)
end

if not getElementData(localPlayer,"mathematics") then
    setElementData(localPlayer, "mathematics", false)
end

if not getElementData(localPlayer,"Russianlanguage") then
    setElementData(localPlayer, "Russianlanguage", false)
end

if not getElementData(localPlayer,"Physics") then
    setElementData(localPlayer, "Physics", false)
end

if not getElementData(localPlayer,"Biology") then
    setElementData(localPlayer, "Biology", false)
end

if not getElementData(localPlayer,"Certificate") then
    setElementData(localPlayer, "Certificate", false)
end

local pickupEducation = createPickup(pickupEducation[1],pickupEducation[2],pickupEducation[3], 3, 1239, 50)
local blip = createBlipAttachedTo(pickupEducation, 21)
setBlipVisibleDistance(blip, 400)

addEventHandler("onClientRender", root, function()
	local x, y, z = getElementPosition(pickupEducation)
	local Mx, My, Mz = getCameraMatrix()
	if (getDistanceBetweenPoints3D(x, y, z, Mx, My, Mz) <= 10) then
		local sx, sy = getScreenFromWorldPosition(x, y, z + 0.5, 0.07)
		if (sx and sy) then
			dxDrawText("[Обучение]\n\n\n", sx - 1, sy + 1, sx - 1, sy + 1, tocolor(0, 0, 0, 255), 1.52, "default-bold", "center", "center", false, false, false, false, false)
			dxDrawText("[Обучение]\n\n\n", sx - 1, sy + 1, sx - 1, sy + 1, tocolor(0, 255, 0, 255), 1.50, "default-bold", "center", "center", false, false, false, false, false)
			
			dxDrawText("Получение аттестата", sx - 1, sy + 1, sx - 1, sy + 1, tocolor(0, 0, 0, 255), 1.52, "default-bold", "center", "center", false, false, false, false, false)
			dxDrawText("Получение аттестата", sx - 1, sy + 1, sx - 1, sy + 1, tocolor(255, 255, 255, 255), 1.50, "default-bold", "center", "center", false, false, false, false, false)
		end
	end
end)

wndEducation = guiCreateWindow(0,0,450,310,"Система образования",false)
centerWindow(wndEducation)
guiSetVisible(wndEducation,false)

labelEducation1 = guiCreateLabel(5,25,440,150,"Чтобы получить аттестат, вы должны изучить\nвсе представленные ниже предметы.",false,wndEducation)
guiSetFont(labelEducation1, "default-bold-small")
guiLabelSetHorizontalAlign(labelEducation1, "center", false)

labelEducation2 = guiCreateLabel(15,65,440,150,"Стоимость обучения: "..moneyEducation.." руб.",false,wndEducation)
guiSetFont(labelEducation2, "default-bold-small")
guiLabelSetColor(labelEducation2, 0, 255, 0)
guiLabelSetHorizontalAlign(labelEducation2, "center", false)

labelEducation3 = guiCreateLabel(5,90,440,150,"Изучаемые предметы:",false,wndEducation)
guiSetFont(labelEducation3, "default-bold-small")
guiLabelSetHorizontalAlign(labelEducation3, "center", false)

buttonEducation1 = guiCreateButton(20,130,150,35,"Математика",false,wndEducation)
guiSetFont(buttonEducation1,"default-bold-small")

imageEducation1 = guiCreateStaticImage(180,133,25,25,"files/false.png",false,wndEducation)

buttonEducation2 = guiCreateButton(240,130,150,35,"Русский язык",false,wndEducation)
guiSetFont(buttonEducation2,"default-bold-small")

imageEducation2 = guiCreateStaticImage(400,133,25,25,"files/false.png",false,wndEducation)

buttonEducation3 = guiCreateButton(20,180,150,35,"Физика",false,wndEducation)
guiSetFont(buttonEducation3,"default-bold-small")

imageEducation3 = guiCreateStaticImage(180,183,25,25,"files/false.png",false,wndEducation)

buttonEducation4 = guiCreateButton(240,180,150,35,"Биология",false,wndEducation)
guiSetFont(buttonEducation4,"default-bold-small")

imageEducation4 = guiCreateStaticImage(400,183,25,25,"files/false.png",false,wndEducation)

labelEducation3 = guiCreateLabel(5,225,440,150,"__________________________________________________________",false,wndEducation)
guiSetFont(labelEducation3, "default-bold-small")
guiLabelSetHorizontalAlign(labelEducation3, "center", false)

buttonEducation5 = guiCreateButton(10,260,205,35,"Начать обучение",false,wndEducation)
guiSetFont(buttonEducation5,"default-bold-small")

buttonEducation6 = guiCreateButton(230,260,205,35,"Отмена",false,wndEducation)
guiSetFont(buttonEducation6,"default-bold-small")

----------------------------------------------------------- Решение примеров. (Математика)

local TesTrue3 = 0
local Tesfalse3 = 0

TestMathematics = guiCreateWindow(0,0,450,150,"Решение примеров (Математика)",false)
centerWindow(TestMathematics)
guiSetVisible(TestMathematics, false)

QuestionMathematics = guiCreateLabel(360, 25, 150, 150, "Пример: 1/10", false, TestMathematics)
guiSetFont(QuestionMathematics, "default-bold-small")

QuestionMathematics2 = guiCreateLabel(5, 55, 440, 150, "0 + 0 = ?", false, TestMathematics)
guiSetFont(QuestionMathematics2, "default-bold-small")
guiLabelSetHorizontalAlign(QuestionMathematics2, "center", false)

editMathematics = guiCreateEdit(10,100,205,35,"",false,TestMathematics)
guiSetFont(editMathematics,"default-bold-small")
guiEditSetMaxLength(editMathematics, 8)
addEventHandler("onClientGUIChanged", editMathematics, function(element) 
	guiSetText(editMathematics, string.gsub(guiGetText(editMathematics), "%D", ""))
end)
	
buttonMathematics = guiCreateButton(230,100,205,35,"Ответить",false,TestMathematics)
guiSetFont(buttonMathematics,"default-bold-small")

function DrawMathematicsTest()
	guiSetText(editMathematics,"")
	if getElementData(localPlayer,"progress") >= 11 then
        FinishMathematicsTest()
	else
		local s1 = math.random(500,1000)
		local s2 = math.random(1,500)
		guiSetText(QuestionMathematics, "Пример: "..getElementData(localPlayer,"progress").."/10")
		guiSetText(QuestionMathematics2, s1.." + "..s2.." = ?")
	end
end

addEventHandler("onClientGUIClick", getRootElement(), function ()
    if source == buttonMathematics then
		if guiGetText(editMathematics) ~= "" then
			local num1, num2 = string.match(guiGetText(QuestionMathematics2), "(%d+)[^%d]+(%d+)[^%d]")
			textOtvet = num1 + num2
			if tonumber(guiGetText(editMathematics)) == tonumber(textOtvet) then
				TesTrue3 = TesTrue3+1
				outputChatBox("#00FF00[Верно] #FFFFFFВы ответили: #00FF00"..guiGetText(editMathematics).." #FFFFFFПравильный ответ: #00FF00"..textOtvet..".", 255, 255, 255, true)
			else
				Tesfalse3 = Tesfalse3+1
				outputChatBox("#FF0000[Неверно] #FFFFFFВы ответили: #FF0000"..guiGetText(editMathematics).." #FFFFFFПравильный ответ: #00FF00"..textOtvet..".", 255, 255, 255, true)
			end
			setElementData(localPlayer,"progress", getElementData(localPlayer,"progress") + 1)
			DrawMathematicsTest()
		else
            outputChatBox("#FF0000[Обучение] #FFFFFFПоля пустые, либо заполнены неверно.", 255, 255, 255, true)
        end
    end
end)

function FinishMathematicsTest()
	if TesTrue3 == 10 then
        guiSetVisible(TestMathematics, false)
        showCursor(false)
		outputChatBox("#00FF00[Обучение] #FFFFFFВы успешно ответили на все #00FF0010 #FFFFFFпримеров. Математика сдана.", 255, 255, 255, true)
        setTimer(function()
            setElementData(localPlayer, "CertificateProcess", getElementData(localPlayer, "CertificateProcess") + 1)
			setElementData(localPlayer, "mathematics", true)
            TesTrue3 = 0
            Tesfalse3 = 0
        end, 100, 1)
	else
        guiSetVisible(TestMathematics, false)
        showCursor(false)
		outputChatBox("#FF0000[Обучение] #FFFFFFВы провалили тест. Результат: #00FF00"..TesTrue3.." #FFFFFFправильных из #00FF0010.", 255, 255, 255, true)
        setTimer(function()
            TesTrue3 = 0
            Tesfalse3 = 0
        end, 100, 1)
	end
end

----------------------------------------------------------- Русский язык.

local TesTrue4 = 0
local Tesfalse4 = 0

TestRussian = guiCreateWindow(0,0,450,150,"Русский язык",false)
centerWindow(TestRussian)
guiSetVisible(TestRussian, false)

QuestionRussian = guiCreateLabel(390, 25, 150, 150, "1/10", false, TestRussian)
guiSetFont(QuestionRussian, "default-bold-small")

QuestionRussian2 = guiCreateLabel(5, 45, 440, 150, "Вставьте пропущенную букву в слово:", false, TestRussian)
guiSetFont(QuestionRussian2, "default-bold-small")
guiLabelSetHorizontalAlign(QuestionRussian2, "center", false)

QuestionRussian3 = guiCreateLabel(5, 65, 440, 150, "", false, TestRussian)
guiSetFont(QuestionRussian3, "default-bold-small")
guiLabelSetHorizontalAlign(QuestionRussian3, "center", false)

QuestionRussian4 = guiCreateLabel(5, 65, 440, 150, "", false, TestRussian)
guiSetFont(QuestionRussian4, "default-bold-small")
guiLabelSetHorizontalAlign(QuestionRussian4, "center", false)
guiSetAlpha(QuestionRussian4,0)

editRussian = guiCreateEdit(10,100,205,35,"",false,TestRussian)
guiSetFont(editRussian,"default-bold-small")
guiEditSetMaxLength(editRussian, 1)
	
buttonRussian = guiCreateButton(230,100,205,35,"Ответить",false,TestRussian)
guiSetFont(buttonRussian,"default-bold-small")

function DrawRussianTest()
	guiSetText(editRussian,"")
	if getElementData(localPlayer,"progress") >= 11 then
        FinishRussianTest()
	else
		local d = math.random(1,#dataRussian)
		local word, answer = dataRussian[d][1], dataRussian[d][2]
		guiSetText(QuestionRussian, ""..getElementData(localPlayer,"progress").."/10")
		guiSetText(QuestionRussian3, word)
		guiSetText(QuestionRussian4, answer)
	end
end

addEventHandler("onClientGUIClick", getRootElement(), function ()
    if source == buttonRussian then
		if guiGetText(editRussian) ~= "" then
			if tostring(guiGetText(editRussian)) == tostring(guiGetText(QuestionRussian4)) then
				TesTrue4 = TesTrue4+1
			else
				Tesfalse4 = Tesfalse4+1
			end
			setElementData(localPlayer,"progress", getElementData(localPlayer,"progress") + 1)
			DrawRussianTest()
		else
            outputChatBox("#FF0000[Обучение] #FFFFFFПоля пустые, либо заполнены неверно.", 255, 255, 255, true)
        end
    end
end)

function FinishRussianTest()
	if TesTrue4 == 10 then
        guiSetVisible(TestRussian, false)
        showCursor(false)
		outputChatBox("#00FF00[Обучение] #FFFFFFВы успешно ответили на все #00FF0010 #FFFFFFпримеров. Русский язык сдан.", 255, 255, 255, true)
        setTimer(function()
            setElementData(localPlayer, "CertificateProcess", getElementData(localPlayer, "CertificateProcess") + 1)
			setElementData(localPlayer, "Russianlanguage", true)
            TesTrue4 = 0
            Tesfalse4 = 0
        end, 100, 1)
	else
        guiSetVisible(TestRussian, false)
        showCursor(false)
		outputChatBox("#FF0000[Обучение] #FFFFFFВы провалили тест. Результат: #00FF00"..TesTrue4.." #FFFFFFправильных из #00FF0010.", 255, 255, 255, true)
        setTimer(function()
            TesTrue4 = 0
            Tesfalse4 = 0
        end, 100, 1)
	end
end

----------------------------------------------------------- Тест по физике.

local answint2
local playerAnsw2
local TesTrue2 = 0
local Tesfalse2 = 0 
local GUIEditor = {
    anscheck = {},
}

local Tests2 = {}

TestPhysics = guiCreateWindow(0,0,450,200,"Тест по физике",false)
centerWindow(TestPhysics)
guiSetVisible(TestPhysics, false)

QuestionData2 = guiCreateLabel(370, 25, 150, 150, "Вопрос: 1/5", false, TestPhysics)
guiSetFont(QuestionData2, "default-bold-small")

Question2 = guiCreateLabel(5, 55, 440, 150, "", false, TestPhysics)
guiSetFont(Question2, "default-bold-small")
guiLabelSetHorizontalAlign(Question2, "center", false)

GUIEditor.anscheck[1] = guiCreateCheckBox(10, 100, 440, 25, "", false, false, TestPhysics)
guiSetFont(GUIEditor.anscheck[1], "default-bold-small")
GUIEditor.anscheck[2] = guiCreateCheckBox(10, 130, 440, 25, "", false, false, TestPhysics)
guiSetFont(GUIEditor.anscheck[2], "default-bold-small")
GUIEditor.anscheck[3] = guiCreateCheckBox(10, 160, 440, 25, "", false, false, TestPhysics)
guiSetFont(GUIEditor.anscheck[3], "default-bold-small")

function DrawPhysicsTest()
	guiCheckBoxSetSelected(GUIEditor.anscheck[1],false)
	guiCheckBoxSetSelected(GUIEditor.anscheck[2],false)
	guiCheckBoxSetSelected(GUIEditor.anscheck[3],false)
	if getElementData(localPlayer,"progress") >= 6 then
        FinishPhysicsTest()
	else
		local number = math.random(1,table.maxn(test_type2))
		if Tests2[number] == true then
			DrawBiologyTest()
			return
		end
		Tests2[number] = true
		for key,v in pairs(test_type2) do
			if (key == number) then
                guiSetText(QuestionData2, "Вопрос: "..getElementData(localPlayer,"progress").."/5")
                guiSetText(Question2, v[1])
				answint2 = v[3]
				guiSetText(GUIEditor.anscheck[1], v[2][1])
				guiSetText(GUIEditor.anscheck[2], v[2][2])
				guiSetText(GUIEditor.anscheck[3], v[2][3])
			end
		end
	end
end

addEventHandler("onClientGUIClick", getRootElement(), function ()
    if source == GUIEditor.anscheck[1] then
        for key,val in pairs(GUIEditor.anscheck) do
            if guiCheckBoxGetSelected(GUIEditor.anscheck[key]) then
                playerAnsw2 = key
            end
        end
        if playerAnsw2 == answint2 then
            TesTrue2 = TesTrue2+1
        else
            Tesfalse2 = Tesfalse2+1
        end
        setElementData(localPlayer,"progress", getElementData(localPlayer,"progress") + 1)
        DrawPhysicsTest()
    elseif source == GUIEditor.anscheck[2] then
        for key,val in pairs(GUIEditor.anscheck) do
            if guiCheckBoxGetSelected(GUIEditor.anscheck[key]) then
                playerAnsw2 = key
            end
        end
        if playerAnsw2 == answint2 then
            TesTrue2 = TesTrue2+1
        else
            Tesfalse2 = Tesfalse2+1
        end
        setElementData(localPlayer,"progress", getElementData(localPlayer,"progress") + 1)
        DrawPhysicsTest()
    elseif source == GUIEditor.anscheck[3] then
        for key,val in pairs(GUIEditor.anscheck) do
            if guiCheckBoxGetSelected(GUIEditor.anscheck[key]) then
                playerAnsw2 = key
            end
        end
        if playerAnsw2 == answint2 then
            TesTrue2 = TesTrue2+1
        else
            Tesfalse2 = Tesfalse2+1
        end
        setElementData(localPlayer,"progress", getElementData(localPlayer,"progress") + 1)
        DrawPhysicsTest()
    end
end)

function FinishPhysicsTest()
	if TesTrue2 == 5 then
        guiSetVisible(TestPhysics, false)
        showCursor(false)
		outputChatBox("#00FF00[Обучение] #FFFFFFВы успешно ответили на все #00FF005 #FFFFFFвопросов. Физика сдана.", 255, 255, 255, true)
        setTimer(function()
            setElementData(localPlayer, "CertificateProcess", getElementData(localPlayer, "CertificateProcess") + 1)
			setElementData(localPlayer, "Physics", true)
            Tests2 = {}
            TesTrue2 = 0
            Tesfalse2 = 0
        end, 100, 1)
	else
        guiSetVisible(TestPhysics, false)
        showCursor(false)
		outputChatBox("#FF0000[Обучение] #FFFFFFВы провалили тест. Результат: #00FF00"..TesTrue2.." #FFFFFFправильных из #00FF005.", 255, 255, 255, true)
        setTimer(function()
            Tests2 = {}
            TesTrue2 = 0
            Tesfalse2 = 0
        end, 100, 1)
	end
end

----------------------------------------------------------- Тест по биологии.

local answint
local playerAnsw
local TesTrue = 0
local Tesfalse = 0 
local GUIEditor = {
    anscheck = {},
}

local Tests = {}

TestBiology = guiCreateWindow(0,0,450,200,"Тест по биологии",false)
centerWindow(TestBiology)
guiSetVisible(TestBiology, false)

QuestionData = guiCreateLabel(370, 25, 150, 150, "Вопрос: 1/10", false, TestBiology)
guiSetFont(QuestionData, "default-bold-small")

Question = guiCreateLabel(5, 55, 440, 150, "", false, TestBiology)
guiSetFont(Question, "default-bold-small")
guiLabelSetHorizontalAlign(Question, "center", false)

GUIEditor.anscheck[1] = guiCreateCheckBox(10, 100, 440, 25, "", false, false, TestBiology)
guiSetFont(GUIEditor.anscheck[1], "default-bold-small")
GUIEditor.anscheck[2] = guiCreateCheckBox(10, 130, 440, 25, "", false, false, TestBiology)
guiSetFont(GUIEditor.anscheck[2], "default-bold-small")
GUIEditor.anscheck[3] = guiCreateCheckBox(10, 160, 440, 25, "", false, false, TestBiology)
guiSetFont(GUIEditor.anscheck[3], "default-bold-small")

function DrawBiologyTest()
	guiCheckBoxSetSelected(GUIEditor.anscheck[1],false)
	guiCheckBoxSetSelected(GUIEditor.anscheck[2],false)
	guiCheckBoxSetSelected(GUIEditor.anscheck[3],false)
	if getElementData(localPlayer,"progress") >= 11 then
        FinishBiologyTest()
	else
		local number = math.random(1,table.maxn(test_type))
		if Tests[number] == true then
			DrawBiologyTest()
			return
		end
		Tests[number] = true
		for key,v in pairs(test_type) do
			if (key == number) then
                guiSetText(QuestionData, "Вопрос: "..getElementData(localPlayer,"progress").."/10")
                guiSetText(Question, v[1])
				answint = v[3]
				guiSetText(GUIEditor.anscheck[1], v[2][1])
				guiSetText(GUIEditor.anscheck[2], v[2][2])
				guiSetText(GUIEditor.anscheck[3], v[2][3])
			end
		end
	end
end

addEventHandler("onClientGUIClick", getRootElement(), function ()
    if source == GUIEditor.anscheck[1] then
        for key,val in pairs(GUIEditor.anscheck) do
            if guiCheckBoxGetSelected(GUIEditor.anscheck[key]) then
                playerAnsw = key
            end
        end
        if playerAnsw == answint then
            TesTrue = TesTrue+1
        else
            Tesfalse = Tesfalse+1
        end
        setElementData(localPlayer,"progress", getElementData(localPlayer,"progress") + 1)
        DrawBiologyTest()
    elseif source == GUIEditor.anscheck[2] then
        for key,val in pairs(GUIEditor.anscheck) do
            if guiCheckBoxGetSelected(GUIEditor.anscheck[key]) then
                playerAnsw = key
            end
        end
        if playerAnsw == answint then
            TesTrue = TesTrue+1
        else
            Tesfalse = Tesfalse+1
        end
        setElementData(localPlayer,"progress", getElementData(localPlayer,"progress") + 1)
        DrawBiologyTest()
    elseif source == GUIEditor.anscheck[3] then
        for key,val in pairs(GUIEditor.anscheck) do
            if guiCheckBoxGetSelected(GUIEditor.anscheck[key]) then
                playerAnsw = key
            end
        end
        if playerAnsw == answint then
            TesTrue = TesTrue+1
        else
            Tesfalse = Tesfalse+1
        end
        setElementData(localPlayer,"progress", getElementData(localPlayer,"progress") + 1)
        DrawBiologyTest()
    end
end)

function FinishBiologyTest()
	if TesTrue == 10 then
        guiSetVisible(TestBiology, false)
        showCursor(false)
		outputChatBox("#00FF00[Обучение] #FFFFFFВы успешно ответили на все #00FF0010 #FFFFFFвопросов. Биология сдана.", 255, 255, 255, true)
        setTimer(function()
            setElementData(localPlayer, "CertificateProcess", getElementData(localPlayer, "CertificateProcess") + 1)
			setElementData(localPlayer, "Biology", true)
            Tests = {}
            TesTrue = 0
            Tesfalse = 0
        end, 100, 1)
	else
        guiSetVisible(TestBiology, false)
        showCursor(false)
		outputChatBox("#FF0000[Обучение] #FFFFFFВы провалили тест. Результат: #00FF00"..TesTrue.." #FFFFFFправильных из #00FF0010.", 255, 255, 255, true)
        setTimer(function()
            Tests = {}
            TesTrue = 0
            Tesfalse = 0
        end, 100, 1)
	end
end

-----------------------------------------------------------

function startClick()
    local player = getLocalPlayer()
	if (source == buttonEducation1) then
		setElementData(localPlayer,"progress", 1)
		setTimer(function()
			guiSetVisible(wndEducation,false)
			guiSetVisible(TestMathematics,true)
			DrawMathematicsTest()
		end, 100, 1)
	elseif (source == buttonEducation2) then
		setElementData(localPlayer,"progress", 1)
		setTimer(function()
			guiSetVisible(wndEducation,false)
			guiSetVisible(TestRussian,true)
			DrawRussianTest()
		end, 100, 1)
	elseif (source == buttonEducation3) then
		setElementData(localPlayer,"progress", 1)
		setTimer(function()
			guiSetVisible(wndEducation,false)
			guiSetVisible(TestPhysics,true)
			DrawPhysicsTest()
		end, 100, 1)
	elseif (source == buttonEducation4) then
		setElementData(localPlayer,"progress", 1)
		setTimer(function()
			guiSetVisible(wndEducation,false)
			guiSetVisible(TestBiology,true)
			DrawBiologyTest()
		end, 100, 1)
	elseif (source == buttonEducation5) then
		if getElementData(localPlayer, "CertificateProcess") >= 4 then
			triggerServerEvent("FinishEducation", localPlayer, localPlayer)
			guiSetVisible(wndEducation,false)
			showCursor(false)
		else
			if getElementData(localPlayer, "Education") == false then
				triggerServerEvent("takeMoneyEducation", localPlayer, localPlayer)
				guiSetVisible(wndEducation,false)
				showCursor(false)
			else
				outputChatBox("#FF0000[Обучение] #FFFFFFЧтобы завершить обучение, сдайте все предметы.", 255, 255, 255, true)
				outputChatBox("#FF0000[Обучение] #FFFFFFНа данный момент сдано: #00FF00"..getElementData(localPlayer, "CertificateProcess")..".", 255, 255, 255, true)
			end
		end
    elseif (source == buttonEducation6) then
        guiSetVisible(wndEducation,false)
        showCursor(false)
    end
end
addEventHandler("onClientGUIClick", getRootElement(), startClick)

addEventHandler ( "onClientPickupHit", getRootElement(), function(ply)
	if ply ~= localPlayer then return end
	if source == pickupEducation then
        local veh = getPedOccupiedVehicle(ply)
        if not veh then
			if getElementData(ply, "Certificate") == false then
				guiSetVisible(wndEducation,true)
				showCursor(true)
			
				if getElementData(ply, "Education") == true then
					guiSetEnabled(buttonEducation1,true)
					guiSetEnabled(buttonEducation2,true)
					guiSetEnabled(buttonEducation3,true)
					guiSetEnabled(buttonEducation4,true)
					guiSetText(buttonEducation5,"Завершить обучение")
				
					if getElementData(ply, "mathematics") == true then
						guiStaticImageLoadImage(imageEducation1,"files/true.png")
						guiSetEnabled(buttonEducation1,false)
					else
						guiStaticImageLoadImage(imageEducation1,"files/false.png")
						guiSetEnabled(buttonEducation1,true)
					end
				
					if getElementData(ply, "Russianlanguage") == true then
						guiStaticImageLoadImage(imageEducation2,"files/true.png")
						guiSetEnabled(buttonEducation2,false)
					else
						guiStaticImageLoadImage(imageEducation2,"files/false.png")
						guiSetEnabled(buttonEducation2,true)
					end
				
					if getElementData(ply, "Physics") == true then
						guiStaticImageLoadImage(imageEducation3,"files/true.png")
						guiSetEnabled(buttonEducation3,false)
					else
						guiStaticImageLoadImage(imageEducation3,"files/false.png")
						guiSetEnabled(buttonEducation3,true)
					end
				
					if getElementData(ply, "Biology") == true then
						guiStaticImageLoadImage(imageEducation4,"files/true.png")
						guiSetEnabled(buttonEducation4,false)
					else
						guiStaticImageLoadImage(imageEducation4,"files/false.png")
						guiSetEnabled(buttonEducation4,true)
					end
				else
					guiSetEnabled(buttonEducation1,false)
					guiSetEnabled(buttonEducation2,false)
					guiSetEnabled(buttonEducation3,false)
					guiSetEnabled(buttonEducation4,false)
					guiSetText(buttonEducation5,"Начать обучение")
				end
			else
				outputChatBox("#00FF00[Обучение] #FFFFFFУ вас уже есть аттестат.", 255, 255, 255, true)
			end
        end
	end
end)