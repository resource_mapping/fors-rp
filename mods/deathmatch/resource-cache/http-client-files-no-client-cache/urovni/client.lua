local x, y = guiGetScreenSize()
local wight, height = x/1366, y/768
local backColor = tocolor ( 10, 10, 10, 255) 
local xpColor = tocolor ( 30, 255, 0, 255) 
local newColor = tocolor ( 255, 255, 255, 210 )
local drawingTimeXP = 2000
local timeToIncrease = 1000
local drawinTimeStart = 0
local xp = 450
local level = 0
local size = 0

local tempXP = 0
local drawingXP = false
local increasingXP = false
local lineWidth, lineHeight = 150, 16

hudFont = dxCreateFont("font.ttf",10)

addEventHandler( "onClientRender", root,
    function()
		local xp = getElementData ( localPlayer, "player:xp" )
		if xp then
			if (not isPlayerMapVisible()) then
				--dxDrawRectangle ( 20, y-185, lineWidth, lineHeight, backColor, false )
				local level = getElementData (localPlayer,"player:level")+1
				if drawingXP then
					if increasingXP then
						size = interpolateBetween ( 0, 0, 0, tempXP/(level*200)*lineWidth, 0, 0, (getTickCount()-drawinTimeStart)/drawingTimeXP, "Linear" )
					end
					--dxDrawRectangle ( 20+(xp-tempXP)/(level*200)*lineWidth+size, y-185, tempXP/(level*200)*lineWidth-size, lineHeight, newColor, false )
				
					--dxDrawRectangle ( 20, y-185, (xp-tempXP)/(level*200)*lineWidth+size, lineHeight, xpColor, false )
				else
					--dxDrawRectangle ( 20, y-185, xp/(level*200)*lineWidth, lineHeight, xpColor, false )
				end
				dxDrawBorderedText ( "Опыт: "..xp.." из "..(level*200), 40, y-185, lineWidth, lineHeight, tocolor(255,255,255), 1, hudFont, "center", "top", false, false, false)
			end	
		end
    end
)

function playerGivenXP (quant)
	size = 0
	tempXP = quant
	
	setTimer (function() drawingXP = false tempXP = 0 end, drawingTimeXP+timeToIncrease,1)
	setTimer ( function ()drawinTimeStart = getTickCount() increasingXP = true end, timeToIncrease, 1 )
	drawingXP = true
end
addEvent ( "trailer:playerGivenXP", true )
addEventHandler ( "trailer:playerGivenXP", root, playerGivenXP )

function dxDrawBorderedText( text, x, y, w, h, color, scale, font, alignX, alignY, clip, wordBreak, postGUI )
	dxDrawText ( text, x - 1, y - 1, w - 1, h - 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x + 1, y - 1, w + 1, h - 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x - 1, y + 1, w - 1, h + 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x + 1, y + 1, w + 1, h + 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x - 1, y, w - 1, h, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x + 1, y, w + 1, h, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x, y - 1, w, h - 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x, y + 1, w, h + 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x, y, w, h, color, scale, font, alignX, alignY, clip, wordBreak, postGUI )
end