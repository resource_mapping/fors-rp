-- TrollingCont. 15.02.2020

local notifyTextFont = nil
local text = nil
local width, height = guiGetScreenSize()
local startTime = nil

function renderNotify()
    if not text then return end
    local textWidth = dxGetTextWidth(text, 1, notifyTextFont)
    --dxDrawText(tostring(textWidth), 400, 400)
    dxDrawRectangle((width - textWidth + 30) / 2, 60, textWidth + 30, 50, 0xA0101010)
    dxDrawText(text, (width - textWidth + 30) / 2, 75, (width - textWidth + 30) / 2 + textWidth + 30, 125, 0xFFFFFFFF, 1.0, notifyTextFont, "center", "top", false, true)

    if getTickCount() - startTime > 5000 then
        removeEventHandler("onClientRender", root, renderNotify)
    end
end

function startup()
    addEvent("tcAdminNotify", true)

    notifyTextFont = dxCreateFont("Res/Common/Roboto-Bold.ttf", 16)

    addEventHandler("tcAdminNotify", root,
    function(notifyText)
        text = notifyText
        startTime = getTickCount()
        addEventHandler("onClientRender", root, renderNotify)
    end)
end

addEventHandler("onClientResourceStart", resourceRoot, startup)