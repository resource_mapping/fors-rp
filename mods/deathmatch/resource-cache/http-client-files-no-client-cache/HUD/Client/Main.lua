-- TrollingCont. 15.02.2020

local width, height = guiGetScreenSize()
local posX,posY = width-480, height-240

local textures = {}
local fonts = {}
local playerHealth = nil
local moneyExplodedStr = nil
local money = nil
local newMoneyVal = nil
local currentVehicle = false
local armor = nil

local sAlert = ""

local wantedData = nil
local baseXOffset = 600

wantedReasons = {
    [1] = { name="1.1 УК РФ" },
    [2] = { name="1.2 УК РФ" }
}

local indicators =
{
	{
        img = dxCreateTexture("Res/Vehicle/icon_brake.png"),
	    func = function()
		    return getPedControlState(localPlayer, "brake_reverse")
        end
    },
	{
        img = dxCreateTexture("Res/Vehicle/icon_fuel.png"), -- Почему у топлива иконка масла?
	    func = function()
            --local fFuel = veh:GetFuel()
            local fFuel = 400
            --local fFuelMax = veh:GetMaxFuel()
            local fFuelMax = 400
	    	if fFuel == 0 then
		    	sAlert = "Нет топлива"
			    return true
		    elseif fFuel/fFuelMax <= 0.1 then
			    sAlert = "У вас мало топлива"
			    return true
		    end
        end
    },
	{
        img = dxCreateTexture("Res/Vehicle/icon_engine.png"),
	    func = function()
            --local state = veh:IsBroken()
            local state = false
		    if state then
			    sAlert = "Двигатель сломан"
			    return true
		    end
        end
    }
}

function roundNumber(num)
	local s = math.floor(num)
	if num - s >= 0.5 then
		return s + 1
	else
		return s
	end
end

function explodeNumber(number)
	number = tostring(number)
	local k
	repeat
		number, k = string.gsub(number, "^(-?%d+)(%d%d%d)", '%1 %2')
	until (k==0)	-- true - выход из цикла
	return number
end

function dxDrawCircle( posX, posY, radius, width, angleAmount, startAngle, stopAngle, color, postGUI )
	if ( type( posX ) ~= "number" ) or ( type( posY ) ~= "number" ) then
		return false
	end
 
	local function clamp( val, lower, upper )
		if ( lower > upper ) then lower, upper = upper, lower end
		return math.max( lower, math.min( upper, val ) )
	end
 
	radius = type( radius ) == "number" and radius or 50
	width = type( width ) == "number" and width or 5
	angleAmount = type( angleAmount ) == "number" and angleAmount or 1
	startAngle = clamp( type( startAngle ) == "number" and startAngle or 0, 0, 360 )
	stopAngle = clamp( type( stopAngle ) == "number" and stopAngle or 360, 0, 360 )
	color = color or tocolor( 255, 255, 255, 200 )
	postGUI = type( postGUI ) == "boolean" and postGUI or false
 
	if ( stopAngle < startAngle ) then
		local tempAngle = stopAngle
		stopAngle = startAngle
		startAngle = tempAngle
	end
 
	for i = startAngle, stopAngle, angleAmount do
		local startX = math.cos( math.rad( i ) ) * ( radius - width )
		local startY = math.sin( math.rad( i ) ) * ( radius - width )
		local endX = math.cos( math.rad( i ) ) * ( radius + width )
		local endY = math.sin( math.rad( i ) ) * ( radius + width )
 
		dxDrawLine( startX + posX, startY + posY, endX + posX, endY + posY, color, width, postGUI )
	end
 
	return true
end

function getRPM(this)
    local iVehicleRPM =  0

    local bEngine = getVehicleEngineState(this);
    if ( isVehicleOnGround(this) ) then
        if ( bEngine ) then
            local fSpeed = getSpeed(this);

            if ( getVehicleCurrentGear(this) > 0 ) then
                iVehicleRPM = math.floor( ( ( fSpeed / getVehicleCurrentGear(this) )* 180 ) + 0.5 );

                if ( iVehicleRPM < 650 ) then
                    iVehicleRPM = math.random( 650, 750 );
                elseif ( iVehicleRPM >= 9800 ) then
                    iVehicleRPM = 9800;
                end
            else
                iVehicleRPM = math.floor( ( ( fSpeed / 1 ) * 180 ) + 0.5 );

                if ( iVehicleRPM < 650 ) then
                    iVehicleRPM = math.random( 650, 750 );
                elseif ( iVehicleRPM >= 9800 ) then
                    iVehicleRPM = 9800;
                end
            end
        else
            iVehicleRPM = 0;
        end
    else
        if ( bEngine ) then
            iVehicleRPM = iVehicleRPM - 150;

            if ( iVehicleRPM < 650 ) then
                iVehicleRPM = math.random( 650, 750 );
            elseif ( iVehicleRPM >= 9800 ) then
                iVehicleRPM = 9800;
            end
        else
            iVehicleRPM = 0;
        end
    end

    return iVehicleRPM;
end

function getSpeed(veh)
    local sX, sY, sZ = getElementVelocity(currentVehicle)
    return math.sqrt(sX*sX + sY*sY + sZ*sZ) * 180
end

function renderMain()

    dxDrawImage(width - 236, 60, 176, 44, textures.moneyPanel)

    dxDrawImage(60, 60, 176, 44, textures.logo)
    dxDrawText("ASTRORP", 110, 67, leftX, topY, white, 1.0, fonts.fontRT11)
    dxDrawText("Южный город", 110, 84, leftX, topY, white, 1.0, fonts.fontRL10)

    newMoneyVal = getPlayerMoney(localPlayer)
    if newMoneyVal ~= money then
        moneyExplodedStr = explodeNumber(newMoneyVal)
        money = newMoneyVal
    end
    dxDrawText(moneyExplodedStr, width - 211, 73, width - 101, topY, white, 1.0, fonts.font13, "right")

    armor = getPedArmor(localPlayer)
    if armor ~= 0 then
        dxDrawRectangle(width - 275, 60, 35, 44, 0x80000000)
        dxDrawText(roundNumber(armor), width - 266, 64, width - 250, topY, white, 1.0, fonts.font11, "center")
        dxDrawImage(width - 268, 89, 21, 25, textures.armor)
    end

    dxDrawRectangle(width - 236, 116, 176, 16, 0x80000000)
    playerHealth = getElementHealth(localPlayer)
    dxDrawRectangle(width - 236, 116, roundNumber(playerHealth * 1.76), 16, 0xFFE00000)
    dxDrawImage(width - 220, 114, 23, 23, textures.healthIcon)
    dxDrawText(roundNumber(playerHealth), width - 195, 114, leftX, topY, white, 1.0, fonts.font11)

    --dxDrawRectangle(width - 220, 75, 110, 20, 0xA0FF0000)
end

function renderVehicleHud()
    currentVehicle = getPedOccupiedVehicle(localPlayer)
    if not currentVehicle then return end

    local speed = getSpeed(currentVehicle)

    dxDrawImage(posX, posY, 291, 220, textures.speedom)

    dxDrawText(tostring(roundNumber(speed)), posX, posY + 58, posX + 291, posY + 128, white, 1.0, fonts.font, "center", "center")

    local iSpeed = getSpeed(currentVehicle)
    local fRPM = getRPM(currentVehicle)
    local fRotation = ( 360 - 120 ) / ( 9800 / math.floor( fRPM + 0.5 ) )
    if not hide then
        dxDrawCircle(posX + 140, posY + 128, 92, 10, 4, 129+math.floor(fRotation), 130, tocolor(39, 118, 185, 255))
    end

    local indicatorsX, indicatorsY = posX + 291 + 20, posY + 26

	for k,v in pairs(indicators) do
		if v.func() then
			dxDrawImage(indicatorsX, indicatorsY, 29, 22, v.img)
		end
		indicatorsY = indicatorsY + 42
    end
    
    local circleIndicatorsX,circleIndicatorsY = indicatorsX + 60, posY + 17

    dxDrawImage(circleIndicatorsX, circleIndicatorsY, 82, 78, textures.tempInd)
    dxDrawImage(circleIndicatorsX - 60, posY - 12, 134, 134, textures.needle, 220)

    circleIndicatorsY = circleIndicatorsY + 110

    --local fFuel = veh:GetFuel()
	--local fFuelMax = veh:GetMaxFuel()
    --local fRotation = Lerp(270,160,fFuel/fFuelMax)
    local fFuel = 3
    local fFuelMax = 32
    local fRotation = 210
	dxDrawImage(circleIndicatorsX, circleIndicatorsY, 87, 81, textures.fuelInd)
    dxDrawImage(circleIndicatorsX - 60, posY + 104, 134, 134, textures.needle, fRotation)
    
    if sAlert ~= "" then
        dxDrawImage(width - 300, posY - 63, 300, 37, textures.infoBg)
        dxDrawText(sAlert, posX + 210, posY - 63, 300, posY - 26, 0xFFFFFFFF, 1, fonts.fontIt20, "left", "center")
    end
end

function renderWeapon()
    --local iWeapon = localPlayer:getWeapon()
    local iWeapon = getPedWeapon(localPlayer)
    --dxDrawText(tostring(iWeapon), 600, 300, leftX, topY)
	if iWeapon ~= 0 then
		if fileExists("Res/Weapons/"..iWeapon..".png") then
			local px, py, sx, sy = width - 180, height - 100, 155, 54
			dxDrawRectangle(px, py+5, sx, 44, 0xBF181818)
			dxDrawRectangle(px + sx - 45, py, 54, 54, 0xFF181818)
			--local size = ammoIconSize[iWeapon] or {40,40}
			dxDrawImage(px + sx - 41, py + 4, 45, 45, "Res/Weapons/"..iWeapon..".png")

			local clip = getPedAmmoInClip(localPlayer) or 0
			local total = getPedTotalAmmo(localPlayer) - clip or 0

			dxDrawText(clip.."/", px, py, px + 45, py + 54, 0xFFFFFFFF, 1, fonts.boldSmall, "right", "center")
			dxDrawText(total, px + 46, py, px, py + 54, 0xFFFFFFFF, 1, fonts.boldBig, "left", "center")
		end
	end
end

local notificationText = ""
local notificationStartTime = nil
local notificationDuration = nil
local isShown = false
local wndWidth, wndHeight = 430, 90
local wndXOffset = (width - wndWidth) / 2

function showNotification(show, text, duration)
    if show then
        notificationStartTime = getTickCount()
        notificationDuration = duration
        notificationText = text
        if not isShown then
            addEventHandler("onClientRender", root, renderNotification)
            isShown = true
        end
    elseif not show and isShown then
        removeEventHandler("onClientRender", root, renderNotification)
        isShown = false
    end
end

function renderNotification()
    if getTickCount() - notificationStartTime >= notificationDuration then
        removeEventHandler("onClientRender", root, renderNotification)
        isShown = false
        return
    end
    dxDrawRectangle(wndXOffset, height - 150, 430, 90, 0xA0000000)
    dxDrawImage(wndXOffset + 30, height - 125, 42, 42, textures.info)
    --dxDrawRectangle(wndXOffset + 80, height - 145, 340, 80, 0xA0FF0000)
    dxDrawText(notificationText, wndXOffset + 80, height - 145, wndXOffset + 420, height - 65, white, 1, "default", "center", "center", false, true)
end

function renderWanted()
    if not wantedData then return end

    local testCoeff = 200

    dxDrawText("Вас разыскивает полиция", 300, height - 90 - testCoeff, leftX, topY, white, 1, fonts.robotoBold)

    local xOffset = baseXOffset

    --wantedData = { [1] = 1, [2] = 2 }

    for i,id in ipairs(wantedData) do
        --dxDrawRectangle(xOffset, height - 100 - testCoeff, 120, 50, 0xA0000000)
        dxDrawImage(xOffset, height - 100 - testCoeff, 100, 37, textures.wantedBg)
        dxDrawText(wantedReasons[id].name, xOffset + 10, height - 100 - testCoeff, xOffset + 90, height - 100 - testCoeff + 37, white, 1, fonts.font13, "center", "center")
        xOffset = xOffset + 150
    end
end

function startup()

    textures.healthIcon = dxCreateTexture("Res/Common/Health.png")
    textures.healthStrip = dxCreateTexture("Res/Common/health1.png")
    textures.logo = dxCreateTexture("Res/Common/hud2.png")
    textures.moneyPanel = dxCreateTexture("Res/Common/ui.png")
    textures.speedom = dxCreateTexture("Res/Vehicle/speed_frame.png")
    textures.fuelInd = dxCreateTexture("Res/Vehicle/fuel_frame.png")
    textures.tempInd = dxCreateTexture("Res/Vehicle/temp_frame.png")
    textures.needle = dxCreateTexture("Res/Vehicle/needle.png")
    textures.armor = dxCreateTexture("Res/Common/armor.png")
    textures.infoBg = dxCreateTexture("Res/Vehicle/info_bg.png")
    textures.wantedBg = dxCreateTexture("Res/Common/wanted_bg.png")
    textures.info = dxCreateTexture("Res/Common/Info.png")

    fonts.font11 = dxCreateFont("Res/Fonts/Roboto-Bold.ttf", 11)
    fonts.font13 = dxCreateFont("Res/Fonts/Roboto-Bold.ttf", 13)
    fonts.fontRL10 = dxCreateFont("Res/Fonts/Roboto-Light.ttf", 10)
    fonts.fontRT11 = dxCreateFont("Res/Fonts/Roboto-Medium.ttf", 11)
    fonts.font = dxCreateFont("Res/Fonts/NeoSansProBold.ttf", 25)
    fonts.fontIt20 = dxCreateFont("Res/Fonts/NeoSansProBoldItalic.ttf", 18)
    fonts.boldSmall = dxCreateFont("Res/Fonts/Roboto-Bold.ttf", 10)
    fonts.boldBig = dxCreateFont("Res/Fonts/Roboto-Bold.ttf", 13)
    fonts.robotoBold = dxCreateFont("Res/Fonts/Roboto-Bold.ttf", 12)

    setPlayerHudComponentVisible("ammo", false)
    setPlayerHudComponentVisible("clock", false)
    setPlayerHudComponentVisible("health", false)
    setPlayerHudComponentVisible("money", false)
    setPlayerHudComponentVisible("weapon", false)
    setPlayerHudComponentVisible("armour", false)
    setPlayerHudComponentVisible("breath", false)

    money = getPlayerMoney(localPlayer)
    moneyExplodedStr = explodeNumber(money)
    armor = getPedArmor(localPlayer)

    addEventHandler("onClientRender", root, renderMain)
    addEventHandler("onClientRender", root, renderVehicleHud)
    addEventHandler("onClientRender", root, renderWeapon)
    addEventHandler("onClientRender", root, renderWanted)

    wantedData = getElementData(localPlayer, "tcfs.wantedStatus")

    addEventHandler("onClientElementDataChange", localPlayer,
    function(key, oldVal, newVal)
        if key == "tcfs.wantedStatus" then
            wantedData = newVal
        end
    end)
end

addEventHandler("onClientResourceStart", resourceRoot, startup)