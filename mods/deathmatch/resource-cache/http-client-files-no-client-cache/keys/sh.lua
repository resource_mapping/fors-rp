caseFon = {
	["noob"]  =	{"donat_casino_1_bg","donat_casino_1_icon","Кейс 45Р.\n«НОВИЧОК»",tocolor(0,0,0),45},
	["zver"]  =	{"donat_casino_2_bg","donat_casino_2_icon","Кейс 65Р.\n«ЗВЕРЬЁ»",tocolor(255,0,255),65},
	["yda"]   =	{"donat_casino_3_bg","donat_casino_3_icon","Кейс 149Р.\n«НА УДАЧУ»",tocolor(255,0,0),149},
	["major"] = {"donat_casino_4_bg","donat_casino_4_icon","Кейс 249Р.\n«МАЖОР»",tocolor(0,0,255),249},

}
-- ТИПЫ призов
-- vehicle , money,vip,bilet,donate


-- Прописи
-- Картинка (без пути и .png), название, тип , модель(в некоторых случаях сумма), стоимость продажи -- ниже
-- процент чем выше тем меньше шанс выподения от 1 до 95 выше 95 это топовые (вроде как сути не имеит =D)
-- By loki_jon
prizCase = {
	["noob"] = { 
		{"1_1","Nissan GTR 2019","vehicle",400,10,1},
		{"1_2","loki_jon","vehicle",401,10,15},
		{"1_3","Nissan Silvia S15","vehicle",402,10,30},
		{"1_4","Hounda Accord","vehicle",403,10,60},
		{"1_5","Mitsybishi Lancer Evo X","vehicle",404,10,55},
		{"1_6","BMW M5F90","vehicle",405,10,48},
		{"1_7","Subaru STI","vehicle",406,10,39},
		{"1_8","Toyota","vehicle",407,10,30},
		{"1_9","1111111111111111","vehicle",408,10,12},
		{"1_10","222222222222222","vehicle",409,10,84},
		{"1_11","3333333333333","vehicle",410,10,23},
		{"1_12","44444444444444","vehicle",411,10,89},
		{"1_13","55555555555555","vehicle",412,10,61},
		{"1_14","666666666666","vehicle",413,10,16},
	},
	
	["zver"] = {
		{"2_1","Nissan GTR 2019","vehicle",411,10},
		{"2_2","Nissan GTR 2019","vehicle",411,10},
		{"2_3","Nissan GTR 2019","vehicle",411,10},
		{"2_4","Nissan GTR 2019","vehicle",411,10},
		{"2_5","Nissan GTR 2019","vehicle",411,10},
		{"2_6","Nissan GTR 2019","vehicle",411,10},
		{"2_7","Nissan GTR 2019","vehicle",411,10},
		{"2_8","Nissan GTR 2019","vehicle",411,10},
		{"2_9","Nissan GTR 2019","vehicle",411,10},
		{"2_10","Nissan GTR 2019","vehicle",411,10},
		{"2_11","Nissan GTR 2019","vehicle",411,10},
		{"2_12","Nissan GTR 2019","vehicle",411,10},
		{"2_13","Nissan GTR 2019","vehicle",411,10},
		{"2_14","Nissan GTR 2019","vehicle",411,10},
	},
	["yda"] = {
		{"3_1","Nissan GTR 2019","vehicle",411,10},
		{"3_2","Nissan GTR 2019","vehicle",411,10},
		{"3_3","Nissan GTR 2019","vehicle",411,10},
		{"3_4","Nissan GTR 2019","vehicle",411,10},
		{"3_5","Nissan GTR 2019","vehicle",411,10},
		{"3_6","Nissan GTR 2019","vehicle",411,10},
		{"3_7","Nissan GTR 2019","vehicle",411,10},
		{"3_8","Nissan GTR 2019","vehicle",411,10},
		{"3_9","Nissan GTR 2019","vehicle",411,10},
		{"3_10","Nissan GTR 2019","vehicle",411,10},
		{"3_11","Nissan GTR 2019","vehicle",411,10},
		{"3_12","Nissan GTR 2019","vehicle",411,10},
		{"3_13","Nissan GTR 2019","vehicle",411,10},
		{"3_14","Nissan GTR 2019","vehicle",411,10},
	},
	["major"] = {
		{"4_1","Nissan GTR 2019","vehicle",411,10},
		{"4_2","Nissan GTR 2019","vehicle",411,10},
		{"4_3","Nissan GTR 2019","vehicle",411,10},
		{"4_4","Nissan GTR 2019","vehicle",411,10},
		{"4_5","Nissan GTR 2019","vehicle",411,10},
		{"4_6","Nissan GTR 2019","vehicle",411,10},
		{"4_7","Nissan GTR 2019","vehicle",411,10},
		{"4_8","Nissan GTR 2019","vehicle",411,10},
		{"4_9","Nissan GTR 2019","vehicle",411,10},
		{"4_10","Nissan GTR 2019","vehicle",411,10},
		{"4_11","Nissan GTR 2019","vehicle",411,10},
		{"4_12","Nissan GTR 2019","vehicle",411,10},
		{"4_13","Nissan GTR 2019","vehicle",411,10},
		{"4_14","Nissan GTR 2019","vehicle",411,10},
	}
	
}