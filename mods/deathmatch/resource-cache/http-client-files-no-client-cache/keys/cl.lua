local scx, scy = guiGetScreenSize()
GUI = {
	mainCaseOpen = false,
	screen = 'homeCase',
	data1 = nil,
	data2 = nil,
	data3 = nil,
	data4 = nil,

	case = "noob",
	casePrize = nil,
	numberPrize = nil,
	color = nil,
	elem = {},
	window = {}
}
white = tocolor(255,255,255)

puti1 = "images/"
puti2 = "images/casino_items/"

function calculateGUI()
	local scaleX, scaleY = scx/1280, scy/959
	GUI.elem.background = {				
		left	= 0, 	
		top		= scy/2+70,								
		width	= scx,
		height	= scy/2-70
	}
	GUI.elem.background2 = {				
		left	= 0, 	
		top		= 0,								
		width	= scx,
		height	= scy
	}
	GUI.elem.textKeis = {				
		left	= 50, 	
		top		= 50,								
		width	= scx,
		height	= scy
	}
	GUI.elem.imageMoney = {				
		left	= scx-400, 	
		top		= 50,								
		width	= 35,
		height	= 35
	}
	GUI.elem.textMoney = {				
		left	= GUI.elem.imageMoney.left+40, 	
		top		= 50,								
		width	= 350,
		height	= scy
	}
	

	GUI.elem.caseNoob = {				
		left	= (scx-215*4+40)/2.5, 	
		top		= 200,								
		width	= 215,
		height	= 175
	}
	GUI.elem.caseNoobcnop = {				
		left	= GUI.elem.caseNoob.left+215/5, 	
		top		= GUI.elem.caseNoob.top+GUI.elem.caseNoob.height+20,								
		width	= 125,
		height	= 35
	}
	GUI.elem.caseNoobText = {				
		left	= GUI.elem.caseNoob.left, 	
		top		= GUI.elem.caseNoob.top-20,								
		width	= GUI.elem.caseNoob.width,
		height	= GUI.elem.caseNoob.height
	}

	GUI.elem.caseZver = {				
		left	= GUI.elem.caseNoob.left+GUI.elem.caseNoob.width+10, 	
		top		= GUI.elem.caseNoob.top,								
		width	= GUI.elem.caseNoob.width,
		height	= GUI.elem.caseNoob.height
	}
	GUI.elem.caseZvercnop = {				
		left	= GUI.elem.caseZver.left+215/5, 	
		top		= GUI.elem.caseZver.top+GUI.elem.caseZver.height+20,								
		width	= 125,
		height	= 35
	}
	GUI.elem.caseZverText = {				
		left	= GUI.elem.caseZver.left, 	
		top		= GUI.elem.caseZver.top-20,								
		width	= GUI.elem.caseZver.width,
		height	= GUI.elem.caseZver.height
	}

	GUI.elem.caseYda = {				
		left	= GUI.elem.caseZver.left+GUI.elem.caseZver.width+10, 	
		top		= GUI.elem.caseNoob.top,								
		width	= GUI.elem.caseNoob.width,
		height	= GUI.elem.caseNoob.height
	}
	GUI.elem.caseYdacnop = {				
		left	= GUI.elem.caseYda.left+215/5, 	
		top		= GUI.elem.caseYda.top+GUI.elem.caseYda.height+20,								
		width	= 125,
		height	= 35
	}
	GUI.elem.caseYdaText = {				
		left	= GUI.elem.caseYda.left, 	
		top		= GUI.elem.caseYda.top-20,								
		width	= GUI.elem.caseYda.width,
		height	= GUI.elem.caseYda.height
	}

	GUI.elem.caseMajor = {				
		left	= GUI.elem.caseYda.left+GUI.elem.caseYda.width+10, 	
		top		= GUI.elem.caseNoob.top,								
		width	= GUI.elem.caseNoob.width,
		height	= GUI.elem.caseNoob.height
	}
	GUI.elem.caseMajorcnop = {				
		left	= GUI.elem.caseMajor.left+215/5, 	
		top		= GUI.elem.caseMajor.top+GUI.elem.caseMajor.height+20,								
		width	= 125,
		height	= 35
	}
	GUI.elem.caseMajorText = {				
		left	= GUI.elem.caseMajor.left, 	
		top		= GUI.elem.caseMajor.top-20,								
		width	= GUI.elem.caseMajor.width,
		height	= GUI.elem.caseMajor.height
	}
end
calculateGUI()


Exo = dxCreateFont("images/Exo2-ExtraLight.otf", 18)
Exo2 = dxCreateFont("images/Exo2-ExtraLight.otf", 25)

local myScreenSource

function cleanmyscreen()
	if myScreenSource then
		dxUpdateScreenSource( myScreenSource )                  
		dxDrawImage(scx - scx, scy - scy, scx, scy, myScreenSource, 0, 0, 0, tocolor(255,255,255, 255), true)      
	end
end

addEventHandler("onClientRender", root, function()
	local x, y = getCursorPosition()
	x = (x or 0) * scx
	y = (y or 0) * scy
	if (GUI.mainCaseOpen) then
		if (GUI.screen == "homeCase") then
			for k,v in pairs(caseFon) do
				if k == GUI.case then
					drawElem(GUI.elem.background2, puti1.."fon.png")		
					drawElem(GUI.elem.background, puti1..v[1]..".png")		
					GUI.color = v[4]
					if isCursorInElement(GUI.elem.caseNoob) then
						GUI.case = "noob"
						GUI.color = v[4]
					elseif isCursorInElement(GUI.elem.caseZver) then
						GUI.case = "zver"
						GUI.color = v[4]
					elseif isCursorInElement(GUI.elem.caseYda) then
						GUI.case = "yda"
						GUI.color = v[4]
					elseif isCursorInElement(GUI.elem.caseMajor) then
						GUI.case = "major"
						GUI.color = v[4]
					end
				end
			end
			drawTextInElem(GUI.elem.textKeis, "ОТКРЫТИЕ КЕЙСОВ:", 3.0, "default-bold", "left", "top", false, white)
			drawTextInElem(GUI.elem.textMoney, exports.bank:getPlayerBankMoney("DONATE").." ед.", 3.0, "default-bold", "left", "top", false, white)

			drawElem(GUI.elem.imageMoney, puti1.."section_donat.png")	

			drawElem(GUI.elem.caseNoob, puti1..caseFon["noob"][2]..".png")	
			drawTextInElem(GUI.elem.caseNoobText, caseFon["noob"][3], 2.0, "default-bold", "left", "top", false, white)
			drawElem(GUI.elem.caseZver, puti1..caseFon["zver"][2]..".png")	
			drawTextInElem(GUI.elem.caseZverText, caseFon["zver"][3], 2.0, "default-bold", "left", "top", false, white)
			drawElem(GUI.elem.caseYda, puti1..caseFon["yda"][2]..".png")
			drawTextInElem(GUI.elem.caseYdaText, caseFon["yda"][3], 2.0, "default-bold", "left", "top", false, white)
			drawElem(GUI.elem.caseMajor, puti1..caseFon["major"][2]..".png")			
			drawTextInElem(GUI.elem.caseMajorText, caseFon["major"][3], 2.0, "default-bold", "left", "top", false, white)
			drawElem(GUI.elem.caseNoobcnop, puti1.."donat_casino_button_bg.png",(isCursorInElement(GUI.elem.caseNoobcnop) and tocolor(187,26,37) or tocolor(130,18,27)))			
			drawTextInElem(GUI.elem.caseNoobcnop, "Открыть", 1.7, "default-bold", "center", "center", false, white)
			drawElem(GUI.elem.caseZvercnop, puti1.."donat_casino_button_bg.png",(isCursorInElement(GUI.elem.caseZvercnop) and tocolor(31,99,182) or tocolor(25,81,150)))			
			drawTextInElem(GUI.elem.caseZvercnop, "Открыть", 1.7, "default-bold", "center", "center", false, white)
			drawElem(GUI.elem.caseYdacnop, puti1.."donat_casino_button_bg.png",(isCursorInElement(GUI.elem.caseYdacnop) and tocolor(34,179,117) or tocolor(26,131,85)))			
			drawTextInElem(GUI.elem.caseYdacnop, "Открыть", 1.7, "default-bold", "center", "center", false, white)
			drawElem(GUI.elem.caseMajorcnop, puti1.."donat_casino_button_bg.png",(isCursorInElement(GUI.elem.caseMajorcnop) and tocolor(206,121,6) or tocolor(135,79,1)))			
			drawTextInElem(GUI.elem.caseMajorcnop, "Открыть", 1.7, "default-bold", "center", "center", false, white)
			local img = 0
			local stroka = 0
			local otstyp = 150
			cbx = 0
			for k,v in pairs(prizCase[GUI.case]) do
				GUI.elem.priz = {				
					left	= GUI.elem.background.left+(150*7-125*7)+cbx, 	
					top		= GUI.elem.background.top+20+ stroka*otstyp,								
					width	= 130,
					height	= 130
				}
				drawElem(GUI.elem.priz, puti2..v[1]..".png")
				cbx = cbx + 150
				img = img + 1
				if img == 7 then
					img = 0
					stroka = stroka + 1
					cbx = 0
				end
				if isCursorInElement(GUI.elem.priz) then
					GUI.elem.cursor = {				
						left	= x+10, 	
						top		= y+10,								
						width	= 250,
						height	= 35
					}
					drawRen(GUI.elem.cursor,GUI.color)
					drawTextInElem2(GUI.elem.cursor, v[2], 1.8, "default", "center", "center", false, white)
				end
			end
		elseif (GUI.screen == "prizeCase") then
			for k,v in pairs(caseFon) do
				if k == GUI.case then
					drawElem(GUI.elem.background2, puti1.."fon.png")		
					drawElem(GUI.elem.background, puti1..v[1]..".png")		
					GUI.color = v[4]
				end
			end
			drawTextInElem(GUI.elem.textKeis, "ОТКРЫТИЕ КЕЙСОВ:", 3.0, "default-bold", "left", "top", false, white)
			drawTextInElem(GUI.elem.textMoney, exports.bank:getPlayerBankMoney("DONATE").." ед.", 3.0, "default-bold", "center", "top", false, white)

			drawElem(GUI.elem.caseNoob, puti1..caseFon["noob"][2]..".png")	
			drawTextInElem(GUI.elem.caseNoobText, caseFon["noob"][3], 2.0, "default-bold", "left", "top", false, white)
			drawElem(GUI.elem.caseZver, puti1..caseFon["zver"][2]..".png")	
			drawTextInElem(GUI.elem.caseZverText, caseFon["zver"][3], 2.0, "default-bold", "left", "top", false, white)
			drawElem(GUI.elem.caseYda, puti1..caseFon["yda"][2]..".png")
			drawTextInElem(GUI.elem.caseYdaText, caseFon["yda"][3], 2.0, "default-bold", "left", "top", false, white)
			drawElem(GUI.elem.caseMajor, puti1..caseFon["major"][2]..".png")			
			drawTextInElem(GUI.elem.caseMajorText, caseFon["major"][3], 2.0, "default-bold", "left", "top", false, white)
			drawElem(GUI.elem.caseNoobcnop, puti1.."donat_casino_button_bg.png",tocolor(130,18,27))			
			drawTextInElem(GUI.elem.caseNoobcnop, "Открыть", 1.7, "default-bold", "center", "center", false, white)
			drawElem(GUI.elem.caseZvercnop, puti1.."donat_casino_button_bg.png",tocolor(25,81,150))			
			drawTextInElem(GUI.elem.caseZvercnop, "Открыть", 1.7, "default-bold", "center", "center", false, white)
			drawElem(GUI.elem.caseYdacnop, puti1.."donat_casino_button_bg.png",tocolor(26,131,85))		
			drawTextInElem(GUI.elem.caseYdacnop, "Открыть", 1.7, "default-bold", "center", "center", false, white)
			drawElem(GUI.elem.caseMajorcnop, puti1.."donat_casino_button_bg.png",tocolor(135,79,1))		
			drawTextInElem(GUI.elem.caseMajorcnop, "Открыть", 1.7, "default-bold", "center", "center", false, white)
			local img = 0
			local stroka = 0
			local otstyp = 150
			cbx = 0
			for k,v in pairs(prizCase[GUI.case]) do
				GUI.elem.priz = {				
					left	= GUI.elem.background.left+(150*7-125*7)+cbx, 	
					top		= GUI.elem.background.top+20+ stroka*otstyp,								
					width	= 130,
					height	= 130
				}
				drawElem(GUI.elem.priz, puti2..v[1]..".png")
				cbx = cbx + 150
				img = img + 1
				if img == 7 then
					img = 0
					stroka = stroka + 1
					cbx = 0
				end
			end
			drawElem(GUI.elem.background2, puti1.."donat_casino_prize_bg.png",tocolor(255,255,255,200))	
			for k,v in pairs(prizCase[GUI.casePrize]) do
				if k == GUI.numberPrize then
					GUI.elem.prizData = {				
						left	= scx/2-350/2, 	
						top		= scy/2-350/2,								
						width	= 350,
						height	= 350
					}
					GUI.elem.prizData2 = {				
						left	= scx/2-130/2, 	
						top		= scy/2-130/2,								
						width	= 130,
						height	= 130
					}
					GUI.elem.give = {				
						left	= GUI.elem.prizData2.left-190/2, 	
						top		= GUI.elem.prizData2.top+GUI.elem.prizData2.height+50,								
						width	= 155,
						height	= 40
					}
					GUI.elem.take = {				
						left	= GUI.elem.give.left+GUI.elem.give.width+20, 	
						top		= GUI.elem.give.top,								
						width	= GUI.elem.give.width,
						height	= GUI.elem.give.height
					}
					
					--drawTextInElem(GUI.elem.caseNoobText, GUI.numberPrize, 1.0, Exo2, "center", "center", false, white)

					drawElem(GUI.elem.prizData, puti1.."donat_casino_item_bg.png")	
					drawElem(GUI.elem.prizData2, puti2..v[1]..".png")

					drawElem(GUI.elem.give, puti1.."donat_casino_button_bg.png",(isCursorInElement(GUI.elem.give) and tocolor(187,26,37) or tocolor(130,18,27)))			
					drawTextInElem(GUI.elem.give, "Забрать", 1.8, "default-bold", "center", "center", false, white)

					drawElem(GUI.elem.take, puti1.."donat_casino_button_bg.png",(isCursorInElement(GUI.elem.take) and tocolor(187,26,37) or tocolor(130,18,27)))			
					drawTextInElem(GUI.elem.take, "Продать", 1.8, "default-bold", "center", "center", false, white)
				end	
			end
		end
	end
end)


addEventHandler("onClientClick", root, function(button, state, x, y)
	if (GUI.mainCaseOpen) and (button == "left") and (state == "up") then		
		if (GUI.screen == "homeCase") then
			if isCursorInElement(GUI.elem.caseNoobcnop) then
				triggerServerEvent("startGameCasino", localPlayer,localPlayer,"noob",45)
			elseif isCursorInElement(GUI.elem.caseZvercnop) then
				triggerServerEvent("startGameCasino", localPlayer,localPlayer,"zver",65)
			elseif isCursorInElement(GUI.elem.caseYdacnop) then
				triggerServerEvent("startGameCasino", localPlayer,localPlayer,"yda",149)
			elseif isCursorInElement(GUI.elem.caseMajorcnop) then
				triggerServerEvent("startGameCasino", localPlayer,localPlayer,"major",249)
			end
		elseif (GUI.screen == "prizeCase") then
			if isCursorInElement(GUI.elem.give) then
				triggerServerEvent("startGame", localPlayer,localPlayer, GUI.data1, GUI.data2, GUI.data3)
				GUI.screen = "homeCase"
				GUI.casePrize = nil
				GUI.numberPrize = nil
				GUI.data1 = nil
				GUI.data2 = nil
				GUI.data3 = nil
				GUI.data4 = nil
			elseif isCursorInElement(GUI.elem.take) then
				triggerServerEvent("sellElement", localPlayer,localPlayer, GUI.data4)
				GUI.screen = "homeCase"
				GUI.casePrize = nil
				GUI.numberPrize = nil
				GUI.data1 = nil
				GUI.data2 = nil
				GUI.data3 = nil
				GUI.data4 = nil
			end
		end
	end
end)



function openPrizeWindow(casino,k ,name,tip,data,pric)
	GUI.screen = "prizeCase"
	GUI.casePrize = casino
	GUI.numberPrize = k
	GUI.data1 = name
	GUI.data2 = tip
	GUI.data3 = data
	GUI.data4 = pric
end
addEvent("openPrizeWindow", true)
addEventHandler("openPrizeWindow", root, openPrizeWindow)






-- ==========     Отрисовка отдельных элементов     ==========
function drawElem(elem, path,color)
	if (elem.centerX) then
		dxDrawImage(elem.centerX - elem.hWidth, elem.centerY - elem.hHeight, elem.width, elem.height, path, 0, 0, 0,  color or white, false)
	else
		dxDrawImage(elem.left, elem.top, elem.width, elem.height, path,  0, 0, 0,  color or white, false)
	end
end

function drawRen(elem,color)
	if (elem.centerX) then
		dxDrawRectangle(elem.centerX - elem.hWidth, elem.centerY - elem.hHeight, elem.width, elem.height,color or white,true)
	else
		dxDrawRectangle(elem.left, elem.top, elem.width, elem.height,color or white,true)
	end
end


function drawTextInElem(elem, text, scale, font, alignX, alignY, wordBreak, color)
	if (elem.left) then
		dxDrawText(text, elem.left, elem.top, elem.left+elem.width, elem.top+elem.height, color or white, scale, font, alignX, alignY, true, wordBreak, false, false, false)
	else
		dxDrawText(text, elem.centerX-elem.hWidth, elem.centerY-elem.hHeight, elem.centerX+elem.hWidth, elem.centerY+elem.hHeight, color or white, scale, font, alignX, alignY, true, wordBreak, false, false, false)
	end
end

function isCursorInElement(elem)
	local x, y = getCursorPosition()
	x = (x or 0) * scx
	y = (y or 0) * scy
	if (elem.left) then
		return (elem.left < x) and (x < elem.left+elem.width) and (elem.top < y) and (y < elem.top+elem.height)
	else
		return (elem.centerX-elem.hWidth < x) and (x < elem.centerX+elem.hWidth) and (elem.centerY-elem.hHeight < y) and (y < elem.centerY+elem.hHeight)
	end
end


--- связка курсора и текста на курсор



function drawTextInElem2(elem, text, scale, font, alignX, alignY, wordBreak, color)
	if (elem.left) then
		dxDrawText(text, elem.left, elem.top, elem.left+elem.width, elem.top+elem.height, color or white, scale, font, alignX, alignY, true, wordBreak, true, false, false)
	else
		dxDrawText(text, elem.centerX-elem.hWidth, elem.centerY-elem.hHeight, elem.centerX+elem.hWidth, elem.centerY+elem.hHeight, color or white, scale, font, alignX, alignY, true, wordBreak, false, false, false)
	end
end






bindKey("3", "down", function (ply)
	if GUI.mainCaseOpen == false then
		GUI.mainCaseOpen = true
		GUI.screen = "homeCase"
		showCursor(true)
		showChat(false)
		addEventHandler( "onClientRender", root, cleanmyscreen)
	else
		GUI.mainCaseOpen = false
		GUI.screen = nil
		showCursor(false)
		showChat(true)
		removeEventHandler( "onClientRender", root, cleanmyscreen)
		networClosPlayer()
	end
end)

function networClosPlayer()
	triggerServerEvent("startGame", localPlayer,localPlayer, GUI.data1, GUI.data2, GUI.data3)
	GUI.casePrize = nil
	GUI.numberPrize = nil
	GUI.data1 = nil
	GUI.data2 = nil
	GUI.data3 = nil
	GUI.data4 = nil
end