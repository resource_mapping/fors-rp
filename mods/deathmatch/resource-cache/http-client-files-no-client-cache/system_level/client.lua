﻿function getPlayerLevel(player)
	local current_level = tonumber(getElementData(player, "player:level")) or 1
	return current_level
end

function getPlayerExperience(player)
	local current_exp = tonumber(getElementData(player, "player:experience")) or 0
	return current_exp
end

function getPlayerExperienceByLevel(player)
	local current_level = tonumber(getElementData(player, "player:level")) or 1
	return current_level*125
end