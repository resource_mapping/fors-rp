down = tocolor(100, 100, 100, 150) -- Цвет полоски грид листа когда нажал
up = tocolor(0, 0, 0, 200) -- Цвет полоски грид листа в обычном положении

function dxDrawButtonText_pas(x, y, w, h, text, Font_1, Font_2)
	local mx, my = getMousePos()
	local tcolor = tocolor(255, 255, 255, 255)
	dxDrawText(text, x, y, w + x, h + y, tcolor, Font_1, Font_2, "default-bold", "center", "center", true, false, false, true)
end

local function setWheelsDatas(dataName, dataValue)
	if dataName ~= "lob_steklo" and dataName ~= "pered_steklo" and dataName ~= "zad_steklo" then
		localPlayer.vehicle:setData(dataName, tonumber(dataValue))
	else
		local data = localPlayer.vehicle:getData("toner")
		if not data then data = {} end
		data[dataName] = dataValue
		localPlayer.vehicle:setData("toner", data)
	end
end

function dxDrawScrollBar(x, y, w, h, sc, text, data, Proc, status)
	local mx, my = getMousePos()
	local bgcolor = tocolor(22, 22, 22, 255)
	local tcolor = tocolor(255, 255, 255, 255)
	local tcolorr = tocolor(255, 255, 255, 255)
	if status == true then
		tcolor = tocolor(191, 32, 32, 255)
	else
		tcolor = tocolor(255, 255, 255, 255)
	end
	dxDrawRectangle(x, y, w, h, bgcolor, false)
	dxDrawText(text, x, y, w + x, h + y, tcolorr, 1,1, "default-bold", "center", "center", true, false, false, true)
	if sc >= x and sc <= (x+w)-5 then
		dxDrawRectangle(sc, y-5, 5, h+10, tcolor, false)
		scrollProcent = ((sc-x)/w)*190
		if status == true then
			setWheelsDatas(data, 0 + math.floor(scrollProcent)*Proc)
		end
	else
		if sc <= x then
			dxDrawRectangle(x, y-5, 5, h+10, tcolor, false)
			scrollProcent = 0
			if status == true then
				setWheelsDatas(data, 0 + math.floor(scrollProcent)*Proc)
			end
		else
			dxDrawRectangle((x+w)-5, y-5, 5, h+10, tcolor, false)
			scrollProcent = 190
			if status == true then
				setWheelsDatas(data, 0 + math.floor(scrollProcent)*Proc)
			end
		end
	end
end

function dxDrawScrollBarHedit(x, y, w, h, sc, text, Proc, status)
	local mx, my = getMousePos()
	local bgcolor = tocolor(22, 22, 22, 255)
	local tcolor = tocolor(255, 255, 255, 255)
	local tcolorr = tocolor(255, 255, 255, 255)
	if status == true then
		tcolor = tocolor(191, 32, 32, 255)
	else
		tcolor = tocolor(255, 255, 255, 255)
	end
	dxDrawRectangle(x, y, w, h, bgcolor, false)
	dxDrawText(text, x, y, w + x, h + y, tcolorr, 1,1, "default-bold", "center", "center", true, false, false, true)
	if sc >= x and sc <= (x+w)-5 then
		dxDrawRectangle(sc, y-5, 5, h+10, tcolor, false)
		scrollProcent = ((sc-x)/w)*190
		if status == true then
			triggerServerEvent( "onVehicleChangeHandling_1", localPlayer, "Nastr1", getPedOccupiedVehicle( localPlayer ), 0 + math.floor(scrollProcent)*Proc )
		end
	else
		if sc <= x then
			dxDrawRectangle(x, y-5, 5, h+10, tcolor, false)
			scrollProcent = 0
			if status == true then
				triggerServerEvent( "onVehicleChangeHandling_1", localPlayer, "Nastr1", getPedOccupiedVehicle( localPlayer ), 0 + math.floor(scrollProcent)*Proc )
			end
		else
			dxDrawRectangle((x+w)-5, y-5, 5, h+10, tcolor, false)
			scrollProcent = 190
			if status == true then
				triggerServerEvent( "onVehicleChangeHandling_1", localPlayer, "Nastr1", getPedOccupiedVehicle( localPlayer ), 0 + math.floor(scrollProcent)*Proc )
			end
		end
	end
end

function dxDrawEditBox(text, x, y, w, h, stat, s, testo)
	local mx, my = getMousePos()
	
	local CR = getElementData(localPlayer, "ColorR") or standartR
	local CG = getElementData(localPlayer, "ColorG") or standartG
	local CB = getElementData(localPlayer, "ColorB") or standartB
	local bgcolor = tocolor(22, 22, 22, 255)
	local tcolor = tocolor(255, 255, 255, 255)
	local bgcolorp = tocolor(CR, CG, CB, 255)
	if stat == true then
		bgcolor = tocolor(22, 22, 22, 150)
	else
		bgcolor = tocolor(22, 22, 22, 255)
	end
	dxDrawRectangle(x + s, y, w - s, h, bgcolor, false)
	dxDrawRectangle(x, y, s, h, bgcolorp, false)
	dxDrawText(testo, x+5, y, w + x, h + y, tcolor, 1,1, "default-bold", "left", "center", true, false, false, true)
	dxDrawText(text, x+5 + s, y, w + x, h + y, tcolor, 1,1, "default-bold", "left", "center", true, false, false, true)
end


function dxDrawButton(x, y, w, h, text)
	local mx, my = getMousePos()
	local CR = getElementData(localPlayer, "ColorR") or standartR
	local CG = getElementData(localPlayer, "ColorG") or standartG
	local CB = getElementData(localPlayer, "ColorB") or standartB
	local CH = getElementData(localPlayer, "ColorH") or standartH
	local bgcolor = tocolor(CR,CG,CB,255)
	if isPointInRect(mx, my, x, y, w, h) then
		bgcolor = tocolor(CR,CG,CB,CH)
	else
		bgcolor = tocolor(CR,CG,CB, 255)
	end
	dxDrawRectangle(x, y, w, h, bgcolor, false)
	dxDrawText(text, x, y, w + x, h + y, tocolor(255, 255, 255, 255), 1,1, "default-bold", "center", "center", true, false, false, true)
end

function dxDrawButtonText(x, y, w, h, text, Font_1, Font_2)
	local mx, my = getMousePos()
	local tcolor = tocolor(255, 255, 255, 255)
	dxDrawText(text, x, y, w + x, h + y, tcolor, Font_1, Font_2, "default-bold", "left", "center", true, false, false, true)
end

function dxDrawWindowll(x, y, w, h, text)
	local mx, my = getMousePos()
	local CR = getElementData(localPlayer, "ColorR") or standartR
	local CG = getElementData(localPlayer, "ColorG") or standartG
	local CB = getElementData(localPlayer, "ColorB") or standartB
	local bgcolor = tocolor(0, 0, 0, 150)
	local bgcolors = tocolor(22, 22, 22, 255)
	local tcolor = tocolor(255, 255, 255, 255)
	local bgcolorp = tocolor(CR, CG, CB, 255)
	dxDrawRectangle(x - 120, y, w + 120, h, bgcolor, false)
	dxDrawRectangle(x - 120, y, w + 120, 5, bgcolorp, false)
	--dxDrawRectangle(x, y, w, 25, bgcolors, false)
	dxDrawImage(x - 107, y+20, 100, 100, "file/logo_1.png")
	--dxDrawImage(x + w / 4, y, 100, 100, "file/logo.png")
	dxDrawText(text, x + 4, y - 5, w + x - 8, 30 + y, tcolor, 1,1, "default-bold", "center", "center", true, false, false, true)
end

function dxDrawWindow(x, y, w, h, text)
	local mx, my = getMousePos()
	local CR = getElementData(localPlayer, "ColorR") or standartR
	local CG = getElementData(localPlayer, "ColorG") or standartG
	local CB = getElementData(localPlayer, "ColorB") or standartB
	local bgcolor = tocolor(0, 0, 0, 150)
	local tcolor = tocolor(255, 255, 255, 255)
	local bgcolorp = tocolor(CR, CG, CB, 255)
	dxDrawRectangle(x, y, w, h, bgcolor, false)
	dxDrawRectangle(x, y, w, 5, bgcolorp, false)
	dxDrawText(text, x + 4, y + 5, w + x, 30 + y, tcolor, 1,1, "default-bold", "center", "center", true, false, false, true)
end

function dxDrawWindowG(x, y, w, h, text)
	local mx, my = getMousePos()
	local CR = getElementData(localPlayer, "ColorR") or standartR
	local CG = getElementData(localPlayer, "ColorG") or standartG
	local CB = getElementData(localPlayer, "ColorB") or standartB
	local bgcolor = tocolor(0, 0, 0, 150)
	local bgcolors = tocolor(22, 22, 22, 255)
	local tcolor = tocolor(255, 255, 255, 255)
	local bgcolorp = tocolor(CR, CG, CB, 255)
	dxDrawRectangle(x, y, w, h, bgcolor, false)
	dxDrawRectangle(x, y, w, 5, bgcolorp, false)
	dxDrawImage(x + 10, y + 30, 230, 82, "img/t.png")
end

function dxDrawButtonL(x, y, w, h, text)
	local mx, my = getMousePos()
	local bgcolor = tocolor(22, 22, 22, 255)
	local tcolor = tocolor(255, 255, 255, 255)
	if isPointInRect(mx, my, x - 2, y, w, h) then
		bgcolor = tocolor(22, 22, 22, 150)
	else
		bgcolor = tocolor(22, 22, 22, 255)
	end
	dxDrawRectangle(x, y, w, h, bgcolor, false)
	dxDrawText(text, x, y, w + x, h + y, tcolor, 1,1, "default-bold", "center", "center", true, false, false, true)
end

function dxDrawButtonTextL(x, y, w, h, text, Font_1, Font_2)
	local mx, my = getMousePos()
	local bgcolor = tocolor(0, 230, 230, 255)
	local tcolor = tocolor(255, 255, 255, 255)
	if isPointInRect(mx, my, x - 2, y, w, h) then
		bgcolor = tocolor(200, 200, 200, 150)
	else
		bgcolor = tocolor(150, 0, 0, 255)
	end
	dxDrawText(text, x, y, w + x, h + y, tcolor, Font_1, Font_2, "default-bold", "center", "center", true, false, false, true)
end

function dxDrawWindowL(x, y, w, h, text)
	local mx, my = getMousePos()
	local bgcolor = tocolor(46, 46, 46, 255)
	local bgcolors = tocolor(22, 22, 22, 255)
	local tcolor = tocolor(255, 255, 255, 255)
	dxDrawRectangle(x, y, w, h, bgcolor, false)
	dxDrawRectangle(x, y, w, 25, bgcolors, false)
	dxDrawText(text, x + 4, y - 5, w + x - 8, 30 + y, tcolor, 1,1, "default-bold", "center", "center", true, false, false, true)
end

function dxDrawCheckButton(x, y, w, h, text, chek )
	local mx, my = getMousePos()
	local color1 = tocolor(191, 32, 32, 255)
	local color2 = tocolor(255, 255, 255, 255)
	local color3 = tocolor(191, 32, 32, 255)
	if isPointInRect(mx, my, x, y, w, h) then
		color3 = tocolor(191, 32, 32, 200)
	end
	local pos = x
	dxDrawRectangle(x, y, w, h, color1, false)
	dxDrawRectangle(x + 2, y + 2, w - 4, h - 4, color2, false)
	if chek then
		dxDrawRectangle(pos + 4, y + 4, w - 8, h - 8, color3, false)
	end
	dxDrawText(text, pos + 25, y + 6, 30 + pos + 25, h - 12 + y + 6, tcolor, 1,1, "default-bold", "left", "center", true, false, false, true)
end

function dxDrawtext(x, y, w, h, text)
	dxDrawText(text, x, y, w + x, h + y, tcolor, 1, "default-bold", "center", "center", true, false, false, true)
end

function isPointInRect(x, y, rx, ry, rw, rh)
	if x >= rx and y >= ry and x <= rx + rw and y <= ry + rh then
		return true
	else
		return false
	end
end
function cursorPosition(x, y, w, h)
	if (not isCursorShowing()) then
		return false
	end
	local mx, my = getCursorPosition()
	local fullx, fully = guiGetScreenSize()
	cursorx, cursory = mx*fullx, my*fully
	if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
		return true
	else
		return false
	end
end
function getMousePos()
	local xsc, ysc = guiGetScreenSize()
	local mx, my = getCursorPosition()
	if not mx or not my then
		mx, my = 0, 0
	end
	return mx * xsc, my * ysc
end

function isMouseClick()
	return wasMousePressedInCurrentFrame
end