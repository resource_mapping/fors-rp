sw, sh = guiGetScreenSize()

pickerTable = {}

colorPicker = {}
colorPicker.__index = colorPicker

color_panel = false

function colorPicker.create(id, start, title)

--if color_panel == true then
  local cp = {}
  setmetatable(cp, colorPicker)
  cp.id = id
  cp.color = {}
  cp.color.h, cp.color.s, cp.color.v, cp.color.r, cp.color.g, cp.color.b, cp.color.hex = 0, 1, 1, 255, 0, 0, "#FF0000"
  cp.color.white = tocolor(255,255,255,255)
  cp.color.black = tocolor(0,0,0,255)
  cp.color.current = tocolor(255,0,0,255)
  cp.color.huecurrent = tocolor(255,0,0,255)
  if start and getColorFromString(start) then
    cp.color.h, cp.color.s, cp.color.v = rgb2hsv(getColorFromString(start))
  end
  cp.gui = {}

  cp.gui.svmap = guiCreateStaticImage(10, sh / 2 - 130, 256, 256, "color/Image/blank.png", false)
  cp.gui.hbar = guiCreateStaticImage(280, sh / 2 - 130, 32, 256, "color/Image/blank.png", false)

  cp.handlers = {}
  cp.handlers.mouseDown = function() cp:mouseDown() end
  cp.handlers.mouseSnap = function() cp:mouseSnap() end
  cp.handlers.mouseUp = function(b,s) cp:mouseUp(b,s) end
  cp.handlers.mouseMove = function(x,y) cp:mouseMove(x,y) end
  cp.handlers.render = function() cp:render() end
  cp.handlers.guiFocus = function() cp:guiFocus() end
  cp.handlers.guiBlur = function() cp:guiBlur() end
  
  addEventHandler("onClientGUIMouseDown", cp.gui.svmap, cp.handlers.mouseDown, false)
  addEventHandler("onClientMouseLeave", cp.gui.svmap, cp.handlers.mouseSnap, false)
  addEventHandler("onClientMouseMove", cp.gui.svmap, cp.handlers.mouseMove, false)
  addEventHandler("onClientGUIMouseDown", cp.gui.hbar, cp.handlers.mouseDown, false)
  addEventHandler("onClientMouseMove", cp.gui.hbar, cp.handlers.mouseMove, false)
  addEventHandler("onClientClick", root, cp.handlers.mouseUp)
  addEventHandler("onClientGUIMouseUp", root, cp.handlers.mouseUp)
  addEventHandler("onClientRender", root, cp.handlers.render)
  
  return cp
  
 -- else
  
 -- end
end

chek_stat_1 = false
chek_stat_2 = false
chek_stat_3 = false
chek_stat_4 = false
chek_stat_5 = false
chek_stat_6 = false
chek_stat_7 = false
click_colors_chek = false
Stat_but_color = true
itog_color = 0

function colorPicker:render()
	if color_panel == true then
		dxDrawWindow(5, sh / 2 - 150, 310, 350, "")	
		dxDrawCheckButton(10, sh / 2 + 135, 15, 15, "Авто #1", chek_stat_1 )
		dxDrawCheckButton(90, sh / 2 + 135, 15, 15, "Авто #2", chek_stat_2 )
		dxDrawCheckButton(10, sh / 2 + 155, 15, 15, "Авто #3", chek_stat_3 )
		dxDrawCheckButton(90, sh / 2 + 155, 15, 15, "Авто #4", chek_stat_4 )
		
		dxDrawCheckButton(170, sh / 2 + 135, 15, 15, "Передние диски", chek_stat_5 )
		dxDrawCheckButton(170, sh / 2 + 155, 15, 15, "Задние диски", chek_stat_6 )
		dxDrawCheckButton(10, sh / 2 + 175, 15, 15, "Фары", chek_stat_7 )
		dxDrawButton(90, sh / 2 + 175, 220, 20, "Купить : "..itog_color.." $", Stat_but_color)
	
	
	  dxDrawRectangle(10, sh / 2 - 130, 256, 256, self.color.huecurrent, self.gui.focus)
	  dxDrawImage(10, sh / 2 - 130, 256, 256, "color/Image/sv.png", 0, 0, 0, self.color.white, self.gui.focus)
	  dxDrawImage(280, sh / 2 - 130, 32, 256, "color/Image/h.png", 0, 0, 0, self.color.white, self.gui.focus)
	  dxDrawImageSection(2+math.floor(256*self.color.s), sh / 2 - 138 +(256-math.floor(256*self.color.v)), 16, 16, 0, 0, 16, 16, "color/Image/cursor.png", 0, 0, 0, self.color.white, self.gui.focus)
	  dxDrawImageSection(272, sh / 2 - 138 +(256-math.floor(256*self.color.h)), 48, 16, 16, 0, 48, 16, "color/Image/cursor.png", 0, 0, 0, self.color.huecurrent, self.gui.focus) --полоса
	end
end


colorPicker.create(id, start, title)
function Open_colors(id, start, title)
	if color_panel == false then
		color_panel = true
		if id and not pickerTable[id] then
			pickerTable[id] = colorPicker.create(id, start, title)
			pickerTable[id]:updateColor()
		end
	end
end
addEvent("ColorPanel:Open_colors", true)
addEventHandler("ColorPanel:Open_colors", root, Open_colors)
function close_color (id, start, title)
if color_panel == true then
		color_panel = false
		chek_stat_1 = false
		chek_stat_2 = false
		chek_stat_3 = false
		chek_stat_4 = false
		chek_stat_5 = false
		chek_stat_6 = false
		chek_stat_7 = false
		click_colors_chek = false
		Stat_but_color = true
		if id and pickerTable[id] then
			pickerTable[id]:destroy(id, start, title)
		end
		end
end
addEvent("ColorPanel:close_color", true)
addEventHandler("ColorPanel:close_color", root, close_color)

--bindKey("f2", "down", Open_colors)
--bindKey("f3", "down", close_color)

function colorPicker:mouseDown()
  if source == self.gui.svmap or source == self.gui.hbar then
    self.gui.track = source
    local cx, cy = getCursorPosition()
    self:mouseMove(sw*cx, sh*cy)
	
  end  
end

function colorPicker:mouseUp(button, state)
  if not state or state ~= "down" then
    if self.gui.track then 
    end
    self.gui.track = false 
  end  
end


function onClick_color(button, state)
	if color_panel == true then
		if button == "left" and state == "down" then
			if cursorPosition(10, sh / 2 + 135, 15, 15) then
				if chek_stat_2 == true or chek_stat_3 == true or chek_stat_4 == true or chek_stat_5 == true or chek_stat_6 == true or chek_stat_7 == true then
					showHelpMessage("Тюнинг", "Вы не оплатили прошлую покупку, нажмите 'Купить'", 3)
				else
					if chek_stat_1 == false then
						chek_stat_1 = true
						click_colors_chek = true
						click_buy_colors_1 = true
						stat_destroy_wnd_color = true
					end
				end
			end
			if cursorPosition(90, sh / 2 + 135, 15, 15) then
				if chek_stat_1 == true or chek_stat_3 == true or chek_stat_4 == true or chek_stat_5 == true or chek_stat_6 == true or chek_stat_7 == true then
					showHelpMessage("Тюнинг", "Вы не оплатили прошлую покраску, нажмите 'Купить'", 3)
				else
					if chek_stat_2 == false then
						chek_stat_2 = true
						click_colors_chek = true
						click_buy_colors_2 = true
						stat_destroy_wnd_color = true
					end
				end
			end
			if cursorPosition(10, sh / 2 + 155, 15, 15) then
				if chek_stat_2 == true or chek_stat_1 == true or chek_stat_4 == true or chek_stat_5 == true or chek_stat_6 == true or chek_stat_7 == true then
					showHelpMessage("Тюнинг", "Вы не оплатили прошлую покраску, нажмите 'Купить'", 3)
				else
					if chek_stat_3 == false then
						chek_stat_3 = true
						click_colors_chek = true
						click_buy_colors_3 = true
						stat_destroy_wnd_color = true
					end
				end
			end
			if cursorPosition(90, sh / 2 + 155, 15, 15) then
				if chek_stat_2 == true or chek_stat_3 == true or chek_stat_1 == true or chek_stat_5 == true or chek_stat_6 == true or chek_stat_7 == true then
					showHelpMessage("Тюнинг", "Вы не оплатили прошлую покраску, нажмите 'Купить'", 3)
				else
					if chek_stat_4 == false then
						chek_stat_4 = true
						click_colors_chek = true
						click_buy_colors_4 = true
						stat_destroy_wnd_color = true
					end
				end
			end
			if cursorPosition(170, sh / 2 + 135, 15, 15) then
				if chek_stat_2 == true or chek_stat_3 == true or chek_stat_1 == true or chek_stat_4 == true or chek_stat_6 == true or chek_stat_7 == true then
					showHelpMessage("Тюнинг", "Вы не оплатили прошлую покраску, нажмите 'Купить'", 3)
				else
					if chek_stat_5 == false then
						chek_stat_5 = true
						click_colors_chek = true
						click_buy_colors_5 = true
						stat_destroy_wnd_color = true
					end
				end
			end
			if cursorPosition(170, sh / 2 + 155, 15, 15) then
				if chek_stat_2 == true or chek_stat_3 == true or chek_stat_1 == true or chek_stat_4 == true or chek_stat_5 == true or chek_stat_7 == true then
					showHelpMessage("Тюнинг", "Вы не оплатили прошлую покраску, нажмите 'Купить'", 3)
				else
					if chek_stat_6 == false then
						chek_stat_6 = true
						click_colors_chek = true
						click_buy_colors_6 = true
						stat_destroy_wnd_color = true
					end
				end
			end
			if cursorPosition(10, sh / 2 + 175, 15, 15) then
				if chek_stat_2 == true or chek_stat_3 == true or chek_stat_1 == true or chek_stat_4 == true or chek_stat_6 == true or chek_stat_5 == true then
					showHelpMessage("Тюнинг", "Вы не оплатили прошлую покраску, нажмите 'Купить'", 3)
				else
					if chek_stat_7 == false then
						chek_stat_7 = true
						click_colors_chek = true
						click_buy_colors_7 = true
						stat_destroy_wnd_color = true
					end
				end
			end
			if cursorPosition(90, sh / 2 + 175, 220, 20) then
				if getPlayerMoney() >= itog_color - 1 then--price
					triggerServerEvent("Spisanie_money", getRootElement(), localPlayer, itog_color )
					if chek_stat_1 == true then
						chek_stat_1 = false
						click_buy_colors_1 = false
						showHelpMessage("Тюнинг", "Вы успешно купили покраску [ Авто #1 ] за "..itog_color.." $", 3)
						stat_destroy_wnd_color = false
					end
					if chek_stat_2 == true then
						chek_stat_2 = false
						click_buy_colors_2 = false
						showHelpMessage("Тюнинг", "Вы успешно купили покраску [ Авто #2 ] за "..itog_color.." $", 3)
						stat_destroy_wnd_color = false
					end
					if chek_stat_3 == true then
						chek_stat_3 = false
						click_buy_colors_3 = false
						showHelpMessage("Тюнинг", "Вы успешно купили покраску [ Авто #3 ] за "..itog_color.." $", 3)
						stat_destroy_wnd_color = false
					end
					if chek_stat_4 == true then
						chek_stat_4 = false
						click_buy_colors_4 = false
						showHelpMessage("Тюнинг", "Вы успешно купили покраску [ Авто #4 ] за "..itog_color.." $", 3)
						stat_destroy_wnd_color = false
					end
					if chek_stat_5 == true then
						chek_stat_5 = false
						click_buy_colors_5 = false
						showHelpMessage("Тюнинг", "Вы успешно купили покраску [ Передние диски #1 ] за "..itog_color.." $", 3)
						stat_destroy_wnd_color = false
					end
					if chek_stat_6 == true then
						chek_stat_6 = false
						click_buy_colors_6 = false
						showHelpMessage("Тюнинг", "Вы успешно купили покраску [ Задние диски ] за "..itog_color.." $", 3)
						stat_destroy_wnd_color = false
					end
					if chek_stat_7 == true then
						chek_stat_7 = false
						click_buy_colors_7 = false
						showHelpMessage("Тюнинг", "Вы успешно купили покраску [ Фары ] за "..itog_color.." $", 3)
						stat_destroy_wnd_color = false
					end
					click_colors_chek = false
					itog_color = 0
				else
					if chek_stat_1 == true then
						chek_stat_1 = false
					end
					if chek_stat_2 == true then
						chek_stat_2 = false
					end
					if chek_stat_3 == true then
						chek_stat_3 = false
					end
					if chek_stat_4 == true then
						chek_stat_4 = false
					end
					if chek_stat_5 == true then
						chek_stat_5 = false
					end
					if chek_stat_6 == true then
						chek_stat_6 = false
					end
					if chek_stat_7 == true then
						chek_stat_7 = false
					end
					showHelpMessage("Тюнинг", "У вас не хватает денег", 3)
					click_colors_chek = false
					itog_color = 0
				end
			end
		end
	end
end
addEventHandler("onClientClick", root, onClick_color)
--5, sh / 2 + 135, 310, 60
function cena_prosmotr()
	if color_panel == true then
		if click_colors_chek == false then
			if cursorPosition(5, sh / 2 + 135, 310, 60) then
				if cursorPosition(10, sh / 2 + 135, 15, 15) then
					itog_color = color_buy_1
				end
				if cursorPosition(90, sh / 2 + 135, 15, 15) then
					itog_color = color_buy_2
				end
				if cursorPosition(10, sh / 2 + 155, 15, 15) then
					itog_color = color_buy_3
				end
				if cursorPosition(90, sh / 2 + 155, 15, 15) then
					itog_color = color_buy_4
				end
				if cursorPosition(170, sh / 2 + 135, 15, 15) then
					itog_color = color_buy_5
				end
				if cursorPosition(170, sh / 2 + 155, 15, 15) then
					itog_color = color_buy_6
				end
				if cursorPosition(10, sh / 2 + 175, 15, 15) then
					itog_color = color_buy_7
				end
			else
				itog_color = 0
			end
		end
	end
end
addEventHandler("onClientRender", root, cena_prosmotr)

function colorPicker:mouseMove(x,y)
  if self.gui.track and source == self.gui.track then
    if source == self.gui.svmap then
      local offsetx, offsety = x - (10), y - (sh / 2 - 130)
      self.color.s = offsetx/255
      self.color.v = (255-offsety)/255
    elseif source == self.gui.hbar then
      local offset = y - (sh / 2 - 130)
      self.color.h = (255-offset)/255
    end 
    self:updateColor()
	if chek_stat_1 == true then
		triggerServerEvent("color1", getRootElement(), localPlayer, self.color.r, self.color.g, self.color.b )
	end
	if chek_stat_2 == true then
		triggerServerEvent("color2", getRootElement(), localPlayer, self.color.r, self.color.g, self.color.b )
	end
	if chek_stat_3 == true then
		triggerServerEvent("color3", getRootElement(), localPlayer, self.color.r, self.color.g, self.color.b )
	end
	if chek_stat_4 == true then
		triggerServerEvent("color4", getRootElement(), localPlayer, self.color.r, self.color.g, self.color.b )
	end
	if chek_stat_5 == true then
		vehicle = getPedOccupiedVehicle(localPlayer) 
		vehicle:setData("WheelsColorF",{self.color.r, self.color.g, self.color.b})
	end
	if chek_stat_6 == true then
		vehicle = getPedOccupiedVehicle(localPlayer) 
		vehicle:setData("WheelsColorR",{self.color.r, self.color.g, self.color.b})
	end
	if chek_stat_7 == true then
		triggerServerEvent("color_Light", getRootElement(), localPlayer, self.color.r, self.color.g, self.color.b )
	end
	if Tumbler_color_stat_5 == false then
		vehicle = getPedOccupiedVehicle(localPlayer) 
		vehicle:setData("SmokeColor",{self.color.r, self.color.g, self.color.b})
	end
  end
end


function colorPicker:destroy()
  removeEventHandler("onClientGUIMouseDown", self.gui.svmap, self.handlers.mouseDown)
  removeEventHandler("onClientMouseLeave", self.gui.svmap, self.handlers.mouseSnap)
  removeEventHandler("onClientMouseMove", self.gui.svmap, self.handlers.mouseMove)
  removeEventHandler("onClientGUIMouseDown", self.gui.hbar, self.handlers.mouseDown)
  removeEventHandler("onClientMouseMove", self.gui.hbar, self.handlers.mouseMove)
  removeEventHandler("onClientClick", root, self.handlers.mouseUp)
  removeEventHandler("onClientGUIMouseUp", root, self.handlers.mouseUp)
  removeEventHandler("onClientRender", root, self.handlers.render)

  pickerTable[self.id] = nil
  setmetatable(self, nil)
  showCursor(areThereAnyPickers())
end

function colorPicker:mouseSnap()
  if self.gui.track and source == self.gui.track then
    self:updateColor()
  end
end

function colorPicker:updateColor()
  self.color.r, self.color.g, self.color.b = hsv2rgb(self.color.h, self.color.s, self.color.v)
  self.color.current = tocolor(self.color.r, self.color.g, self.color.b,255)
  self.color.huecurrent = tocolor(hsv2rgb(self.color.h, 1, 1))
end

function areThereAnyPickers()
  for _ in pairs(pickerTable) do
    return true
  end
  return false
end

function hsv2rgb(h, s, v)
  local r, g, b
  local i = math.floor(h * 6)
  local f = h * 6 - i
  local p = v * (1 - s)
  local q = v * (1 - f * s)
  local t = v * (1 - (1 - f) * s)
  local switch = i % 6
  if switch == 0 then
    r = v g = t b = p
  elseif switch == 1 then
    r = q g = v b = p
  elseif switch == 2 then
    r = p g = v b = t
  elseif switch == 3 then
    r = p g = q b = v
  elseif switch == 4 then
    r = t g = p b = v
  elseif switch == 5 then
    r = v g = p b = q
  end
  return math.floor(r*255), math.floor(g*255), math.floor(b*255)
end

function rgb2hsv(r, g, b)
  r, g, b = r/255, g/255, b/255
  local max, min = math.max(r, g, b), math.min(r, g, b)
  local h, s 
  local v = max
  local d = max - min
  s = max == 0 and 0 or d/max
  if max == min then 
    h = 0
  elseif max == r then 
    h = (g - b) / d + (g < b and 6 or 0)
  elseif max == g then 
    h = (b - r) / d + 2
  elseif max == b then 
    h = (r - g) / d + 4
  end
  h = h/6
  return h, s, v
end

function math.round(v)
  return math.floor(v+0.5)
end

addEvent("onColorPickerOK", true)
addEvent("onColorPickerChange", true)