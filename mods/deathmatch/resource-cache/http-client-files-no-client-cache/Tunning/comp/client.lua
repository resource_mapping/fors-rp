local scx,scy = guiGetScreenSize()
local px = scx/1920
local sizeX, sizeY = 400*px,600*px
local posX,posY = 10*px,scy-sizeY-100*px
local screen = dxCreateScreenSource( scx,scy )

local selection_comp = 1
local press_comp = false
local scroll_comp = 0
local scrollMax_comp = 0
local rt_comp = dxCreateRenderTarget( 240,145, true )
local clk_comp = false


vehiclenotremap = {
	--[id] = false,
	
}

motoTable = {
	[523] = true,
}

upgradesFromData = {
	["Spoilers"] = {1000, 1001, 1002, 1003, 1014, 1015, 1016, 1023, 1049, 1050, 1058, 1060, 1138, 1139, 1146, 1147, 1158, 1162, 1163, 1164}
}


tableComponents = {
	[602] = {
		["RearBump"] = {
			{"RearBump", "Убрать бампер", 0},
			{"RearBump0", "Сток бампер", 1000},
			{"RearBump2", "Бампер 1", 2500},
			{"RearBump1", "Бампер 2", 2500},
			{"RearBump3", "Бампер 3", 2500},
		},
		["FrontBump"] = {
			{"FrontBump", "Убрать бампер" , 0},
			{"FrontBump0", "Сток бампер" , 1000},
			{"FrontBump1", "Бампер 1", 2500},
			{"FrontBump2", "Бампер 2", 3500},
		},
		["Bonnets"] = {
			{"Bonnets", "Убрать капот", 0},
			{"Bonnets0", "Сток капот", 1000},
			{"Bonnets1", "Капот 1", 1500},
			{"Bonnets2", "Капот 2", 2500},
		},
		["FrontFends"] = {
			{"FrontFends0", "Сток крылья", 1000},
			{"FrontFends1", "Крылья 1", 1500},
			{"FrontFends2", "Крылья 2", 2500},
		},
		["RearFends"] = {
			{"RearFends0", "Сток крылья", 1000},
			{"RearFends1", "Крылья 1", 1500},
			{"RearFends2", "Крылья 2", 2500},
		},
		["Spoilers"] = {
			{"Spoilers0", "Сток спойлер", 1000},
			{"Spoilers1", "Спойлер 1", 1500},
			{"Spoilers2", "Спойлер 2", 2500},
		},
	},
}

function updateVehicleTuningComponent(vehicle, componentName, forceId)
    if not isElement(vehicle) then return false end
    if not vehicle then return end
    if not getElementModel(vehicle) then return end
    local i = 0
    while i <= 20 do
        local name = componentName .. tostring(i)       
        setVehicleComponentVisible(vehicle, name, false)
        setVehicleComponentVisible(vehicle, componentName .. "Glass" .. tostring(i), false)
        if i > 0 and not getVehicleComponentPosition(vehicle, name) then
            break
        end
        i = i + 1
    end
    local id = 0
    if componentName == "Spoilers" then
        updateVehicleTuningUpgrade(vehicle, "Spoilers")
    end
    if type(forceId) == "number" then
        id = componentName..forceId
    elseif not forceId then
        id = getElementData(vehicle, componentName)
    else
        id = forceId
    end
    if not getElementData(vehicle, componentName)  then
        setTimer(function()
            if not getElementData(vehicle, componentName) then
                setElementData(vehicle, componentName, componentName.."0")
                id = getElementData(vehicle, componentName)
            end
            if getElementData(vehicle, componentName) then
                glass = string.gsub(getElementData(vehicle, componentName), componentName, "")
                setVehicleComponentVisible(vehicle, getElementData(vehicle, componentName) , true)
                setVehicleComponentVisible(vehicle, componentName .. "Glass" .. tonumber(glass), true)
            end
        end,500,1)
        return
    end
    glass = string.gsub(id, componentName, "")
    setVehicleComponentVisible(vehicle, id, true)
    setVehicleComponentVisible(vehicle, componentName .. "Glass" .. tonumber(glass), true)
end

function updateVehicleTuningUpgrade(vehicle, upgradeName)
    if not isElement(vehicle) then return false end
    if type(upgradeName) ~= "string" or not upgradesFromData[upgradeName] then 
        return false 
    end

    for i, id in ipairs(upgradesFromData[upgradeName]) do
        vehicle:removeUpgrade(id)
    end

    local index = tonumber(vehicle:getData(upgradeName)) 
    if not index then
        return false
    end
    if upgradeName == "Spoilers" then
        if index > #upgradesFromData[upgradeName] then
            return updateVehicleTuningComponent(vehicle, upgradeName, index - #upgradesFromData[upgradeName])
        elseif index == 0 then
            return updateVehicleTuningComponent(vehicle, upgradeName, 0)
        else
            updateVehicleTuningComponent(vehicle, upgradeName, -1)
        end
    end
    local id = upgradesFromData[upgradeName][index]         
    if id then
        return vehicle:addUpgrade(id)
    end 
end

-- Полностью обновить тюнинг на автомобиле
local function updateVehicleTuning(vehicle)
    if not isElement(vehicle) then
        return false
    end
    for name in pairs(componentsFromData) do
        if not getElementData(vehicle, name) then break end
        updateVehicleTuningComponent(vehicle, name)
    end
    for name in pairs(upgradesFromData) do
        if not getElementData(vehicle, name) then break end
        updateVehicleTuningUpgrade(vehicle, name)
    end 
    return true
end

comp_wnd_stat = false
comp_wnd_stat_but = false
componentt = ""
price = "0"
sizws = 180
spok = 500
cc = true
function comp_wnd ()
	local veh = getPedOccupiedVehicle(localPlayer)

	dxDrawWindow(5, scy / 2 - 80, 250, sizws, "")
	if comp_wnd_stat_but == true then
		dxDrawButton(5, scy / 2 + 100, 122.5, 25, "НАЗАД", true)
		dxDrawButton(132.5, scy / 2 + 100, 122.5, 25, "КУПИТЬ: "..price.." $", true)
	end
	
	dxUpdateScreenSource( screen )
	dxSetRenderTarget( rt_comp,true )
	if scroll_comp < 0 then scroll_comp = 0
	elseif scroll_comp >= scrollMax_comp then scroll_comp = scrollMax_comp end
	local sy = 0
		if comp_wnd_stat_but == false then
			for k,v in pairs(componentsFromData) do
				spok = v
				if getVehicleComponentPosition(veh, k..tostring(1)) then
					dxDrawRectangle(0,sy-scroll_comp,250,25,up)
					dxDrawText(v,0,sy-scroll_comp,240,sy-scroll_comp+26,tocolor(255,255,255),1,1,"default-bold","center","center")
					sy = sy + 30
					cc = false
				end
				if cc == true then
					dxDrawButtonText_pas(5, 70, 240, 0, "На данное авто нет тюнинга", 1, 1)
				end
			end
		else
			for i=0,20 do
				for name,comp in pairs(componentsFromData) do
					if getVehicleComponentPosition(veh, name..tostring(i)) then
						if name == componentt then
								dxDrawRectangle(0,sy-scroll_comp,250,25,up)
							if i == 0 then
								dxDrawText(comp.." #0",5,sy-scroll_comp,150,sy-scroll_comp+26,tocolor(255,255,255),1,1,"default-bold","center","center")
								dxDrawText("Цена : 0",160,sy-scroll_comp,240,sy-scroll_comp+26,tocolor(255,255,255),1,1,"default-bold","left","center")
							else
								dxDrawText(comp.." #"..tostring(i).."",5,sy-scroll_comp,150,sy-scroll_comp+26,tocolor(255,255,255),1,1,"default-bold","center","center")
								dxDrawText("Цена : "..tostring(i).."000 $",160,sy-scroll_comp,240,sy-scroll_comp+26,tocolor(255,255,255),1,1,"default-bold","left","center")
							end
							sy = sy + 30
						end
					end
				end
			end
		end
	dxSetRenderTarget()
	dxDrawImage(5, scy / 2 - 60,240,145,rt_comp)
	if sy >= 145 then
		scrollMax_comp = sy-145
		local size = 145/sy*145
		dxDrawRectangle(245,scy / 2 - 60+scroll_comp/scrollMax_comp*(140-size),10,size,tocolor(255,255,255,240))
	end
	local spy = 0
	if comp_wnd_stat_but == false then
		for k,v in pairs(componentsFromData) do
			if getVehicleComponentPosition(veh, k..tostring(1)) then
				if cursorPosition(5, scy / 2 - 60,240,145)then
					if cursorPosition(5, scy / 2 - 60+spy-scroll_comp,240,25) then
						if getKeyState("mouse1") and not clk_comp then
							for name, comp in pairs(componentsFromData) do
							updateVehicleTuningComponent(veh, name)
							--outputChatBox(tonumber(price))
							end
							componentt = k
							comp_wnd_stat_but = true
							sizws = 210
							scroll_comp = 0
						end
					end
				end
				spy = spy + 30
			end
		end
	else		
		for i=0,20 do
			for k,v in pairs(componentsFromData) do	
				if k == componentt then	
					if getVehicleComponentPosition(veh, k..tostring(i)) then
						if cursorPosition(5, scy / 2 - 60,240,145)then
							if cursorPosition(5, scy / 2 - 60+spy-scroll_comp,240,25) then
								if getKeyState("mouse1") and not clk_comp then
									updateVehicleTuningComponent(veh, k, k..i)
									component = k..i
									nameComp = k
									scroll_comp = 0
									if i == 0 then
									price = tostring(i)
									else
									price = tostring(i).."000"
									end
									--price = tostring(i[2])
								end
							end
						end
						spy = spy + 30
					end
				end
			end
		end
	end
	if getKeyState("mouse1") then clk_comp = true else clk_comp = false end
end

addEventHandler("onClientKey",root,function(key,press_comp)
	if press_comp then
		if not comp_wnd_stat then return end
		if key == "mouse_wheel_down" then
			scroll_comp = scroll_comp + 15*px
		elseif key == "mouse_wheel_up" then
			scroll_comp = scroll_comp - 15*px
		end
	end
end)

function on_click_heds(button, state)
	if comp_wnd_stat_but == true then
		if button == "left" and state == "down" then
			if cursorPosition(5, scy / 2 + 100, 122.5, 25) then
				scroll_comp = 0
				sizws = 180
				if comp_wnd_stat_but == true then
					comp_wnd_stat_but = false
				else
				
				end
			end
			if cursorPosition(132.5, scy / 2 + 100, 122.5, 25) then
				local money_comp = getElementData(localPlayer, "nal" ) or 0
				if getPlayerMoney() >= tonumber(price - 1) then--price
					showHelpMessage("Тюнинг", "Запчасть куплена за "..tonumber(price).." $", 3)
					local veh = getPedOccupiedVehicle(localPlayer)
					setElementData(veh, nameComp, component)
					triggerServerEvent("takeMoneyPly",localPlayer, veh)
					triggerServerEvent("Spisanie_money", getRootElement(), localPlayer, tonumber(price) )
				else
					showHelpMessage("Тюнинг", "У вас не хватает денег", 3)
				end
			end
		end
	end
end
addEventHandler("onClientClick", root, on_click_heds)




addEventHandler( "onClientElementStreamIn", getRootElement(), function()
    if getElementType( source ) == "vehicle" then
        for name, comp in pairs(componentsFromData) do
            updateVehicleTuningComponent(source, name)
        end
        updateVehicleTuning(source)
    end
end)
function ska ()
 local vehicles = getElementsByType("vehicle")
    for k,v in pairs(vehicles) do
        if not v then return end
        updateVehicleTuning(v)
    end
end
addEventHandler("onClientResourceStart", resourceRoot, function()
    local vehicles = getElementsByType("vehicle")
    for k,v in pairs(vehicles) do
        if not v then return end
        updateVehicleTuning(v)
    end
end)

addEventHandler("onClientElementDataChange", root, function (nameData, oldVaue)
    if source.type == "vehicle" then
        for name in pairs(componentsFromData) do
            if name == nameData then
                updateVehicleTuning(source)
            end
        end
	end
end)