local scx,scy = guiGetScreenSize()
local px = scx/1920
local sizeX, sizeY = 400*px,600*px
local posX,posY = 10*px,scy-sizeY-100*px
local screen = dxCreateScreenSource( scx,scy )

function getVehicleHandlingProperty ( element, property )
    if isElement ( element ) and getElementType ( element ) == "vehicle" and type ( property ) == "string" then 
        local handlingTable = getVehicleHandling ( element ) 
        local value = handlingTable[property] 
        
        if value then 
            return value 
        end
    end
    
    return false 
end

wheels_setting_wnd_stat = false

data_1 = "WheelsSize"
stat_1 = false 
scroll_1 = 5

data_2 = "wheelsCastor"
stat_2 = false
scroll_2 = 5

data_3 = "WheelsAngleF"
stat_3 = false
scroll_3 = 5

data_4 = "WheelsAngleR"
stat_4 = false
scroll_4 = 5

data_5 = "WheelsWidthF"
stat_5 = false
scroll_5 = 5

data_6 = "WheelsWidthR"
stat_6 = false
scroll_6 = 5

data_7 = "wheelsOffsetF"
stat_7 = false
scroll_7 = 5

data_8 = "wheelsOffsetR"
stat_8 = false
scroll_8 = 5

stat_9 = false
scroll_9 = 5

setting_cena_W_1 = 10000
setting_cena_W_2 = 34990
setting_cena_W_3 = 15860
setting_cena_W_4 = 15860
setting_cena_W_5 = 7240
setting_cena_W_6 = 7240
setting_cena_W_7 = 14864
setting_cena_W_8 = 14864
setting_cena_W_9 = 20000

wheels_itog = 0



function wheels_setting_wnd ()
	local veh = getPedOccupiedVehicle(localPlayer)
	local sterling = getVehicleHandlingProperty ( veh, "steeringLock" )

	dxDrawWindow(5, scy / 2 - 150, 300, 350, "")
	dxDrawButton(5, scy / 2 + 170, 300, 25,"КУПИТЬ : "..wheels_itog.." $", true)
	
	dxDrawScrollBar(5, scy / 2 - 130, 250, 15, scroll_1, "РАДИУС ВСЕХ КОЛЁС : "..setting_cena_W_1.."$",data_1,Procent_1,stat_1)
	
	dxDrawScrollBar(5, scy / 2 - 100, 250, 15, scroll_2, "КАСТОР ПЕРЕДНИХ КОЛЁС : "..setting_cena_W_2.."$",data_2,Procent_2,stat_2)
	
	dxDrawScrollBar(5, scy / 2 - 70, 250, 15, scroll_3, "РАЗВАЛ ПЕРЕДНИХ КОЛЁС : "..setting_cena_W_3.."$",data_3,Procent_3,stat_3)
	
	dxDrawScrollBar(5, scy / 2 - 40, 250, 15, scroll_4, "РАЗВАЛ ЗАДНИХ КОЛЁС : "..setting_cena_W_4.."$",data_4,Procent_4,stat_4)
	
	dxDrawScrollBar(5, scy / 2 - 10, 250, 15, scroll_5, "ШИРИНА ПЕРЕДНИХ КОЛЁС : "..setting_cena_W_5.."$",data_5,Procent_5,stat_5)
	
	dxDrawScrollBar(5, scy / 2 + 20, 250, 15, scroll_6, "ШИРИНА ЗАДНИХ КОЛЁС : "..setting_cena_W_6.."$",data_6,Procent_6,stat_6)
	
	dxDrawScrollBar(5, scy / 2 + 50, 250, 15, scroll_7, "ВЫНОС ПЕРЕДНИХ КОЛЁС : "..setting_cena_W_7.."$",data_7,Procent_7,stat_7)

	dxDrawScrollBar(5, scy / 2 + 80, 250, 15, scroll_8, "ВЫНОС ЗАДНИХ КОЛЁС : "..setting_cena_W_8.."$",data_8,Procent_8,stat_8)
	
	dxDrawScrollBarHedit(5, scy / 2 + 110, 250, 15, scroll_9, "ВЫВОРОТ ПЕРЕДНИХ КОЛЁС : "..setting_cena_W_9.."$", Procent_9,stat_9)
	
	dxDrawButtonText_pas(5, scy / 2 + 140, 300, 15, "ВЫВОРОТ : "..math.floor(sterling), 1, 1)
	
	dxDrawButton(260, scy / 2 - 130, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 - 100, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 - 70, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 - 40, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 - 10, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 + 20, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 + 50, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 + 80, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 + 110, 40, 15, "X", true)
end


addEventHandler( "onClientClick", root, function(button, state)
	if wheels_setting_wnd_stat == true then
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 - 130, 250, 15
			if cursorPosition( scroll_1, y-5, 10, h+10 ) then
				scrollMoving_1 = true
			else
				if scroll_1 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						scrollMoving_1 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						scrollMoving_1 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if scrollMoving_1 then
				scrollMoving_1 = false
			end
		end
		-------------------------------------------------------------------------
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 - 100, 250, 15
			if cursorPosition( scroll_2, y-5, 10, h+10 ) then
				scrollMoving_2 = true
			else
				if scroll_2 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						scrollMoving_2 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						scrollMoving_2 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if scrollMoving_2 then
				scrollMoving_2 = false
			end
		end
		-------------------------------------------------------------------------
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 - 70, 250, 15
			if cursorPosition( scroll_3, y-5, 10, h+10 ) then
				scrollMoving_3 = true
			else
				if scroll_3 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						scrollMoving_3 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						scrollMoving_3 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if scrollMoving_3 then
				scrollMoving_3 = false
			end
		end
		-------------------------------------------------------------------------
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 - 40, 250, 15
			if cursorPosition( scroll_4, y-5, 10, h+10 ) then
				scrollMoving_4 = true
			else
				if scroll_4 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						scrollMoving_4 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						scrollMoving_4 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if scrollMoving_4 then
				scrollMoving_4 = false
			end
		end
		-------------------------------------------------------------------------
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 - 10, 250, 15
			if cursorPosition( scroll_5, y-5, 10, h+10 ) then
				scrollMoving_5 = true
			else
				if scroll_5 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						scrollMoving_5 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						scrollMoving_5 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if scrollMoving_5 then
				scrollMoving_5 = false
			end
		end
		-------------------------------------------------------------------------
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 + 20, 250, 15
			if cursorPosition( scroll_6, y-5, 10, h+10 ) then
				scrollMoving_6 = true
			else
				if scroll_6 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						scrollMoving_6 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						scrollMoving_6 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if scrollMoving_6 then
				scrollMoving_6 = false
			end
		end
		-------------------------------------------------------------------------
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 + 50, 250, 15
			if cursorPosition( scroll_7, y-5, 10, h+10 ) then
				scrollMoving_7 = true
			else
				if scroll_7 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						scrollMoving_7 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						scrollMoving_7 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if scrollMoving_7 then
				scrollMoving_7 = false
			end
		end
		-------------------------------------------------------------------------
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 + 80, 250, 15
			if cursorPosition( scroll_8, y-5, 10, h+10 ) then
				scrollMoving_8 = true
			else
				if scroll_8 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						scrollMoving_8 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						scrollMoving_8 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if scrollMoving_8 then
				scrollMoving_8 = false
			end
		end
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 + 110, 250, 15
			if cursorPosition( scroll_9, y-5, 10, h+10 ) then
				scrollMoving_9 = true
			else
				if scroll_9 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						scrollMoving_9 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						scrollMoving_9 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if scrollMoving_9 then
				scrollMoving_9 = false
			end
		end
	end
end)

addEventHandler( "onClientCursorMove", getRootElement( ), function ( _, _, xMove )
    if scrollMoving_1 then
		scroll_1 = xMove
	end
    if scrollMoving_2 then
		scroll_2 = xMove
	end
    if scrollMoving_3 then
		scroll_3 = xMove
	end
    if scrollMoving_4 then
		scroll_4 = xMove
	end
    if scrollMoving_5 then
		scroll_5 = xMove
	end
    if scrollMoving_6 then
		scroll_6 = xMove
	end
    if scrollMoving_7 then
		scroll_7 = xMove
	end
    if scrollMoving_8 then
		scroll_8 = xMove
	end
    if scrollMoving_9 then
		scroll_9 = xMove
	end
end)


function onClick_wheels_setting(button, state)
	if wheels_setting_wnd_stat == true then
		if button == "left" and state == "down" then
			if cursorPosition(5, scy / 2 - 135, 250, 25) then
				stat_1 = true
				if scroll_1 == 5 then
					scroll_1 = 6
					wheels_itog = wheels_itog + setting_cena_W_1
					Stat_buy_setting_wheels_1 = false
					Stat_but_setting_wheels_1 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				stat_1 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 - 105, 250, 25) then
				stat_2 = true
				if scroll_2 == 5 then
					scroll_2 = 6
					wheels_itog = wheels_itog + setting_cena_W_2
					Stat_buy_setting_wheels_2 = false
					Stat_but_setting_wheels_2 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				stat_2 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 - 75, 250, 25) then
				stat_3 = true
				if scroll_3 == 5 then
					scroll_3 = 6
					wheels_itog = wheels_itog + setting_cena_W_3
					Stat_buy_setting_wheels_3 = false
					Stat_but_setting_wheels_3 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				stat_3 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 - 45, 250, 25) then
				stat_4 = true
				if scroll_4 == 5 then
					scroll_4 = 6
					wheels_itog = wheels_itog + setting_cena_W_4
					Stat_buy_setting_wheels_4 = false
					Stat_but_setting_wheels_4 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				stat_4 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 - 15, 250, 25) then
				stat_5 = true
				if scroll_5 == 5 then
					scroll_5 = 6
					wheels_itog = wheels_itog + setting_cena_W_5
					Stat_buy_setting_wheels_5 = false
					Stat_but_setting_wheels_5 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				stat_5 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 + 15, 250, 25) then
				stat_6 = true
				if scroll_6 == 5 then
					scroll_6 = 6
					wheels_itog = wheels_itog + setting_cena_W_6
					Stat_buy_setting_wheels_6 = false
					Stat_but_setting_wheels_6 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				stat_6 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 + 45, 250, 25) then
				stat_7 = true
				if scroll_7 == 5 then
					scroll_7 = 6
					wheels_itog = wheels_itog + setting_cena_W_7
					Stat_buy_setting_wheels_7 = false
					Stat_but_setting_wheels_7 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				stat_7 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 + 75, 250, 25) then
				stat_8 = true
				if scroll_8 == 5 then
					scroll_8 = 6
					wheels_itog = wheels_itog + setting_cena_W_8
					Stat_buy_setting_wheels_8 = false
					Stat_but_setting_wheels_8 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				stat_8 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 + 105, 250, 25) then
				stat_9 = true
				if scroll_9 == 5 then
					scroll_9 = 6
					wheels_itog = wheels_itog + setting_cena_W_9
					Stat_buy_setting_wheels_9 = false
					Stat_but_setting_wheels_9 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				stat_9 = false
			end
			--------------------------------------------------------------------------------------------------------
			if cursorPosition(260, scy / 2 - 130, 40, 15) then
				if scroll_1 >= 5.5 then
					setElementData (getPedOccupiedVehicle (localPlayer), "WheelsSize", Stok_wheels_7 )
					wheels_itog = wheels_itog - setting_cena_W_1
					scroll_1 = 5
					Stat_but_setting_wheels_1 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Радиус всех коёлс ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 - 100, 40, 15) then
				if scroll_2 >= 5.5 then
					setElementData (getPedOccupiedVehicle (localPlayer), "wheelsCastor", Stok_wheels_10 )
					wheels_itog = wheels_itog - setting_cena_W_2
					scroll_2 = 5
					Stat_but_setting_wheels_2 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Кастор передних колёс ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 - 70, 40, 15) then
				if scroll_3 >= 5.5 then
					setElementData (getPedOccupiedVehicle (localPlayer), "WheelsAngleF", Stok_wheels_3 )
					wheels_itog = wheels_itog - setting_cena_W_3
					scroll_3 = 5
					Stat_but_setting_wheels_3 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Развал передних колёс ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 - 40, 40, 15) then
				if scroll_4 >= 5.5 then
					setElementData (getPedOccupiedVehicle (localPlayer), "WheelsAngleR", Stok_wheels_4 )
					wheels_itog = wheels_itog - setting_cena_W_4
					scroll_4 = 5
					Stat_but_setting_wheels_4 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Развал задних колёс ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 - 10, 40, 15) then
				if scroll_5 >= 5.5 then
					setElementData (getPedOccupiedVehicle (localPlayer), "WheelsWidthF", Stok_wheels_1 )
					wheels_itog = wheels_itog - setting_cena_W_5
					scroll_5 = 5
					Stat_but_setting_wheels_5 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Ширина передних колёс ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 + 20, 40, 15) then
				if scroll_6 >= 5.5 then
					setElementData (getPedOccupiedVehicle (localPlayer), "WheelsWidthR", Stok_wheels_2 )
					wheels_itog = wheels_itog - setting_cena_W_6
					scroll_6 = 5
					Stat_but_setting_wheels_6 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Ширина задних колёс ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 + 50, 40, 15) then
				if scroll_7 >= 5.5 then
					setElementData (getPedOccupiedVehicle (localPlayer), "wheelsOffsetF", Stok_wheels_8 )
					wheels_itog = wheels_itog - setting_cena_W_7
					scroll_7 = 5
					Stat_but_setting_wheels_7 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Вынос передних колёс ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 + 80, 40, 15) then
				if scroll_8 >= 5.5 then
					setElementData (getPedOccupiedVehicle (localPlayer), "wheelsOffsetR", Stok_wheels_9 )
					wheels_itog = wheels_itog - setting_cena_W_8
					scroll_8 = 5
					Stat_but_setting_wheels_8 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Вынос задних колёс ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 + 110, 40, 15) then
				if scroll_9 >= 5.5 then
					triggerServerEvent( "onVehicleChangeHandling_1", localPlayer, "Nastr1", getPedOccupiedVehicle( localPlayer ), Stok_wheels_11 )
					wheels_itog = wheels_itog - setting_cena_W_9
					scroll_9 = 5
					Stat_but_setting_wheels_9 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Выворот передних колёс ]", 3)
				end
			end
			--------------------------------------------------------------------------------------------------------
			if cursorPosition(5, scy / 2 + 170, 300, 25) then
				if getPlayerMoney() >= wheels_itog - 1 then--price
					triggerServerEvent("Spisanie_money", getRootElement(), localPlayer, wheels_itog )	
					showHelpMessage("Тюнинг", "Вы успешно купили настройку колёс за "..wheels_itog.." $", 3)
					wheels_itog = 0
					
					if scroll_1 >= 6 then
						Stat_buy_setting_wheels_1 = true
						Stat_but_setting_wheels_1 = false
						scroll_1 = 5
					end
					if scroll_2 >= 6 then
						Stat_buy_setting_wheels_2 = true
						Stat_but_setting_wheels_2 = false
						scroll_2 = 5
					end
					if scroll_3 >= 6 then
						Stat_buy_setting_wheels_3 = true
						Stat_but_setting_wheels_3 = false
						scroll_3 = 5
					end
					if scroll_4 >= 6 then
						Stat_buy_setting_wheels_4 = true
						Stat_but_setting_wheels_4 = false
						scroll_4 = 5
					end
					if scroll_5 >= 6 then
						Stat_buy_setting_wheels_5 = true
						Stat_but_setting_wheels_5 = false
						scroll_5 = 5
					end
					if scroll_6 >= 6 then
						Stat_buy_setting_wheels_6 = true
						Stat_but_setting_wheels_6 = false
						scroll_6 = 5
					end
					if scroll_7 >= 6 then
						Stat_buy_setting_wheels_7 = true
						Stat_but_setting_wheels_7 = false
						scroll_7 = 5
					end
					if scroll_8 >= 6 then
						Stat_buy_setting_wheels_8 = true
						Stat_but_setting_wheels_8 = false
						scroll_8 = 5
					end
					if scroll_9 >= 6 then
						Stat_buy_setting_wheels_9 = true
						Stat_but_setting_wheels_9 = false
						scroll_9 = 5
					end
				else
					showHelpMessage("Тюнинг", "У вас не хватает денег", 3)
				end
			end
		end
	end
end
addEventHandler("onClientClick", root, onClick_wheels_setting)