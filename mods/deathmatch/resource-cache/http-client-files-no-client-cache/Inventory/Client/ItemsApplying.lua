-- TrollingCont. 29.02.2020

local hudExport = exports.HUD

local width, height = guiGetScreenSize()

usedItemName = nil
local usedItemIndex = nil
local usedItemPath = nil

local imageCoordX = nil
local imageCoordY = nil
local cursorOffsetX = nil
local cursorOffsetY = nil

function renderMovedIcon()
    dxDrawImage(imageCoordX, imageCoordY, 60, 60, usedItemPath)
end

function handleCursorMove(relX, relY, absX, absY)
    imageCoordX = absX - cursorOffsetX
    imageCoordY = absY - cursorOffsetY
end

function setMovedIconRendered(state, path)
    if state then
       local curX, curY = getCursorPosition()
       curX = math.floor(curX * width)
       curY = math.floor(curY * height)
       cursorOffsetX = curX - itemsToRender[usedItemIndex].coordX
       cursorOffsetY = curY - itemsToRender[usedItemIndex].coordY
       usedItemPath = path
       imageCoordX = itemsToRender[usedItemIndex].coordX
       imageCoordY = itemsToRender[usedItemIndex].coordY
       addEventHandler("onClientCursorMove", root, handleCursorMove)
       addEventHandler("onClientRender", root, renderMovedIcon)
    else
        removeEventHandler("onClientRender", root, renderMovedIcon)
        removeEventHandler("onClientCursorMove", root, handleCursorMove)
    end
end

function startup()
    addEventHandler("onClientClick", root,
    function(button, state, absX, absY, fX, fY, fZ, clickedWorld)
        if button ~= "left" then return end

        if inventoryDisplayed then
            if state == "down" then

                for index,item in ipairs(itemsToRender) do
                    if absX >= item.regionX and absX <= item.regionX + item.regionSize and absY >= item.regionY and absY <= item.regionY + item.regionSize then
                        setItemDisplayed(item.name, false)
                        usedItemName = item.name
                        usedItemIndex = index
                        setMovedIconRendered(true, item.path)
                        return
                    end
                end
                usedItemName = nil
            else
                if not usedItemName then return end
                if not clickedWorld then
                    setMovedIconRendered(false)
                    setItemDisplayed(usedItemName, true)
                else
                    setMovedIconRendered(false)
                    --local clickedWorldType = getElementType(clickedWorld)
                    triggerServerEvent("tcOnItemApply", localPlayer, clickedWorld, usedItemName)
                end
            end
        end
    end)

    addEventHandler("tcOnItemApply", localPlayer,
    function(notificationText, success, itemName)
        if not success then
            setItemDisplayed(usedItemName, true)
        end
        
        if notificationText then
            hudExport:showNotification(true, notificationText, 4000)
        end

        if itemName == "drugs_2" then
            setDragEffects(true)
            setTimer(function() setDragEffects(false) end, 60000, 1)
        end
    end)

    --setDragEffects(true)
end

addEventHandler("onClientResourceStart", resourceRoot, startup)