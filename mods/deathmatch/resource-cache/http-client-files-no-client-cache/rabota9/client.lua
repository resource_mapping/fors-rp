﻿addEvent("openinf_trash", true)
screenWidth, screenHeight = guiGetScreenSize()
function windm ()
	if not getElementData ( localPlayer, "trash_working") then
		  infowind = guiCreateWindow(screenWidth - 700, screenHeight - 500, 200, 229, "Работа мусорщиком", false)
		  infotext = guiCreateMemo(15,20,165,150,"Ваша задача собирать пикапы мусора за вознаграждение - 500$!",false,infowind)
		  guiMemoSetReadOnly( infotext, true )
		  --guiSetFont(text2, "default-bold-small") 
		  showCursor(true)
		  guiSetVisible ( infowind , true) 
		  guiWindowSetSizable(infowind, false) 
		  Button_Glose = guiCreateButton(10, 174, 90, 45, "Закрыть", false, infowind)
		  Button_Start = guiCreateButton(100, 174, 90, 45, "Начать", false, infowind)
		  addEventHandler("onClientGUIClick", Button_Start, kit )
			addEventHandler("onClientGUIClick", Button_Glose, noshow )
	else
		for i, v in ipairs ( createdPickups ) do
			if isElement ( v ) then
				destroyElement ( v )
			end
		end
		triggerServerEvent ( "finitoWork_trash", localPlayer )
		setElementData ( localPlayer, "trash_working", false )
		outputChatBox ( "Вы закончили работу", 255, 255, 255, true )
	end
end
addEventHandler("openinf_trash", root, windm )
function noshow ()
 if ( source == Button_Glose ) then
   destroyElement ( infowind )
   showCursor ( false ) 
 end
end
function kit ()
 if ( source == Button_Start ) then
	startWorking()
    destroyElement ( infowind )
    showCursor ( false )
  end 
end

addEvent ( "pay", true )
function giving ()
 outputChatBox ( "Вы собрали мусор и получили - 500$ ", getRootElement(), 255, 100, 100, true )
end
addEventHandler("pay", resourceRoot, giving )
addEvent ( "get", true )
function peremen (plr)
 plr = getLocalPlayer ()
end
addEventHandler("get", resourceRoot, peremen )

pickupSpawns = {

{ -1888.6102294922,-1646.9315185547,21.75 },
{ -1875.5611572266,-1665.4851074219,24.15781021118 },
{ -1857.8540039063,-1650.8083496094,26.555233001709 },
{ -1859.8540039063,-1687.8083496094,40 },
{ -1842.1394042969,-1641.1545410156,21.75 },
{ -1891.6102294922,-1678.9315185547,22.75 },
{ -1880.1394042969,-1690.1545410156,21.75 },
{ -1891.6102294922,-1678.9315185547,22.75 },
{ -1825.1394042969,-1685.1545410156,22.75 },
{ -1816.1394042969,-1632.1545410156,22.75 },
}

max_pickups_set = 9 

createdPickups = {}

max_pickups = max_pickups_set
if max_pickups_set > #pickupSpawns then
	max_pickups = #pickupSpawns
end

function startWorking ()
	if not getElementData ( localPlayer, "trash_working" ) then
		createdPickups = {}
		triggerServerEvent ( "picku_trash", localPlayer )
		setElementData ( localPlayer, "trash_working", true )
		setElementData ( localPlayer, "trash_working_hit", 0 )
		for i, v in ipairs ( pickupSpawns ) do
			local pick = createPickup ( v[1],v[2],v[3], 3, 1450, 10000 )
			setElementData ( pick, "taken", false )
			setElementData ( pick, "trash_pickup", true )
			table.insert (createdPickups, pick)
		end
	end
end	

function clientPickupHit(thePlayer, matchingDimension)
	if thePlayer == localPlayer and getElementData ( source, "trash_pickup" ) and not getElementData ( source, "taken" ) then
		setElementData ( source, "taken", true )
		outputChatBox ( "Вы собрали мусор и получили - 500$ ", 255, 100, 100, true )
		exports [ "urovni" ]:givePlayerXP (thePlayer, 20) 
		triggerServerEvent ( "giveMoneyFromClient_trash", localPlayer, 500 ) -
		local hitted = getElementData ( localPlayer, "trash_working_hit" ) or 0
		hitted = hitted+1
		destroyElement(source)
		setElementData ( localPlayer, "trash_working_hit", hitted)
		if hitted >= max_pickups then
			for i, v in ipairs ( createdPickups ) do
				if isElement ( v ) then
					destroyElement ( v )
				end
			end
			setElementData ( localPlayer, "trash_working", false )
			triggerServerEvent ( "finitoWork_trash", localPlayer )
			outputChatBox ( "Вы закончили работу", 255, 255, 255, true )
		end
	end
end
addEventHandler("onClientPickupHit", root, clientPickupHit)







