﻿-- ["Автосалон"] = {x,y,z,x2,y2,z2,rx,ry,rz}, 
-- где x,y,z это позиция маркера 
-- и x2,y2,z2,rx,ry,rz это позиция спауна автомобиля при покупке.

vehShopsTable = 
{
	["Elite Salon"] = {-1954.122,301.607,33.7,-1928.046508,273.3952,40.521,0,0,180},
	["Europa"] = {-2097.3369140625,-757.513671875,31, -2104.384765625,-778.845703125,32,0,0,270},
	["Audi"] = {3400.9,-514.4,11, 3361.9897460938,-560.8,10.9,0,0,270},
	["Mercedes"] = {-1657.3,1210.1,6.25, -1637.2,1207.7,7.2,0,0,270},
	["Vaz"] = {562.7,-1270.6,16.2,508.8,-1305.8,17.2,0,0,270},
	["Spawn"] = {3435.8,-606.5,9.93, 3382.1,-608.0,10.9,0,0,270},
	["BMW"] = {1096.5,1598.7,11.5,1028.6,1750.4,10.8,0,0,270},
	
}


vehShopVehicles =
{
	["Elite Salon"] = 
	{
		{411,"Bugatti Chiron",90000000}, -- {ID, Название, Цена}
		{402,"Jaguar F-Type",7000000},
		{503,"Ferrari 458",19000000},
		{415,"Lamborghini Gallardo", 20000000},
		{506,"McLaren P1", 75000000},
		{416,"Rolls Royce Ghost",23000000},
		{502,"Chevrolet Corvette C7",1},
		{495,"Porsche Cayenne Turbo S",1},
		{494,"Nissan GT-R",1},
	},
	["Mercedes"] = 
	{
		{589,"Mersedes-Benz E500",6500}, -- {ID, Название, Цена}
		{400,"Mersedes-Benz Gl63",850},
		{470,"Mercedes-Benz G65 AMG",100},
		{526,"Mercedes-Benz W126 Coupe",6500},
		{516,"Mercedes-Benz S65 W221",6500},
		{420,"Mercedes-AMG C63 W205",6500},
		{540,"Mercedes-Benz E500 W124",6500},
		{418,"Mercedes-Benz S63 W2223",6500},
		{598,"Mercedes-Benz E63 AMG",6500},
		{587,"Mersedes-Benz SLS AMG",6500},
	},
	["BMW"] = 
	{
		{507,"BMW 525i E34",0}, -- {ID, Название, Цена}	
		{580,"BMW X5M",0},
		{409,"BMW M5 E39",0},
		{550,"BMW M5 E60",0},
		{479,"BMW M5 E60",0},,
		{458,"BMW M5 F10",0},
		{535,"BMW X5 E53",0},
	},
	["Europa"] = 
	{
		{579,"Chevrolet Suburban",1}, -- {ID, Название, Цена}	
		{546,"Renault Fluence ",1},
		{500,"Shelby GT500",1},
		{489,"Ford F-150 SVT Raptor ",1},
		{439,"Ford Mustang Fastbacк ",1},
		{429,"Volkswagen Golf IV",1},
		{541,"RUF CTR",1},
		{517,"Porsche Boxster GTS",1},
		{400,"Range Rover",1},
	},
	["Audi"] = 
	{
		{477,"Audi RS5",7500000}, -- {ID, Название, Цена}
		{492,"Audi A6",800000},
		{551,"Audi A4",1200000},
		{554,"Audi Q7", 3500000},
		{508,"Audi RS7", 7500000},
	},
	["Vaz"] = 
	{
		{547,"Лада Приора",1}, -- {ID, Название, Цена}	
		{421,"ВАЗ 2109",1},
		{404,"ВАЗ 2106",1},
		{518,"ВАЗ 2113",1},
		{401,"Лада Нива",1},
		{585,"Волга",1},
		{445,"ВАЗ 2101",1},
		{410,"ВАЗ 2107",1},
		{457,"ВАЗ 1111 Ока",1},
		{419,"ВАЗ 2108",1},
	},
	["Spawn"] = 
	{
		{462,"Скутер",0}, -- {ID, Название, Цена}	
	},
	["Donate"] = {} -- Этот раздел не удалять
}

vehShopColors = -- Цвета на выбор в салоне.
{
	{200,0,0},
	{0,0,0},
	{0,255,255},
	{0,11,240},
	{0,160,10},
	{255,255,255},
}

testDriveSpawnPosition = {-468.36276245117,592.77911376953,18,90} -- Позиция начала тест-драйвов

vehShopWheels = {1073,1074,1075,1076,1077,1078,1079,1080,1081} -- ID дисков

numberSwitchPrice = 1000 -- Цена установки номера

tradeMarkerPositions = -- Позиции маркеров трейда
{
	{-1635.5539550781,653.48583984375,6.1875},
}

defaultHandlingTable = -- Здесь просто паста из стандартного handling editor'а
{
    [411] = "INFERNUS 600 800 1.5 0 0 -0.25 70 1 1 0.3 5 420 60 -1 4 p 15 0 false 36 4 0.19 0 0.25 -0.1 0.5 0.4 0.37 0.5 95000 40002004 C04000 1 1 1",
	[445] = "SULTAN 1400 3400 2.4 0 0.1 -0.1 75 0.8 0.8 0.5 5 200 11.2 5 4 p 10 0.5 false 30 1.2 0.15 0 0.28 -0.2 0.5 0.3 0.25 0.6 35000 2800 4000002 1 1 0",
	[479] = "REGINA 1500 3800 2 0 0.2 0 75 0.65 0.85 0.52 4 165 15 25 f p 5 0.6 false 30 1 0.1 0 0.27 -0.17 0.5 0.2 0.24 0.48 18000 2020 1 1 1 0",
	[405] = "SENTINEL 2500 4000 2.2 0 0 -0.2 75 0.65 0.75 0.5 5 330 15 10 4 p 10 0.5 false 27 1 0.08 0 0.3 -0.25 0.5 0.3 0.2 0.4 35000 2000 484400 0 1 0",
	[402] = "BUFFALO 2500 4000 2 0 0 -0.1 85 0.7 0.9 0.5 5 300 25 5 4 p 11 0.45 false 30 1.2 0.12 0 0.28 -0.1 0.5 0.4 0.25 0.3 35000 2800 204400 1 1 ",
	[549] = "TAMPA 1700 4166.4 2.5 0 0.15 0 70 0.65 0.85 0.52 5 150 10 100 r p 8.17 0.45 false 35 1 0.9 0 0.3 -0.15 0.5 0.5 0.3 0.5 19000 40000004 4 1 1 1 ",
	[436] = "PREVION 1700 3000 2 0 0.3 -0.1 70 0.7 0.8 0.45 5 200 9.1 79 f p 8 0.65 false 35 1.2 0.16 2 0.31 -0.08 0.55 0.3 0.21 0.5 9000 220000 4400 0 0 0 ",
	[451] = "TURISMO 2300 3400 2 0 -0.3 -0.2 70 1 1 0.43 5 380 36 16 r p 11 0.51 false 30 1 0.13 0 0.18 -0.14 0.5 0.4 0.17 0.72 95000 40022004 B08403 1 1 1 ",
	[533] = "FELTZER 1575 3150 2.5 0 0 -0.2 70 0.85 0.85 0.5 5 354 50 120 4 p 7 0.45 false 45 1.6 0.12 1.3 0.32 -0.05 0.45 0.9 0.22 0.54 35000 2000 1008003 1 1 19"
}

function isDonateVehicle(id)
	for k,v in pairs(vehShopVehicles["Donate"]) do
		if id == v[1] then 
			return true
		end
	end
end

function getVehicleData(id)
	for key,val in pairs(vehShopVehicles) do
		for k,v in pairs(val) do
			if v[1] == tonumber(id) then
				return v
			end
		end
	end
end

function findVeh(id)
	local vehicles = getElementsByType("vehicle")
	for k,v in pairs(vehicles) do
		if getElementData(v,"ID") == id then
			return v
		end
	end
	return false
end

function convertNumber(amount)
	local formatted = amount
	while true do  
    formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1 %2')
    if (k==0) then
     	break
    end
  	end
	return formatted
end