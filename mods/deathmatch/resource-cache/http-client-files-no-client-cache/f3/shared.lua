keyToOpen = "F3"

customVehicleNames = {
	[502] = "Рено",
	[503] = "Хатринг",
	[494] = "Первооткрыватель",
}

function isCarSpawned(ID)
	assert(ID and type(ID) == "number", "Неверный ID @isCarSpawned")
	for i,v in ipairs(getElementsByType("vehicle")) do 
		if getElementData(v,"ID") == ID then 
			return v
		end
	end
	return false
end

function getVehNameFromModel(ID)
	if customVehicleNames[ID] then 
		return customVehicleNames[ID]
	else
		return getVehicleNameFromModel(ID)
	end
end
