﻿function dxDrawWindow(text, x, y, w, h)
	local colorWindow = tocolor(220, 220, 220, 220)
	local lineColor = tocolor(20, 20, 20, 220)
	local textColor = tocolor(255, 255, 255, 255)
	local fontText = "default-bold"
	dxDrawRectangle(x, y, w, h, colorWindow)
	dxDrawRectangle(x+4, y+4, w-8, 22, lineColor)
	dxDrawText(text, x*2, y+6, w, h, textColor, 1, fontText, "center", "top")
end

function dxDrawButton(x, y, w, h, text)
	local mx, my = getMousePos()
	local colorButton = tocolor(40, 40, 40)
	local textColor = tocolor(255, 255, 255)
	local fontText = "default-bold"
	if isPointInRect(mx, my, x - 2, y, w, h) then
     colorButton = tocolor(60, 60, 60)
	  else
	 colorButton = tocolor(40, 40, 40)
	end
	dxDrawRectangle(x, y, w, h, colorButton, false)
	dxDrawText(text, x, y, w + x, h + y, textColor, 1, fontText, "center", "center", true, false, false, true)
end


function dxDrawShadowText( text, x, y, w, h, color, scale, font, alignX, alignY, clip, wordBreak, postGUI, colorCode )
	dxDrawText ( text:gsub( "#%x%x%x%x%x%x", "" ), x - 1, y - 1, w - 1, h - 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false, true )
	dxDrawText ( text:gsub( "#%x%x%x%x%x%x", "" ), x + 1, y - 1, w + 1, h - 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false, true )
	dxDrawText ( text:gsub( "#%x%x%x%x%x%x", "" ), x - 1, y + 1, w - 1, h + 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false, true )
	dxDrawText ( text:gsub( "#%x%x%x%x%x%x", "" ), x + 1, y + 1, w + 1, h + 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false, true )
	dxDrawText ( text:gsub( "#%x%x%x%x%x%x", "" ), x - 1, y, w - 1, h, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false, true )
	dxDrawText ( text:gsub( "#%x%x%x%x%x%x", "" ), x + 1, y, w + 1, h, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false, true )
	dxDrawText ( text:gsub( "#%x%x%x%x%x%x", "" ), x, y - 1, w, h - 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false, true )
	dxDrawText ( text:gsub( "#%x%x%x%x%x%x", "" ), x, y + 1, w, h + 1, tocolor ( 0, 0, 0, 155 ), scale, font, alignX, alignY, clip, wordBreak, false, true)
	dxDrawText ( text, x, y, w, h, color, scale, font, alignX, alignY, clip, wordBreak, postGUI, true )
end



function dxDrawHelp(name, description)
	local screenW, screenH = guiGetScreenSize()


	dxDrawImage( screenW/2-150, screenH-140, 320, 70, 'bg.png', angle, 0, -120 )
	dxDrawText ( name, screenW/1.95, screenH-125, screenW/2, screenH-120, tocolor ( 205, 205, 205, 255 ), 1.25,"default-bold", "center", "center" )
	dxDrawText  (description, screenW/1.95, screenH-75, screenW/2, screenH-100, tocolor ( 3, 192, 21, 255 ), 1.7,"default-bold", "center", "center" )
end

function dxDrawCorner( x, y, r, color, corner, postGUI )
	local corner = corner or 1
	local start = corner % 2 == 0 and 0 or -r
	local stop = corner % 2 == 0 and r or 0
	local m = corner > 2 and -1 or 1
	local h = ( corner == 1 or corner == 3 ) and -1 or 1
 	for yoff = start, stop do
 	local xoff = math.sqrt( r * r - yoff * yoff ) * m
 	dxDrawRectangle( x - xoff, y + yoff, xoff, h, color, postGUI )
	end 
end

function dxDrawRecCircle( posX, posY, width, height, radius, color, postGUI )	
	local posX, posY, width, height = round( posX ), round( posY ), round( width ), round( height )
	local radius = radius and math.min( radius, math.min( width, height ) / 2 )  or 12
	dxDrawRectangle( posX, posY + radius, width, height - radius * 2, color, postGUI )
	dxDrawRectangle( posX + radius, posY, width - 2 * radius, radius, color, postGUI )
	dxDrawRectangle( posX + radius, posY + height - radius, width - 2 * radius, radius, color, postGUI )
	dxDrawCorner( posX + radius, posY + radius, radius, color, 1, postGUI )
	dxDrawCorner( posX + radius, posY + height - radius, radius, color, 2, postGUI )
	dxDrawCorner( posX + width - radius, posY + radius, radius, color, 3, postGUI )
	dxDrawCorner( posX + width - radius, posY + height - radius, radius, color, 4, postGUI )
end


function isPointInRect(x, y, rx, ry, rw, rh)
	if x >= rx and y >= ry and x <= rx + rw and y <= ry + rh then
		return true
	else
		return false
	end
end

function cursorPosition(x, y, w, h)
	if (not isCursorShowing()) then
		return false
	end
	local mx, my = getCursorPosition()
	local fullx, fully = guiGetScreenSize()
	cursorx, cursory = mx*fullx, my*fully
	if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
		return true
	else
		return false
	end
end

function getMousePos()
	local xsc, ysc = guiGetScreenSize()
	local mx, my = getCursorPosition()
	if not mx or not my then
		mx, my = 0, 0
	end
	return mx * xsc, my * ysc
end