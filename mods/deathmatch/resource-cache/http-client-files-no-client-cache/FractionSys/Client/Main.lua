-- TrollingCont. 24.02.2020

screenWidth, screenHeight = guiGetScreenSize()
wndTitleFont = nil

local hudExport = exports.HUD

local playerFractionId = nil
openedWndCount = 0

noBindsEdits = {}

local teleportMarkerIndex = nil
local teleportMarkerGroup = nil

function startup()
	
    --[[txd = engineLoadTXD("zazik.txd")
    engineImportTXD(txd, 604)
    dff = engineLoadDFF("zazik.dff")
	engineReplaceModel(dff, 604)--]]

    loadstring(exports.dgs:dgsImportFunction())()
	wndTitleFont = dgsCreateFont("Res/HelveticaRegular.ttf", 11)

    addEventHandler("onClientKey", root,
	function(button, isPress)
		if isPress then return end

        if button == "F6" then
            if not playerFractionId then return end
			setWindowOpened(fracSysWindows[playerFractionId].window, not dgsGetVisible(fracSysWindows[playerFractionId].window))
		elseif button == "lalt" and teleportMarkerGroup then
			triggerServerEvent("tcTeleportPlayer", localPlayer, teleportMarkerGroup, teleportMarkerIndex)
		end
	end)
	
	addEventHandler("onClientMarkerHit", root,
	function(player, matchingDim)
		local notifyStatus = getElementData(source, "tcfs.notifyMarker")

		if player == localPlayer and matchingDim and notifyStatus then
			teleportMarkerGroup = getElementData(source, "tcfs.nmId")
			if notifyStatus == "in" then
				hudExport:showNotification(true, "Нажмите левый Alt, чтобы попасть в интерьер", 5000)
				teleportMarkerIndex = 2
			else
				hudExport:showNotification(true, "Нажмите левый Alt, чтобы покинуть интерьер", 5000)
				teleportMarkerIndex = 1
			end
		end
	end)

	addEventHandler("onClientMarkerLeave", root,
	function(player, matchingDim)
		if player == localPlayer and getElementData(source, "tcfs.notifyMarker") then
			teleportMarkerGroup = nil
		end
	end)
    
    playerFractionId = getElementData(localPlayer, FRACTION_ID_KEY)

    addEventHandler("onClientElementDataChange", localPlayer,
    function(key, oldVal, newVal)
        if key == FRACTION_ID_KEY then
			playerFractionId = newVal

			local text = "Ваша фракция: "
			if not newVal then
				text = text.."отсутствует"
			elseif newVal == HWPATROL_FRACTION_ID then
				text = text.."ДПС"
			end
			hudExport:showNotification(true, text, 5000)
        end
	end)

	guiSetInputMode("allow_binds")
	
	addEventHandler("onDgsFocus", root,
	function()
		if noBindsEdits[source] then
			guiSetInputMode("no_binds")
		end
	end)

	addEventHandler("onDgsBlur", root,
	function()
		if noBindsEdits[source] then
			guiSetInputMode("allow_binds")
		end
	end)
end

function displayCursor(display)
	if display then
		showCursor(true)
	else
		if openedWndCount == 0 then
			showCursor(false)
		end
	end
end

function setWindowOpened(window, opened)
	if opened then
		if not dgsGetVisible(window) then
			openedWndCount = openedWndCount + 1
			dgsSetVisible(window, true)
		end
		displayCursor(true)
	else
		if dgsGetVisible(window) then
			openedWndCount = openedWndCount - 1
			dgsSetVisible(window, false)
		end
		displayCursor(false)
	end

	addEventHandler("onClientResourceStop", resourceRoot,
	function()
		guiSetInputMode("allow_binds")
	end)
end

addEventHandler("onClientResourceStart", resourceRoot, startup)