-- TrollingCont. 15.02.2020

local tabElements = {}
local activeTab = nil

local workersButton = nil
local logsButton = nil

-- fracPlayersList[accountName] = {
--	rank = ...,
--  isOnline = ...
-- }
local fracPlayersList = { }

-- Статистика, операции, розыск - все
-- Сотрудники - viewWorkers
-- Журнал - viewLogs

function hasPlayerHwPatrolRight(player, right)
	local fracData = getElementData(localPlayer, FRACTION_DATA_KEY)
	if not fracData then return false end
	if not hwPatrolRights[fracData.rank] then return false end
	return hwPatrolRights[fracData.rank][right]
end

function updateWorkersList()
	dgsGridListClear(tabElements[workersButton].playersGridList)

	for accName,player in pairs(fracPlayersList) do
		dgsGridListAddRow(tabElements[workersButton].playersGridList, i, accName, player.rank, player.isOnline and "Да" or "Нет")
	end
end

function selectTab(buttonAsTab)
	if buttonAsTab == workersButton and not hasPlayerHwPatrolRight(localPlayer, "viewWorkers") then return end
	if buttonAsTab == logsButton and not hasPlayerHwPatrolRight(localPlayer, "viewLogs") then return end
	
	if activeTab then
		dgsSetProperty(activeTab, "color", {0xFF181818, 0xFF303030, 0xFFA00000})
		for i,elem in pairs(tabElements[activeTab]) do
			dgsSetVisible(elem, false)
		end
	end
	dgsSetProperty(buttonAsTab, "color", {0xFFA00000, 0xFFA00000, 0xFFA00000})
	for i,elem in pairs(tabElements[buttonAsTab]) do
		dgsSetVisible(elem, true)
	end
	activeTab = buttonAsTab
end

function startup()
    loadstring(exports.dgs:dgsImportFunction())()

	local wndWidth = 700
	local wndHeight = 480

	fracSysWindows[HWPATROL_FRACTION_ID].window = dgsCreateWindow((screenWidth - wndWidth)/2, (screenHeight - wndHeight)/2, wndWidth, wndHeight,
	"Управление фракцией ДПС", false, 0xFFE0E0E0, 28, nil, 0xFF202020, nil, 0xFF101010, 5, true)
	dgsSetFont(fracSysWindows[HWPATROL_FRACTION_ID].window, wndTitleFont)
	dgsSetVisible(fracSysWindows[HWPATROL_FRACTION_ID].window, false)

	local commonFont = dgsCreateFont("Res/HelveticaRegular.ttf", 10)
	local buttonsFont = dgsCreateFont("Res/HelveticaRegular.ttf", 11)
	local searchPlayerFont = dgsCreateFont("Res/HelveticaRegular.ttf", 11)
	local comboBoxFont = dgsCreateFont("Res/HelveticaRegular.ttf", 9)

	local statButton = dgsCreateButton(10, 10, 128, 30, "Статистика", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000)
	dgsSetFont(statButton, buttonsFont)
	tabElements[statButton] = {
		playersCountText = dgsCreateLabel(20, 75, 100, 40, "", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0)
	}
	dgsSetFont(tabElements[statButton].playersCountText, commonFont)

	local operationsButton = dgsCreateButton(148, 10, 128, 30, "Операции", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000)
	dgsSetFont(operationsButton, buttonsFont)
	tabElements[operationsButton] = {
		nameSurnameText = dgsCreateLabel(20, 60, 100, 20, "Имя и фамилия", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0),
		playerNameEdit = dgsCreateEdit(20, 90, 200, 30, "", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, 0xFF202020),
		removeFromFracButton = dgsCreateButton(240, 90, 120, 30, "Уволить", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000),
		rankText = dgsCreateLabel(20, 140, 100, 20, "Изменение должности:", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0),
		ranksComboBox = dgsCreateComboBox(20, 170, 200, 25, "", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 20, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF202020, 0xFF303030, 0xFFA00000),
		saveRankButton = dgsCreateButton(20, 300, 200, 30, "Изменить должность", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000),
		giveFeeText = dgsCreateLabel(240, 140, 120, 30, "Выдача штрафа:", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0),
		feeComboBox = dgsCreateComboBox(240, 170, 200, 25, "", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 20, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF202020, 0xFF303030, 0xFFA00000),
		giveFeeButton = dgsCreateButton(240, 300, 200, 30, "Выдать штраф", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000),
		getWantedText = dgsCreateLabel(460, 140, 100, 25, "Розыск:", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0),
		getWantedComboBox = dgsCreateComboBox(460, 170, 200, 25, "", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 20, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF202020, 0xFF303030, 0xFFA00000),
		getWantedButton = dgsCreateButton(460, 300, 200, 30, "Выдать розыск по статье", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000),
		removeWantedButton = dgsCreateButton(460, 340, 200, 30, "Снять розыск по статье", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000)
	}
	noBindsEdits[tabElements[operationsButton].playerNameEdit] = true
	dgsSetFont(tabElements[operationsButton].nameSurnameText, commonFont)
	dgsSetFont(tabElements[operationsButton].playerNameEdit, searchPlayerFont)
	dgsSetFont(tabElements[operationsButton].removeFromFracButton, commonFont)
	dgsSetFont(tabElements[operationsButton].rankText, commonFont)
	dgsSetFont(tabElements[operationsButton].ranksComboBox, comboBoxFont)
	dgsSetFont(tabElements[operationsButton].giveFeeText, commonFont)
	dgsSetFont(tabElements[operationsButton].getWantedText, commonFont)
	dgsSetFont(tabElements[operationsButton].getWantedComboBox, commonFont)
	dgsSetFont(tabElements[operationsButton].saveRankButton, commonFont)
	dgsSetFont(tabElements[operationsButton].giveFeeButton, commonFont)
	dgsSetFont(tabElements[operationsButton].getWantedButton, commonFont)
	dgsSetFont(tabElements[operationsButton].removeWantedButton, commonFont)
	for rankId,rankName in pairs(hwPatrolRanks) do
		local itemId = dgsComboBoxAddItem(tabElements[operationsButton].ranksComboBox, rankId.." | "..rankName)
		dgsComboBoxSetItemColor(tabElements[operationsButton].ranksComboBox, itemId, 0xFFE0E0E0)
	end
	for id,elem in ipairs(wantedReasons) do
		local itemId = dgsComboBoxAddItem(tabElements[operationsButton].getWantedComboBox, elem.name)
		dgsComboBoxSetItemColor(tabElements[operationsButton].getWantedComboBox, itemId, 0xFFE0E0E0)
	end

	local wantedButton = dgsCreateButton(286, 10, 128, 30, "Розыск", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000)
	dgsSetFont(wantedButton, buttonsFont)
	tabElements[wantedButton] = {}

	workersButton = dgsCreateButton(424, 10, 128, 30, "Сотрудники", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000)
	dgsSetFont(workersButton, buttonsFont)
	tabElements[workersButton] = {
		playersGridList = dgsCreateGridList(10, 45, 680, 350, false, fracSysWindows[HWPATROL_FRACTION_ID].window, 26, 0xFF202020, 0xFFE0E0E0, 0xFF202020, 0xFF202020, 0xFF808080, 0xFFA00000),
		showOffline = dgsCreateCheckBox(14, 405, 100, 30, "Оффлайн", false, false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0),
		searchPlayerEdit = dgsCreateEdit(150, 405, 540, 30, "", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, 0xFF202020)
	}
	noBindsEdits[tabElements[workersButton].searchPlayerEdit] = true
	dgsSetFont(tabElements[workersButton].playersGridList, commonFont)
	dgsSetProperty(tabElements[workersButton].playersGridList, "rowTextColor", 0xFFE0E0E0)
	dgsSetProperty(tabElements[workersButton].playersGridList, "rowHeight", 25)
	dgsGridListAddColumn(tabElements[workersButton].playersGridList, "Имя и фамилия", 0.45)
	dgsGridListAddColumn(tabElements[workersButton].playersGridList, "Должность", 0.3)
	dgsGridListAddColumn(tabElements[workersButton].playersGridList, "Онлайн", 0.21)
	dgsSetFont(tabElements[workersButton].showOffline, commonFont)
	dgsSetFont(tabElements[workersButton].searchPlayerEdit, searchPlayerFont)
	dgsEditSetPlaceHolder(tabElements[workersButton].searchPlayerEdit, "Поиск по сотрудникам...")
	dgsSetProperty(tabElements[workersButton].searchPlayerEdit, "placeHolderFont", searchPlayerFont)
	dgsSetProperty(tabElements[workersButton].searchPlayerEdit, "placeHolderColor", 0xFF505050)
	dgsSetVisible(workersButton, hasPlayerHwPatrolRight(localPlayer, "viewWorkers"))

	logsButton = dgsCreateButton(562, 10, 128, 30, "Журнал", false, fracSysWindows[HWPATROL_FRACTION_ID].window, 0xFFE0E0E0, 1, 1, nil, nil, nil, 0xFF181818, 0xFF303030, 0xFFA00000)
	dgsSetFont(logsButton, buttonsFont)
	tabElements[logsButton] = {}
	dgsSetVisible(logsButton, hasPlayerHwPatrolRight(localPlayer, "viewLogs"))

	for name,tab in pairs(tabElements) do
		for i,elem in pairs(tab) do
			dgsSetVisible(elem, false)
		end
	end

	selectTab(statButton)

	addEventHandler("onDgsMouseClickUp", root,
	function()
		if tabElements[source] then
			selectTab(source)
			return
		end
	end)

	addEventHandler("onDgsMouseClickUp", root,
	function()
		if source == tabElements[operationsButton].getWantedButton or source == tabElements[operationsButton].removeWantedButton then

			local accountName = dgsGetText(tabElements[operationsButton].playerNameEdit)
			local selectedItem = dgsComboBoxGetSelectedItem(tabElements[operationsButton].getWantedComboBox)
			if selectedItem == -1 then
				outputChatBox("#CCCCCCВыберите статью", 0, 0, 0, true)
				return
			end

			if source == tabElements[operationsButton].getWantedButton then
				triggerServerEvent("tcChangeWantedStatus", localPlayer, accountName, "getWanted", selectedItem)
			else
				triggerServerEvent("tcChangeWantedStatus", localPlayer, accountName, "clearWanted", selectedItem)
			end
		end
	end)

	addEventHandler("onDgsMouseClickUp", tabElements[operationsButton].removeFromFracButton,
	function()
		local accountName = dgsGetText(tabElements[operationsButton].playerNameEdit)
		triggerServerEvent("tcSetHwPatrolFraction", localPlayer, accountName, "remove")
	end, false)

	addEventHandler("onDgsMouseClickUp", tabElements[operationsButton].saveRankButton,
	function()
		local accountName = dgsGetText(tabElements[operationsButton].playerNameEdit)

		local selectedItem = dgsComboBoxGetSelectedItem(tabElements[operationsButton].ranksComboBox)
		if selectedItem == -1 then
			outputChatBox("#CCCCCCВыберите звание", 0, 0, 0, true)
			return
		end

		local newRankIdEnd = string.find(dgsComboBoxGetItemText(tabElements[operationsButton].ranksComboBox, selectedItem), "|")
		local newRankId = string.sub(dgsComboBoxGetItemText(tabElements[operationsButton].ranksComboBox, selectedItem), 1, newRankIdEnd - 2)

		triggerServerEvent("tcSetHwPatrolFraction", localPlayer, accountName, "setRank", newRankId)
	end, false)

	addEventHandler("tcRequestFractionWorkers", localPlayer,
	function(accTable)
		dgsSetText(tabElements[statButton].playersCountText, "Игроков во фракции: "..tostring(#accTable))

		for i,acc in ipairs(accTable) do
			fracPlayersList[acc.accountName] = { rank = hwPatrolRanks[fromJSON(acc.fracData)["rank"]], isOnline = acc.isOnline }
		end

		updateWorkersList()
	end)

	addEventHandler("tcOnFractionChanged", resourceRoot,
	function(accName, fraction, accPlayer)
		if fraction == HWPATROL_FRACTION_ID then
			fracPlayersList[accName] = { fractionId = fraction, isOnline = (accPlayer ~= false) }
		elseif not fraction then
			fracPlayersList[accName] = nil
		end

		updateWorkersList()
	end)

	addEventHandler("tcOnFractionDataChanged", resourceRoot,
	function(accName, fractionData, accPlayer)
		if fracPlayersList[accName] then
			fracPlayersList[accName].rank = hwPatrolRanks[fractionData.rank]
		end

		updateWorkersList()
	end)

	addEventHandler("onClientElementDataChange", localPlayer,
	function(key, oldVal, newVal)
		if key == FRACTION_ID_KEY then
			if oldVal then
				setWindowOpened(fracSysWindows[HWPATROL_FRACTION_ID].window, false)
			end

			if newVal then
				triggerServerEvent("tcRequestFractionWorkers", localPlayer, newVal)
			end
		end

		if key == FRACTION_DATA_KEY and newVal then
			setWindowOpened(fracSysWindows[HWPATROL_FRACTION_ID].window, false)

			dgsSetVisible(workersButton, hasPlayerHwPatrolRight(localPlayer, "viewWorkers"))
			dgsSetVisible(logsButton, hasPlayerHwPatrolRight(localPlayer, "viewLogs"))
		end

		if key == ACCOUNT_NAME_KEY then
			if fracPlayersList[newVal] then
				fracPlayersList[newVal].isOnline = true
				updateWorkersList()
			end
		end
	end)

	addEventHandler("onClientPlayerQuit", root,
	function()
		local accName = getElementData(source, ACCOUNT_NAME_KEY)
		if accName and fracPlayersList[accName] then
			fracPlayersList[accName].isOnline = nil
			updateWorkersList()
		end
	end)

	triggerServerEvent("tcRequestFractionWorkers", localPlayer, getElementData(localPlayer, FRACTION_ID_KEY))

	--[[for i = 1, 23 do
		dgsGridListAddRow(playersGridList, i)
		dgsGridListSetItemText(playersGridList, i, 1, "Константин Михалыч")
		dgsGridListSetItemText(playersGridList, i, 2, "Полковник полиции")
		dgsGridListSetItemText(playersGridList, i, 3, "В сети")
	end--]]
end

addEventHandler("onClientResourceStart", resourceRoot, startup)