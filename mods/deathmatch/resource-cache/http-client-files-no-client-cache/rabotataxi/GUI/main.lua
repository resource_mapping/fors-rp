taxi = { window = {}, button = {}, gridlist = {}, edit = {}}

function createTaximenu ()
    if isElement(taxi.window[1]) then
	    destroyElement(taxi.window[1])
		showCursor(false)
		return
	end
    taxi.window[1] = guiCreateWindow(0.31, 0.31, 0.4, 0.10, "Taxi", true)
    guiWindowSetMovable(taxi.window[1], false)
    guiWindowSetSizable(taxi.window[1], false)
    
    taxi.button[1] = guiCreateButton(0, 0.35, 0.31, 0.5, "Устроиться", true, taxi.window[1])
    taxi.button[2] = guiCreateButton(0.67, 0.35, 0.31, 0.5, "Отмена", true, taxi.window[1])
    Rules = guiCreateButton(0.34, 0.35, 0.31, 0.5, "Правила (Обязательно к прочтению)", true, taxi.window[1])
	
	for i = 1,2 do
	    guiSetProperty(taxi.button[i], "NormalTextColour", "FFAAAAAA")    
	end
	showCursor(true)
end

function createTaxistMenu (waiters)
    if isElement(taxi.window[2]) then
	    destroyElement(taxi.window[2])
		showCursor(false)
	end
	taxi.window[2] = guiCreateWindow(0.37, 0.23, 0.25, 0.55, "Диспетчер", true)
	guiWindowSetMovable(taxi.window[2], false)
	guiWindowSetSizable(taxi.window[2], false)

	taxi.gridlist[1] = guiCreateGridList(0.03, 0.07, 0.95, 0.79, true, taxi.window[2])
	guiGridListAddColumn(taxi.gridlist[1], "Ник", 0.5)
	guiGridListAddColumn(taxi.gridlist[1], "Местоположение", 0.4)
	
	for i, v in ipairs (waiters or {}) do
	    local row = guiGridListAddRow ( taxi.gridlist[1] )
		guiGridListSetItemText ( taxi.gridlist[1], row, 1, getPlayerName(v[1]), false, false )
		guiGridListSetItemText ( taxi.gridlist[1], row, 2, v[2], false, false )
	end
	
	taxi.button[3] = guiCreateButton(0.03, 0.87, 0.47, 0.10, "Принять заказ", true, taxi.window[2])
	taxi.button[4] = guiCreateButton(0.52, 0.87, 0.47, 0.10, "Закрыть", true, taxi.window[2])

    for i = 3,4 do
	    guiSetProperty(taxi.button[i], "NormalTextColour", "FFAAAAAA")    
	end	
	showCursor(true)
end
addEvent("createTaxistMenu", true)
addEventHandler("createTaxistMenu", getRootElement(), createTaxistMenu)


function createEditMenu ()
    taxi.window[3] = guiCreateWindow(0.36, 0.34, 0.27, 0.13, "Укажите место для поездки", true)
    guiWindowSetMovable(taxi.window[3], false)
    guiWindowSetSizable(taxi.window[3], false)

    taxi.edit[1] = guiCreateEdit(0.02, 0.17, 0.96, 0.38, "", true, taxi.window[3])
    taxi.button[5] = guiCreateButton(0.02, 0.59, 0.96, 0.30, "Едем! (150 р.)", true, taxi.window[3])
	showCursor(true)
end
addEvent("createEditMenu", true)
addEventHandler("createEditMenu", getRootElement(), createEditMenu)

function createTaxiRules ()
	taxi.window[4] = guiCreateWindow(0.31, 0.31, 0.4, 0.34, "Правила работы", true)
	guiWindowSetMovable(taxi.window[4], false)
	guiWindowSetSizable(taxi.window[4], false)
	
	memoTaxiRules1 = guiCreateLabel(0.01,0.08,1,1,"Разрешено:", true, taxi.window[4])
    guiSetFont(memoTaxiRules1, "default-bold-small")
    guiLabelSetHorizontalAlign(memoTaxiRules1, "center", false)
    memoTaxiRules2 = guiCreateLabel(0.03,0.16,1,1,"- Принимать заказы от клиентов и отвозить их в указанное место.\n- Быть вежливым с пассажирами.\n- Следить за уровнем топлива и своевременно заправлять машину.\n- Намекнуть на чаевые.", true, taxi.window[4])
    guiSetFont(memoTaxiRules2, "default-bold-small")
    memoTaxiRules3 = guiCreateLabel(0.01,0.36,1,1,"Запрещено:", true, taxi.window[4])
    guiSetFont(memoTaxiRules3, "default-bold-small")
    guiLabelSetHorizontalAlign(memoTaxiRules3, "center", false)
    memoTaxiRules4 = guiCreateLabel(0.03,0.43,1,1,"- Использовать такси в личных целях.\n- Нарушать ПДД.\n- Создавать помехи другим участникам движения.\n- Умышленно таранить других игроков и устраивать ДТП.\n- Продажа такси или намек на это.\n- Требовать с пассажиров доплату за проезд.\n- Обманывать пассажиров на деньги.", true, taxi.window[4])
    guiSetFont(memoTaxiRules4, "default-bold-small")
    taxi.button[7] = guiCreateButton(0, 0.82, 0.99, 0.13, "Прочитал и обязаюсь все соблюдать.", true, taxi.window[4])
	showCursor(true)
end
addEvent("createTaxiRules", true)
addEventHandler("createTaxiRules", getRootElement(), createTaxiRules)

function onPlayerEnterMarker ()
    createTaximenu ()
    local isPlayerForestWorker = getElementData(localPlayer, "taxi:worker")
	if isPlayerForestWorker == true then
	    guiSetText(taxi.button[1], "Уволиться")
	elseif isPlayerForestWorker == false then
	    guiSetText(taxi.button[1], "Устроиться")
	end
end
addEvent("onPlayerEnterMarker", true)
addEventHandler("onPlayerEnterMarker", getRootElement(), onPlayerEnterMarker)

function onClientGUIClick ()
    if source == taxi.button[1] then
		triggerServerEvent("onPlayerStartWork", localPlayer, guiGetText(taxi.button[1]))
		createTaximenu ()
	elseif source == taxi.button[2] then
	    createTaximenu ()
	elseif source == taxi.button[3] then
	    local name = guiGridListGetItemText(taxi.gridlist[1], guiGridListGetSelectedItem(taxi.gridlist[1]), 1)
	    triggerServerEvent("updateWaiters", localPlayer, name)
	elseif source == taxi.button[4] then
	    destroyElement(taxi.window[2])
		showCursor(false)
	elseif source == taxi.button[5] then
	    local text = guiGetText(taxi.edit[1])
		triggerServerEvent("givePlayerWork", localPlayer, text)
		destroyElement(taxi.window[3])
		showCursor(false)
    elseif source == Rules then
		destroyElement(taxi.window[1])
        createTaxiRules ()
    elseif source == taxi.button[7] then
		destroyElement(taxi.window[4])
        createTaximenu ()
	end
end
addEventHandler("onClientGUIClick", getRootElement(), onClientGUIClick)