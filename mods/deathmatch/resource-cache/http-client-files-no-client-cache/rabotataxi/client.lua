function checkVehiclePosition ()
    local veh = getPedOccupiedVehicle(localPlayer)
	local player = getElementData(localPlayer, "client:Wait")
	if veh and getElementData(veh, "taxi") == true and player ~= false then
	    local tX, tY, tZ = getElementPosition(localPlayer)
		local pX, pY, pZ = getElementPosition(player)
	    if getDistanceBetweenPoints3D(tX, tY, tZ, pX, pY, pZ) <= 5 then
		    triggerServerEvent("onPlayerNearTaxist", localPlayer, veh, player)
		end
	end
end
addEventHandler("onClientRender", getRootElement(), checkVehiclePosition)


function onClientPickupHit (player)
    if getElementData(source, "taxi:client") == true and player == localPlayer then
	    if isPedInVehicle(player) then return end
        onPlayerEnterMarker ()
	end
end
addEventHandler("onClientPickupHit", getRootElement(), onClientPickupHit)

function onClientResourceStart ()
	setElementData(localPlayer, "player:Taxi", false)

	local pickup = createPickup(893.63726806641, -1636.0031738281, 14.9296875, 3, 1275)
    setElementData(pickup, "taxi:client", true)
    local blip = createBlipAttachedTo(pickup, 12)
    setBlipVisibleDistance(blip, 400)
end
addEventHandler("onClientResourceStart", getResourceRootElement( getThisResource() ), onClientResourceStart)