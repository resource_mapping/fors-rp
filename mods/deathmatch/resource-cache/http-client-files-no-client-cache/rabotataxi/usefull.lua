function getPlayer (name)
    local name = utf8.lower(name)
	local bestProcent = 0
	local fPlayer, sPos, ePos
	
	for _, player in ipairs(getElementsByType("player")) do
	    local pName = utf8.gsub(getPlayerName(player), "#%x%x%x%x%x%x", "")
		local psPos, pePos = utf8.find(utf8.lower(pName), name)
		if psPos and pePos then
		    if pePos - psPos > bestProcent then
			    bestProcent = pePos - psPos
				fPlayer = player
			end
		end
	end 
	return fPlayer or false
end

vehicles = {
    {1774.7021484375, 343.162109375, 60.5944213},
    {888.05236816406, -1669.162109375, .546875},
    {883.69403076172, -1669.5518798828, 13.546875},
    {879.03790283203, -1669.5518798828, 13.546875},
    {874.66931152344, -1669.5518798828, 13.546875},
    {870.15191650391, -1669.5518798828, 13.546875},
}