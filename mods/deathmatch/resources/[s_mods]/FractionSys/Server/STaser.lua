-- TrollingCont. 08.02.2020

function startup()
    addEventHandler("onElementDataChange", root,
    function(key, oldVal, newVal)
        if key == FRACTION_ID_KEY then
            if newVal == HWPATROL_FRACTION_ID then
                giveWeapon(source, 23)
            else
                takeWeapon(source, 23)
            end
        end
    end)

    local allPlayers = getElementsByType("player")
    for i,player in ipairs(allPlayers) do
        if getElementData(player, FRACTION_ID_KEY) == HWPATROL_FRACTION_ID then
            giveWeapon(player, 23)
        end
    end

    addEventHandler("tcTaserFired", root,
    function(target)
        setPedAnimation( target,  "ped", "ko_shot_front", -1, false, false, true, false )
        toggleControl(target, "forwards", false)
        toggleControl(target, "backwards", false)
        toggleControl(target, "left", false)
        toggleControl(target, "right", false)
        toggleControl(target, "jump", false)
		setTimer(function()
			setPedAnimation( target,  "crack", "crckidle"..math.random(1,4), 15000, true, false, false, false )
        end,500,1)
        setTimer(
            function()
                if not isElement(target) then return end 
                toggleControl(target, "jump", true)
                toggleControl(target, "forwards", true)
                toggleControl(target, "backwards", true)
                toggleControl(target, "left", true)
                toggleControl(target, "right", true)
            end,
        15000, 1)
        
        local attX, attY, attZ = getElementPosition(client)
        local circle = createColSphere(attX, attY, attZ, 15)
        local playersWithinCircle = getElementsWithinColShape(circle, "player")
        triggerClientEvent(playersWithinCircle, "tcTaserFired", client, target)
        triggerClientEvent(target, "tcPlayHeartbit", target)
    end)
end

addEventHandler("onResourceStart", resourceRoot, startup)