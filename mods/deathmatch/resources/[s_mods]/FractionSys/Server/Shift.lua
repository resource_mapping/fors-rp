-- TrollingCont. 24.02.2020

TEMPSKIN_KEY = "tcfs.tempskin"
PSKIN_KEY = "tcfs.pskin"

local shiftMarker = nil

function setAccountPSkin(account, skinId)
    if skinId then
        setAccountData(account, PSKIN_KEY, tostring(skinId))
    else
        setAccountData(account, PSKIN_KEY, false)
    end

    local accPlayer = getAccountPlayer(account)
    if accPlayer and not getElementData(accPlayer, "isWorking") then
        if skinId then
            setElementData(accPlayer, PSKIN_KEY, skinId)
        else
            setElementData(accPlayer, PSKIN_KEY, defaultSkinId)
        end
    end
end

function setAccountTempSkin(account, skinId)
    local res = nil

    if skinId then
        res = setAccountData(account, TEMPSKIN_KEY, tostring(skinId))
    else
        res = setAccountData(account, TEMPSKIN_KEY, false)
    end

    if not res then return false end

    local accPlayer = getAccountPlayer(account)
    if accPlayer and getElementData(accPlayer, "isWorking") then
        if skinId then
            if setElementData(accPlayer, TEMPSKIN_KEY, skinId) then
                outputChatBox("Failed to set temp skin")
            end
        else
            setElementData(accPlayer, TEMPSKIN_KEY, hwPatrolSkins[getElementData(accPlayer, FRACTION_DATA_KEY)["rank"]])
        end
    end
end

function applyShiftParams(player, account)
    local pSkinId = getAccountData(account, PSKIN_KEY)
    if pSkinId then
        if not setElementData(player, PSKIN_KEY, tonumber(pSkinId)) then
            outputChatBox("Failed to set PSKIN_KEY to "..tonumber(pSkinId))
        end
    else
        if not setElementData(player, PSKIN_KEY, defaultSkinId) then
            outputChatBox("Failed to set PSKIN_KEY to "..defaultSkinId)
        end
    end

    local playerFrac = getElementData(player, FRACTION_ID_KEY)
    if playerFrac == HWPATROL_FRACTION_ID then
        setElementVisibleTo(shiftMarker, player, true)
    end

    local tempSkinId = getAccountData(account, TEMPSKIN_KEY)
    if tempSkinId then
        if not setElementData(player, TEMPSKIN_KEY, tonumber(tempSkinId)) then
            outputChatBox("Failed to set TEMPSKIN_KEY to "..tonumber(tempSkinId))
        end
    else
        if playerFrac == HWPATROL_FRACTION_ID then
            setElementData(player, TEMPSKIN_KEY, hwPatrolSkins[getElementData(player, FRACTION_DATA_KEY)["rank"]])
        end
    end
end

function startup()

    shiftMarker = createMarker(-2442, -620, 131, "cylinder", 3, 240, 240, 240, 255, resourceRoot)
    setElementData(shiftMarker, "shiftMarker", true)

    addEventHandler("onMarkerHit", shiftMarker,
    function(elemHit, matchingDim)
        if getElementType(elemHit) == "player" and getElementData(source, "shiftMarker") == true and matchingDim then
           
            if getElementData(elemHit, "isWorking") then
                setElementData(elemHit, "isWorking", false)

                outputChatBox("#CCCCCCВы закончили смену", elemHit, 255, 0, 0, true)
            else
                setElementData(elemHit, "isWorking", true)

                outputChatBox("#CCCCCCВы заступили на смену", elemHit, 255, 0, 0, true)
            end
        end
    end)

    addEventHandler("onElementDataChange", root,
    function(key, oldVal, newVal)

        if (key == "isWorking" and newVal) or (key == TEMPSKIN_KEY and getElementData(source, "isWorking")) then
            -- установка скина из TEMPSKIN_KEY
            setElementModel(source, getElementData(source, TEMPSKIN_KEY))
            outputChatBox("TEMPSKIN = "..tostring(getElementData(source, TEMPSKIN_KEY)))
        elseif (key == "isWorking" and not newVal) or (key == PSKIN_KEY and not getElementData(source, "isWorking")) then
            -- установка скина из PSKIN_KEY
            setElementModel(source, getElementData(source, PSKIN_KEY))
            outputChatBox("PSKIN = "..tostring(getElementData(source, PSKIN_KEY)))
        elseif key == FRACTION_ID_KEY then
            if newVal then
                -- включение отображения маркера
                setElementVisibleTo(shiftMarker, source, true)
                outputChatBox("Marker ON")
            else
                -- отключение отображения маркера
                setElementVisibleTo(shiftMarker, source, false)
                outputChatBox("Marker OFF")
            end
        end

    end)

    addEventHandler("onPlayerLogin", root,
    function(prevAcc, currAcc)
        -- назначение в эл. дату постоянного и временного скинов
        -- включение отображения маркера, если игрок во фракции
        applyShiftParams(source, currAcc)
    end)

    local allPlayers = getElementsByType("player")

    for i,player in ipairs(allPlayers) do
        local account = getPlayerAccount(player)
        
        if not isGuestAccount(account) then
            -- назначение в эл. дату постоянного и временного скинов
            -- включение отображения маркера тем, кто во фракции
            applyShiftParams(player, account)
        end
    end
end

addEventHandler("onResourceStart", resourceRoot, startup)