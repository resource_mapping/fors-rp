-- TrollingCont. 15.02.2020

db = nil

function hasPlayerAdminRights(player)
	if getElementType(player) == "console" then return true end
	local account = getPlayerAccount(player)
	return isObjectInACLGroup("user."..getAccountName(account), aclGetGroup("Admin"))
end

function setAccountFraction(account, fraction)
    if fraction ~= false and not isFracNameValid(fraction) then return false end
    local accName = getAccountName(account)
    if not accName then return false end

    local res, rows
    if not fraction then
        res, rows = dbPoll(dbQuery(db, "DELETE FROM Fractions WHERE accountName = ?", accName), -1)
    else
        res, rows = dbPoll(dbQuery(db, "SELECT * FROM Fractions WHERE accountName = ?", accName), -1)
        if #res < 1 then
            res, rows = dbPoll(dbQuery(db, "INSERT INTO Fractions (accountName, fractionId, fracData) VALUES (?, ?, ?)", accName, fraction, toJSON({})), -1)
        else
            res, rows = dbPoll(dbQuery(db, "UPDATE Fractions SET fractionId = ?, fracData = ? WHERE accountName = ?", fraction, toJSON({}), accName), -1)
        end
    end

    if rows == 1 then
        local accountPlayer = getAccountPlayer(account)
        triggerEvent("tcOnFractionChanged", root, accName, fraction, accountPlayer)
        triggerClientEvent("tcOnFractionChanged", resourceRoot, accName, fraction, accountPlayer)
        if accountPlayer then
            setElementData(accountPlayer, FRACTION_ID_KEY, fraction)
            setElementData(accountPlayer, FRACTION_DATA_KEY, false)
        end
        return true
    end
    return false
end

function getAccountFraction(account)
    local accName = getAccountName(account)
    if not accName then return false end

    local res = dbPoll(dbQuery(db, "SELECT fractionId FROM Fractions WHERE accountName = ?", accName), -1)
    if #res < 1 then return false end
    return res[1].fractionId
end

function setAccountFracData(account, fractionData)
    local accName = getAccountName(account)
    if not accName then return false end

    local res, rows = dbPoll(dbQuery(db, "UPDATE Fractions SET fracData = ? WHERE accountName = ?", toJSON(fractionData), accName), -1)
    if rows == 1 then
        local accountPlayer = getAccountPlayer(account)
        triggerEvent("tcOnFractionDataChanged", root, accName, fractionData, accountPlayer)
        triggerClientEvent("tcOnFractionDataChanged", resourceRoot, accName, fractionData, accountPlayer)
        if accountPlayer then
            setElementData(accountPlayer, FRACTION_DATA_KEY, fractionData)
        end
        return true
    end
    return false
end

function getAccountFracData(account)
    local accName = getAccountName(account)
    if not accName then return false end

    local res = dbPoll(dbQuery(db, "SELECT fracData FROM Fractions WHERE accountName = ?", accName), -1)
    if #res < 1 then return false end
    return fromJSON(res[1].fracData)
end

function getAccountWanted(account, reason)
    local accName = getAccountName(account)
    if not accName then return false end

    local res = dbPoll(dbQuery(db, "SELECT reasons FROM Wanted WHERE accountName = ?", accName), -1)
    if #res < 1 then return false end
    if reason == "all" then
        return fromJSON(res[1].reasons)
    end
    res[1] = fromJSON(res[1].reasons)
    for i,reasonId in ipairs(res[1]) do
        if reasonId == reason then
            return true
        end
    end
    return false
end

function setAccountWanted(account, reasonId, state)
    if type(reasonId) ~= "number" then return false end
    local accName = getAccountName(account)
    if not accName then return false end

    local res = nil
    local rows = nil
    local wantedStatus = getAccountWanted(account, "all")
    local accountPlayer = getAccountPlayer(account)

    if wantedStatus then
        if state then
            table.insert(wantedStatus, reasonId)
            -- UPDATE
        else
            for i,reason in ipairs(wantedStatus) do
                if reason == reasonId then
                    table.remove(wantedStatus, i)
                    if #wantedStatus == 0 then
                        -- DELETE
                        res, rows = dbPoll(dbQuery(db, "DELETE FROM Wanted WHERE accountName = ?", accName), -1)
                        if rows == 1 then
                            if accountPlayer then
                                setElementData(accountPlayer, WANTED_STATUS_KEY, false)
                            end
                            return true
                        end
                        return false
                    else
                        -- UPDATE
                        break
                    end
                end
            end
        end
    else
        if state then
            wantedStatus = { [1] = reasonId }
            -- INSERT
            res, rows = dbPoll(dbQuery(db, "INSERT INTO Wanted (accountName, reasons) VALUES (?, ?)", accName, toJSON(wantedStatus)), -1)
            if rows == 1 then
                if accountPlayer then
                    setElementData(accountPlayer, WANTED_STATUS_KEY, wantedStatus)
                end
                return true end
            return false
        else
            return false
        end
    end

    res, rows = dbPoll(dbQuery(db, "UPDATE Wanted SET reasons = ? WHERE accountName = ?", toJSON(wantedStatus), accName), -1)
    if rows == 1 then
        if accountPlayer then
            setElementData(accountPlayer, WANTED_STATUS_KEY, wantedStatus)
        end
        return true end
    return false
end

function startup()
    db = dbConnect("sqlite", "Fractions.db")
    dbPoll(dbQuery(db, "CREATE TABLE IF NOT EXISTS Fractions (accountName TEXT UNIQUE, fractionId TEXT, fracData TEXT)"), -1)
    dbPoll(dbQuery(db, "CREATE TABLE IF NOT EXISTS Wanted (accountName TEXT UNIQUE, reasons TEXT)"), -1)
end

addEventHandler("onResourceStart", resourceRoot, startup)