-- TrollingCont. 24.02.2020

-- [fraction_id] = team | nil
local fractionTeams = {}

function startup()

    addEventHandler("onElementDataChange", root,
    function(key, oldVal, newVal)
        if key ~= FRACTION_ID_KEY then return end

        if oldVal then
            setPlayerTeam(source, nil)
            if countPlayersInTeam(fractionTeams[oldVal]) == 0 then
                destroyElement(fractionTeams[oldVal])
                fractionTeams[oldVal] = nil
            end
        end

        if newVal then
            if not fractionTeams[newVal] then
                fractionTeams[newVal] = createTeam(fractionTeamParams[newVal].name, fractionTeamParams[newVal].r, fractionTeamParams[newVal].g, fractionTeamParams[newVal].b)
            end
            setPlayerTeam(source, fractionTeams[newVal])
        end
    end)

    -----------------------

    addEventHandler("onPlayerLogin", root,
    function(prevAcc, currAcc)
        local accFrac = getAccountFraction(currAcc)
        local fracData = getAccountFracData(currAcc)
        setElementData(source, FRACTION_ID_KEY, accFrac)
        setElementData(source, FRACTION_DATA_KEY, fracData)
        setElementData(source, ACCOUNT_NAME_KEY, getAccountName(currAcc))
        setElementData(source, WANTED_STATUS_KEY, getAccountWanted(currAcc, "all"))
    end)

    -----------------------

    for i,player in ipairs(getElementsByType("player")) do

        local account = getPlayerAccount(player)

        if not isGuestAccount(account) then
            setElementData(player, ACCOUNT_NAME_KEY, getAccountName(account))

            local playerFraction = getAccountFraction(account)
            local fracData = getAccountFracData(account)

            if playerFraction then
                if not fractionTeams[playerFraction] then
                    fractionTeams[playerFraction] = createTeam(fractionTeamParams[playerFraction].name, fractionTeamParams[playerFraction].r, fractionTeamParams[playerFraction].g, fractionTeamParams[playerFraction].b)
                end
                setPlayerTeam(player, fractionTeams[playerFraction])
                setElementData(player, FRACTION_ID_KEY, playerFraction)
                setElementData(player, FRACTION_DATA_KEY, fracData)
            end
        end
    end

    -----------------------

    for i,marker in ipairs(markers) do
        local markIn = createMarker(marker[1].x, marker[1].y, marker[1].z, "cylinder", marker[1].size, 240, 240, 240)
        setElementDimension(markIn, marker[1].dim)
        setElementInterior(markIn, marker[1].interior)
        setElementData(markIn, "tcfs.notifyMarker", "in")
        setElementData(markIn, "tcfs.nmId", i)
        
        local markOut = createMarker(marker[2].x, marker[2].y, marker[2].z, "cylinder", marker[2].size, 240, 240, 240)
        setElementDimension(markOut, marker[2].dim)
        setElementInterior(markOut, marker[2].interior)
        setElementData(markOut, "tcfs.notifyMarker", "out")
        setElementData(markOut, "tcfs.nmId", i)
    end

    -----------------------

    addEventHandler("onPlayerQuit", root,
    function()
        local fractionId = getElementData(source, FRACTION_ID_KEY)

        if fractionId then
            setPlayerTeam(source, nil)
            if countPlayersInTeam(fractionTeams[fractionId]) == 0 then
                destroyElement(fractionTeams[fractionId])
                fractionTeams[fractionId] = nil
            end
        end
    end)

    -----------------------

    addEventHandler("onPlayerChat", root,
    function(message, type)
        if type == 2 then
            local fractionId = getElementData(source, FRACTION_ID_KEY)
            if not fractionId then return end

            local senderName = getElementData(source, "name")
            local senderSurname = getElementData(source, "surname")

            if fractionId == HWPATROL_FRACTION_ID then
                cancelEvent()

                local fractionPlayers = getPlayersInTeam(fractionTeams[fractionId])
                local teamColorCode = string.format("#%02X%02X%02X", fractionTeamParams[fractionId].r, fractionTeamParams[fractionId].g, fractionTeamParams[fractionId].b)

                for i,player in ipairs(fractionPlayers) do
                    local rankId = getElementData(player, FRACTION_DATA_KEY)["rank"]
                    
                    outputChatBox(teamColorCode.."["..hwPatrolRanks[rankId].."] #FFFFFF"..tostring(senderName).." "..tostring(senderSurname)..teamColorCode..": "..message, player, 0, 0, 0, true)
                end
            end
        end
    end)

    -----------------------

    addEventHandler("tcRequestFractionWorkers", root,
    function(fraction)
        if not fraction then return end

        local res = dbPoll(dbQuery(db, "SELECT * FROM Fractions WHERE fractionId = ?", fraction), -1)

        for i,acc in ipairs(res) do
            local mtaAccount = getAccount(acc.accountName)
            if mtaAccount then
                if getAccountPlayer(mtaAccount) then
                    acc.isOnline = true
                end
            end
        end

        triggerClientEvent(source, "tcRequestFractionWorkers", source, res)
    end)

    -----------------------

    addEventHandler("tcSetHwPatrolFraction", root,
    function(accountName, action, newRankId)
        if not hwPatrolRights[getElementData(client, FRACTION_DATA_KEY)["rank"]] or not hwPatrolRights[getElementData(client, FRACTION_DATA_KEY)["rank"]]["editPlayers"] then
			outputChatBox("#CCCCCCУ вас нет прав на изменение игроков фракции", client, 0, 0, 0, true)
			return
        end
        
        local playerAcc = getAccount(accountName)
        if not playerAcc then
            outputChatBox("#CCCCCCТакого аккаунта не существует", client, 0, 0, 0, true)
            return
        end

        if action == "remove" then
            if getAccountFraction(playerAcc) ~= HWPATROL_FRACTION_ID then
                outputChatBox("#CCCCCCЭтот аккаунт не находится во фракции ДПС", client, 0, 0, 0, true)
                return
            end
            if setAccountFraction(playerAcc, false) then
                outputChatBox("#FF8000"..accountName.." #CCCCCCуволен из фракции", client, 0, 0, 0, true)
            else
                outputChatBox("#FF2020Ошибка увольнения", client, 0, 0, 0, true)
            end
        elseif action == "setRank" then
            local accFraction = getAccountFraction(playerAcc)
            if not accFraction then
                setAccountFraction(playerAcc, HWPATROL_FRACTION_ID)
            elseif accFraction ~= HWPATROL_FRACTION_ID then
                outputChatBox("#CCCCCCЭтот аккаунт находится в другой фракции", client, 0, 0, 0, true)
                return
            end
            if setAccountFracData(playerAcc, { rank = newRankId }) then
                outputChatBox("#CCCCCCДолжность #FF8000"..accountName.." #CCCCCCизменена: #FF8000"..hwPatrolRanks[newRankId], client, 0, 0, 0, true)
            end
        end
    end)

    -----------------------

    addEventHandler("tcChangeWantedStatus", root,
    function(accName, action, reason)
        local account = getAccount(accName)

        if not account then
            outputChatBox("#CCCCCCТакого аккаунта не существует", client, 0, 0, 0, true)
            return
        end

        if action == "getWanted" then
            if getAccountWanted(account, reason) then
                outputChatBox("#CCCCCCАккаунт уже имеет розыск по этой статье", client, 0, 0, 0, true)
                return
            end
            if setAccountWanted(account, reason, true) then
                outputChatBox("#CCCCCCВы успешно выдали розыск по статье #FF8000"..wantedReasons[reason].name, client, 0, 0, 0, true)
            else
                outputChatBox("#FF3030Ошибка выдачи розыска", client, 0, 0, 0, true)
            end
        else
            if not getAccountWanted(account, reason) then
                outputChatBox("#CCCCCCУ аккаунта нет розыска по этой статье", client, 0, 0, 0, true)
                return
            end
            if setAccountWanted(account, reason, false) then
                outputChatBox("#CCCCCCВы успешно сняли розыск по статье #FF8000"..wantedReasons[reason].name, client, 0, 0, 0, true)
            else
                outputChatBox("#FF3030Ошибка снятия розыска", client, 0, 0, 0, true)
            end
        end
    end)

    -----------------------

    --[[addEventHandler("tcOnFractionDataChanged", root,
    function(accName, fracData, accountPlayer)
        if accountPlayer and hwPatrolSkins[fracData.rank] then
            setElementModel(accountPlayer, hwPatrolSkins[fracData.rank])
        end
    end)--]]

    -----------------------

    addEventHandler("tcTeleportPlayer", root,
    function(teleportMarkerGroup, teleportMarkerIndex)
        local health = getElementHealth(client)
        spawnPlayer(client, markers[teleportMarkerGroup][teleportMarkerIndex].x, markers[teleportMarkerGroup][teleportMarkerIndex].y, markers[teleportMarkerGroup][teleportMarkerIndex].z + 1,
        0, getElementModel(client), markers[teleportMarkerGroup][teleportMarkerIndex].interior, markers[teleportMarkerGroup][teleportMarkerIndex].dim)
        setElementHealth(client, health)
    end)
end

addEventHandler("onResourceStart", resourceRoot, startup)