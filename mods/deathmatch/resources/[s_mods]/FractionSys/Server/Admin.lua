-- TrollingCont. 24.02.2020

function setSkins(playerSource, cmdName, accountName, skinId)
    if hasPlayerAdminRights(playerSource) then
        if not accountName then
            outputChatBox("#FF2020Введите имя аккаунта", playerSource, 0, 0, 0, true)
            return
        end

        local messageText = "#CCCCCCВременный скин"
        local func = setAccountTempSkin
        if cmdName == "pskin" then
            messageText = "#CCCCCCПостоянный скин"
            func = setAccountPSkin
        end

        local account = getAccount(accountName)
        
        if not account then
            outputChatBox("#FF2020Аккаунт #FF8000"..accountName.." #FF2020 не найден", playerSource, 0, 0, 0, true)
            return
        end
        
        if not skinId then
            outputChatBox("#CCCCCCВведите ID скина", playerSource, 0, 0, 0, true)
        elseif skinId == "null" then
            func(account, false)
            outputChatBox(messageText.." сброшен", playerSource, 0, 0, 0, true)
        else
            if not tonumber(skinId) or tonumber(skinId) < 0 or tonumber(skinId) > 255 then
                outputChatBox("#CCCCCCНеправильное значение ID скина", playerSource, 0, 0, 0, true)
            else
                func(account, skinId)
                outputChatBox(messageText.." назначен", playerSource, 0, 0, 0, true)
            end
        end
    end
end

function startup()
    addCommandHandler("setfrac",
    function(playerSource, cmdName, accountName, fraction, fractionData)
        if hasPlayerAdminRights(playerSource) then

            if not accountName then
                outputChatBox("#FF2020Введите имя аккаунта", playerSource, 0, 0, 0, true)
                return
            end

            if not fraction then
                outputChatBox("#FF2020Введите имя фракции", playerSource, 0, 0, 0, true)
                return
            end

            local account = getAccount(accountName)

            if not account then
                outputChatBox("#FF2020Аккаунт #FF8000"..accountName.." #FF2020 не найден", playerSource, 0, 0, 0, true)
                return
            end

            if fraction == "null" then
                if setAccountFraction(account, false) then
                    outputChatBox("#CCCCCCВы успешно убрали фракцию аккаунту #FF8000"..accountName, playerSource, 0, 0, 0, true)
                end
                return
            end

            if not isFracNameValid(fraction) then
                outputChatBox("Неправильное имя фракции", playerSource, 255, 32, 32)
                return
            end

            if fraction == HWPATROL_FRACTION_ID then
                  if fractionData == nil then
                    outputChatBox("В этой фракции обязательно ввести звание", playerSource, 255, 32, 32)
                    return
                  end

                  if hwPatrolRanks[fractionData] ~= nil then
                    if setAccountFraction(account, fraction) and setAccountFracData(account, { rank = fractionData }) then
                        outputChatBox("#CCCCCCВы успешно изменили фракцию аккаунту #FF8000"..accountName.." #CCCCCC(#FF8000"..fraction.."#CCCCCC, #FF8000"..hwPatrolRanks[fractionData].."#CCCCCC)",
                        playerSource, 0, 0, 0, true)
                    else
                        outputChatBox("Ошибка назначения фракции", playerSource, 255, 32, 32)
                    end
                  else
                    outputChatBox("Неправильное звание", playerSource, 255, 32, 32)
                    return
                  end
            end
        end
    end)

    -----------------------

    addCommandHandler("getfrac",
    function(playerSource, cmdName, accountName)
        if hasPlayerAdminRights(playerSource) then
            if not accountName then
                outputChatBox("#FF2020Введите имя аккаунта", playerSource, 0, 0, 0, true)
                return
            end

            local account = getAccount(accountName)
            
            if not account then
                outputChatBox("#FF2020Аккаунт #FF8000"..accountName.." #FF2020 не найден", playerSource, 0, 0, 0, true)
                return
            end

            local fraction = getAccountFraction(account)
            local fracData = getAccountFracData(account)

            if not fraction then
                outputChatBox("#CCCCCCАккаунт #FF8000"..accountName.."#CCCCCC не имеет фракции", playerSource, 0, 0, 0, true)
            end

            if fraction == HWPATROL_FRACTION_ID then
                outputChatBox("#CCCCCCАккаунт: #FF8000"..accountName.."#CCCCCC, фракция: #FF8000"..fraction.."#CCCCCC, звание: #FF8000"..hwPatrolRanks[fracData.rank], playerSource, 0, 0, 0, true)
            end
        end
    end)

    -----------------------

    addCommandHandler("tempskin", setSkins)

    -----------------------

    addCommandHandler("pskin", setSkins)
end

addEventHandler("onResourceStart", resourceRoot, startup)