-- TrollingCont. 15.02.2020

function isFracNameValid(fracName)
    for _, name in ipairs(fractionIds) do
        if fracName == name then return true end
    end
    return false
end

addEvent("tcOnFractionChanged", true)
addEvent("tcOnFractionDataChanged", true)

addEvent("tcRequestFractionWorkers", true)
addEvent("tcSetHwPatrolFraction", true)

addEvent("tcTaserFired", true)
addEvent("tcPlayHeartbit", true)

addEvent("tcChangeWantedStatus", true)

addEvent("tcTeleportPlayer", true)