-- TrollingCont. 09.02.2020

local iLastShot = 0
local iRechargeDelay = 10000
local iMaxDistance = 9

function taserFire()
	if getTickCount() - iLastShot <= iRechargeDelay then
		return
	end

	setPedAnimation(localPlayer, "python", "python_fire", 150, false, false, true, false)
	local sx, sy, sz = getPedTargetStart(localPlayer)
	local tx, ty, tz = getPedTargetEnd(localPlayer)

	local _, _, _, _, pElement = processLineOfSight( sx, sy, sz, tx, ty, tz )

	if pElement and isElement(pElement) then
		if pElement == localPlayer then return end
		local plX, plY, plZ = getElementPosition(localPlayer)
		local pX, pY, pZ = getElementPosition(pElement)
		local distance = math.sqrt( (plX - pX)^2 + (plY - pY)^2 + (plZ - pZ)^2 ) 

		if getElementType(pElement) == "player" and distance <= iMaxDistance then
			triggerServerEvent("tcTaserFired", localPlayer, pElement)
			iLastShot = getTickCount()
		end
	end
end

function onPlayerTaserFire( key, state )
	if not state or getElementData(localPlayer, "jailed") then return end

	if key == "mouse1" then
		if getPedControlState( localPlayer, "aim_weapon" ) and getElementData(localPlayer, FRACTION_ID_KEY) == HWPATROL_FRACTION_ID then
			taserFire()
		end
	end
end

function onPlayerWeaponSwitchHandler(prev_slot, current_slot)
	toggleControl("fire", true)
	toggleControl("action", true)
	removeEventHandler("onClientKey", root, onPlayerTaserFire)
	if current_slot == 2 and getPedWeapon( localPlayer ) == 23 then
		toggleControl("fire", false)
		toggleControl("action", false)
		addEventHandler("onClientKey", root, onPlayerTaserFire)
	end
end

local shotsToRender = {}
local heartbeatSound = nil

function startup()
	local txd = engineLoadTXD ( "Res/taser.txd" )
	engineImportTXD ( txd, 347 )
	local dff = engineLoadDFF ( "Res/taser.dff" )
	engineReplaceModel ( dff, 347 )
	addEventHandler("onClientPlayerWeaponSwitch", root, onPlayerWeaponSwitchHandler)

	addEventHandler("tcTaserFired", root,
	function(target)
		local attX, attY, attZ = getElementPosition(source)
		playSound3D("Res/taser.mp3", attX, attY, attZ)
		shotsToRender[source] = { ["target"] = target, timeStartAnim = getTickCount() }
	end)

	addEventHandler("tcPlayHeartbit", root,
	function()
		local tX, tY, tZ = getElementPosition(source)
		if heartbeatSound then stopSound(heartbeatSound) end
		heartbeatSound = playSound3D("Res/heartbeat.mp3", tX, tY, tZ)
	end)

	local pTaserBeamMaterial = dxCreateTexture("Res/beam.png")

	addEventHandler("onClientRender", root,
	function()
		for attacker,shot in pairs(shotsToRender) do

			if getTickCount() - shot.timeStartAnim > 5000 then
				shotsToRender[attacker] = nil
			else
				local vecRandBias = Vector3( math.random(1,3)/10, math.random(1,3)/10, math.random(1,3)/10 )
				local aX, aY, aZ = getPedBonePosition(attacker, 25)
				local tX, tY, tZ = getPedBonePosition( shot.target, 3 )
				dxDrawMaterialLine3D(aX, aY, aZ, Vector3(tX,tY,tZ)+vecRandBias, pTaserBeamMaterial, 0.05 )
			end
		end
	end)
end

addEventHandler("onClientResourceStart", resourceRoot, startup)