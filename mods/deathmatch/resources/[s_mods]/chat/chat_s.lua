
-- by Poletaeff

addEventHandler ( "onPlayerChat", root, function ( message, mType )
	if ( mType == 0 or mType == 1 or mType == 2 ) then
		cancelEvent ( );
	end
	if ( mType == 0 ) then
		for i, v in pairs ( getElementsByType( "player" ) ) do
			local x, y, z = getElementPosition ( source )
			local xt, yt, zt = getElementPosition ( v )
			if getDistanceBetweenPoints3D ( x, y, z, xt, yt, zt ) <= 10 then
				outputChatBox ( getPlayerName(source).." сказал: "..message, v, 255, 255, 255, true )
			end
		end
	elseif ( mType == 1 ) then
		for i, v in pairs ( getElementsByType( "player" ) ) do
			local x, y, z = getElementPosition ( source )
			local xt, yt, zt = getElementPosition ( v )
			if getDistanceBetweenPoints3D ( x, y, z, xt, yt, zt ) <= 10 then
				outputChatBox ( "#7656b2* #7656b2"..getPlayerName(source).."  #7656b2"..message, v, 255, 0, 255, true )
			end
		end
	end
end
)

-- Небольшое дополнение для купивших, разъеснение принципа функции на примере комманды /do =)
function doChat ( player, cmd, ... ) -- Функция комманды /do
if not isPlayerMuted ( player ) then -- проверяем, нет ли мута на игроке
	if ... then -- проверяем, не задано ли пустое значение комманды
		local message = table.concat( {...}, " ") -- обрабатываем сообщение
		for i, v in pairs ( getElementsByType( "player" ) ) do -- поучаем список всех игроков, т.е. все, что ниже - будет выполняться для каждого отдельного игрока
			local x, y, z = getElementPosition ( player ) -- получаем координаты источника сообщения
			local xt, yt, zt = getElementPosition ( v ) -- получаем координаты других игроков (напоминаю, что это будет делаться для КАЖДОГО игрока)
			if getDistanceBetweenPoints3D ( x, y, z, xt, yt, zt ) <= 10 then -- проверяем дистанцию между игроками, если она будет больше, чем 10 - сообщение не будет видно
				outputChatBox ( "#7656b2"..message.." #7656b2(( "..getPlayerName(player).." #7656b2))", v, 255, 125, 125, true ) -- вывод самого сообщения, если дистанция меньше 15 или равна 15
			end -- конец вывода сообщения
		end -- конец проверки всех игроков
	end -- конец проверки наличия сообщения
end -- конец проверки на мут
end -- конец функции
addEventHandler ( "CommmandHandler", getRootElement(), doChat ) -- добавляем событие
addCommandHandler ( "do", doChat ) -- добавляем саму команду /do

	-- Небольшое дополнение для купивших, разъеснение принципа функции на примере комманды /m =)
function meChat ( player, cmd, ... ) -- Функция комманды /m
if not isPlayerMuted ( player ) then -- проверяем, нет ли мута на игроке
	if ... then -- проверяем, не задано ли пустое значение комманды
		local message = table.concat( {...}, " ") -- обрабатываем сообщение
		for i, v in pairs ( getElementsByType( "player" ) ) do -- поучаем список всех игроков, т.е. все, что ниже - будет выполняться для каждого отдельного игрока
			local x, y, z = getElementPosition ( player ) -- получаем координаты источника сообщения
			local xt, yt, zt = getElementPosition ( v ) -- получаем координаты других игроков (напоминаю, что это будет делаться для КАЖДОГО игрока)
			if getDistanceBetweenPoints3D ( x, y, z, xt, yt, zt ) <= 10 then -- проверяем дистанцию между игроками, если она будет больше, чем 10 - сообщение не будет видно
				outputChatBox ( "#7656b2* #7656b2"..getPlayerName(player).."  #7656b2"..message.." ", v, 255, 125, 125, true ) -- вывод самого сообщения, если дистанция меньше 15 или равна 15
			end -- конец вывода сообщения
		end -- конец проверки всех игроков
	end -- конец проверки наличия сообщения
end -- конец проверки на мут
end -- конец функции
addEventHandler ( "CommmandHandler", getRootElement(), meChat ) -- добавляем событие
addCommandHandler ( "m", meChat ) -- добавляем саму команду /m

	-- Небольшое дополнение для купивших, разъеснение принципа функции на примере комманды /try =)
function tryChat ( player, cmd, ... ) -- Функция комманды /try
if not isPlayerMuted ( player ) then -- проверяем, нет ли мута на игроке
	if ... then -- проверяем, не задано ли пустое значение комманды
		local message = table.concat( {...}, " ") -- обрабатываем сообщение
		for i, v in pairs ( getElementsByType( "player" ) ) do -- поучаем список всех игроков, т.е. все, что ниже - будет выполняться для каждого отдельного игрока	
			local x, y, z = getElementPosition ( player ) -- получаем координаты источника сообщения
			local xt, yt, zt = getElementPosition ( v ) -- получаем координаты других игроков (напоминаю, что это будет делаться для КАЖДОГО игрока)
			if getDistanceBetweenPoints3D ( x, y, z, xt, yt, zt ) <= 10 then -- проверяем дистанцию между игроками, если она будет больше, чем 10 - сообщение не будет видно
				outputChatBox ( "#7656b2*** #7656b2"..getPlayerName(player).." попытался #7656b2"..message.."", v, 255, 125, 125, true ) -- вывод самого сообщения, если дистанция меньше 15 или равна 15
			end -- конец вывода сообщения
		end -- конец проверки всех игроков
	end -- конец проверки наличия сообщения
end -- конец проверки на мут
end -- конец функции
addEventHandler ( "CommmandHandler", getRootElement(), tryChat ) -- добавляем событие
addCommandHandler ( "try", tryChat ) -- добавляем саму команду /try

function wChat ( player, cmd, ... )
if not isPlayerMuted ( player ) then
	if ... then
		local message = table.concat( {...}, " ")
		for i, v in pairs ( getElementsByType( "player" ) ) do
			local x, y, z = getElementPosition ( player )
			local xt, yt, zt = getElementPosition ( v )
			if getDistanceBetweenPoints3D ( x, y, z, xt, yt, zt ) <= 5 then
				outputChatBox ( "#ffffff"..getPlayerName(player).." #ffffffшепчет: "..message, v, 150, 150, 150, true )
			end
		end
	end
end
end
addEventHandler ( "CommmandHandler", getRootElement(), wChat )
addCommandHandler ( "w", wChat )

function bChat ( player, cmd, ... )
if not isPlayerMuted ( player ) then
	if ... then
		local message = table.concat( {...}, " ")
		for i, v in pairs ( getElementsByType( "player" ) ) do
			local x, y, z = getElementPosition ( player )
			local xt, yt, zt = getElementPosition ( v )
	        local id = getElementData (player, "ID")			
			if getDistanceBetweenPoints3D ( x, y, z, xt, yt, zt ) <= 10 then
				outputChatBox ( "#ffffff"..getPlayerName(player).."#ffffff["..tonumber(id).."]: #ffffff(( #ffffff"..message.." #ffffff))", v, 150, 150, 150, true )
			end
		end
	end
end
end
addCommandHandler ("OOC", bChat)

addEventHandler ("onResourceStart", resourceRoot, function ()
for i, v in ipairs (getElementsByType ("player")) do
bindKey (v, "b", "down", "chatbox", "OOC")
end
end)

addEventHandler ("onPlayerJoin", root, function()
bindKey (source, "b", "down", "chatbox", "OOC")
end)

_getPlayerName = getPlayerName
function getPlayerName(player)
  return getElementData(player, "rp.name") and getElementData(player, "rp.name") or _getPlayerName(player)
end