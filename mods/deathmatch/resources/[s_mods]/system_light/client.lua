local shader = {}
shader[1] = dxCreateShader("files/shader.fx") -- Blinkers
shader[2] = dxCreateShader("files/shader3.fx") -- Alternative blinkers
local animShads = {}

local tex_left = "rpb_left" -- Левая сторона мигает текстуры
local tex_right = "rpb_right" -- Правая сторона мигает текстуры
local tex_reverse = "vehiclelights128" -- Текста фары

function getShaderType(veh)
	if aVehicles[getElementModel(veh)] then 
		return 2 
	else 
		return 1
	end
end

function updateShaders(veh)
	local state = getElementData(veh,"signal")
	local t = getShaderType(veh)
	if state == 0 then
		engineRemoveShaderFromWorldTexture( shader[t], tex_left, veh )
		engineRemoveShaderFromWorldTexture( shader[t], tex_right, veh )
	elseif state == 1 then
		engineApplyShaderToWorldTexture( shader[t],tex_left,veh )
		engineRemoveShaderFromWorldTexture( shader[t], tex_right, veh )
	elseif state == 2 then
		engineApplyShaderToWorldTexture( shader[t],tex_right,veh )
		engineRemoveShaderFromWorldTexture( shader[t], tex_left, veh )
	elseif state == 3 then
		engineApplyShaderToWorldTexture( shader[t],tex_left,veh )
		engineApplyShaderToWorldTexture( shader[t],tex_right,veh )
	end
end

function updateLights(veh)
	local state = getElementData(veh,"lights")
	if state == 0 then
		setVehicleOverrideLights( veh, 1 )
		if animShads[veh] then
			destroyElement(animShads[veh][1])
			animShads[veh] = nil
		end
	elseif state == 1 then
		local shad = dxCreateShader("files/shader2.fx")
		animShads[veh] = {shad,0}
		engineApplyShaderToWorldTexture( shad, tex_reverse, veh)
	elseif state == 2 then
		setVehicleOverrideLights( veh, 2 )
		setVehicleHeadLightColor(veh,255,255,255)
	end
end

local last = 0

function updateMultipliers()
	local cur = getTickCount()-last
	last = getTickCount()
	for k,v in pairs(animShads) do
		v[2] = v[2] + cur/1000
		dxSetShaderValue(v[1],"gMultiplier",v[2])
		if v[2] >= 1 then
			destroyElement(v[1])
			setVehicleOverrideLights(k, 2 )
			setVehicleHeadLightColor(k,0,0,0)
			animShads[k] = nil
			--setVehicleHeadLightColor(veh,0,0,0)
		end
	end
end
addEventHandler("onClientRender",root,updateMultipliers)

bindKey("l","down",function()
	local veh = getPedOccupiedVehicle( localPlayer )
	if veh then
		if getVehicleController(veh) ~= localPlayer then return end
		triggerServerEvent( "switchLights", localPlayer, veh )
	end
end)

bindKey(",","down",function()
	local veh = getPedOccupiedVehicle( localPlayer )
	if veh then
		if getVehicleController(veh) ~= localPlayer then return end
		local cur = getElementData(veh,"signal")
		if cur == 1 then
			setElementData(veh,"signal",0)
		else
			setElementData(veh,"signal",1)
		end
	end
end)

bindKey(".","down",function()
	local veh = getPedOccupiedVehicle( localPlayer )
	if veh then
		if getVehicleController(veh) ~= localPlayer then return end
		local cur = getElementData(veh,"signal")
		if cur == 2 then
			setElementData(veh,"signal",0)
		else
			setElementData(veh,"signal",2)
		end
	end
end)

bindKey("/","down",function()
	local veh = getPedOccupiedVehicle( localPlayer )
	if veh then
		if getVehicleController(veh) ~= localPlayer then return end
		local cur = getElementData(veh,"signal")
		if cur == 3 then
			setElementData(veh,"signal",0)
		else
			setElementData(veh,"signal",3)
		end
	end
end)

toggleControl("vehicle_look_right",false)
toggleControl("vehicle_look_left",false)

addEventHandler ( "onClientElementDataChange", getRootElement(),
function ( dataName )
	if getElementType ( source ) == "vehicle" then
		if dataName == "signal" then
			updateShaders(source)
		elseif dataName == "lights" then
			updateLights(source)
		end
	end
end)