-- TrollingCont. 18.02.2020

local width, height = guiGetScreenSize()

local isRendered = false
local textures = {}
local fonts = {}

local baseX = width - 268
local baseY = height - 503

local currentContent = "home"

local timeStr = ""
local displayedTime = 0
local lockScreenDateStr = ""
local displayedDate = 0

local weekDays = {
    [0] = "Воскресенье",
    [1] = "Понедельник",
    [2] = "Вторник",
    [3] = "Среда",
    [4] = "Четверг",
    [5] = "Пятница",
    [6] = "Суббота"
}

local months = {
    [0] = "Января",
    [1] = "Февраля",
    [2] = "Марта",
    [3] = "Апреля",
    [4] = "Мая",
    [5] = "Июня",
    [6] = "Июля",
    [7] = "Августа",
    [8] = "Сентября",
    [9] = "Октября",
    [10] = "Ноября",
    [11] = "Декабря"
}

local regions = {
    ["global"] =
    {
        { 
            x = 45, y = 415, w = 20, h = 20,
            func = function()
                currentContent = "home"
            end
        }
    },
    ["home"] =
    {
        {
            x = 100, y = 355, w = 42, h = 44,
            func = function()
                currentContent = "apps"
            end
        }
    }
}

function renderPhone()

    dxDrawImage(baseX, baseY, 238, 473, textures.phoneBg)

    --dxDrawRectangle(baseX + 100, baseY + 355, 42, 44, 0xA0FF0000)

    dxDrawText("Моя связь", baseX + 9, baseY + 49, leftX, topY, white, 1, fonts.oper)

    local time = getRealTime()
    if time.hour * 60 + time.minute ~= displayedTime then
        displayedTime = time.hour * 60 + time.minute
        timeStr = string.format("%02d:%02d", time.hour, time.minute)
    end

    dxDrawText(timeStr, baseX + 200, baseY + 49, leftX, topY, white, 1, fonts.oper)

    if currentContent == "home" then
        dxDrawImage(baseX + 23, baseY + 350, 64, 62, textures.call)
        dxDrawImage(baseX + 87, baseY + 350, 64, 62, textures.list)
        dxDrawImage(baseX + 151, baseY + 350, 64, 62, textures.camera)
        dxDrawText(timeStr, baseX, baseY + 100, baseX + 238, topY, white, 1, fonts.lockScreenTime, "center")
        if time.monthday ~= displayedDate then
            displayedDate = time.monthday
            lockScreenDateStr = weekDays[time.weekday]..string.format(" %02d ", time.monthday)..months[time.month]
        end
        dxDrawText(lockScreenDateStr, baseX, baseY + 180, baseX + 238, topY, white, 1, fonts.lockScreenDate, "center")
    elseif currentContent == "apps" then
        dxDrawImage(baseX + 30, baseY + 85, 39, 39, textures.sms)
        dxDrawText("Сообщения", baseX + 20, baseY + 130, baseX + 80, topY, white, 1, fonts.oper, "center")
        dxDrawImage(baseX + 100, baseY + 85, 39, 39, textures.calc)
        dxDrawText("Калькулятор", baseX + 90, baseY + 130, baseX + 150, topY, white, 1, fonts.oper, "center")
        dxDrawImage(baseX + 170, baseY + 85, 39, 39, textures.notes)
        dxDrawText("Заметки", baseX + 160, baseY + 130, baseX + 220, topY, white, 1, fonts.oper, "center")
        dxDrawImage(baseX + 30, baseY + 155, 39, 39, textures.store)
        dxDrawText("Магазин", baseX + 20, baseY + 200, baseX + 80, topY, white, 1, fonts.oper, "center")
        dxDrawImage(baseX + 100, baseY + 155, 39, 39, textures.settings)
        dxDrawText("Настройки", baseX + 90, baseY + 200, baseX + 150, topY, white, 1, fonts.oper, "center")
        dxDrawImage(baseX + 170, baseY + 155, 39, 39, textures.towTruck)
        dxDrawText("Эвакуатор", baseX + 160, baseY + 200, baseX + 220, topY, white, 1, fonts.oper, "center")
    end
end

function startup()

    textures.phoneBg = dxCreateTexture("Res/Phone.png")
    textures.call = dxCreateTexture("Res/call.png")
    textures.camera = dxCreateTexture("Res/camera.png")
    textures.list = dxCreateTexture("Res/list.png")
    textures.calc = dxCreateTexture("Res/AppIcons/calc.png")
    textures.notes = dxCreateTexture("Res/AppIcons/notes.png")
    textures.policeScan = dxCreateTexture("Res/AppIcons/police_scan.png")
    textures.settings = dxCreateTexture("Res/AppIcons/settings.png")
    textures.sms = dxCreateTexture("Res/AppIcons/sms.png")
    textures.store = dxCreateTexture("Res/AppIcons/store.png")
    textures.towTruck = dxCreateTexture("Res/AppIcons/tow_truck.png")

    fonts.oper = dxCreateFont("Res/OpenSans-Light.ttf", 9)
    fonts.lockScreenTime = dxCreateFont("Res/OpenSans-Light.ttf", 50)
    fonts.lockScreenDate = dxCreateFont("Res/OpenSans-Light.ttf", 10)

    --showCursor(true)
    --isRendered = true
    --addEventHandler("onClientRender", root, renderPhone)

    addEventHandler("onClientKey", root,
    function(key, press)
        if press then return end

        if key == "p" then
            if isRendered then
                showCursor(false)
                removeEventHandler("onClientRender", root, renderPhone)
            else
                currentContent = "home"
                showCursor(true)
                addEventHandler("onClientRender", root, renderPhone)
            end
            isRendered = not isRendered
        end
    end)

    addEventHandler("onClientClick", root,
    function(button, state, absX, absY)

        if not isRendered then return end

        if button == "left" and state == "up" then

            absX = absX - baseX
            absY = absY - baseY

            for i,elem in ipairs(regions["global"]) do
                if absX >= elem.x and absX <= elem.x + elem.w and absY >= elem.y and absY <= elem.y + elem.h then
                    elem.func()
                    break
                end
            end

            if not regions[currentContent] then return end

            for i,elem in ipairs(regions[currentContent]) do
                if absX >= elem.x and absX <= elem.x + elem.w and absY >= elem.y and absY <= elem.y + elem.h then
                    elem.func()
                    break
                end
            end
        end
    end)
end

addEventHandler("onClientResourceStart", resourceRoot, startup)