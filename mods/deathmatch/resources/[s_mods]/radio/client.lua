﻿local screenW, screenH = guiGetScreenSize()
local num_radio = 0
local alpha = 0
local music = false
local tick = getTickCount()

local font = dxCreateFont("Gilroy-SemiBold.ttf", 18)

local numRad = { -- Названия и ссылки на радиостанции
[1] = {"Радиостанция: EuropaPlus", "http://ep128server.streamr.ru:8030/ep128"},
[2] = {"Радиостанция: EuropaPlus Top 20", "http://eptop128server.streamr.ru:8033/eptop128"},
[3] = {"Радиостанция: Radio Record", "http://online.radiorecord.ru:8101/rr_128"},
[4] = {"Радиостанция: Rap", "http://music.myradio.ua/rep128.mp3"},
[5] = {"Радиостанция: Russia Mix", "http://air.radiorecord.ru:8102/rus_320"},
[6] = {"Радиостанция: Шансон", "http://chanson.hostingradio.ru:8041/chanson-uncensored128.mp3"},
[7] = {"Радиостанция: Russian Hits", "http://air2.radiorecord.ru:9003/russianhits_320"},
[8] = {"Радио выключено", false},
}

addEventHandler('onClientPlayerRadioSwitch', getRootElement(),
	function()
		setRadioChannel(0)
		cancelEvent()
	end
)

local min_rad, max_rad = 1,#numRad

function mouse_left()
	num_radio = num_radio -1

	local veh = getPedOccupiedVehicle(localPlayer)
	if(num_radio < min_rad)then
		num_radio = max_rad
	end

	if music ~= false then
		destroyElement(music)
		music = false
	end
	if numRad[num_radio][2] == false then return end
	music = playSound(numRad[num_radio][2])
	setSoundVolume(music, 1)
end

function mouse_right()
	local veh = getPedOccupiedVehicle(localPlayer)
	num_radio = num_radio +1

	if(num_radio > max_rad)then
		num_radio = min_rad
	end

	if music ~= false then
		destroyElement(music)
		music = false
	end
	if numRad[num_radio][2] == false then return end
	music = playSound(numRad[num_radio][2])
	setSoundVolume(music, 1)
end

bindKey("mouse_wheel_up", "down", function()
	if not getPedOccupiedVehicle(localPlayer) then return end
	mouse_left() 
	if alpha ~= 255 then
		alpha = 255
		tick = getTickCount()
	end
end)

bindKey("mouse_wheel_down", "down", function()
	if not getPedOccupiedVehicle(localPlayer) then return end
	mouse_right()
	if alpha ~= 255 then
		alpha = 255
		tick = getTickCount()
	end
end)

addEventHandler("onClientRender", root, function()
	local veh = getPedOccupiedVehicle(localPlayer)
	if music ~= false and not veh then
		destroyElement(music)
		music = false
	end
	if veh then
		local now_time = getTickCount()
		if tick ~= 0 and now_time > tick+2000 then
			local Progress = (now_time-tick)/10000
			alpha = interpolateBetween(alpha,0,0,0,0,0,Progress,"InOutQuad")
		end
		if alpha < 1 then return end
        dxDrawText(numRad[num_radio][1], (screenW * 0.0000) + 1, (screenH * 0.0625) + 1, (screenW * 1.0000) + 1, (screenH * 0.1289) + 1, tocolor(0, 0, 0, alpha), 1, font, "center", "center", false, false, false, false, false)
        dxDrawText(numRad[num_radio][1], screenW * 0.0000, screenH * 0.0625, screenW * 1.0000, screenH * 0.1289, tocolor(255, 255, 255, alpha), 1, font, "center", "center", false, false, false, false, false)
	end
end)

addEventHandler("onClientVehicleExit", root, function(player, seat)
	if player ~= localPlayer then return end
	if music ~= false then
		destroyElement(music)
		music = false
	end
end)

addEventHandler("onClientVehicleEnter", root, function(player, seat)
	if player ~= localPlayer then return end
	if alpha ~= 255 then
		alpha = 255
		tick = getTickCount()
	end
	if num_radio == 0 then
		num_radio = #numRad
	end
	if numRad[num_radio][2] == false then return end
	music = playSound(numRad[num_radio][2])
	setSoundVolume(music, 1)
end)