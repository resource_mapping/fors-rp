-- TrollingCont. 29.02.2020

local width, height = guiGetScreenSize()

itemsToRender = {}
local fonts = {}
inventoryDisplayed = false

gunsGiven = {}

function addItemToRender(itemId, itemCount, filePath)
    local indexToAdd = #itemsToRender
    local xIndex = math.fmod(indexToAdd, 4)
    local yIndex = math.floor(indexToAdd / 4)
    local item = {
        name = itemId,
        path = "Res/Icons/"..itemId..".png",
        coordX = 55 + 80*xIndex,
        coordY = height - 165 - 80*(3 - yIndex),
        regionX = 50 + 80*xIndex,
        regionY = height - 170 - 80*(3 - yIndex),
        regionSize = 70,
        display = true
    }
    if itemCount then
        item.count = tostring(itemCount)
    end
    if not filePath then
        item.path = "Res/Icons/"..itemId..".png"
    else
        item.path = filePath
    end
    table.insert(itemsToRender, item)
    indexToAdd = indexToAdd + 1
end

function setItemDisplayed(itemId, display)
    local itemIndex = nil

    for i,item in ipairs(itemsToRender) do
        if item.name == itemId then
            itemIndex = i
            break
        end
    end
    if not itemIndex then return end

    itemsToRender[itemIndex].display = display
end

function removeItemToRender(itemId)
    local indexToRemove = nil

    for i,item in ipairs(itemsToRender) do
        if item.name == itemId then
            indexToRemove = i
            break
        end
    end

    table.remove(itemsToRender, indexToRemove)

    if not indexToRemove or indexToRemove >= #itemsToRender + 1 then return end

    for i = indexToRemove, #itemsToRender do
        local xIndex = math.fmod((i - 1), 4)
        local yIndex = math.floor((i - 1) / 4)
        itemsToRender[i].coordX = 55 + 80*xIndex
        itemsToRender[i].coordY = height - 165 - 80*(3 - yIndex)
        itemsToRender[i].regionX = 50 + 80*xIndex
        itemsToRender[i].regionY = height - 170 - 80*(3 - yIndex)
        itemsToRender[i].regionSize = 70
    end
end

function renderInventory()
    dxDrawRectangle(30, height - 460, 350, 30, 0xFF000000)
    dxDrawRectangle(30, height - 430, 350, 350, 0xA0000000)
    dxDrawText("Инвентарь", 40, height - 455, leftX, topY, 0xFFFFFFFF, 1, fonts.caption)

    for y = 0, 3 do
        for x = 0, 3 do
            dxDrawRectangle(50 + 80*x, height - 170 - 80*y, 70, 70, 0xFF505050)
        end
    end

    for i = 0, 3 do
        dxDrawRectangle(400, height - 170 - 80*i, 70, 70, 0xFF505050)
    end

    for i,item in ipairs(itemsToRender) do
        if item.display then
            dxDrawImage(item.coordX, item.coordY, 60, 60, item.path)
            if item.count then

                --dxDrawRectangle(item.coordX + 37, item.coordY + 48, 28, 17, 0xE0FF0000)

                dxDrawText(item.count, item.coordX + 35, item.coordY + 46, item.coordX + 63, item.coordY + 63, 0xFFFFFFFF, 1, fonts.c, "right", "center")
            end
        end
    end
end

function setInventoryDisplayed(key, state)
    if state == "down" then
        setPlayerHudComponentVisible("radar", false)
        inventoryDisplayed = true
        showCursor(true)
        addEventHandler("onClientRender", root, renderInventory)
    else
        setPlayerHudComponentVisible("radar", true)
        inventoryDisplayed = false
        showCursor(false)
        setMovedIconRendered(false)
        setItemDisplayed(usedItemName, true)
        removeEventHandler("onClientRender", root, renderInventory)
    end
end

function startup()

    fonts.inv = dxCreateFont("Res/Roboto-Medium.ttf", 15)
    fonts.caption = dxCreateFont("Res/Roboto-Light.ttf", 14)
    fonts.q = dxCreateFont("Res/Roboto-Medium.ttf", 19)
    fonts.c = dxCreateFont("Res/Roboto-Light.ttf", 9)

    addEventHandler("onClientRender", root,
    function()
        dxDrawText("Инвентарь", 30, height - 50, leftX, topY, 0xFFFFFFFF, 1, fonts.inv)
        dxDrawImage(160, height - 60, 43, 43, "Res/Key.png")
        dxDrawText("Q", 170, height - 57, leftX, topY, 0xFFFFFFFF, 1, fonts.q)
    end)

    addItemToRender("passport", nil, "Res/passport.png")
    addItemToRender("job_history", nil, "Res/job_history.png")
    local items = getElementData(localPlayer, ACCOUNT_INVENTORY_KEY)
    if items then
        for name,count in pairs(items) do
            addItemToRender(name, count)
        end
    end

    bindKey("q", "both", setInventoryDisplayed)

    addEventHandler("onClientElementDataChange", localPlayer,
    function(key, oldVal, newVal)
        if key == ACCOUNT_INVENTORY_KEY and type(newVal) == "table" then
            itemsToRender = {}
            addItemToRender("passport", nil, "Res/passport.png")
            addItemToRender("job_history", nil, "Res/job_history.png")
            for name,count in pairs(newVal) do
                addItemToRender(name, count)
            end
        end
    end)
end

addEventHandler("onClientResourceStart", resourceRoot, startup)