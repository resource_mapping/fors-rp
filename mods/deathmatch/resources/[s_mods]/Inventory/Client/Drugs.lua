-- TrollingCont. 25.02.2020

local screenSource
local width, height
local shader

function renderEffects()
    dxUpdateScreenSource(screenSource)
    dxSetShaderValue(shader, "Tex0", screenSource)
    dxDrawImage(0, 0, width, height, shader)
end

function setDragEffects(state)
    if state then
        --engineApplyShaderToWorldTexture(shader, "waterclear256")
        --engineApplyShaderToWorldTexture(shader, "cypress2")
        --engineApplyShaderToWorldTexture(shader, "vehiclegrunge256")
        addEventHandler("onClientRender", root, renderEffects)
    else
        removeEventHandler("onClientRender", root, renderEffects)
        --engineRemoveShaderFromWorldTexture(shader, "waterclear256")
        --engineRemoveShaderFromWorldTexture(shader, "cypress2")
        --engineRemoveShaderFromWorldTexture(shader, "vehiclegrunge256")
    end
end

function startup()
    width, height = guiGetScreenSize()
    screenSource = dxCreateScreenSource(width, height)
    shader = dxCreateShader("Client/Shader.fx")
    
    --dxSetShaderValue(shader, "Tex0", dxCreateTexture("Res/Azure.png"))
    engineApplyShaderToWorldTexture(shader, "waterclear256")

    --dxSetShaderValue(shader, "Tex0", dxCreateTexture("Res/Milos.png"))
end

addEventHandler("onClientResourceStart", resourceRoot, startup)