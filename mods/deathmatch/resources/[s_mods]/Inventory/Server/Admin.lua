-- TrollingCont. 22.02.2020

function hasPlayerAdminRights(player)
	if getElementType(player) == "console" then return true end
	local account = getPlayerAccount(player)
	return isObjectInACLGroup("user."..getAccountName(account), aclGetGroup("Admin"))
end

function startup()
    addCommandHandler("giveitem",
    function(playerSource, cmd, accountName, itemId, count)
        if not hasPlayerAdminRights(playerSource) then return end

        if not accountName then
            outputChatBox("#CCCCCCВведите имя аккаунта", playerSource, 0, 0, 0, true)
            return
        end

        if not itemId then
            outputChatBox("#CCCCCCВведите ID предмета", playerSource, 0, 0, 0, true)
            return
        end

        if not count then
            count = 1
        else
            count = tonumber(count)
        end

        if not count or count < 1 then
            outputChatBox("#CCCCCCНеправильное значение числа предметов", playerSource, 0, 0, 0, true)
            return
        end

        local account = getAccount(accountName)
        if not account then
            outputChatBox("#CCCCCCТакого аккаунта не существует", playerSource, 0, 0, 0, true)
            return
        end

        local currentItemCount = getAccountInventoryItemCount(account, itemId)

        if fileExists("Res/Icons/"..itemId..".png") then
            if setAccountInventoryItem(account, itemId, currentItemCount + count) then
                outputChatBox("#CCCCCCАккаунту #FF8000"..accountName.." #CCCCCCвыдано #FF8000"..count.."#CCCCCC предметов (#FF8000"..itemId.."#CCCCCC)", playerSource, 0, 0, 0, true)
            else
                outputChatBox("#FF2020Ошибка выдачи предмета", playerSource, 0, 0, 0, true)
            end
        end
    end)

    addCommandHandler("takeitem",
    function(playerSource, cmd, accountName, itemId, count)
        if not hasPlayerAdminRights(playerSource) then return end
        if type(count) == "string" then
            count = tonumber(count)
        end

        if not accountName then
            outputChatBox("#CCCCCCВведите имя аккаунта", playerSource, 0, 0, 0, true)
            return
        end

        if not itemId then
            outputChatBox("#CCCCCCВведите ID предмета", playerSource, 0, 0, 0, true)
            return
        end

        if count and count < 1 then
            outputChatBox("#CCCCCCНеправильное значение числа предметов", playerSource, 0, 0, 0, true)
            return
        end

        local account = getAccount(accountName)
        if not account then
            outputChatBox("#CCCCCCТакого аккаунта не существует", playerSource, 0, 0, 0, true)
            return
        end

        local currentItemCount = getAccountInventoryItemCount(account, itemId)

        if currentItemCount == 0 then
            outputChatBox("#CCCCCCАккаунт не имеет данных предметов", playerSource, 0, 0, 0, true)
            return
        end
        if count and count > currentItemCount then
            outputChatBox("#CCCCCCУ аккаунта меньше таких предметов (#FF8000"..currentItemCount.."#CCCCCC), чем вы пытались забрать (#FF8000"..count.."#CCCCCC)", playerSource, 0, 0, 0, true)
            return
        end

        local itemsToRemove = nil

        if not count then
            count = 0
            itemsToRemove = currentItemCount
        else
            itemsToRemove = count
            count = currentItemCount - count
        end

        if setAccountInventoryItem(account, itemId, count) then
            outputChatBox("#CCCCCCВы успешно забрали у аккаунта #FF8000"..accountName.." "..itemsToRemove.." #CCCCCCпредметов", playerSource, 0, 0, 0, true)
            return
        end
    end)
end

addEventHandler("onResourceStart", resourceRoot, startup)