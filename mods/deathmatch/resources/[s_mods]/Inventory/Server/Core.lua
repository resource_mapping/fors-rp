-- TrollingCont. 22.02.2020

local db = nil

function getAccountInventoryItems(account)
    local accName = getAccountName(account)
    if not accName then return false end

    local res, rows = dbPoll(dbQuery(db, "SELECT items FROM Inventory WHERE accountName = ?", accName), -1)
    if #res < 1 then return false end
    return fromJSON(res[1].items)
end

function setAccountInventoryItem(account, itemId, count)
    if type(count) ~= "number" then return false end
    if count < 0 then return false end
    local accName = getAccountName(account)
    if not accName then return false end

    local accItems = getAccountInventoryItems(account)
    if count ~= 0 then
        if not accItems then
            accItems = { [itemId] = count }
            res, rows = dbPoll(dbQuery(db, "INSERT INTO Inventory (accountName, items) VALUES (?, ?)", accName, toJSON(accItems)), -1)
        else
            accItems[itemId] = count
            res, rows = dbPoll(dbQuery(db, "UPDATE Inventory SET items = ? WHERE accountName = ?", toJSON(accItems), accName), -1)
        end
    else
        if not accItems then return false end
        accItems[itemId] = nil
        local itemsCount = 0
        for i,item in pairs(accItems) do
            itemsCount = itemsCount + 1
        end
        if itemsCount == 0 then
            accItems = false
            res, rows = dbPoll(dbQuery(db, "DELETE FROM Inventory WHERE accountName = ?", accName), -1)
        else
            accItems[itemId] = nil
            res, rows = dbPoll(dbQuery(db, "UPDATE Inventory SET items = ? WHERE accountName = ?", toJSON(accItems), accName), -1)
        end
    end

    local accountPlayer = getAccountPlayer(account)

    if rows == 1 then
        if accountPlayer then
            setElementData(accountPlayer, ACCOUNT_INVENTORY_KEY, accItems)
        end
        return true
    end
    return false
end

function getAccountInventoryItemCount(account, itemId)
    local accName = getAccountName(account)
    if not accName then return false end

    local items = getAccountInventoryItems(account)

    if not items or not items[itemId] then return 0 end
    return items[itemId]
end

function startup()
    db = dbConnect("sqlite", "Inventory.db")

    dbPoll(dbQuery(db, "CREATE TABLE IF NOT EXISTS Inventory (accountName TEXT UNIQUE, items TEXT)"), -1)

    local allPlayers = getElementsByType("player")

    for i,player in ipairs(allPlayers) do
        local playerAcc = getPlayerAccount(player)

        if playerAcc then
            setElementData(player, ACCOUNT_INVENTORY_KEY, getAccountInventoryItems(playerAcc))
        end
    end

    addEventHandler("onPlayerLogin", root,
    function(oldAcc, newAcc)
        setElementData(source, ACCOUNT_INVENTORY_KEY, getAccountInventoryItems(newAcc))
    end)
end

addEventHandler("onResourceStart", resourceRoot, startup)