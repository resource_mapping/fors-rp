-- TrollingCont. 25.02.2020

local itemsList = {
    ["player"] = {
        "heavyarmor",
        "firstaid",
        "drugs_2",
        "16", "17", "18", "22", "23",
        "24", "25", "26", "28", "29",
        "30", "31", "32", "33", "34",
        "35", "36", "38", "39", "41",
        "42", "43"
    },
    ["vehicle"] = {
        "repairbox"
    }
}

function isItemApplicableToElement(itemId, element)
    local elemType = getElementType(element)
    if not itemsList[elemType] then return false end

    for i,item in ipairs(itemsList[elemType]) do
        if itemId == item then return true end
    end
    return false
end

function startup()
    addEventHandler("tcOnItemApply", root,
    function(appliedTo, itemName)

        local sourceAcc = getPlayerAccount(client)

        if not isItemApplicableToElement(itemName, appliedTo) or isGuestAccount(sourceAcc) then
            triggerClientEvent(client, "tcOnItemApply", client, nil, false)
            return
        end

        local itemCount = getAccountInventoryItemCount(sourceAcc, itemName)

        if tonumber(itemName) and fileExists("Res/Icons/"..itemName..".png") then
            giveWeapon(appliedTo, tonumber(itemName), itemCount * 30)
            if setAccountInventoryItem(sourceAcc, itemName, 0) then
                triggerClientEvent(client, "tcOnItemApply", client, "Вы извлекли оружие из инвентаря ("..getWeaponNameFromID(tonumber(itemName))..", "..(itemCount * 30)..")", true)
            else
                triggerClientEvent(client, "tcOnItemApply", client, "Не удалось извлечь оружие из инвентаря", false)
            end
            return
        end

        local success = setAccountInventoryItem(sourceAcc, itemName, itemCount - 1)
        local notificationText = nil

        if itemName == "heavyarmor" then
            if success then
                setPedArmor(appliedTo, 100)
                notificationText = "Вы успешно применили броню"
            else
                notificationText = "Не удалось применить броню"
            end
        elseif itemName == "firstaid" then
            if success then
                setElementHealth(appliedTo, 100)
                notificationText = "Вы успешно применили аптечку"
            else
                notificationText = "Не удалось применить аптечку"
            end
        elseif itemName == "repairbox" then
            if success then
                fixVehicle(appliedTo)
                notificationText = "Транспортное средство отремонтировано"
            else
                notificationText = "Не удалось отремонтировать транспортное средство"
            end
        elseif itemName == "drugs_2" then
            if success then
                notificationText = "Вы приняли наркотики"
                triggerClientEvent(appliedTo, "tcOnItemApply", client, notificationText, success, itemName)
            else
                notificationText = "Ошибка инвентаря"
                triggerClientEvent(appliedTo, "tcOnItemApply", client, notificationText, success, itemName)
            end

            return
        end

        triggerClientEvent(client, "tcOnItemApply", client, notificationText, success, itemName)
    end)
end

addEventHandler("onResourceStart", resourceRoot, startup)