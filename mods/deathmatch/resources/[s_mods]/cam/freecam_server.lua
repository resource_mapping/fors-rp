﻿function setPlayerFreecamEnabled(player, x, y, z, dontChangeFixedMode)
	return triggerClientEvent(player,"doSetFreecamEnabled", getRootElement(), x, y, z, dontChangeFixedMode)
end

function setPlayerFreecamDisabled(player, dontChangeFixedMode)
	return triggerClientEvent(player,"doSetFreecamDisabled", getRootElement(), dontChangeFixedMode)
end

function setPlayerFreecamOption(player, theOption, value)
	return triggerClientEvent(player,"doSetFreecamOption", getRootElement(), theOption, value)
end

function isPlayerFreecamEnabled(player)
	return getElementData(player,"freecam:state")
end

function isPlayerAdmin ( player )
    local account = getPlayerAccount( player )
	if isGuestAccount( account ) then
	    return false
	end
    return isObjectInACLGroup("user." ..getAccountName( account ), aclGetGroup("Admin"))
end

function onPlayerLogin ()
    setElementData( source, "isPlayerAdmin", isPlayerAdmin ( source ) )
end
addEventHandler( "onPlayerLogin", getRootElement(), onPlayerLogin )

function onResourceStart ()
    for i, player in ipairs ( getElementsByType("player") ) do
		setElementData( player, "isPlayerAdmin", isPlayerAdmin ( player ) )
	end
end
addEventHandler( "onResourceStart", getResourceRootElement( getThisResource() ), onResourceStart )