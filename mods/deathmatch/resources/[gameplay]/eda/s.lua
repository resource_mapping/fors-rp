
function cpizza ()
	mPizza = 50 --Цена пиццы
	if getPlayerMoney(client) >= mPizza then
		setElementHealth(client, getElementHealth(client)+50)
		takePlayerMoney(client,mPizza)
		outputChatBox('Вы купили пиццу!',client,0,255,0)
	else outputChatBox('Недостаточно денег!',client,255,0,0)
	end
end
addEvent('mcPizza',true)
addEventHandler('mcPizza',getRootElement(),cpizza)

function ckeks ()
	mKeks = 10 --Цена кекса
	if getPlayerMoney(client) >= mKeks then
		setElementHealth(client, getElementHealth(client)+30)
		takePlayerMoney(client,mKeks)
		outputChatBox('Вы купили кекс!',client,0,255,0)
	else outputChatBox('Недостаточно денег!',client,255,0,0)
	end
end
addEvent('mcKeks',true)
addEventHandler('mcKeks',getRootElement(),ckeks)

function cpirog ()
	mPirog = 50 --Цена пирога
	if getPlayerMoney(client) >= mPirog then
		setElementHealth(client, getElementHealth(client)+50)
		takePlayerMoney(client,mPirog)
		outputChatBox('Вы купили пирог!',client,0,255,0)
	else outputChatBox('Недостаточно денег!',client,255,0,0)
	end
end
addEvent('mcPirog',true)
addEventHandler('mcPirog',getRootElement(),cpirog)

function ckola ()
	mKola = 10 --Цена колы
	if getPlayerMoney(client) >= mKola then
		setElementHealth(client, getElementHealth(client)+30)
		takePlayerMoney(client,mKola)
		outputChatBox('Вы купили колу!',client,0,255,0)
	else outputChatBox('Недостаточно денег!',client,255,0,0)
	end
end
addEvent('mcKola',true)
addEventHandler('mcKola',getRootElement(),ckola)

function cpivo ()
	mPivo = 15 --Цена пива
	if getPlayerMoney(client) >= mPivo then
		setElementHealth(client, getElementHealth(client)+40)
		takePlayerMoney(client,mPivo)
		outputChatBox('Вы купили пиво!',client,0,255,0)
	else outputChatBox('Недостаточно денег!',client,255,0,0)
	end
end
addEvent('mcPivo',true)
addEventHandler('mcPivo',getRootElement(),cpivo)

function csup ()
	mSup = 50 --Цена супа
	if getPlayerMoney(client) >= mSup then
		setElementHealth(client, getElementHealth(client)+70)
		takePlayerMoney(client,mSup)
		outputChatBox('Вы купили суп!',client,0,255,0)
	else outputChatBox('Недостаточно денег!',client,255,0,0)
	end
end
addEvent('mcSup',true)
addEventHandler('mcSup',getRootElement(),csup)

function cburger ()
	mBurger = 30 --Цена бургера
	if getPlayerMoney(client) >= mBurger then
		setElementHealth(client, getElementHealth(client)+40)
		takePlayerMoney(client,mBurger)
		outputChatBox('Вы купили бургер!',client,0,255,0)
	else outputChatBox('Недостаточно денег!',client,255,0,0)
	end
end
addEvent('mcBurger',true)
addEventHandler('mcBurger',getRootElement(),cburger)
