﻿loginPanel = {}

local sx, sy = guiGetScreenSize ()
local px, py = 1366, 768
local x, y = (sx / px), (sy / py)
local gothampro = dxCreateFont("/text/gothampro.ttf", 14)

local enterText
local login
local password
local password2

local toggle = false
local Y
local sAutologin
local Autologin
local sButton = nil
local windowType

local allowedLetters = {"mouse1", "mouse2", "mouse3", "mouse4", "mouse5", "mouse_wheel_up", "mouse_wheel_down", "arrow_l", "arrow_u", "arrow_r", "arrow_d", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "escape", "tab", "lalt", "ralt", "pgup", "pgdn", "end", "home", "insert", "delete", "lshift", "rshift", "lctrl", "rctrl", "pause", "capslock", "scroll", "backspace"}
local translate = {
    ["q"] = "й",
    ["w"] = "ц",
    ["e"] = "у",
    ["r"] = "к",
    ["t"] = "е",
    ["y"] = "н",
    ["u"] = "г",
    ["i"] = "ш",
    ["o"] = "щ",
    ["p"] = "з",
    ["["] = "х",
    ["]"] = "ъ",
    ["a"] = "ф",
    ["s"] = "ы",
    ["d"] = "в",
    ["f"] = "а",
    ["g"] = "п",
    ["h"] = "р",
    ["j"] = "о",
    ["k"] = "л",
    ["l"] = "д",
    [";"] = "ж",
    ["'"] = "э",
    ["z"] = "я",
    ["x"] = "ч",
    ["c"] = "с",
    ["v"] = "м",
    ["b"] = "и",
    ["n"] = "т",
    ["m"] = "ь",
    [","] = "б",
    ["."] = "ю",
    ["-"] = "_",
    --["/"] = "."
}

local allow = {
    ["й"] = true, ["ц"] = true, ["у"] = true, ["к"] = true, ["е"] = true, ["н"] = true, ["г"] = true, ["ш"] = true, ["щ"] = true, ["з"] = true, ["х"] = true, ["ъ"] = true, ["ф"] = true, ["ы"] = true, ["в"] = true, ["а"] = true, ["п"] = true, ["р"] = true, ["о"] = true, ["л"] = true, ["д"] = true, ["ж"] = true, ["э"] = true, ["я"] = true, ["ч"] = true, ["с"] = true, ["м"] = true, ["и"] = true, ["т"] = true, ["ь"] = true, ["б"] = true, ["ю"] = true, ["_"] = true,
}

function loginPanel.draw()
    if not Y then Y = 0 end
    if not windowType then img = "login" but = "in" else img = "register" but = "bck" end
    dxDrawImage(x * 466, y * 161, x * 433, y * 446, "/images/" .. img .. ".png", 0, 0, 0, tocolor(255, 255, 255, 255)) --x * 401, y * 219
    
    if sButton ~= nil then
        if sButton then
            dxDrawRectangle(x * 488, x * (530 + Y * 2.1), x * 172, y * 47, tocolor(92, 42, 210, 255))
        else
            dxDrawRectangle(x * 704, x * (530 + Y * 2.1), x * 172, y * 47, tocolor(92, 42, 210, 255))
        end
    end
    
    dxDrawImage(x * 525, y * (531 + Y * 2.1), x * 97, y * 46, "/images/" .. but .. ".png", 0, 0, 0, tocolor(255, 255, 255, 255)) --x * 401, y * 219
    dxDrawImage(x * 703, y * (531 + Y * 2.1), x * 173, y * 46, "/images/reg.png", 0, 0, 0, tocolor(255, 255, 255, 255)) --x * 401, y * 219
    
    --dxDrawImage(sx / 2 - sx * 680 / 2, sy / 2 - sx * 680 / 2, sx * 680, sx * 680, "/images/login.png", 0, 0, 0, tocolor(255, 255, 255, 255))
    
    if not login then loginPanel[1] = "Никнейм (Никита_Иванов)" else loginPanel[1] = login end
    if not password then loginPanel[2] = "Пароль" else loginPanel[2] = password end
    if not password2 then loginPanel[3] = "Повтор пароля" else loginPanel[3] = password2 end
    
    dxDrawText(loginPanel[1], x * 487, y * (368 - Y * 2.5), x * 878, y * 415, tocolor(0, 0, 0, 255), 1.00, gothampro, "center", "center", false, false, false, false, false)
    dxDrawText(loginPanel[2], x * 487, y * (435 - Y * 2.5), x * 878, y * 472, tocolor(0, 0, 0, 255), 1.00, gothampro, "center", "center", false, false, false, false, false)
    
    if windowType then
        dxDrawText(loginPanel[3], x * 487, y * 480, x * 878, y * 527, tocolor(0, 0, 0, 255), 1.00, gothampro, "center", "center", false, false, false, false, false)
    end
    
    if not windowType then
        if sAutologin or Autologin then
            dxDrawCircle(x * 590, y * 492, x * 7.5, _, _, tocolor(64, 62, 176, 255))
        end
    end
end

function loginPanel.mouseMove(_, _, x, y)
    if not Autologin then
        if x >= sx * 0.4224 and x <= sx * 0.4385 and y >= sy * 0.6276 and y <= sy * 0.6549 then
            sAutologin = true
        else
            sAutologin = false
        end
    end
    
    if x >= sx * 0.3565 and x <= sx * 0.4817 and y >= sy * (0.6900 + Y / sx * 4) and y <= sy * (0.7474 + Y / sx * 4) then
        sButton = true
    else
        sButton = nil
    end
    
    if x >= sx * 0.5146 and x <= sx * 0.6398 and y >= sy * (0.6900 + Y / sx * 4) and y <= sy * (0.7474 + Y / sx * 4) then
        sButton = false
    end
end

function loginPanel.click(button, state, x, y)
    if state == "down" and button == "left" then
        if not windowType then
            if x >= sx * 0.3558 and x <= sx * 0.6606 and y >= sy * 0.4857 and y <= sy * 0.5391 then
                --Никнейм (Никита_Иванов)
                login = ""
                enterText = ""
                toggle = false
                loginPanel[1] = login
            end
            
            if x >= sx * 0.3558 and x <= sx * 0.6606 and y >= sy * 0.5508 and y <= sy * 0.6133 then
                --пароль
                toggle = true
                password = ""
                enterText = ""
                loginPanel[2] = password
            end
            
            if x >= sx * 0.4224 and x <= sx * 0.4385 and y >= sy * 0.6276 and y <= sy * 0.6549 then
                --автологин
                sAutologin = not sAutologin
                Autologin = not Autologin
            end
            
            if x >= sx * 0.5146 and x <= sx * 0.6398 and y >= sy * 0.6888 and y <= sy * 0.7474 then
                --регистрация
                Y = 8
                windowType = true
            end
            
            if x >= sx * 0.3565 and x <= sx * 0.4824 and y >= sy * 0.6888 and y <= sy * 0.7574 then
                --войти
                if login and passwordHash then
                    if not utf8.match(login, "^([А-Я][а-я]+_[А-Я][а-я]+)$") then
                        exports['game_hud_notify']:alert(source, 'Вы ввели не РП ник!', '', 'тут любое название ошибки можете написать')
                     --   outputChatBox("Вы ввели не РП ник!", 250, 150, 150)
                        return
                    end
                    
                    if utf8.len(login) >= 6 then
                        if type(passwordHash) ~= "function" and utf8.len(passwordHash) >= 8 then
                            triggerServerEvent("loginPanel.login", resourceRoot, login, passwordHash, Autologin)
                        else
                            exports['game_hud_notify']:alert(source, 'Пароль должен быть длиной', 'не менее 8 символов.', 'тут любое название ошибки можете написать')
                     --       outputChatBox("Пароль должен быть длиной не менее 8 символов.", 250, 150, 150)
                        end
                    else
                        exports['game_hud_notify']:alert(source, 'Никнейм должен быть длиной', 'не менее 6 символов.', 'тут любое название ошибки можете написать')
                    --    outputChatBox("Никнейм должен быть длиной не менее 6 символов.", 250, 150, 150)
                    end
                end
            end
        else
            if x >= sx * 0.3565 and x <= sx * 0.6398 and y >= sy * 0.4688 and y <= sy * 0.5286 then
                --Никнейм (Никита_Иванов)
                enterText = ""
                login = ""
                toggle = false
                loginPanel[1] = login
            end
            
            if x >= sx * 0.3565 and x <= sx * 0.642 and y >= sy * 0.5417 and y <= sy * 0.6016 then
                --пароль
                enterText = ""
                toggle = true
                password = ""
                loginPanel[2] = password
            end
            
            if x >= sx * 0.3551 and x <= sx * 0.6413 and y >= sy * 0.6172 and y <= sy * 0.6771 then
                --пароль2
                enterText = ""
                toggle = nil
                password2 = ""
                loginPanel[3] = password2
            end
            
            if x >= sx * 0.3565 and x <= sx * 0.4824 and y >= sy * 0.7122 and y <= sy * 0.7734 then
                --Назад
                windowType = false
                Y = 0
            end
            
            if x >= sx * 0.5007 and x <= sx * 0.6713 and y >= sy * 0.7214 and y <= sy * 0.776 then
                --Регистрация
                if utf8.len(login) >= 6 then
                    if not utf8.match(login, "^([А-Я][а-я]+_[А-Я][а-я]+)$") then
                        exports['game_hud_notify']:alert(source, 'Вы ввели NRP ник!', '', 'тут любое название ошибки можете написать')
                   --     outputChatBox("Вы ввели NRP ник!", 250, 150, 150)
                        return
                    end

                    if passwordHash and passwordHash2 and type(passwordHash) ~= "function" and utf8.len(passwordHash) >= 8 and type(passwordHash2) ~= "function" and utf8.len(passwordHash2) >= 8 then
                        if passwordHash == passwordHash2 then
                            Y = 0
                            windowType = false
                            triggerServerEvent("loginPanel.register", resourceRoot, login, passwordHash)
                        else
                            exports['game_hud_notify']:alert(source, 'Пароли не совпадают', '', 'тут любое название ошибки можете написать')
                       --     outputChatBox("Пароли не совпадают.", 250, 150, 150)
                        end
                    else
                        exports['game_hud_notify']:alert(source, 'Пароль должен быть длиной', 'не менее 8 символов.', 'тут любое название ошибки можете написать')
                     --   outputChatBox("Пароль должен быть длиной не менее 8 символов.", 250, 150, 150)
                    end
                else
                    exports['game_hud_notify']:alert(source, 'Никнейм должен быть длиной', 'не менее 6 символов.', 'тут любое название ошибки можете написать')
               --     outputChatBox("Никнейм должен быть длиной не менее 6 символов.", 250, 150, 150)
                end
            end
        end
    end
end

function loginPanel.key(button, press)
    if press then
        isFind = false
        
        if button == "backspace" then
            if utf8.len(enterText) > 0 then
                enterText = utf8.sub(enterText, 1, -2)
            end
        --elseif button == "space" then
        --    enterText = enterText .. ' '
        else
            for i = 1, #allowedLetters do
                if button == allowedLetters[i] then
                    isFind = true
                    break
                end
            end
            
            if isFind then return end
            
            if button then
            	if not allow[button] and not translate[button] and not utf8.match(button, "%d") then 
            		return 
            	end

            	if translate[button] then
            		button = translate[button]
            	end

				if getKeyState('lshift') then
					button = utf8.upper(button)
				end

                enterText = enterText .. button
            end
        end
        
        if toggle == false and loginPanel[1] ~= enterText then login = enterText return end
        
        if toggle and loginPanel[2] ~= enterText then
            passwordHash = enterText
            
            if button == "backspace" then password = utf8.sub(password, 1, -2) return end
            password = password .. "*"
        end
        
        if toggle == nil and loginPanel[3] ~= enterText then
            passwordHash2 = enterText
            
            if button == "backspace" then password2 = utf8.sub(password2, 1, -2) return end
            password2 = password2 .. "*"
        end
    end
end

function loginPanel.enter()
    showChat(true)
    setCameraTarget(localPlayer)
    
    showCursor(false)
    guiSetInputEnabled(false)
    guiSetInputMode("allow_binds")
    removeEventHandler("onClientRender", root, loginPanel.draw)
    removeEventHandler("onClientClick", root, loginPanel.click)
    removeEventHandler("onClientKey", root, loginPanel.key)
    removeEventHandler("onClientCursorMove", root, loginPanel.mouseMove)
end

function loginPanel.open()
    Camera.setMatrix(2142, -1904, 185, 2234, 1160, 82, 0, 120)
    
    showCursor(true)
    guiSetInputEnabled(true)
    guiSetInputMode("no_binds")
    addEventHandler("onClientRender", root, loginPanel.draw)
    addEventHandler("onClientClick", root, loginPanel.click)
    addEventHandler("onClientKey", root, loginPanel.key)
    addEventHandler("onClientCursorMove", root, loginPanel.mouseMove)
end
addEvent("loginPanel.open", true)
addEvent("loginPanel.enter", true)
addEventHandler("loginPanel.open", localPlayer, loginPanel.open)
addEventHandler("loginPanel.enter", localPlayer, loginPanel.enter)
