﻿loginPanel = {}

function loginPanel.login(login, password, autologin)
    if Account(login, password) then
        local account = Account(login, password)
        client:logIn(account, password)
        client:setData("rp.name", login)
        
        if autologin and not account:getData("autologin") then
            account:setData("autologin", true)
            account:setData("password", password)
        end
    else
        outputChatBox("Аккаунта не существует.", client, 250, 150, 150)
    end
end
addEvent("loginPanel.login", true)
addEventHandler("loginPanel.login", root, loginPanel.login)

addEventHandler("onPlayerLogin", root, function (_, account)
    source:spawn(-1, -834, 21.5, 0, 58)
    setCameraTarget(source, source)
    --showChat(source, true)

    triggerClientEvent(source, "loginPanel.enter", source)
    source:setData("accName", account.name)
end)

function loginPanel.autologin()
    local serial = source:getSerial()

    --showChat(source, false)
    
    if type(Account.getAllBySerial(serial)) == "table" and #Account.getAllBySerial(serial) > 0 then
        for i, k in ipairs(Account.getAllBySerial(serial)) do
            local login = k:getName()
            
            if Account(login) and Account(login):getData("password") then
                source:logIn(Account(login), Account(login):getData("password"))
                source:setData("rp.name", login)
                return
            end
        end
    end
    
    triggerClientEvent(source, "loginPanel.open", source)
end

addEventHandler("onPlayerJoin", root, loginPanel.autologin)

function loginPanel.register(login, password)
    if not Account(login) then
        local account = Account.add(login, password)
        if account then
            outputChatBox("Вы успешно зарегистрировались.", client, 150, 250, 150)
        else
            outputChatBox("Произошла ошибка добавления аккаунта в систему.", client, 250, 150, 150)
            outputChatBox("Возможно, ресурс не добавлен в ACL.", client, 250, 150, 150)
        end
    else
        outputChatBox("Это имя занято.", client, 250, 150, 150)
    end
end
addEvent("loginPanel.register", true)
addEventHandler("loginPanel.register", root, loginPanel.register)
