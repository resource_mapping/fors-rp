﻿addEventHandler("onResourceStart", resourceRoot, function()
    db = dbConnect("sqlite", "database.db")
	dbExec(db, "CREATE TABLE IF NOT EXISTS ListBonus (account, day, time)")
end)

function regBonus(player)
	local account = getAccountName(getPlayerAccount(player))
	local result = dbPoll(dbQuery(db, "SELECT * FROM ListBonus"),-1)
	for i, data in ipairs (result) do 
        if data["account"] == account then
			triggerClientEvent(player, "DayLogin", player, result)
			return
		end
    end
	
	local day = 1
	local time = 0
	dbExec(db,"INSERT INTO ListBonus VALUES(?, ?, ?)", account, day, time)
	
	local result = dbPoll(dbQuery(db, "SELECT * FROM ListBonus"),-1)
	triggerClientEvent(player, "DayLogin", player, result)
end
addEvent("regBonus", true)
addEventHandler("regBonus", getRootElement(), regBonus)

function giveBonus(player, day)
	local account = getAccountName(getPlayerAccount(player))
	local time = 1440
	
	local moneyBonus = {5000,12000,20000,35000,50000,80000,110000} -- Бонус за каждый день. От 1 до 7 дня.
	
	if day == 1 then
		moneyBonus = moneyBonus[1]
	elseif day == 2 then
		moneyBonus = moneyBonus[2]
	elseif day == 3 then
		moneyBonus = moneyBonus[3]
	elseif day == 4 then
		moneyBonus = moneyBonus[4]
	elseif day == 5 then
		moneyBonus = moneyBonus[5]
	elseif day == 6 then
		moneyBonus = moneyBonus[6]
	elseif day == 7 then
		moneyBonus = moneyBonus[7]
	end
	givePlayerMoney(player,moneyBonus)
	outputChatBox("Вы успешно получили #00FF00ежедневный бонус #FFFFFFв размере #00FF00"..moneyBonus.." #FFFFFFруб.", player, 255, 255, 255, true)
	
	day = day + 1
	if day > 7 then
		day = 1
	end
	dbExec(db, "UPDATE ListBonus SET day = ?, time = ? WHERE account = ?", day, time, account)
end
addEvent("giveBonus", true)
addEventHandler("giveBonus", getRootElement(), giveBonus)

function timeDay()
    local result = dbPoll(dbQuery(db, "SELECT * FROM ListBonus"),-1)
	for i, data in ipairs (result) do
	    local account, day, time = data.account, data.day, data.time
		if time > 0 then
		    dbExec(db, "UPDATE ListBonus SET time = ? WHERE account = ? AND day = ?", time - 1, account, day)
			outputChatBox(tostring(time))
		end
	end
end
setTimer(timeDay, 60000, 0)

----------------------------------------------------------

for k,v in pairs(getElementsByType("player")) do
	if not getElementData(v, "login") then
		setElementData(v, "login", getAccountName(getPlayerAccount(v)))
	end
end

function onPlayerLogin()
	setElementData(source, "login", getAccountName(getPlayerAccount(source)))
	
	regBonus(source)
end 
addEventHandler("onPlayerLogin", root, onPlayerLogin)

function onPlayerQuit()
	if getElementData(source, "login") == true then
        setElementData(source, "login", nil)
	end
end 
addEventHandler("onPlayerQuit", root, onPlayerQuit)