﻿local infoBonus

function centerWindow ( center_window )
	local sx, sy = guiGetScreenSize ( )
	local windowW, windowH = guiGetSize ( center_window, false )
	local x, y = ( sx - windowW ) / 2, ( sy - windowH ) / 2
	guiSetPosition ( center_window, x, y, false )
end

local wndBonus = guiCreateStaticImage(0, 0, 0.5, 0.5,"files/day_1.png",true)
centerWindow(wndBonus)
guiSetVisible(wndBonus,false)

buttonBonus_s = guiCreateStaticImage(0.03, 0.82, 0.94, 0.14, "files/button_standard.png", true, wndBonus)
buttonBonus_o = guiCreateStaticImage(0.03, 0.82, 0.94, 0.14, "files/button_mouse.png", true, wndBonus)
buttonBonus = guiCreateLabel(0.03, 0.82, 0.94, 0.14, "Получить", true, wndBonus)
guiSetFont(buttonBonus, "default-bold-small")
guiLabelSetColor(buttonBonus, 255, 255, 255)
guiLabelSetVerticalAlign(buttonBonus, "center")
guiLabelSetHorizontalAlign(buttonBonus, "center")
guiSetAlpha(buttonBonus,0)
guiSetEnabled(buttonBonus_s,false)
guiSetVisible(buttonBonus_o,false)

addEventHandler( "onClientMouseLeave", root, function()
    if source == buttonBonus then
		guiSetVisible(buttonBonus_s,true)
		guiSetVisible(buttonBonus_o,false)
	end
end)

addEventHandler( "onClientMouseEnter", root, function()
    if source == buttonBonus then
		guiSetVisible(buttonBonus_s,false)
		guiSetVisible(buttonBonus_o,true)
	end
end)

function DayLogin(result)
	infoBonus = result
	for i, data in ipairs (infoBonus) do
		local account, day, time = data.account, data.day, data.time
		if account == getElementData(localPlayer, "login") then
			if tonumber(time) <= tonumber(0) then
				guiStaticImageLoadImage(wndBonus,"files/day_"..day..".png")
				
				guiStaticImageLoadImage(buttonBonus_s,"files/button_standard.png")
				guiStaticImageLoadImage(buttonBonus_o,"files/button_mouse.png")
				guiSetVisible(wndBonus,true)
				showCursor(true)
			else
				outputChatBox("Бонус не доступен. #00FF0024 ч. #FFFFFFеще не прошло.", 255, 255, 255, true)
			end
		end
	end
end
addEvent("DayLogin", true)
addEventHandler("DayLogin", getRootElement(), DayLogin)

addEventHandler("onClientGUIClick", root, function ()
    if (source == buttonBonus) then
		for i, data in ipairs (infoBonus) do
			local account, day, time = data.account, data.day, data.time
			if account == getElementData(localPlayer, "login") then
				triggerServerEvent("giveBonus", localPlayer, localPlayer, day)
				guiSetVisible(wndBonus,false)
				showCursor(false)
			end
		end
    end
end)