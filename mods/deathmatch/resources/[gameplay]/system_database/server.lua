local projectName = "Liberty City"
local version = "V1.0"
local connection = nil

addEventHandler("onResourceStart",resourceRoot,function(resource)
	connection = dbConnect("sqlite","database.db")
	if connection then
		outputDebugString("База данных мода "..projectName.." запущена! Версия базы данных: "..version)
		setGameType(projectName.."  "..version)
		return true
	else
		outputDebugString(getResourceName(resource) .. " : Не удалось")
		return false
	end
end)

function _Query( ... )
	if connection then
		local query = dbQuery(connection, ... )
		local result = dbPoll(query,-1)
		return result
	else
		return false
	end
end

function _QuerySingle(str,...)
	if connection then
		local result = _Query(str,...)
		if type(result) == 'table' then
			return result[1]
		end
	else
		return false
	end
end

function _Exec(str,...)
	if connection then
		local query = dbExec(connection,str,...)
		return query
	else
		return false
	end
end