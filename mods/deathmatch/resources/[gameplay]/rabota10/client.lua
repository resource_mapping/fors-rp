--------------------------------------- Configuration ---------------------------------------
jobs = {
	{name = "Доставка Мебели",reward = 8000,truck = 498,trailer = 10,x = 2104.6611328125,y = -2086.919921875,z = 13.546875}
}
---------------------------------------------------------------------------------------------

local root = getRootElement()
local localPlayer = getLocalPlayer()
player = getLocalPlayer()
waypointBlip = waypointBlip

marker = createMarker(-2767.336425,-911.101,22.2,"cylinder",1,255,0,40,170)
truckingBlip = createBlip(148.261245,-1020.708,20.763,51,1)
window = guiCreateWindow(435,218,528,462,"Работа дальнобойщика",false)
guiWindowSetSizable(window,false)
gridlist = guiCreateGridList(9,24,509,428,false,window)
column1 = guiGridListAddColumn(gridlist,"Job Name",0.6)
column2 = guiGridListAddColumn(gridlist,"Reward",0.3)

function startJob()
	local selectedRow,selectedCol = guiGridListGetSelectedItem(gridlist)
    local name = guiGridListGetItemText(gridlist,selectedRow,selectedCol)
	for i,job in pairs(jobs) do
		if job.name == name then
			triggerServerEvent("createJobVeh",root,player,job.name,job.truck,job.trailer,job.x,job.y,job.z,job.reward)
		end
	end
end

addEventHandler("onClientResourceStart",root,function()
	guiSetVisible(window,false)
	addEventHandler("onClientGUIDoubleClick",gridlist,startJob,false)
end)

addEventHandler("onClientMarkerHit",marker,function(element,dimension)
	if (getElementType(element) == "player") then
		if (element == localPlayer) then
			guiSetVisible(window,true)
			showCursor(true)
			outputChatBox("Нажмите BackSpace для выхода или двойной щелчок на работу, чтобы начать!",0,255,255)
			player = element
			for i,j in pairs(jobs) do
				local row = guiGridListAddRow(gridlist)
				guiGridListSetItemText(gridlist,row,column1,j.name,false,false)
				guiGridListSetItemText(gridlist,row,column2,"$"..tostring(j.reward),false,false)
			end
			bindKey("backspace","down",killWindow)
		end
	end
end)

addEventHandler("onClientMarkerLeave",marker,function(element,dimension)
	if (getElementType(element) == "player") then
		if (element == localPlayer) then 
			guiSetVisible(window,false)
			showCursor(false)
			guiGridListClear(gridlist)
			unbindKey("backspace","down",killWindow)
		end
	end
end)

addEvent("addBlip",true)
addEventHandler("addBlip",root,function(x,y,z)
	waypointBlip = createBlip(x,y,z,41,1)
end)

addEvent("killBlip",true)
addEventHandler("killBlip",root,function()
	destroyElement(waypointBlip)
end)

function killWindow()
	guiSetVisible(window,false)
	showCursor(false)
end