local root = getRootElement()

addEvent("createJobVeh",true)
addEventHandler("createJobVeh",root,function(player,job,vehID,trailerID,x,y,z,reward)
	local truck = createVehicle(vehID,-2734.618896,-905.965209,22.26,0,0,90)
	local trailer = createVehicle(trailerID,0,0,0,0,0,90)
	triggerClientEvent(player,"addBlip",player,x,y,z)
	local marker = createMarker(x,y,z-148.261245,-1020.708,20.763,"cylinder",3,255,0,40,170)
	outputChatBox("Работа началась, езжай на красный маркер, чтобы закончить маршрут!",player,0,255,255)
	attachTrailerToVehicle(truck,trailer)
	warpPedIntoVehicle(player,truck)
	setVehicleLocked(truck,true)
	setJob(player,tostring(job))
	addEventHandler("onVehicleExit",root,function(player)
		if source == truck then
			cancelJob(player,blip,marker,truck,trailer)
		end
	end)
	addEventHandler("onTrailerDetach",root,function()
		if source == trailer then
			cancelJob(player,blip,marker,truck,trailer)
		end
	end)
	addEventHandler("onVehicleExplode",root,function()
		if source == truck or source == trailer then
			cancelJob(player,blip,marker,truck,trailer)
		end
	end)
	addEventHandler("onMarkerHit",marker,function(element,dimension)
		if element == truck or element == trailer then
			finishJob(getVehicleOccupant(truck),reward,truck,trailer,blip,marker)
		end
	end)
end)

function setJob(player,job)
	setElementData(player,"job",job)
end

function cancelJob(player,blip,marker,truck,trailer)
	setElementData(player,"job",false)
	triggerClientEvent(player,"killBlip",player)
	destroyElement(marker)
	destroyElement(truck)
	destroyElement(trailer)
	outputChatBox("Работа провалена!",player,255,0,0)
end

function finishJob(player,reward,truck,trailer,blip,marker)
	setElementData(player,"job",false)
	triggerClientEvent(player,"killBlip",player)
	destroyElement(marker)
	destroyElement(truck)
	destroyElement(trailer)
	outputChatBox("Работа была успешно завершена!",player,0,255,0)
	exports [ "urovni" ]:givePlayerXP (player, 20)
	setPlayerMoney(player,getPlayerMoney(player)+reward)
end