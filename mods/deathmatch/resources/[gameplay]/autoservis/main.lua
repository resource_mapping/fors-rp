-----------------------------------------------------------------------------------------------------------------------
-- This script allows to create a special markers on the map to repair vehicles.
--
-- You can manually add positions of markers in garages.xml file.
-- Also you can use exported function createGarage() to create marker in the
-- current position of player.
--
-- For every registered garage there is an icon on the map. You can show or hide icons
-- with exported function showGarageBlips() or set up "show_blips" flag in settings.
--
-- This script contains only four garages from single player located in the Los Santos.
-- They are linked with id field. This link allows you to open or close gates of garages
-- with exported function openGarages().
--
-- But you can add as many garages as you like and anywhere you like.
-- It's not necessary to link garage with id.
--
-- When vehicle hits the marker script trigger autorepairMarkerHit event.
-- In handler you can check if this vehicle need to be repaired and f.e.
-- does a driver of vehicle have enough money for repair. So you can
-- cancel repair operation with cancelEvent().
--
-- If event returns true then script fixes vehicle and trigger vehicleRepaired event.
-- In handler you can add custom operations like withdrawing money for repair f.e.
--
-- This code is in public domain. You can use it as you like without any limits.
--
-- Hitori (2012)
-----------------------------------------------------------------------------------------------------------------------

MARKER_SIZE = 2.0
GARAGE_ICON = 63

g_garages = {}  -- here we store all garages

-----------------------------------------------------------------------------------------------------------------------

function resourceStart()
    local ver = getResourceInfo(getThisResource(), "version")
    if ver then
        outputServerLog("AR: version "..ver)
    end

    local xml_doc = xmlLoadFile("garages.xml")
    if xml_doc then
        outputServerLog("AR: garages.xml was found")

        local children = xmlNodeGetChildren(xml_doc)
        if children then
            outputServerLog("AR: contains "..#children.." garages")

            for i, node in ipairs(children) do
                local x = tonumber(xmlNodeGetAttribute(node, "x"))
                local y = tonumber(xmlNodeGetAttribute(node, "y"))
                local z = tonumber(xmlNodeGetAttribute(node, "z"))
                local id= tonumber(xmlNodeGetAttribute(node, "id"))
                local name= xmlNodeGetAttribute(node, "name")

                if createGarageObj(id, name, x, y, z) == false then
                    outputServerLog("AR: ERROR: cannot create a marker element - quit")
                    cancelEvent()
                    return
                end
            end
        else
            outputServerLog("AR: contains no garages")
        end
        xmlUnloadFile(xml_doc)

        if addEvent("autorepairMarkerHit", true) then
            outputServerLog("AR: autorepairMarkerHit event was registered")
        else
            outputServerLog("AR: ERROR: cannot register autorepairMarkerHit event")
        end

        if addEvent("vehicleRepaired", true) then
            outputServerLog("AR: vehicleRepaired event was registered")
        else
            outputServerLog("AR: ERROR: cannot register vehicleRepaired event")
        end

        -- read open flag from settings and open or close all registered garages
        local flag = (get("open_garages") == "true")
        openGarages(flag)

        -- read show flag from settings for garage icons
        flag = (get("show_blips") == "true")
        showGarageBlips(flag)
    else
        outputServerLog("AR: ERROR: garages.xml was not found")
    end
end
addEventHandler("onResourceStart", resourceRoot, resourceStart)
-----------------------------------------------------------------------------------------------------------------------

function openGarages(is_open)

    if is_open then
        outputServerLog("AR: open garages")
    else
        outputServerLog("AR: close garages")
    end

    for marker, obj in pairs(g_garages) do
        local garage_id = obj.id
        if garage_id and garage_id > -1 then
            setGarageOpen(garage_id, is_open)
        end
    end
end
-----------------------------------------------------------------------------------------------------------------------

function showGarageBlips(show)

    if show then
        outputServerLog("AR: show blips of garages")
    else
        outputServerLog("AR: hide blips of garages")
    end

    for marker, obj in pairs(g_garages) do
        if show then
            if obj.blip == nil then
                obj.blip = createBlip(obj.x, obj.y, obj.z, GARAGE_ICON)
            end
        else
            if obj.blip then
                destroyElement(obj.blip)
                obj.blip = nil
            end
        end
    end
end
-----------------------------------------------------------------------------------------------------------------------

function onMarkerHit(hitElement, matchingDimension)
    if not matchingDimension then
        return
    end

    if getElementType(hitElement) == "vehicle" then
        local g_obj = g_garages[source]
        if g_obj then
            outputServerLog("AR: some vehicle hits garage marker")

            -- Trigger event for all resources. Vehicle element is the source of event.
            if triggerEvent("autorepairMarkerHit", hitElement, g_obj.id) then

                -- Now we can fix vehicle.
                if fixVehicle(hitElement) then
                    outputServerLog("AR: vehicle was fixed")

                    local driver = getVehicleOccupant(hitElement)   -- get the player sitting in seat 0
                    if driver then
                        --playSoundFrontEnd(driver, 46)   -- repair sound. Crapy sound..
                        outputServerLog("AR: try to play custom sound")
                        triggerClientEvent(driver, "playRepairSound", driver)
                    end

                    -- Trigger event for all resources. Vehicle element is the source of event.
                    triggerEvent("vehicleRepaired", hitElement, g_obj.id)
                else
                    outputServerLog("AR: ERROR: cannot fix vehicle")
                end
            else
                outputServerLog("AR: repair was cancelled by client")
            end
        end
    end
end
-----------------------------------------------------------------------------------------------------------------------

function createGarageObj(id, name, x, y, z)

    local obj = {}
    obj.x = x
    obj.y = y
    obj.z = z
    obj.id= id
    obj.name = name

    local marker = createMarker(x, y, z, "cylinder", MARKER_SIZE, 255, 0, 0)
    if marker then
        g_garages[marker] = obj
        addEventHandler("onMarkerHit", marker, onMarkerHit)
        return obj
    else
        outputServerLog("AR: ERROR: cannot create a marker element")
        return false
    end
end
-----------------------------------------------------------------------------------------------------------------------

function createGarage(client, id, name)
    outputServerLog("AR: createGarage() enter, id: "..id..", name: "..name)

    local x, y, z = getElementPosition(client)
    z = z - 1
    createGarageObj(id, name, x, y, z)

    local xml_doc = xmlLoadFile("garages.xml")
    if xml_doc then
        outputServerLog("AR: garages.xml was found")

        local node = xmlCreateChild(xml_doc, "g")
        xmlNodeSetAttribute(node, "x", x)
        xmlNodeSetAttribute(node, "y", y)
        xmlNodeSetAttribute(node, "z", z)
        xmlNodeSetAttribute(node, "id",id)
        xmlNodeSetAttribute(node, "name", name)

        xmlSaveFile(xml_doc)
        outputServerLog("AR: new garage was added")
    else
        outputServerLog("AR: ERROR: garages.xml was not found")
    end
end
-----------------------------------------------------------------------------------------------------------------------
