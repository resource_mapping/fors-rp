exports [ "scoreboard" ]:addScoreboardColumn ( "Уровень", getRootElement() )   
function updatePlayersMoney () 
    for index, player in ipairs ( getElementsByType "player" ) do 
		setElementData ( player, "Уровень", getElementData(player, "player:level") ) 
    end 
end 
setTimer ( updatePlayersMoney, 100, 0 ) 

function givePlayerXP (player, quant)
	local xp_curr = getElementData ( player, "player:xp" ) or 0
	local level_curr = getElementData ( player, "player:level" ) or 0
	setElementData ( player, "player:xp", xp_curr+quant )
	if xp_curr+quant >= getLevelXP(level_curr+1) then
		setElementData (player, "player:xp", 0)
		setElementData (player, "player:level", level_curr+1)
		outputChatBox ( "Вы получили уровень "..(level_curr+1).."!", player, 100, 255, 100 )
	else
		triggerClientEvent (player,"trailer:playerGivenXP", player, quant)
	end
end


function giveXP ( player, cmd, xp)
	givePlayerXP (player, xp)
end
addCommandHandler ("xp", giveXP)

local saveableData = {
	["player:xp"] = true,
	["player:level"] = true,
}

addEventHandler("onPlayerLogin", root,
function(_,acc)
	setElementData ( source, "player:xp", getAccountData ( acc, "player:xp" ) or 0 )
	setElementData ( source, "player:level", getAccountData ( acc, "player:level" ) or 0 )
end)

function outputChange(dataName,oldValue)
	if getElementType(source) == "player" then
		if saveableData[dataName] then
			setAccountData ( getPlayerAccount ( source ), dataName, getElementData ( source, dataName ) )
		end
	end
end
addEventHandler("onElementDataChange",getRootElement(),outputChange)
