startXP = 200 -- Сколько опыта нужно на первом уровне
xpPerLevel = 200 -- На сколько будет увеличиваться опыт на каждом уровне
maxLevel = 30 -- Максимальное количество уровней

function getLevelXP (level)
	return level*xpPerLevel
end