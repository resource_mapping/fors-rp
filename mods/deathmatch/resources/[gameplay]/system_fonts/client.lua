local FONTS	= {}
local S_FONTS_RESOURCE_PATH = "Resources/Fonts/"

addCommandHandler("fonts", function()
	iprint(FONTS) 
end)

function clearFont()
	FONTS = nil
	iprint(FONTS)
end
addEventHandler("onClientResourceStop", resourceRoot, clearFont)

function createFont(font_name, font_size, font_quality)
	local font_size = font_size or 1
	if not FONTS[font_name] then FONTS[font_name] = {} end
	local font_conf = FONTS[font_name]
	font_conf[font_size] = font_conf[font_size] or dxCreateFont(S_FONTS_RESOURCE_PATH .. font_name, font_size, false, font_quality or "antialiased")
	
	return font_conf[font_size]
end