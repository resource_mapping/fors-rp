﻿--------------------------------------------[Устройство]

local alpha = 0

local r, g, b = 255, 230, 0

local size = 1

local typem = "cylinder"

local posx, posy, posz =  -2143.671386, -463.9, 22.1



local entradaB = createMarker (posx, posy, posz, typem, size, r, g, b, alpha)



addEventHandler( "onClientRender", root, function (  )

       local x, y, z = getElementPosition( entradaB )

       local Mx, My, Mz = getCameraMatrix(   )

        if ( getDistanceBetweenPoints3D( x, y, z, Mx, My, Mz ) <= 20 ) then

           local WorldPositionX, WorldPositionY = getScreenFromWorldPosition( x, y, z +1, 0.07 )

            if ( WorldPositionX and WorldPositionY ) then

			    dxDrawText("Работа лесоруба", WorldPositionX - 1, WorldPositionY + 1, WorldPositionX - 1, WorldPositionY + 1, tocolor(0, 0, 0, 255), 1.02, "default-bold", "center", "center", false, false, false, false, false)

			    dxDrawText("Работа лесоруба", WorldPositionX - 1, WorldPositionY + 1, WorldPositionX - 1, WorldPositionY + 1, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, false, false, false)

            end

      end

end 

)

---------------------------------------------

---------------------------------------------[Загрузка]

local alpha = 0

local r, g, b = 255, 230, 0

local size = 1

local typem = "cylinder"

local posx, posy, posz =  -1972.3000488281,-2434.1999511719,29.6



local entradaB = createMarker (posx, posy, posz, typem, size, r, g, b, alpha)



addEventHandler( "onClientRender", root, function (  )

       local x, y, z = getElementPosition( entradaB )

       local Mx, My, Mz = getCameraMatrix(   )

        if ( getDistanceBetweenPoints3D( x, y, z, Mx, My, Mz ) <= 10 ) then

           local WorldPositionX, WorldPositionY = getScreenFromWorldPosition( x, y, z +1, 0.07 )

            if ( WorldPositionX and WorldPositionY ) then

			    dxDrawText("Загрузка дров", WorldPositionX - 1, WorldPositionY + 1, WorldPositionX - 1, WorldPositionY + 1, tocolor(0, 0, 0, 255), 1.02, "default-bold", "center", "center", false, false, false, false, false)

			    dxDrawText("Загрузка дров", WorldPositionX - 1, WorldPositionY + 1, WorldPositionX - 1, WorldPositionY + 1, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, false, false, false)

            end

      end

end 

)

---------------------------------------------

---------------------------------------------[Разгрузка]

local alpha = 0

local r, g, b = 255, 230, 0

local size = 1

local typem = "cylinder"

local posx, posy, posz =  -1971.1999511719,-2368.5,29.6



local entradaB = createMarker (posx, posy, posz, typem, size, r, g, b, alpha)



addEventHandler( "onClientRender", root, function (  )

       local x, y, z = getElementPosition( entradaB )

       local Mx, My, Mz = getCameraMatrix(   )

        if ( getDistanceBetweenPoints3D( x, y, z, Mx, My, Mz ) <= 10 ) then

           local WorldPositionX, WorldPositionY = getScreenFromWorldPosition( x, y, z +1, 0.07 )

            if ( WorldPositionX and WorldPositionY ) then

			    dxDrawText("Разгрузка дров", WorldPositionX - 1, WorldPositionY + 1, WorldPositionX - 1, WorldPositionY + 1, tocolor(0, 0, 0, 255), 1.02, "default-bold", "center", "center", false, false, false, false, false)

			    dxDrawText("Разгрузка дров", WorldPositionX - 1, WorldPositionY + 1, WorldPositionX - 1, WorldPositionY + 1, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, false, false, false)

            end

      end

end 

)

---------------------------------------------

GRUZ = {

    

button = {},

    

window = {},

    

label = {}

}



addEventHandler("onClientResourceStart", resourceRoot, function(res)

    if res == getThisResource() then



	---------------------------------------------------------------------------------

        GRUZ.window[1] = guiCreateStaticImage(452, 285, 463, 262, "gui/gui.png", false)


        ---------------------------------------------------------------------------------

	label1 = guiCreateLabel(111, 35, 265, 24, "Добро пожаловать на работу лесоруба.", false, GRUZ.window[1] )

	guiSetFont(label1, "default-bold-small")

	guiLabelSetColor(label1, 254, 254, 254)

	label2 = guiCreateLabel(23, 73, 265, 24, "1. Устроиться на работу.", false, GRUZ.window[1] )


        guiLabelSetColor(label2, 254, 254, 254)


        label3 = guiCreateLabel(23, 97, 265, 24, "2. Взять дрова.", false, GRUZ.window[1] )


        guiLabelSetColor(label3, 254, 254, 254)


        label4 = guiCreateLabel(23, 121, 265, 24, "3. Отнести дрова на переработку.", false, GRUZ.window[1] )


        guiLabelSetColor(label4, 254, 254, 254)


        label5 = guiCreateLabel(23, 145, 265, 24, "4. Закончить рабочий день.", false, GRUZ.window[1] )


        guiLabelSetColor(label5, 254, 254, 254)

	img = guiCreateStaticImage(240, 40, 230, 180, "gui/tree.png", false, GRUZ.window[1] ) 

        ---------------------------------------------------------------------------------

        GRUZ.button[1] = guiCreateButton(48, 215, 120, 30, "Устроиться", false, GRUZ.window[1] )                

        GRUZ.button[2] = guiCreateStaticImage(294, 215, 120, 30, "gui/exit.png", false, GRUZ.window[1] )   

        guiSetVisible(GRUZ.window[1], false)

        showCursor(false)

	---------------------------------------------------------------------------------

    end

end

)



addEventHandler("onClientResourceStop", resourceRoot, function(res)

    if res == getThisResource() then



        if isElement(GRUZ.window[1]) then destroyElement(GRUZ.window[1]) end

        if isElement(GRUZ.label[1]) then destroyElement(GRUZ.label[1]) end

        if isElement(GRUZ.label[2]) then destroyElement(GRUZ.label[2]) end

        if isElement(GRUZ.button[1]) then destroyElement(GRUZ.button[1]) end

        if isElement(GRUZ.button[2]) then destroyElement(GRUZ.button[2]) end

        GRUZ.window[1] = nil

        GRUZ.label[1] = nil

        GRUZ.label[2] = nil

        GRUZ.button[1] = nil

        GRUZ.button[2] = nil

    end

end

)



function showGruzMenu(player)

    if not guiGetVisible(GRUZ.window[1]) then

        guiSetVisible(GRUZ.window[1], true)

        showCursor(true)

    else

        guiSetVisible(GRUZ.window[1], false)

        showCursor(false)

    end

end

addEvent("JobGruzMenu", true)

addEventHandler("JobGruzMenu", getRootElement(), showGruzMenu)



function onGuiClickPanel (button, state, absoluteX, absoluteY)

    if (source == (GRUZ.button[1])) then

        if not getElementData(localPlayer, "JobGruz") then

            triggerServerEvent("StartJobGruz", getRootElement(), localPlayer, true)

		    guiSetVisible(GRUZ.window[1], false)

            showCursor(false)

        else

            triggerServerEvent("StartJobGruz", getRootElement(), localPlayer, false)

		    guiSetVisible(GRUZ.window[1], false)

            showCursor(false)

        end

    elseif (source == (GRUZ.button[2])) then

        guiSetVisible(GRUZ.window[1], false)

        showCursor(false)

    end

end

addEventHandler ("onClientGUIClick", getRootElement(), onGuiClickPanel)



setTimer(function()

    if getElementData(localPlayer, "JobGruz") then

        GRUZ.button[1] = guiCreateStaticImage(48, 215, 120, 30, "gui/logout.png", false, GRUZ.window[1] )

    else

        GRUZ.button[1] = guiCreateStaticImage(48, 215, 120, 30, "gui/join.png", false, GRUZ.window[1] )

    end

end, 200, 0)



function checktime (player)

    if getElementData(player, "JobGruz") then

        if getControlState("jump") or getControlState("fire") then

            triggerServerEvent("JobGruzcheckAnim", getRootElement(), localPlayer)

        end

    end

end

-- or getControlState("sprint")

function JobGruzCallCheck(player, state)

    if state then

        setPedAnimation(player, "CARRY", "liftup", -1, false, true, false)

        setTimer(setPedAnimation, 1000, 1, player, "CARRY", "crry_prtial", 1, true, true, false)

        if isTimer(timer) then killTimer(timer) end

        timer = setTimer(checktime, 100, 0, player)

    else

        if getAttachedElements(player) then

            setPedAnimation(player, "CARRY", "putdwn", -1, false, true, false)

            setTimer(setPedAnimation, 1500, 1, player, false)

        end

        if isTimer(timer) then killTimer(timer) end

    end

end

addEvent("JobGruzCallCheck", true)

addEventHandler("JobGruzCallCheck", getRootElement(), JobGruzCallCheck)





function beretChtoto(player)

local x,y,z = getElementPosition(player)

local sound3 = playSound3D("beretchtoto.mp3", x,y,z)	

setSoundMaxDistance(sound3, 10)

setSoundVolume(sound3, 0.5)

end

addEvent("beretMusicGUI", true)

addEventHandler("beretMusicGUI", getRootElement(), beretChtoto)



function padaetkaropka(player)

local x,y,z = getElementPosition(player)

local sound4 = playSound3D("padaetkaropka.mp3", x,y,z)	

setSoundMaxDistance(sound4, 10)

setSoundVolume(sound4, 0.5)

end

addEvent("padaetMusicGUI", true)

addEventHandler("padaetMusicGUI", getRootElement(), padaetkaropka)