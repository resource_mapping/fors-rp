﻿blip1 = createBlip(-1945.3,-2423.8,30.6,11,2,0,0,0,0,0,200)

local Money = 50 

local GRMark = {}
local GRMark2 = {}
local GRMark3 = {}
local GRpos = {
{-1945.3,-2423.8,30.6},
{-1972.3000488281,-2434.1999511719,29.75000038147},
{-1971.1999511719,-2368.5,29.75000038147}
}
local Gruz = {}
addEventHandler ( "onResourceStart", getResourceRootElement(), function(res)
if res == getThisResource() then
    GRMark[1] = createMarker( GRpos[1][1], GRpos[1][2], GRpos[1][3], "cylinder", 1.5, 0, 0, 0, 0 )
		createPickup ( -1945.3,-2423.8,30.6, 3, 1275, 2000 )
end
end)

addEventHandler ( "onResourceStop", getResourceRootElement(), function(res)
    if res == getThisResource() then
        if isElement(GRMark[1]) then destroyElement(GRMark[1]) end
        GRMark[1] = nil
        for k, p in ipairs(getElementsByType("player")) do
            if isElement(GRMark2[p]) then destroyElement(GRMark2[p]) end
            if isElement(GRMark3[p]) then destroyElement(GRMark3[p]) end
            if isElement(Gruz[p]) then destroyElement(Gruz[p]) end
            GRMark2[p] = nil
            GRMark3[p] = nil
            Gruz[p] = nil
        end
    end
end)

function markerGruz(marker, player)
    if marker == GRMark[1] then
        if not isPedInVehicle(source) then
		if Gruz[source] then
	    outputChatBox ( "#FF0000[Ошибка]: #FFFFFFСначало отнеси дрова.",source,255, 0, 0, true )
		else
		    triggerClientEvent (source, "JobGruzMenu", getRootElement(), source )
        end
        end
    elseif marker == GRMark2[source] then
	    if not isPedInVehicle(source) then
        if Gruz[source] then
            return false
        end
		GruzYshik()
        triggerClientEvent("JobGruzCallCheck", getRootElement(), source, true)
		trueControl()
		triggerClientEvent("beretMusicGUI", getRootElement(), source)
		end
    elseif marker == GRMark3[source] then
	    if not isPedInVehicle(source) then
            if Gruz[source] then
                outputChatBox ( "#9ACD32[Работа]: #FFFFFFВы успешно отнесли дрова. #9ACD32+".. Money .."$",source, 255, 0, 0, true )
		    givePlayerMoney(source,Money)
	            detachElementFromBone(Gruz[source])
	            destroyElement(Gruz[source])
	            Gruz[source] = false
                triggerClientEvent("JobGruzCallCheck", getRootElement(), source, false)
				falseControl()
				triggerClientEvent("padaetMusicGUI", getRootElement(), source)
                return false                
            else
			end
		end
    end
end
addEventHandler("onPlayerMarkerHit",getRootElement(),markerGruz)


function WorkGruz(player, state)
    if state then
        setElementData ( player, "Работа", "Лесопил")
        setElementData(player, "JobGruz", true)
        if isElement(GRMark2[player]) then destroyElement(GRMark2[player]) end
        GRMark2[player] = createMarker( GRpos[2][1], GRpos[2][2], GRpos[2][3], "cylinder", 1, 255, 0, 0, 100, player )
        if isElement(GRMark3[player]) then destroyElement(GRMark3[player]) end
        GRMark3[player] = createMarker( GRpos[3][1], GRpos[3][2], GRpos[3][3], "cylinder", 1, 255, 0, 0, 100, player )
	outputChatBox ( "#9ACD32[Работа]: #FFFFFFВы успешно устроились на работу лесоруба.",player,255, 0, 0, true )
	outputChatBox ( "#DAA520[Информация]: #FFFFFFВаша задача переносить дрова на переработку.",player,255, 0, 0, true )
		setAccountData( getPlayerAccount( player ), "Job.Gruzchik.Skin", toJSON( { skin = getElementModel( player ), bState = true } ) )
	    setElementModel( player, 260 )
    else
        setElementData(player, "JobGruz", false)
        setElementData(player, "Работа","Безработный")
        if isElement(GRMark2[player]) then destroyElement(GRMark2[player]) end
        if isElement(GRMark3[player]) then destroyElement(GRMark3[player]) end
        derevo = getElementData(player, "derevo")
        if not derevo then derevo = 0 end
        outputChatBox ( "#9ACD32[Работа]: #FFFFFFВы успешно закончили работу.",player,255, 0, 0, true )
        setElementData(player,"derevo", 0)
        --triggerClientEvent("JobGruzCallCheck", getRootElement(), player, false)
		local Data = fromJSON( getAccountData( getPlayerAccount( player ), "Job.Gruzchik.Skin" ) )
        local iSkinID = Data.skin
		setElementModel( player, iSkinID )
		setAccountData( getPlayerAccount( player ), "Job.Gruzchik.Skin", toJSON( { skin = nil, bState = false } ) )
        if Gruz[source] then
	        detachElementFromBone(Gruz[source])
	        destroyElement(Gruz[source])
	        Gruz[source] = false
			return false 
        end
    end
end
addEvent("StartJobGruz", true)
addEventHandler("StartJobGruz", getRootElement(), WorkGruz)

function JobGruzcheckAnim(player)
    if Gruz[source] then
	    detachElementFromBone(Gruz[source])
	    destroyElement(Gruz[source])
	    Gruz[source] = false
        triggerClientEvent("JobGruzCallCheck", getRootElement(), player, false)
    end
end
addEvent("JobGruzcheckAnim", true)
addEventHandler("JobGruzcheckAnim", getRootElement(), JobGruzcheckAnim)

function GruzYshik()
    local x,y,z = getElementPosition(source)
    Gruz[source] = createObject(1463,x,y,z)
    setElementCollisionsEnabled(Gruz[source], false)
    setObjectScale ( Gruz[source], 0.5)
    attachElementToBone(Gruz[source],source,4, 0, 0.47, -0.6, -90, 0, 0 )
end


function trueControl()
toggleControl(source, "enter_exit", false )
toggleControl(source, "jump", false )
toggleControl(source, "fire", false )
toggleControl(source, "sprint", false )
toggleControl(source, "aim_weapon", false )
toggleControl(source, "next_weapon", false )
toggleControl(source, "previous_weapon", false )
end

function falseControl()
toggleControl(source, "enter_exit", true )
toggleControl(source, "jump", true )
toggleControl(source, "fire", true )
toggleControl(source, "sprint", true )
toggleControl(source, "aim_weapon", true )
toggleControl(source, "next_weapon", true )
toggleControl(source, "previous_weapon", true )
end
