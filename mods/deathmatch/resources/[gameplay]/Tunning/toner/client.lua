local scx,scy = guiGetScreenSize()
local px = scx/1920
local sizeX, sizeY = 400*px,600*px
local posX,posY = 10*px,scy-sizeY-100*px
local screen = dxCreateScreenSource( scx,scy )

function getVehicleHandlingProperty ( element, property )
    if isElement ( element ) and getElementType ( element ) == "vehicle" and type ( property ) == "string" then 
        local handlingTable = getVehicleHandling ( element ) 
        local value = handlingTable[property] 
        
        if value then 
            return value 
        end
    end
    
    return false 
end

toner_wnd_stat = false

t_data_1 = "lob_steklo"
t_stat_1 = false 
t_scroll_1 = 5

t_data_2 = "pered_steklo"
t_stat_2 = false
t_scroll_2 = 5

t_data_3 = "zad_steklo"
t_stat_3 = false
t_scroll_3 = 5

setting_cena_T_1 = 20000
setting_cena_T_2 = 20000
setting_cena_T_3 = 20000

toners_itog = 0

t_scrollMoving_1 = false
t_scrollMoving_2 = false
t_scrollMoving_3 = false

function toner_wnd ()
	local veh = getPedOccupiedVehicle(localPlayer)

	dxDrawWindow(5, scy / 2 - 150, 300, 140, "")
	dxDrawButton(5, scy / 2 - 40, 300, 25,"КУПИТЬ : "..toners_itog.." $", true)
	
	dxDrawScrollBar(5, scy / 2 - 130, 250, 15, t_scroll_1, "Лобовое стекло : "..setting_cena_T_1.."$",t_data_1,t_Procent_1,t_stat_1)
	
	dxDrawScrollBar(5, scy / 2 - 100, 250, 15, t_scroll_2, "Боковые стекла : "..setting_cena_T_2.."$",t_data_2,t_Procent_2,t_stat_2)
	
	dxDrawScrollBar(5, scy / 2 - 70, 250, 15, t_scroll_3, "Задние стекла : "..setting_cena_T_3.."$",t_data_3,t_Procent_3,t_stat_3)
		
	dxDrawButton(260, scy / 2 - 130, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 - 100, 40, 15, "X", true)
	
	dxDrawButton(260, scy / 2 - 70, 40, 15, "X", true)
	
end


addEventHandler( "onClientClick", root, function(button, state)
	if toner_wnd_stat == true then
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 - 130, 250, 15
			if cursorPosition( t_scroll_1, y-5, 10, h+10 ) then
				t_scrollMoving_1 = true
			else
				if t_scroll_1 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						t_scrollMoving_1 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						t_scrollMoving_1 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if t_scrollMoving_1 then
				t_scrollMoving_1 = false
			end
		end
		-------------------------------------------------------------------------
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 - 100, 250, 15
			if cursorPosition( t_scroll_2, y-5, 10, h+10 ) then
				t_scrollMoving_2 = true
			else
				if t_scroll_2 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						t_scrollMoving_2 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						t_scrollMoving_2 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if t_scrollMoving_2 then
				t_scrollMoving_2 = false
			end
		end
		-------------------------------------------------------------------------
		if button == "left" and state == "down" then
			local x, y, w, h = 5, scy / 2 - 70, 250, 15
			if cursorPosition( t_scroll_3, y-5, 10, h+10 ) then
				t_scrollMoving_3 = true
			else
				if t_scroll_3 <= x then
					if cursorPosition( x, y-5, 10, h+10 ) then
						t_scrollMoving_3 = true
					end
				else
					if cursorPosition( (x+w)-10, y-5, 10, h+10 ) then
						t_scrollMoving_3 = true
					end
				end
			end
		elseif button == "left" and state == "up" then
			if t_scrollMoving_3 then
				t_scrollMoving_3 = false
			end
		end
	end
end)

addEventHandler( "onClientCursorMove", getRootElement( ), function ( _, _, xMove )
    if t_scrollMoving_1 then
		t_scroll_1 = xMove
	end
    if t_scrollMoving_2 then
		t_scroll_2 = xMove
	end
    if t_scrollMoving_3 then
		t_scroll_3 = xMove
	end
end)


function onClick_toners_setting(button, state)
	if toner_wnd_stat == true then
		if button == "left" and state == "down" then
			if cursorPosition(5, scy / 2 - 135, 250, 25) then
				t_stat_1 = true
				if t_scroll_1 == 5 then
					t_scroll_1 = 6
					toners_itog = toners_itog + setting_cena_T_1
					Stat_buy_setting_toners_1 = false
					Stat_but_setting_toners_1 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				t_stat_1 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 - 105, 250, 25) then
				t_stat_2 = true
				if t_scroll_2 == 5 then
					t_scroll_2 = 6
					toners_itog = toners_itog + setting_cena_T_2
					Stat_buy_setting_toners_2 = false
					Stat_but_setting_toners_2 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				t_stat_2 = false
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 - 75, 250, 25) then
				t_stat_3 = true
				if t_scroll_3 == 5 then
					t_scroll_3 = 6
					toners_itog = toners_itog + setting_cena_T_3
					Stat_buy_setting_toners_3 = false
					Stat_but_setting_toners_3 = true
					showHelpMessage("Тюнинг", "Не забудьте оплатить, иначе мы демонтируем настройку, либо нажммите на X для отмены", 3)
				end
			else
				t_stat_3 = false
			end
			--------------------------------------------------------------------------------------------------------
			if cursorPosition(260, scy / 2 - 130, 40, 15) then
				if t_scroll_1 >= 5.5 then
					--setElementData (getPedOccupiedVehicle (localPlayer), "lob_steklo", Stok_wheels_7 )
					toners_itog = toners_itog - setting_cena_T_1
					t_scroll_1 = 5
					Stat_but_setting_toners_1 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Лобовое стекло ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 - 100, 40, 15) then
				if t_scroll_2 >= 5.5 then
					--setElementData (getPedOccupiedVehicle (localPlayer), "pered_steklo", Stok_wheels_10 )
					toners_itog = toners_itog - setting_cena_T_2
					t_scroll_2 = 5
					Stat_but_setting_toners_2 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Боковые стекла ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(260, scy / 2 - 70, 40, 15) then
				if t_scroll_3 >= 5.5 then
					--setElementData (getPedOccupiedVehicle (localPlayer), "zad_steklo", Stok_wheels_3 )
					toners_itog = toners_itog - setting_cena_T_3
					t_scroll_3 = 5
					Stat_but_setting_toners_3 = false
					showHelpMessage("Тюнинг", "Вы успешно отменили настройку [ Задние стекла ]", 3)
				end
			end
			--------------------------------------------------------------
			if cursorPosition(5, scy / 2 - 40, 300, 25) then
				if getPlayerMoney() >= toners_itog - 1 then--price
					triggerServerEvent("Spisanie_money", getRootElement(), localPlayer, toners_itog )	
					showHelpMessage("Тюнинг", "Вы успешно купили тонировку за "..toners_itog.." $", 3)
					toners_itog = 0
					
					if t_scroll_1 >= 6 then
						Stat_buy_setting_toners_1 = true
						Stat_but_setting_toners_1 = false
						t_scroll_1 = 5
					end
					if t_scroll_2 >= 6 then
						Stat_buy_setting_toners_2 = true
						Stat_but_setting_toners_2 = false
						t_scroll_2 = 5
					end
					if t_scroll_3 >= 6 then
						Stat_buy_setting_toners_3 = true
						Stat_but_setting_toners_3 = false
						t_scroll_3 = 5
					end
				else
					showHelpMessage("Тюнинг", "У вас не хватает денег", 3)
				end
			end
		end
	end
end
addEventHandler("onClientClick", root, onClick_toners_setting)