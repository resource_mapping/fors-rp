local scx,scy = guiGetScreenSize()
local px = scx/1920
local sizeX, sizeY = 400*px,600*px
local posX,posY = 10*px,scy-sizeY-100*px
local screen = dxCreateScreenSource( scx,scy )

local selectionsi_ram = 0
local presi_ram = false
local scrollsi_ram = 0
local scrollMaxsi_ram = 0
local rtsi_ram = dxCreateRenderTarget( 240,145, true )
local clksi_ram = false

ramlShaider = {}

ram_wnd_stat = false
names_rames = ""
numbram = "0"
priceram = 0

function ram_wnd ()
	local veh = getPedOccupiedVehicle(localPlayer)
	local textyres_get = isShaderVehicleRamka(veh,texterws_ramka)
	if textyres_get == true then
	dxDrawWindow(5, scy / 2 - 170, 250, 80, "")
	dxDrawImage(15, scy / 2 - 155, 230, 50, "ramka/image_ram/ram_"..numbram..".png")
	
	
	dxDrawWindow(5, scy / 2 - 80, 250, 210, "")
	dxDrawButton(5, scy / 2 + 100, 122.5, 25, "ПРОСМОТР", true)
	dxDrawButton(132.5, scy / 2 + 100, 122.5, 25, "КУПИТЬ: "..priceram.." $", true)

	dxUpdateScreenSource( screen )
	dxSetRenderTarget( rtsi_ram,true )
	if scrollsi_ram < 0 then scrollsi_ram = 0
	elseif scrollsi_ram >= scrollMaxsi_ram then scrollsi_ram = scrollMaxsi_ram end
	local sy = 0
		for k,v in pairs(raml_list) do
			if k == selectionsi_ram then
				dxDrawRectangle(0,sy-scrollsi_ram,250,25,down)
			else
				dxDrawRectangle(0,sy-scrollsi_ram,250,25,up)
			end
			dxDrawText(v[1],5,sy-scrollsi_ram,150,sy-scrollsi_ram+26,tocolor(255,255,255),1,1,"default-bold","center","center")
			dxDrawText("Цена : "..v[3],160,sy-scrollsi_ram,240,sy-scrollsi_ram+26,tocolor(255,255,255),1,1,"default-bold","left","center")
			sy = sy + 30
		end
	dxSetRenderTarget()
	dxDrawImage(5, scy / 2 - 60,240,145,rtsi_ram)
	if sy >= 145 then
		scrollMaxsi_ram = sy-145
		local size = 145/sy*145
		--dxDrawRectangle(245,scy / 2 - 60,10,145,tocolor(0,0,0,200))
		dxDrawRectangle(245,scy / 2 - 60+scrollsi_ram/scrollMaxsi_ram*(140-size),10,size,tocolor(255,255,255,240))
	end
	local spy = 0
	for k,v in pairs(raml_list) do
		if cursorPosition(5, scy / 2 - 60,240,145) then
			if cursorPosition(5, scy / 2 - 60+spy-scrollsi_ram,240,25) then
				if getKeyState("mouse1") and not clksi_ram then
					selectionsi_ram = k
					--if priceram == 0 then
					--	table.insert(karzina_list,{v[1],v[3]})
					--else
					--	table.remove(karzina_list,v)
					--	table.remove(karzina_list,selectionsi_karzina)
					--	table.insert(karzina_list,{v[1],v[3]})
					--end
				--	spog = isShaderVehicle(veh,shader) 
					--outputChatBox(spog)
					names_rames = v[1]
					numbram = v[2]
					priceram = v[3]
					ram_set = false
				end
			end
		end
		spy = spy + 30
	end
	if getKeyState("mouse1") then clksi_ram = true else clksi_ram = false end
	else
		dxDrawWindow(5, scy / 2 - 80, 250, 210, "")
		dxDrawButtonText_pas(5, scy / 2 + 20, 250, 0, "Данное авто не адаптировано под рамки", 1, 1)
	end
end

addEventHandler("onClientKey",root,function(key,presi_ram)
	if presi_ram then
		if not ram_wnd_stat then return end
		if key == "mouse_wheel_down" then
			scrollsi_ram = scrollsi_ram + 15
		elseif key == "mouse_wheel_up" then
			scrollsi_ram = scrollsi_ram - 15
		end
	end
end)

function onClick_ram(button, state)
	local veh = getPedOccupiedVehicle(localPlayer)
	local textyres_get = isShaderVehicleRamka(veh,texterws_ramka)
	if textyres_get == true then
		if ram_wnd_stat == true then
			if button == "left" and state == "down" then
				if cursorPosition(5, scy / 2 + 100, 122.5, 25) then
					setElementData (getPedOccupiedVehicle (localPlayer), "ramDat", numbram )
					stat_destroy_wnd_ram = true
					showHelpMessage("Тюнинг", "Не забудьте заплатить, иначе мы демонтируем деталь", 3)
				end
				if cursorPosition(132.5, scy / 2 + 100, 122.5, 25) then
					local money = getElementData(localPlayer, "nal" ) or 0
					if getPlayerMoney() >= priceram - 1 then--price
						triggerServerEvent("Spisanie_money", getRootElement(), localPlayer, priceram )
						setElementData (getPedOccupiedVehicle (localPlayer), "ramDat", numbram )
						showHelpMessage("Тюнинг", "Вы успешно купили рамку [ "..names_rames.." ] за "..priceram.." $", 3)
						Stat_buy_ram = true
					else
						showHelpMessage("Тюнинг", "У вас не хватает денег", 3)
					end
				end
			end
		end
	end
end
addEventHandler("onClientClick", root, onClick_ram)

function imageNumberram (vehicle)
	renderTarget = dxCreateRenderTarget(1024, 1024, true)
	dxSetRenderTarget(renderTarget)
	dxDrawImage(0, 0, 1024, 1024, dxCreateTexture( "ramka/image_ram/ram_"..getElementData (vehicle, "ramDat" )..".png"))
	dxSetRenderTarget()
	local texture = getTextureFromRenderTarget(renderTarget)
	--outputChatBox(texture)
	destroyElement(renderTarget)
	return texture
end
function getTextureFromRenderTarget(renderTarget)
	return dxCreateTexture(dxGetTexturePixels(renderTarget))
end

function setram ( vehicle )
	if not ramlShaider[vehicle] then
		ramlShaider[vehicle] = dxCreateShader("ramka/file/texreplace.fx")
	end
	if not getElementData (vehicle, "ramDat" ) then return end
	engineApplyShaderToWorldTexture(ramlShaider[vehicle], texterws_ramka, vehicle)
	texture = imageNumberram(vehicle)
	--outputChatBox(texture)
	if not texture or type (texture) == "table" then return end
	dxSetShaderValue(ramlShaider[vehicle], "TEXTURE_RAM", texture )
end

addEventHandler( "onClientElementStreamIn", getRootElement(), function()
	if getElementType( source ) == "vehicle" then
	if not getElementData (source, "ramDat" ) then return end
	setram ( source )
	end
end)

addEventHandler ( "onClientElementDataChange", getRootElement(),
function ( dataName )
	if getElementType ( source ) == "vehicle" and dataName == "ramDat" then
		setram ( source )
	end
end )

function isShaderVehicleRamka(veh,shader) 
	if ram_wnd_stat == true then
		if veh then 
			local id = tostring(getElementModel(veh)) 
			for _,name in ipairs( engineGetModelTextureNames(id) ) do 
				if name == shader then 
					return true 
				end 
			end 
		end 
	end
end