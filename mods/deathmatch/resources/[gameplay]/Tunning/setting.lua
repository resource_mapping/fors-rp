﻿--------------------------------------------------------------Цвет панелей в тюнинге-----------------------------------------------------------------------
standartR, standartG, standartB, standartH = 191, 32, 32, 150
-----------------------------------------------------------------Кнопки тюнинга----------------------------------------------------------------------------

tuning_but = {
{"КОМПОНЕНТЫ",1},
{"РАМКА",2},
{"КОЛЁСА",3},
{"НАСТРОЙКА КОЛЁС",4},
{"ПОКРАСКА",5},
{"ВИНИЛ",6},
{"НАСТРОЙКА АВТО",7},
--{"ТОНИРОВКА",8},
{"ШИНЫ",9},
}
--[Название кнопки],[Ид кнопки]
--Примечание: Если жедаем удалить кнопку просто сотрите не нужную кнопку либо поставьте перед строкой -- [Ид кнопок не меняем]

--------------------------------------------------Экспорт к ресурсам для их скрытия при въезде в тюнинг-----------------------------------------------------------
--Если у вас нету экспорта в эти ресурсы закоментите их, перед строкой поставьте --
function show ()
	exports.radar:toggleHudRender()
	exports.speedometr:shows_speed()
	exports.hud:shows_hud()
end
-----------------------------------------------------------------Маркер тюнинга и Т.Д.----------------------------------------------------------------------------
ids = 0 -- Не трогать

pos_tun = {
{177.677612, -1549.598999, 20.600,1368.9859619141, 2198.391601, -192.057617, 60},
{536.658630, -808.471252, 20.749,1368.9859619141, -1647.4896240234, 13.01194190979, -90},
{2217.3308, -379.768035, 60.6620,1368.9859619141, -1647.4896240234, 13.01194190979, -90},
{701.5, 1985.3, 5.3, 703.5, 1975.7, 5.4,-180},
{1824.298461,158.748779,60, -2043.6267089844,178.81854248047,29,-90},
}
--[Позиция маркера тюнинга x,y,z],[Позиция спавна при выходе из тюнинга x,y,z,угол авто]
for id,pos in ipairs(pos_tun) do
	blip = createBlip(pos[1], pos[2], pos[3],63) -- блип иконка
	setBlipVisibleDistance(blip, 200)
	marker = createMarker(pos[1], pos[2], pos[3] - 1,"cylinder",2,255,0,255,255) -- маркер
	setElementData(marker, "markerID", id)
	addEventHandler("onClientMarkerHit", marker, function(polion)
	if polion == localPlayer then
			idMarker = getElementData(source, "markerID")
			ids = idMarker
			local veh = getPedOccupiedVehicle(localPlayer)
			if (veh) then
				dim = math.random(1,65000)
				setElementDimension (localPlayer, dim )  
				obj1 = createObject(1851,2656.6001,-1885.7,1969.3) -- позиция 1-го объекта интерьера
				--obj2 = createObject(1852,2656.6001,-1885.7,1969.3) -- позиция 2-го объекта интерьера
				--obj3 = createObject(1853,2656.6001,-1885.7,1969.3) -- позиция 3-го объекта интерьера
				setElementDimension ( obj1, dim ) 
				--setElementDimension ( obj2, dim ) 
				--setElementDimension ( obj3, dim ) 				
				--setElementPosition(veh, 2657.173828125,-1887.3857421875,1969.7) -- Спавн авто а интерьере (Тюнинге)
				setElementPosition(veh, 2656.156,-1885.52,1968.4) -- Спавн авто а интерьере (Тюнинге)
				setElementRotation(veh,0,0,0) -- Вращение авто (rotation)
				open_tuning ()
			end
		end
	end)
end	

-----------------------------------------------------------------Кнопка [Настройка колёс]----------------------------------------------------------------------------

Procent_1 = 0.004 -- числовое значение которое будет плюсоваться при передвижение скролбара [Радиус всех колёс]

Procent_2 = 0.15 -- числовое значение которое будет плюсоваться при передвижение скролбара [Кастро передних колёс]

Procent_3 = 0.01 -- числовое значение которое будет плюсоваться при передвижение скролбара [Развал передних колёс]

Procent_4 = 0.01 -- числовое значение которое будет плюсоваться при передвижение скролбара [Развал задних колёс]

Procent_5 = 0.01 -- числовое значение которое будет плюсоваться при передвижение скролбара [Ширина передних колёс]

Procent_6 = 0.01 -- числовое значение которое будет плюсоваться при передвижение скролбара [Ширина задних колёс]

Procent_7 = 0.001 -- числовое значение которое будет плюсоваться при передвижение скролбара [Вынос передних колёс]

Procent_8 = 0.001 -- числовое значение которое будет плюсоваться при передвижение скролбара [Вынос задних колёс]

Procent_9 = 0.4 -- числовое значение которое будет плюсоваться при передвижение скролбара [Выворот передних колёс]

setting_cena_W_1 = 10000 -- Цена за настройку [Радиус всех колёс]
setting_cena_W_2 = 34990 -- Цена за настройку [Кастро передних колёс]
setting_cena_W_3 = 15860 -- Цена за настройку [Развал передних колёс]
setting_cena_W_4 = 15860 -- Цена за настройку [Развал задних колёс]
setting_cena_W_5 = 7240 -- Цена за настройку [Ширина передних колёс]
setting_cena_W_6 = 7240 -- Цена за настройку [Ширина задних колёс]
setting_cena_W_7 = 14864 -- Цена за настройку [Вынос передних колёс]
setting_cena_W_8 = 14864 -- Цена за настройку [Вынос задних колёс]
setting_cena_W_9 = 20000 -- Цена за настройку [Выворот передних колёс]

-----------------------------------------------------------------Кнопка [Тонировка ]----------------------------------------------------------------------------
--[[
t_Procent_1 = 190^-1 -- числовое значение которое будет плюсоваться при передвижение скролбара

t_Procent_2 = 190^-1 -- числовое значение которое будет плюсоваться при передвижение скролбара

t_Procent_3 = 190^-1 -- числовое значение которое будет плюсоваться при передвижение скролбара

setting_cena_T_1 = 20000 -- Цена за настройку [Радиус всех колёс]
setting_cena_T_2 = 20000 -- Цена за настройку [Кастро передних колёс]
setting_cena_T_3 = 20000 -- Цена за настройку [Развал передних колёс]
]]

-----------------------------------------------------------------Кнопка [Рамки]----------------------------------------------------------------------------

texterws_ramka = "ramka" -- Название текстуры для адаптации

raml_list = {
{"Рамка 0","0",100},
{"Рамка 1","1",1000},
{"Рамка 2","2",1000},
{"Рамка 3","3",1000},
{"Рамка 4","4",100},
{"Рамка 5","5",100},
}
--[Название рамки],[Ид рамки],[Цена за рамку]
--Примечание: Когда добавляете рамку указывайте фото рамок в таком поряде ram_6, ram_7, ram_8 и так далее, В грид листе что выше указываем цифру новой добавленной рамки
-----------------------------------------------------------------Кнопка [Колёса]----------------------------------------------------------------------------

list_wheels = {
{'Stock wheels',0,100},
{'Work Meister S1 (New)',1,5000},
{'Разварочки',2,5000},
{'Штампы (New)',3,5000},
{'Торусы (New)',4,5000},
{'Weds Kranze LXZ (New)',5,5000},
{'Work Gnosis CV201',6,5000},
{'Advan Racing RGII',7,5000},
{'Work Gnosis GS1 (New)',8,5000},
{'Vossen CV5',9,5000},
{'SSR Agle Minerva',10,5000},
{'Work VS-TX (New)',11,5000},
{'Work Gnosis 101',12,5000},
{'BBS RS',13,5000},
{'BBS Large',14,5000},
{'Advan Racing',15,5000},
{'BBS Medium',16,5000},
{'BBS Medium',17,5000},
{'BBS Medium',18,5000},
{'BBS Medium',19,5000},
{'BBS Medium',20,5000},
}
--[Название колеса],[Ид колеса],[Стоимость за колесо]
-----------------------------------------------------------------Кнопка [Компоненты авто]----------------------------------------------------------------------------
--Примечание :  Не забудьте указать такой-же список как и ниже в серверной части setting_server.lua
componentsFromData = {
	["FrontBump"] = "Передний бампер", 
	["RearBump"] = "Задний бампер", 
	["Bonnets"] = "Капот", 
	["SideSkirts"] = "Боковые юбки", 
	["RearLight"] = "Задние фары", 
	["FrontLight"] = "Передние фары", 
	["FrontFends"] = "Передние фендеры", 
	["RearFends"] = "Задние фендеры", 
	["Acces"] = "Аксессуары", 
	["Sprint"] = "Спойлер", 
	["Sprints"] = "Спойлер", 
	["Exhaust"] = "Глушитель", 
	["krusha"] = "Крыша", 
	["FrontResh"] = "Решетка радиатора", 
	["FrontDef"] = "Накладка перед бампера", 
	["RearDef"] = "Накладка заднего бампера", 
	["Packeg"] = "Комплекты", 
	["Kolls"] = "Задние колесо", 
	["Nakladka"] = "Накладки на двери", 
	["Boots"] = "Багажник", 
	["Diffuzor"] = "Диффузор", 
	["Nozdri"] = "Ноздри", 
	["Splitters"] = "Сплиттер", 
	["Lattice"] = "Решётка", 
	["bumper_f"] = "Передний бампер", 
	["bumper_r"] = "Задний бампер", 
	["bonnet"] = "Капот", 
	["tail_lights"] = "Задние фары", 
	["trunk"] = "Багажник", 
	["fenders_f"] = "Передние фендеры", 
	["head_lights"] = "Передние фары", 
	["spoiler"] = "Спойлер", 
	["misc"] = "Прочее", 
	["scoop"] = "Детали крышы", 
	["diffuser"] = "Диффузор", 
	["interiorparts"] = "Части салона", 
	["trunk_badge"] = "Шильдики", 
	["splitter"] = "Сплитер", 
	["skirts"] = "Боковые юбки", 
	["Spoilers"] = "Спойлер", 
	["fenders_f"] = "Передние фендеры", 
	["fenders_r"] = "Задние фендеры", 
	["tail_lights"] = "Задние фары", 
	["head_lights"] = "Передние фары", 
	["interior"] = "Кузов", 
	["doorfender_rr"] = "Обвесы правой задней двери", 
	["doorfender_lr"] = "Обвесы левой задней двери", 
	["door_dside_f"] = "Левое зеркало", 
	["door_pside_f"] = "Правое зеркало", 
	["doorpart_dside_f"] = "Передняя левая обшивка двери", 
	["doorpart_pside_f"] = "Передняя правая обшивка двери",
}
-----------------------------------------------------------------Кнопка [Винил]---------------------------------------------------------------------------
texterws_vinil = "remap" -- Название текстуры для адаптации

-- Тут указываем список настроек, для тех авто которые не прописаны ниже
noModelVinil = {
	{"На данное авто нет винила"},
}

vinil_list = {
	[411] = {
		{"Винил 0","0",100},
		{"Винил 1","1",1000},
		{"Винил 2","2",1000},
		{"Винил 3","3",1000},
		{"Винил 4","4",100},
		{"Винил 5","5",100},
		{"Винил 6","6",100},
	},
	[562] = {
		{"Винил 0","0",100},
		{"Винил 1","1",1000},
		{"Винил 2","2",1000},
		{"Винил 3","3",1000},
		{"Винил 4","4",100},
		{"Винил 5","5",100},
		{"Винил 6","6",100},
	},
}
--[Ид авто]
--[Название винила],[Ид винила],[Цена за винил]
--Примечание: Когда добавляете винил указывайте фото винила в таком поряде vinil_6, vinil_7, vinil_8 и так далее, В грид листе что выше указываем цифру нового добавленного винила

texterws_cord = "cord"

noModelCord = {
	{"На данное авто нет шин"},
}


cordl_list = {
	[411] = {
		{"Шина Cordiant","0",100},
		{"Шина Mishlen","1",1000},
		{"Шина Perelli","2",1000},
	},
	[562] = {
		{"Шина Cordiant","0",100},
		{"Шина Mishlen","1",1000},
		{"Шина Perelli","2",1000},
	},
}
-----------------------------------------------------------------Кнопка [Покраска]---------------------------------------------------------------------------
color_buy_1 = 1000 -- Цена: [Авто #1]
color_buy_2 = 2000 -- Цена: [Авто #2]
color_buy_3 = 3000 -- Цена: [Авто #3]
color_buy_4 = 4000 -- Цена: [Авто #4]
color_buy_5 = 5000 -- Цена: [Передние колёса]
color_buy_6 = 6000 -- Цена: [Задние колёса]
color_buy_7 = 7000 -- Цена: [Фары]

-----------------------------------------------------------------Кнопка [Настройка авто]---------------------------------------------------------------------------

-- Тут указываем список настроек, для тех авто которые не прописаны ниже
noModelHandling = {
	{"Максимальная скорость","maxVelocity",1000,0,500},
	{"Ускорение","engineAcceleration",1000,0,90},
	{"Множитель сципления","tractionMultiplier",1000,0.1,0.9},
	{"Пропорциональность подвески","suspensionFrontRearBias",1000,0.1,1.0},
	{"Инерция","engineInertia",1000,-1000,1000},
	{"Подвеска","suspensionLowerLimit",1000,-1,1},
	--{"Что то","suspensionUpperLimit",1000,-1,1},
}

handling_list = {
	[429] = {
			{"Максимальная скорость","maxVelocity",1000,0,500},
			{"Ускорение","engineAcceleration",1000,0,90},
			{"Множитель сципления","tractionMultiplier",1000,0.1,0.9},
			{"Пропорциональность подвески","suspensionFrontRearBias",1000,0.1,1.0},
			{"Инерция","engineInertia",1000,-1000,1000},
	},
	[411] = {
			{"Максимальная скорость","maxVelocity",1000,0,900},
			{"Ускорение","engineAcceleration",1000,0,90},
			{"Множитель сципления","tractionMultiplier",1000,0.1,0.9},
	},
}
--[Ид авто]
--[Название],[Название функции что будем менять],[Цена за изменение],[Минимальное число],[Максимальное число]


