local SQL = exports.system_database

addEventHandler("onResourceStart", resourceRoot, function(resource)
	SQL:_Exec("CREATE TABLE IF NOT EXISTS job_level (login, level, experience)")
end)

function updateAllInformation(login, level, experience) 
	local current_result = SQL:_Query("SELECT * FROM job_level WHERE login=?", login)
	if #current_result == 0 then
		SQL:_QuerySingle("INSERT INTO job_level (login, level, experience) VALUES (?,?,?)", login, level, experience)
		outputDebugString("[SYSTEM_LEVEL][SAVE][UPDATE]Данные аккаунта ("..login..") сохранены!")
	elseif #current_result == 1 then
		SQL:_Exec("UPDATE job_level SET level=?, experience=? WHERE login=?", level, experience, login)
		outputDebugString("[SYSTEM_LEVEL][SAVE][UPDATE]Данные аккаунта ("..login..") сохранены!")
	end
end

addEventHandler("onPlayerLogin", root, function()
	local login = getAccountName(getPlayerAccount(source))
	local current_result = SQL:_Query("SELECT rowid,* FROM job_level WHERE login=?", login) 
	if current_result then
		for _, row in ipairs(current_result) do
			setElementData(source, "player:level", row["level"] or 1)
            setElementData(source, "player:experience", row["experience"] or 0)
			outputDebugString("[SYSTEM_LEVEL][SAVE][LOGIN] Данные аккаунта "..login.."("..row["level"].." уровень)".."(".. row["experience"].." опыт) загружены!")
		end
	end
end)

addEventHandler("onResourceStart", root, function()
	for index, player in ipairs(getElementsByType("player")) do
		local login = getAccountName(getPlayerAccount(player))
		local current_result = SQL:_Query("SELECT rowid,* FROM job_level WHERE login=?", login) 
		if current_result then
			for _, row in ipairs(current_result) do
				setElementData(player, "player:level", row["level"] or 1)
				setElementData(player, "player:experience", row["experience"] or 0)
				outputDebugString("[SYSTEM_LEVEL][SAVE][LOGIN] Данные аккаунта "..login.."("..row["level"].." уровень)".."(".. row["experience"].." опыт) загружены!")
			end
		end
	end
end)

addEventHandler("onPlayerQuit", root, function()
	local account = getPlayerAccount( source )
	if not isGuestAccount ( account ) then
		local login = getAccountName ( account )
		local levelPlayer = getElementData(source, "player:level") or 1
		local experiencePlayer = getElementData(source, "player:experience") or 0
		local current_result = SQL:_Query("SELECT * FROM job_level WHERE login=?",login)
		if #current_result == 0 then
			SQL:_QuerySingle("INSERT INTO job_level (login, level, experience) VALUES (?,?,?)", login, levelPlayer, experiencePlayer)
			outputDebugString("[SYSTEM_LEVEL][SAVE][QUIT]Данные аккаунта "..login.."("..levelPlayer.." уровень)".."("..experiencePlayer.." опыт) сохранены!")
		elseif #current_result == 1 then
			SQL:_Exec("UPDATE job_level SET level=?,experience=? WHERE login=?", levelPlayer, experiencePlayer, login)
			outputDebugString("[SYSTEM_LEVEL][SAVE][QUIT]Данные аккаунта "..login.."("..levelPlayer.." уровень)".."("..experiencePlayer.." опыт) сохранены!")
		end
	end
end)

--------------------------------------------------------------------------------
--<[ Модуль Level ]>------------------------------------------------------------
--------------------------------------------------------------------------------
local expIncrease = 125	-- на сколько будет увеличиваться опыт на каждом уровне.
local maxLevel = 30 	-- максимальный уровень
local maxExp  = maxLevel*expIncrease

--// Получение уровня игрока
function getPlayerLevel(player)
	levelCurrent = tonumber(getElementData(player, "player:level")) or 1
	return levelCurrent
end

--// Получение опыта игрока
function getPlayerExperience(player)
	expCurrent = tonumber(getElementData(player, "player:experience")) or 0
	return expCurrent
end

--// Получение опыта игрока по уровню
function getPlayerExperienceByLevel(player)
	local levelCurrent = tonumber(getElementData(player, "player:level")) or 1
	return levelCurrent*expIncrease
end

--// Выдача уровня игроку ( игрок, уровень )
function givePlayerLevel(player, setLevel, resultExp)
	if tonumber(setLevel) >= maxLevel then
		setElementData(player, "player:experience", maxExp)
		setElementData(player, "player:level", maxLevel)
		outputChatBox ("[#FF0000Сервер#FFFFFF] Вы достигли максимального уровня!", player, 255, 255, 255, true)
		outputChatBox ("[#FF0000Сервер#FFFFFF] Вы получили награду ("..maxLevel*1000 .."#ffffff Liberty Coins!)", player, 255, 255, 255, true)
		givePlayerMoney( player, maxLevel*1000)
		local name_acc_player = getAccountName(getPlayerAccount(player))
		updateAllInformation(name_acc_player, getElementData(player, "player:level"), getElementData(player, "player:experience"))
	else
		setElementData(player, "player:experience", resultExp)
		setElementData(player, "player:level", setLevel)
		outputChatBox ("[#FF0000Сервер#FFFFFF] Вы получили #00ff00"..setLevel.."#ffffff уровень!", player, 255, 255, 255, true)
		outputChatBox ("[#FF0000Сервер#FFFFFF] Вы получили награду ("..setLevel*1000 .."#ffffff Liberty Coins!)", player, 255, 255, 255, true)
		givePlayerMoney( player, setLevel*1000)
		local name_acc_player = getAccountName(getPlayerAccount(player))
		updateAllInformation(name_acc_player, getElementData(player, "player:level"), getElementData(player, "player:experience"))
	end
end

--// Выдача опыта игроку ( игрок, количество опыта)
function givePlayerExperience(player, addedExp)
	--// Получаем текущие уровень и опыт
	local experienceCurrent = getElementData ( player, "player:experience" ) or 0
	local levelCurrent = getElementData ( player, "player:level" ) or 1
	--// Устанавливаем добавленное количество опыта к текущему
	setElementData(player, "player:experience", experienceCurrent+addedExp)
	outputChatBox ("[#FF0000Сервер#FFFFFF] Вы получили #00ff00"..addedExp.."#ffffff опыта!", player, 255, 255, 255, true)
	local name_acc_player = getAccountName(getPlayerAccount(player))
	updateAllInformation(name_acc_player, getElementData(player, "player:level"), getElementData(player, "player:experience"))
	--// Проверяем, если текущее количество опыта превысило необходимый для повышения
	if experienceCurrent+addedExp >= getPlayerExperienceByLevel(player) then
		--// Проверяем, если текущий уровень больше или равен maxLevel, то устанавливаем все по макимуму. Выдаем награду.
		if levelCurrent+1 >= maxLevel then
			givePlayerLevel(player, maxLevel, maxExp)
		else
		--// Если выше указанное не верно, то устанавливем текущий уровень + 1 и текущий опыт(0) + опыт излишки. Выдаем награду.
			transferExperience = (experienceCurrent+addedExp)-getPlayerExperienceByLevel(player)
			givePlayerLevel(player, levelCurrent+1, transferExperience)
		end
	end
end

--// Выдача опыта игроку по команде
function givePlayerExperienceByCommand(player, command, id, addedExp)
	local player_name = getPlayerName(player)
	local player_who_give, player_name_who_give = exports.system_id:getPlayerFromID(id)
	local accName = getAccountName(getPlayerAccount(player))
	if isObjectInACLGroup ("user."..accName, aclGetGroup("Admin")) then
		if tonumber(addedExp) <= 500 then
			if player_who_give then
				outputChatBox ("[#FF0000Сервер#FFFFFF] Администратор #00ff00"..player_name.." #ffffffвыдал Вам опыт!", player_who_give, 255, 255, 255, true)
				outputChatBox ("[#FF0000Сервер#FFFFFF] Вы выдали игроку "..player_name_who_give.." #00ff00"..addedExp.." #ffffffопыта!", player, 255, 255, 255, true)
				givePlayerExperience(player_who_give, addedExp)
			else
				outputChatBox ("[#FF0000Сервер#FFFFFF] Игрока с ID #00ff00"..id.." #ffffffне найдено!", player, 255, 255, 255, true)
			end
		else
			outputChatBox ("[#FF0000Сервер#FFFFFF] Максимальное количество опыта, которое можно выдать - #00ff00500#ffffff!", player, 255, 255, 255, true)
		end
	else
		outputChatBox ("#FFFFFF[#FF0000Сервер#FFFFFF] Данная команда Вам недоступна!", player, 255, 255, 255, true)
	end
end

--// Выдача уровня игроку по команде
function givePlayerLevelByCommand(player, command, id, setLevel)
	local player_name = getPlayerName(player)
	local player_who_give, player_name_who_give = exports.system_id:getPlayerFromID(id)
	local accName = getAccountName(getPlayerAccount(player))
	if isObjectInACLGroup ("user."..accName, aclGetGroup("Admin")) then
		if player_who_give then
			if tonumber(setLevel) >= maxLevel+1 then
				givePlayerLevel(player, maxLevel, maxExp)
				outputChatBox ("[#FF0000Сервер#FFFFFF] Администратор #00ff00"..player_name.." #ffffffвыдал Вам уровень!", player_who_give, 255, 255, 255, true)

				outputChatBox ("[#FF0000Сервер#FFFFFF] Вы выдали игроку "..player_name_who_give.." #00ff00"..maxLevel.." #ffffffуровень!", player, 255, 255, 255, true)
				local name_acc_player = getAccountName(getPlayerAccount(player_who_give))
				updateAllInformation(name_acc_player, getElementData(player_who_give, "player:level"), getElementData(player_who_give, "player:experience"))
			else
				givePlayerLevel(player_who_give, setLevel, 0)
				outputChatBox ("[#FF0000Сервер#FFFFFF] Администратор #00ff00"..player_name.." #ffffffвыдал Вам уровень!", player_who_give, 255, 255, 255, true)

				outputChatBox ("[#FF0000Сервер#FFFFFF] Вы выдали игроку "..player_name_who_give.." #00ff00"..setLevel.." #ffffffуровень!", player, 255, 255, 255, true)
				local name_acc_player = getAccountName(getPlayerAccount(player_who_give))
				updateAllInformation(name_acc_player, getElementData(player_who_give, "player:level"), getElementData(player_who_give, "player:experience"))
			end
		else
			outputChatBox ("[#FF0000Сервер#FFFFFF] Игрока с ID #00ff00"..id.." #ffffffне найдено!", player, 255, 255, 255, true)
		end
	else
		outputChatBox ("#FFFFFF[#FF0000Сервер#FFFFFF] Данная команда Вам недоступна!", player, 255, 255, 255, true)
	end
end

function wipeAllPlayers()
	for _, pl in ipairs(getElementsByType("player")) do
		local name_acc_player = getAccountName(getPlayerAccount(pl))
		updateAllInformation(name_acc_player, 1, 0)
	end
end

--------------------------------------------------------------------------------
--<[ Тест команды ]>------------------------------------------------------------
--------------------------------------------------------------------------------
addCommandHandler ("experience", givePlayerExperienceByCommand) -- Выдача опыта по команде.
addCommandHandler ("level", givePlayerLevelByCommand) -- Выдача уровня по команде