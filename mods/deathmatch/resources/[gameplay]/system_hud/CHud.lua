---- ==== Отключение стандартного HUD'a ==== ----
setPlayerHudComponentVisible("all", false)

---- ==== Парметры худа ==== ----
local screenW, screenH = guiGetScreenSize()
local mainW, mainH = 340, 130
local mainX, mainY = screenW-mainW-20, 20

local fontMoney = exports.system_fonts:createFont("OpenSans/OpenSans-Bold.ttf", 18, "antialiased")
local fontStat = exports.system_fonts:createFont("OpenSans/OpenSans-SemiBold.ttf", 11, "antialiased")
local fontLevel = exports.system_fonts:createFont("OpenSans/OpenSans-Bold.ttf", 23, "antialiased")
local fontLevelDescr = exports.system_fonts:createFont("OpenSans/OpenSans-Bold.ttf", 10, "antialiased")
local fontExperience = exports.system_fonts:createFont("OpenSans/OpenSans-Regular.ttf", 10, "antialiased")

local levelRadialShader = dxCreateShader("fx/circle.fx")
local levelRadialTexture = dxCreateTexture("img/level_radial_bg.png")

addEventHandler("onClientRender", root, function()
	dxDrawImage(mainX, mainY, mainW, mainH, "img/bg_hud.png", 0,0,0, tocolor(255,255,255,255))
	
	--dxDrawImage(mainX-700, mainY-155, 503, 193, "img/offerbar.png", 0,0,0, tocolor(255,255,255,255))
	
	dxDrawImage(mainX, mainY, mainW, mainH, "img/bg_main.png", 0,0,0, tocolor(255,255,255,255))
	
	
	-- Деньги
	dxCreateText(mainX+198, mainY+13, 0, 28, convertNumber(getPlayerMoney(localPlayer)), fontMoney, tocolor(255,255,255,235), "left", "center")
	
	-- Здоровье
	dxCreateText(mainX+305, mainY+49, 0, 21, math.floor(getElementHealth(localPlayer)), fontStat, tocolor(255,255,255,235), "left", "center")
	dxDrawRectangle(mainX+158, mainY+57, math.floor(getElementHealth(localPlayer))/100*112, 4, tocolor(255,0,0))
	
	-- Еда
	dxCreateText(mainX+305, mainY+72, 0, 21, math.floor(getElementHealth(localPlayer)), fontStat, tocolor(255,255,255,235), "left", "center")
	dxDrawRectangle(mainX+158, mainY+81, math.floor(getElementHealth(localPlayer))/100*112, 4, tocolor(218,165,32))	

	-- Броня
	dxCreateText(mainX+305, mainY+99, 0, 18, math.floor(getPedArmor(localPlayer)), fontStat, tocolor(255,255,255,235), "left", "center")
	dxDrawRectangle(mainX+242, mainY+105, math.floor(getPedArmor(localPlayer))/100*28, 4, tocolor(200,200,200,235))
	
	-- Воздух 
	dxCreateText(mainX+216, mainY+99, 0, 18, math.floor(getPedOxygenLevel(localPlayer) / 10), fontStat, tocolor(255,255,255,235), "left", "center")
	dxDrawRectangle(mainX+158, mainY+105, math.floor(getPedOxygenLevel(localPlayer) / 8.5)/100*28, 4, tocolor(0,191,255))
	
	-- Уровень
	local levelText = exports.system_level:getPlayerLevel(localPlayer)
	dxCreateText(mainX+90, mainY+38, 0, 20, levelText, fontLevel, tocolor(255,255,255,235), "left", "center")
	local levelWidth = dxGetTextWidth(levelText, 1, fontLevel)
	dxCreateText(mainX+89+levelWidth, mainY+40, 0, 20, "ур.", fontLevelDescr, tocolor(255,255,255,235), "left", "bottom")
	
	-- Опыт
	local expText = exports.system_level:getPlayerExperience(localPlayer)
	local nextExpText = exports.system_level:getPlayerExperienceByLevel(localPlayer)
	dxCreateText(mainX+92, mainY+75, 0, 20, expText.." /\n"..nextExpText, fontExperience, tocolor(255,255,255,120), "left", "bottom")
	
	-- Кольцо опыта
	dxSetShaderValue(levelRadialShader, "tex", levelRadialTexture)
	dxSetShaderValue(levelRadialShader, "angle", 1)
	local percentage = nextExpText and math.max( 0, math.min( 1, expText / nextExpText ) ) or 1
	dxSetShaderValue(levelRadialShader, "dg", percentage * 2)
	dxSetShaderValue(levelRadialShader, "rgba", 255, 236, 161)
	dxDrawImage(mainX+20, mainY+35, 60, 60, "img/level_bg.png", 0,0,0, tocolor(255,255,255,255))
	dxDrawImage(mainX+20, mainY+35, 60, 60, levelRadialShader, 90,0,0, tocolor(255,255,255,255))
end)