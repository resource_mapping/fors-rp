﻿--------------------------------------------------------------------------------
--<[ Модуль ID ]>---------------------------------------------------------------
--------------------------------------------------------------------------------
local availableID = {}		-- Таблица с доступным ID для игрока
local allIdIsLoades = false	-- Добавлены-ли доступные ID в таблицу  

--// Загрузка всех доступных ID в таблицу
function loadID()
	allIdIsLoades = true
	local max_players = getMaxPlayers()
	for i = 1, max_players do
		table.insert(availableID, i, true)
	end
	for _, player in ipairs(getElementsByType("player")) do
		assignPlayerID(player)
	end    
	exports.scoreboard:addScoreboardColumn("ID", getRootElement(), 1)
end
addEventHandler("onResourceStart", resourceRoot, loadID)

--// Выдача ID при входе на сервер
function onJoin()
	assignPlayerID(source)
end
addEventHandler("onPlayerJoin", getRootElement(), onJoin)    
 
--// Функция выдачи ID
function assignPlayerID(player)
	local s_id = 1
	while (isIDAvailable (s_id) == false ) do -- если ID - доступный, то выдаём, а если нет, то добавляем +1 и повторяем
		s_id = s_id + 1
	end
	setElementData(player, "ID", s_id) -- если ID - доступный, то устанавливаем игроку
	availableID[s_id] = false -- ID, который мы выдали игроку делаем недоступным
end            
	
--// Проверка на доступность ID
function isIDAvailable(id)
	return availableID[id]
end    

--// Снятие ID с игрока при выходе с сервера
function onLeave()
	local s_id = getElementData(source, "ID")
	availableID[s_id] = true
	removeElementData(source, "ID")     
end
addEventHandler("onPlayerQuit", getRootElement(), onLeave)          
 
--// Функция очистки таблицы при остановке ресурса
function removeData() -- resource stopped, clear everything.
	for k, players in ipairs(getElementsByType("player")) do
		removeElementData(players, "ID")
	end
	availableID = {}
end    
addEventHandler("onResourceStop", resourceRoot, removeData)

--// Функция поиска игрока с помощью ID
function getPlayerFromID(id)
	for k, player in ipairs(getElementsByType("player")) do
		local p_id = getElementData(player, "ID")
		if (p_id == tonumber(id)) then
			player_n = getPlayerName (player)
			return player, player_n
		end
	end
	return false, "Игрок с ID " .. id .. " не найден."
end  

--// Функция поиска ID игрока
function getIDFromPlayer(player)
	if player then
		local p_id
		for _, p in ipairs(getElementsByType("player")) do
			if player == p then
				p_id = getElementData(p, "ID") or 0
			end
		end
		return p_id
	else 
		return false 
	end
end