﻿function centerWindow ( center_window )
	local sx, sy = guiGetScreenSize ( )
	local windowW, windowH = guiGetSize ( center_window, false )
	local x, y = ( sx - windowW ) / 2, ( sy - windowH ) / 2
	guiSetPosition ( center_window, x, y, false )
end
local sellcarColShape = createColSphere(1713.491, -1064.974, 23.906, 40)

local customCarNames = -- новые названия
{
	[445] = 'Toyoya Land Cruiser V8';
	[602] = 'Mercedes-Benz AMG C90';
	[504] = 'BMW M3 E190';
	[401] = 'BMW Schnitzer Turbo ';
	[518] = 'Chervolet Camaro';
	[541] = 'Lamborghini Gallardo';
	[527] = 'Lamborghini Huracan';
	[589] = 'Mercedes AMG C63';
	[507] = 'Mercedes S560';
	[585] = 'Lada 2106';
	[419] = 'Doodge Changer RT';
	[587] = 'Nissan GT-R';
	[490] = 'Vaz Patriot';
	[521] = 'Kawasaki';
	[533] = 'BMW Turbo GT';
	[474] = 'BMW M8 Competition';
	[545] = 'McLaren P1';
	[411] = 'Audi R8';
	[517] = 'OKA';
	[410] = 'Opel Astra';
	[551] = 'Lexus LX570';
	[418] = 'Range Rover SV Cross';
        [516] = 'Lada 1500';
        [467] = 'Volga';
        [470] = 'Porshe Cayene';
        [479] = 'Mercedes AMG GL63';
        [534] = 'Aston Martin';
        [543] = 'Lada Niva 4x4';
        [547] = 'Mercecedes-Benz 4x4';
        [489] = 'CyberVaz';
        [540] = 'Mercedes-Benz C63';
        [405] = 'Mitsubishi Evoletion X';
        [535] = 'Porshe 911';
        [439] = 'BMW M4';
	[560] = 'Subaru WRX';
	[506] = 'Nissan GTR Turbo';
	[566] = 'Renault Logan';
	[549] = 'Jaguar GT';
	[451] = 'Ferrari LaFerrari';
	[558] = 'Ferrari GT';
	[491] = 'Toyoya Supra GT';
	[412] = 'Doodge Challanger SRT';
	[421] = 'BMW M5 Cruiser';
	[529] = 'Volfsvagen Polo';
	[555] = 'Nissan RX7';
	[554] = 'Raptor F150';
	[477] = 'Toyota ZR320';
	[404] = 'Mercedes-Benz_GLE';
	[558] = 'Lamborghini Aventador';
	[451] = 'McLaren P1';
	[496] = 'Nissan Skyline';
	[477] = 'Mazda RX-7';
	[438] = 'Mitsubishi Lancer X';
	[587] = 'Lexus LFA';
	[480] = 'Porsche 911';
        [529] = 'Ваз 2106';
        [445] = 'Ваз 2107';
        [516] = 'Ваз 2109';
        [547] = 'Priora';
        [470] = 'Hummer H1';
        [546] = 'Dodge Charger';
        [436] = 'BMW M5 F10';
        [589] = 'Volkswagen Golf';
        [540] = 'Volkswagen Passat_B8';
        [479] = 'Volkswagen Touareg';
        [438] = 'Gaz Pobeda';
        [401] = 'BMW I8 GT';
}






setTimer ( function ()

local theCol = getElementData(root, "BlockExportCol")
	
function isInColExport ()
	if isElement(theCol) and isElementWithinColShape(localPlayer,theCol) then
		return true else return false
	end
end
 
function ClientExplosionCFunction()
 if isInColExport ()  then
  cancelEvent ()
 end
end
addEventHandler("onClientExplosion", root, ClientExplosionCFunction)

end , 1000, 1 )

local screX, screY = guiGetScreenSize()
 
Window_VS = guiCreateWindow(500, 254, 276, 420,"Управление транспортом",false)
guiSetAlpha(Window_VS, 0.88)
guiWindowSetSizable(Window_VS, false)
guiSetVisible(Window_VS, false)
centerWindow(Window_VS)
Grid_VS = guiCreateGridList(11, 107, 255, 225,false,Window_VS)
guiGridListSetSelectionMode(Grid_VS, 1)
guiGridListAddColumn(Grid_VS,"Транспорт",0.939)
Label_SVS = guiCreateLabel(10, 386, 257, 15, "Парковочных мест занято: *из*.",false,Window_VS)
guiSetAlpha(Window_VS, 0.88)
guiSetFont(Label_VeS, "default-bold-small")
guiSetFont(Label_SVS, "default-bold-small")


Button_VS_sn = guiCreateButton(10, 340, 90, 45, "Респавн", false, Window_VS)
Button_VS_lk = guiCreateButton(184, 54, 90, 43, "Откр/Закр", false, Window_VS)
Button_VS_bp = guiCreateButton(103, 54, 76, 43, "Метка", false, Window_VS)
Button_VS_lk = guiCreateButton(184, 340, 95, 45, "Заблок", false, Window_VS)
Button_VS_give = guiCreateButton(10, 54, 90, 20, "Игроку", false, Window_VS)
Button_VS_sl = guiCreateButton(10, 76, 90, 20, "В а/салон", false, Window_VS)
Button_VS_Warp = guiCreateButton(105, 340, 76, 45, "Телепорт", false, Window_VS)





Window_CHK = guiCreateWindow(screX/2-155,screY/2-60,310,120,"Внимание!",false)
guiSetVisible(Window_CHK, false)
guiSetProperty(Window_CHK, "AlwaysOnTop", "true")
guiWindowSetSizable(Window_CHK, false)
Label_CHK = guiCreateLabel(21,28,266,36,"",false,Window_CHK)
guiLabelSetColor(Label_CHK, 38, 122, 216)
guiLabelSetHorizontalAlign(Label_CHK,"center",true)
Button_CHK_Y = guiCreateButton(17,73,129,36,"Да",false,Window_CHK)
Button_CHK_N = guiCreateButton(161,73,129,36,"Нет",false,Window_CHK)

function updateGridList()
	local data = getElementData(localPlayer, "VehicleInfo")
	if data then
		local rw, cl = guiGridListGetSelectedItem(Grid_VS)
		guiGridListClear(Grid_VS)
		for i, data in ipairs (data) do
			local carName = customCarNames[ data['Model'] ] or getVehicleNameFromModel(data["Model"])
			local ID = data["ID"]
			local Cost = data["Cost"]
			local HP = math.floor(data["HP"])
			local PreCost = math.ceil(Cost*.9*HP/100/10)
			local row = guiGridListAddRow(Grid_VS)
			guiGridListSetItemText(Grid_VS, row, 1, carName, false, true)
			guiGridListSetItemData(Grid_VS, row, 1, ID)
			guiGridListSetItemText(Grid_VS, row, 2, PreCost, false, true)
			guiGridListSetItemText(Grid_VS, row, 3, HP.." HP", false, true)
		end
		guiGridListSetSelectedItem(Grid_VS, rw, cl)
	end
end



bindKey("F3", "down",
function()
if getElementInterior(localPlayer) == 0 and getElementDimension(localPlayer) == 0 then
if getElementData(localPlayer, "MissionWarProtection") and getElementData(localPlayer, "MissionProtection")then return end
    guiSetVisible(Window_VS, not guiGetVisible(Window_VS))
	guiSetVisible (Window_CHK, false)
	showCursor(guiGetVisible(Window_VS))
	end
end)

triggerServerEvent("onOpenGui", localPlayer)

addEventHandler("onClientElementDataChange", root,
function(dd)
	if getElementType(source) == "player" and source == localPlayer and dd == "VehicleInfo" then
		local data = getElementData(source, dd)
		if data then
			updateGridList()
		end
	end
end)

function WINDOW_CLICK_VEHICLE (button, state, absoluteX, absoluteY)
	local id = guiGridListGetSelectedItem(Grid_VS)
	local ID = guiGridListGetItemData(Grid_VS, id, 1)
	if source == Button_VS_close then
		guiSetVisible(Window_VS, false)
		showCursor(false)
	end
	if (source == Grid_VS) then
		if id == -1 and idd then
			guiGridListSetSelectedItem(Grid_VS, idd, 1)
			return false
		else
			idd = guiGridListGetSelectedItem(Grid_VS)
		end
	elseif id == -1 then
	elseif (source == Button_VS_sn) then
	if not isInColExport () then
		triggerServerEvent("SpawnMyVehicle", localPlayer, ID)
                end
	elseif (source == Button_VS_dy) then 
		triggerServerEvent("DestroyMyVehicle", localPlayer, ID)
	elseif (source == Button_VS_lt) then 
		triggerServerEvent("LightsMyVehicle", localPlayer, ID)
	elseif (source == Button_VS_bp) then 
		triggerServerEvent("BlipMyVehicle", localPlayer, ID)
	elseif (source == Button_VS_lk) then 
		triggerServerEvent("LockMyVehicle", localPlayer, ID)
	elseif (source == Button_VS_sl) then 
		if localPlayer.vehicle and localPlayer.vehicle:getData("ID") == ID then
			if isElementWithinColShape(localPlayer.vehicle, sellcarColShape) then
				guiSetVisible(Window_CHK, true)
				local carName = guiGridListGetItemText(Grid_VS, guiGridListGetSelectedItem(Grid_VS), 1)
				local carprice = guiGridListGetItemText(Grid_VS, guiGridListGetSelectedItem(Grid_VS), 2)
				guiSetText(Label_CHK, 'Вы серьёзно хотите продать "'..carName..'" за $'..carprice)
			else
	outputChatBox("[Автосалон]Невозможно продать автомобиль.Вы и ваш автомобиль должны находится на б/у рынке.", 255, 0, 0)
			end
		end
	elseif source == Button_VS_give then
		createPlayersList(id)
	elseif source == Button_CHK_Y then
		triggerServerEvent("SellMyVehicle", localPlayer, ID)
		guiSetVisible(Window_VS, false)
		guiSetVisible(Window_CHK, false)
		showCursor(false)
	elseif source == Button_CHK_N then
		guiSetVisible (Window_CHK, false)
	elseif source == Button_VS_Spc then
      if getElementInterior(localPlayer) == 0 then
if getElementData(localPlayer,"Stats") < 2 then
		SpecVehicle(ID)
end
end
	elseif source == Button_VS_Fix then
		triggerServerEvent("FixMyVehicle", localPlayer, ID)
	elseif source == Button_VS_Warp then
	       if not isInColExport () then
		triggerServerEvent("WarpMyVehicle", localPlayer, ID)
                      end
	elseif source == Button_PLS_Y then
		local row = guiGridListGetSelectedItem ( playerList_PLS )
		if row and row ~= -1 then
			-- if guiGridListGetItemText ( playerList_PLS, row, 1 ) == getPlayerName ( localPlayer ) then
				-- return true
			-- end
			if (tonumber(guiGetText (edit_PLS_price)) or 0) >= 0 then
				outputChatBox ( "Ожидайте ответа игрока", 10, 250, 10 )
				invitations_send = true
				triggerServerEvent ( 'inviteToBuyCarSended', localPlayer, guiGridListGetItemText ( playerList_PLS, row, 1 ), guiGetText (edit_PLS_price) or 0, guiGridListGetItemText(Grid_VS, id, 1), guiGridListGetItemData(Grid_VS, id, 1) )
				destroyElement ( Window_PLS )
			end
		end
	elseif source == Button_PLS_N then
		destroyElement ( Window_PLS)
	end
end
addEventHandler("onClientGUIClick", resourceRoot, WINDOW_CLICK_VEHICLE)

function invitationsClickVehicle ()
	if source == Button_ABC_Y then
		showCursor ( false )
		destroyElement ( Window_ABC )
		if getPlayerMoney () >= ( tonumber(inv_price) or 0 ) then
			triggerServerEvent ("invitationBuyCarAccepted",localPlayer, inv_player, inv_acc, inv_price, inv_veh_name, inv_veh_id)
		else
			outputChatBox ( "У вас не хватает денег, сделка отменена", 250, 10, 10 )
			triggerServerEvent ("invitationBuyCarNotAccepted",localPlayer, inv_player )
		end
		if #listOfInvitations > 0 then
			createAcceptBuyCarWindow (listOfInvitations[1][1],listOfInvitations[1][2],listOfInvitations[1][3],listOfInvitations[1][4] )
			table.remove (listOfInvitations,1)
		end
	elseif source == Button_ABC_N then
		showCursor ( false )
		triggerServerEvent ("invitationBuyCarNotAccepted",localPlayer, inv_player )
		destroyElement ( Window_ABC )
		if #listOfInvitations > 0 then
			createAcceptBuyCarWindow (listOfInvitations[1][1],listOfInvitations[1][2],listOfInvitations[1][3],listOfInvitations[1][4] )
			table.remove (listOfInvitations,1)
		end
	end
end

addEventHandler("onClientGUIClick", resourceRoot, invitationsClickVehicle)

function createPlayersList (row_id)
	showCursor ( true )
	Window_PLS = guiCreateWindow(screX/2-155,screY/2-220,310,420,"Выбор покупателя!",false)
	guiSetVisible(Window_PLS, true)
	guiSetProperty(Window_PLS, "AlwaysOnTop", "true")
	guiWindowSetSizable(Window_PLS, false)
	Label_PLS_info = guiCreateLabel(21,28,266,36,"Выберите покупателя и введите цену:",false,Window_PLS)
	edit_PLS_price = guiCreateEdit ( 110,58,90,36, guiGridListGetItemText(Grid_VS, row_id, 2) or 0, false, Window_PLS )
	guiLabelSetColor(Label_PLS_info, 38, 122, 216)
	guiLabelSetHorizontalAlign(Label_PLS_info,"center",true)
	Button_PLS_Y = guiCreateButton(17,379,129,36,"Выбор",false,Window_PLS)
	Button_PLS_N = guiCreateButton(161,379,129,36,"Отмена",false,Window_PLS)
	addEventHandler("onClientGUIChanged", edit_PLS_price, function(element) 
		guiSetText ( edit_PLS_price, string.gsub (guiGetText( edit_PLS_price ), "%a", "") )
	end)
	playerList_PLS = guiCreateGridList ( 21, 100, 268, 265, false, Window_PLS )
	local column = guiGridListAddColumn( playerList_PLS, "Игроки", 0.85 )
	if ( column ) then
		for id, player in ipairs(getElementsByType("player")) do
			local row = guiGridListAddRow ( playerList_PLS )
			guiGridListSetItemText ( playerList_PLS, row, column, getPlayerName ( player ), false, false )
		end
	end

end

--createPlayersList ()

listOfInvitations = {}
inv_player, inv_acc, inv_price, inv_veh_name, inv_veh_id = nil, nil, nil, nil, nil


addEvent("recieveInviteToBuyCar", true)
addEventHandler("recieveInviteToBuyCar", root, 
function(player, acc, price, veh_name, veh_id)
	if player and price and acc and veh_name and veh_id then
		if getPlayerFromName ( player ) then
			if not isElement ( Window_ABC ) then
				createAcceptBuyCarWindow (player,acc,price, veh_name, veh_id)
			else	
				table.insert ( listOfInvitations, {player,acc,price, veh_name, veh_id})
			end
		else
			outputChatBox ( "Игрок не найден, продажа отменена", source, 250, 10, 10)
		end
	end
end)

addEvent("cleanCarInvitations", true)
addEventHandler("cleanCarInvitations", root, 
function()
	invitations_send = false
end)

function createAcceptBuyCarWindow(player,acc,price, veh_name, veh_id)
	showCursor ( true )
	inv_player, inv_acc, inv_price, inv_veh_name, inv_veh_id = player,acc,price, veh_name, veh_id
	Window_ABC = guiCreateWindow(screX/2-155,screY/2-220,410,100,"Вам предложили купить машину",false)
	guiSetVisible(Window_ABC, true)
	guiSetProperty(Window_ABC, "AlwaysOnTop", "true")
	guiWindowSetSizable(Window_ABC, false)
	Label_ABC_info = guiCreateLabel(10,28,390,36,player.." предложил вам купить его автомобиль "..veh_name.." за "..price.."$ (у вас есть "..getPlayerMoney() .."$)",false,Window_ABC)
	guiLabelSetColor(Label_ABC_info, 38, 216, 38)
	guiLabelSetHorizontalAlign(Label_ABC_info,"center",true)
	Button_ABC_Y = guiCreateButton(17,70,129,36,"Купить",false,Window_ABC)
	Button_ABC_N = guiCreateButton(264,70,129,36,"Не покупать",false,Window_ABC)
end

function SpecVehicle(id)

	if spc then 
		removeEventHandler("onClientPreRender", root, Sp)
		setCameraTarget(localPlayer)
		if isTimer(freezTimer) then killTimer(freezTimer) end
		freezTimer = setTimer(function() setElementFrozen(localPlayer, false) end, 2500, 1)
		spc = false
	return end
	for i, vehicle in ipairs(getElementsByType("vehicle")) do
		if getElementData(vehicle, "Owner") == localPlayer and getElementData(vehicle, "ID") == id then
			cVeh = vehicle
			spc = true
			addEventHandler("onClientPreRender", root, Sp)
			guiSetVisible(Window_VS, false)
			showCursor(false)
			break
		  end
                        
	end
end

function Sp()
	if isElement(cVeh) then
		local x, y, z = getElementPosition(cVeh)
		setElementFrozen(localPlayer, true)
		setCameraMatrix(x, y-1, z+15, x, y, z)

	else
		removeEventHandler("onClientPreRender", root, Sp)
		setCameraTarget(localPlayer)
		if isTimer(freezTimer) then killTimer(freezTimer) end
		freezTimer = setTimer(function() setElementFrozen(localPlayer, false) end, 2500, 1)
		spc = false
      end
end

ShopMarkersTable = {}	

local ShopTable = {
	[1] = {ID = {{507, 6400000} -- мерсы и бехи
		,{400, 7300000}
		,{579, 12100000}
		,{415, 13000000}
		}, vPosX = 806.02, vPosY = -2095.83, vPosZ = 12.9, PosX = 842.21, PosY = -2096.49, PosZ = 11.4, CamX = 800, CamY = -2085.93, CamZ = 15, lookAtX = 808, lookAtY = -2100, lookAtZ = 15},
	[2] = {ID = {{473, 150000} -- салон лодок
		,{453, 1750000}
		,{493, 4950000}
		,{484, 6500000}
		,{454, 10750000}
		}, vPosX = 215, vPosY = -1947, vPosZ = 4, PosX = 164, PosY = -1924.4, PosZ = 0.25, CamX = 190, CamY = -1975, CamZ = 6, lookAtX = 199, lookAtY = -1966, lookAtZ = 6},
	[3] = {ID = {{579, 11000000} -- Автосалон лс у банка
		,{507, 6500000}
		}, vPosX = 1942.5, vPosY = 2052, vPosZ = 11, PosX = 1946, PosY = 2068, PosZ = 10, CamX = 1930.36, CamY = 2052.78, CamZ = 14.71, lookAtX = 1931.36, lookAtY = 2052.78, lookAtZ = 14.43},
	[4] = {ID = {{489, 2100000} -- около вокзала сф
                ,{602, 150000}
                ,{505, 5600000}
		,{429, 2100000}
		,{585, 75000000}
		,{404, 3101000}
		,{558, 25000000}
		,{451, 85000000}
                ,{529, 45000}
                ,{445, 93500}
                ,{516, 138000}
                ,{547, 450000}
                ,{470, 4500000}
                ,{436, 4900000}
                ,{589, 132000}
                ,{540, 1450000}
                ,{479, 3890000}
                ,{409, 814000}
                ,{466, 1450000}
		}, vPosX = -1950, vPosY = 266, vPosZ = 36.2, PosX = -1954, PosY = 299, PosZ = 34, CamX = -1960.18, CamY = 266.06, CamZ = 37.94, lookAtX = -1959.2, lookAtY = 266.06, lookAtZ = 37.73},
	[5] = {ID = {{477, 700000} -- около причала
		,{496, 850000}
		,{438, 950000}
		,{429, 4200000}
		,{587, 13000000}
		}, vPosX = -1660, vPosY = 1213, vPosZ = 7, PosX = -1634, PosY = 1199, PosZ = 6, CamX = -1648.9, CamY = 1212.27, CamZ = 10.16, lookAtX = -1649.88, lookAtY = 1212.27, lookAtZ = 9.94},
	[6] = {ID = {{429, 4200000} -- салон на крыше (vip)
		,{480, 6000000}
		,{558, 23000000}
		,{585, 35000000}
		,{451, 60000000}
		}, vPosX = 2322, vPosY = 1493, vPosZ = 43, PosX = 2346, PosY = 1518, PosZ = 41.89, CamX = 2335, CamY = 1501, CamZ = 43.5, lookAtX = 1, lookAtY = 1, lookAtZ = 1},
}

VehicleShop_Window = guiCreateWindow(screX-350,screY-450, 343, 436, "Автосалон", false)
guiSetVisible(VehicleShop_Window, false)
guiWindowSetSizable(VehicleShop_Window , false)
guiSetAlpha(VehicleShop_Window, 0.8)
carGrid = guiCreateGridList(9, 20, 324, 329, false, VehicleShop_Window)
guiGridListSetSelectionMode(carGrid, 0)
carColumn = guiGridListAddColumn(carGrid, "Транспорт", 0.5)
costColumn = guiGridListAddColumn(carGrid, "$", 0.4)
carButton = guiCreateButton(14, 355, 86, 56,"Купить", false, VehicleShop_Window)
closeButton = guiCreateButton(237, 355, 86, 56, "Закрыть", false, VehicleShop_Window)
carColorButton = guiCreateButton(128, 355, 86, 56,"Выбрать цвет", false, VehicleShop_Window)
guiSetProperty(carButton, "NormalTextColour", "FF069AF8")
guiSetProperty(closeButton, "NormalTextColour", "FF069AF8")
guiSetProperty(carColorButton, "NormalTextColour", "FF069AF8")  





for i, M in ipairs(ShopTable) do
	ShopMarker = createMarker(M["PosX"], M["PosY"], M["PosZ"], "cylinder", 2, 38, 122, 216)
	ShopMarkerShader = createMarker(M["PosX"], M["PosY"], M["PosZ"], "cylinder", 2, 38, 122, 216)
	ShopMarkersTable[ShopMarker] = true
	setElementData ( ShopMarker, "shopID", i )
	setElementID(ShopMarker, tostring(i))
	createBlipAttachedTo(ShopMarker, 55, 2, 255, 255, 255, 255, 0, 400)
end

function getVehicleModelFromNewName (name)
	for i,v in pairs ( customCarNames ) do
		if v == name then
			return i
		end
	end
	return false
end

addEventHandler("onClientGUIClick", resourceRoot,
function()
	if (source == carGrid) then
		local carName = guiGridListGetItemText(carGrid, guiGridListGetSelectedItem(carGrid), 1)
		local carprice = guiGridListGetItemText(carGrid, guiGridListGetSelectedItem(carGrid), 2)
		if guiGridListGetSelectedItem(carGrid) ~= -1 then
			guiSetText(CarName, carName)
			guiSetText(CarPrice, "$"..carprice)
			local carID = getVehicleModelFromNewName(carName) or getVehicleModelFromName(carName)
			if isElement(veh) then
				setElementModel(veh, carID)
			return end
			veh = createVehicle(carID, ShopTable[i]["vPosX"], ShopTable[i]["vPosY"], ShopTable[i]["vPosZ"])
			setVehicleDamageProof(veh, true)
			setElementFrozen(veh, true)
			setVehicleColor(veh, r1, g1, b1, r2, g2, b2)
			timer = setTimer(function() local x, y, z = getElementRotation(veh) setElementRotation(veh, x, y, z+3) end, 50, 0)
		else
			guiSetText(CarName, "Noun")
			guiSetText(CarPrice, "Noun")
			r1, g1, b1, r2, g2, b2 = math.random(0, 255), math.random(0, 255), math.random(0, 255), math.random(0, 255), math.random(0, 255), math.random(0, 255)
			if isElement(veh) then
				destroyElement(veh)
			end
			if isTimer(timer) then
				killTimer(timer)
			end
		end
		elseif (source == carColorButton) then
		openColorPicker()
	elseif (source == carButton) then
		if guiGridListGetSelectedItem(carGrid) then
			local carName = guiGridListGetItemText(carGrid, guiGridListGetSelectedItem(carGrid), 1)
			local carID = getVehicleModelFromNewName(carName) or getVehicleModelFromName(carName)
			local carCost = guiGridListGetItemText (carGrid, guiGridListGetSelectedItem(carGrid), 2)
			local r1, g1, b1, r2, g2, b2 = getVehicleColor(veh, true)
			triggerServerEvent("onBuyNewVehicle", localPlayer, carID, carCost, r1, g1, b1, r2, g2, b2)
			outputChatBox("[Автосалон] #FFFF00Вы купили: #00FFFF"..carName, 38, 122, 216, true)
			guiSetVisible(VehicleShop_Window, false)
			showCursor(false)
			setElementFrozen(localPlayer, false)
			fadeCamera(false, 1.0)
			setTimer(function() fadeCamera(true, 0.5) setCameraTarget(localPlayer) end, 1000, 1)
			if isElement(veh) then
				destroyElement(veh)
			end
			if isTimer(timer) then
				killTimer(timer)
			end
		end
	elseif (source == closeButton) then
		if guiGetVisible(VehicleShop_Window) then 
			guiSetVisible(VehicleShop_Window, false)
			showCursor(false)
			setElementFrozen(localPlayer, false)
			fadeCamera(false, 1.0)
			setElementData ( localPlayer, "atVehShop", false)
			setTimer(function() fadeCamera(true, 0.5) setCameraTarget(localPlayer) end, 1000, 1)
			if isElement(veh) then
				destroyElement(veh)
			end
			if isTimer(timer) then
				killTimer(timer)
			end
		end
	end
end)

function openColorPicker()
	if (colorPicker.isSelectOpen) or not isElement(veh) then return end
	colorPicker.openSelect(colors)
end

function closedColorPicker()
end

function updateColor()
	if (not colorPicker.isSelectOpen) then return end
	local r, g, b = colorPicker.updateTempColors()
	if (veh and isElement(veh)) then
		r1, g1, b1, r2, g2, b2 = getVehicleColor(veh, true)
		if (guiCheckBoxGetSelected(checkColor1)) then
			r1, g1, b1 = r, g, b
		end
		if (guiCheckBoxGetSelected(checkColor2)) then
			r2, g2, b2 = r, g, b
		end
		setVehicleColor(veh, r1, g1, b1, r2, g2, b2)
	end
end
addEventHandler("onClientRender", root, updateColor)

--[[addCommandHandler("xx", function()
	local x, y, z, lx, ly, lz = getCameraMatrix()
	setCameraMatrix(x, y, z, lx, ly, lz)
	outputChatBox(x..", "..y..", "..z..", "..lx..", "..ly..", "..z)
end)]]

addEventHandler("onClientMarkerHit", resourceRoot,
function(player)
	if getElementType(player) ~= "player" or player ~= localPlayer or isPedInVehicle(player) then return end
	if ShopMarkersTable[source] then
		i = tonumber(getElementID(source))
		guiGridListClear(carGrid)
		for i, v in ipairs(ShopTable[i]["ID"]) do
			local carName = customCarNames[ v[1] ] or getVehicleNameFromModel(v[1])
			local row = guiGridListAddRow(carGrid)
			guiGridListSetItemText(carGrid, row, 1, carName, false, true)
			guiGridListSetItemText(carGrid, row, 2, tostring(v[2]), false, true)
		end
		setCameraMatrix(ShopTable[i]["CamX"], ShopTable[i]["CamY"], ShopTable[i]["CamZ"], ShopTable[i]["lookAtX"], ShopTable[i]["lookAtY"], ShopTable[i]["lookAtZ"])
		guiSetVisible(VehicleShop_Window, true)
		showCursor(true)
		setElementData ( player, "atVehShop", getElementData ( source, "shopID"))
		guiGridListSetSelectedItem(carGrid, 0, 1)
		setTimer(function()
			setElementFrozen(localPlayer, true)
			local carName = guiGridListGetItemText(carGrid, 0, 1)
			local carID = getVehicleModelFromNewName(carName) or getVehicleModelFromName(carName)
			local x, y, z = ShopTable[i]["vPosX"], ShopTable[i]["vPosY"], ShopTable[i]["vPosZ"]
			if isElement(veh) then
				destroyElement(veh)
			end
			if isTimer(timer) then
				killTimer(timer)
			end
			r1, g1, b1, r2, g2, b2 = math.random(0, 255), math.random(0, 255), math.random(0, 255), math.random(0, 255), math.random(0, 255), math.random(0, 255)
			veh = createVehicle(carID, x, y, z)
			setVehicleDamageProof(veh, true)
			setElementFrozen(veh, true)
			setVehicleColor(veh, r1, g1, b1, r2, g2, b2)
			timer = setTimer(function() local x, y, z = getElementRotation(veh) setElementRotation(veh, x, y, z+3) end, 50, 0)
		end, 100, 1)
	end
end)

addEventHandler("onClientRender", getRootElement(), function()
	for index, car in pairs(getElementsByType('vehicle')) do
        if car:getData("sellInfo") then
			local sellInfo = car:getData("sellInfo")
			local x, y, z = getElementPosition(car)
			local x2, y2, z2 = getElementPosition(localPlayer)
			z = z+1
			local sx, sy = getScreenFromWorldPosition(x, y, z)
			if(sx) and (sy) then
				local distance = getDistanceBetweenPoints3D(x, y, z, x2, y2, z2)
				if(distance < 20) then
					local fontbig = 2-(distance/10)
					dxDrawBorderedCarText("Продается за $" .. tostring(sellInfo.price), sx, sy, sx, sy, tocolor(0, 255, 0, 200), fontbig, "default-bold", "center")	
				end
			end
		end
	end
end)

function dxDrawBorderedCarText( text, x, y, w, h, color, scale, font, alignX, alignY, clip, wordBreak, postGUI )
	dxDrawText ( text, x - 1, y - 1, w - 1, h - 1, tocolor ( 0, 0, 0, 255 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x + 1, y - 1, w + 1, h - 1, tocolor ( 0, 0, 0, 255 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x - 1, y + 1, w - 1, h + 1, tocolor ( 0, 0, 0, 255 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x + 1, y + 1, w + 1, h + 1, tocolor ( 0, 0, 0, 255 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x - 1, y, w - 1, h, tocolor ( 0, 0, 0, 255 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x + 1, y, w + 1, h, tocolor ( 0, 0, 0, 255 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x, y - 1, w, h - 1, tocolor ( 0, 0, 0, 255 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x, y + 1, w, h + 1, tocolor ( 0, 0, 0, 255 ), scale, font, alignX, alignY, clip, wordBreak, false )
	dxDrawText ( text, x, y, w, h, color, scale, font, alignX, alignY, clip, wordBreak, postGUI )
end

local sellGUI = {}
sellGUI.window = guiCreateWindow(155,60,310,120,"Внимание!",false)
guiSetVisible(sellGUI.window, false)
guiSetProperty(sellGUI.window, "AlwaysOnTop", "true")
guiWindowSetSizable(sellGUI.window, false)
sellGUI.label = guiCreateLabel(21,28,266,36,"",false,sellGUI.window)
guiLabelSetColor(sellGUI.label, 38, 122, 216)
guiLabelSetHorizontalAlign(sellGUI.label,"center",true)
sellGUI.yes = guiCreateButton(17,73,129,36,"Да",false,sellGUI.window)
sellGUI.no = guiCreateButton(161,73,129,36,"Нет",false,sellGUI.window)

addEvent("showBuyGUI", true)
addEventHandler("showBuyGUI", root, function ()
	local sellInfo = source:getData("sellInfo") 
	if type(sellInfo) ~= "table" then
		return
	end
	showCursor(true)
	sellGUI.window.visible = true
	sellGUI.label.text = "Купить этот автомобиль за $" .. tostring(sellInfo.price) .. "?"
end)


addEventHandler("onClientGUIClick", sellGUI.window, function ()
	showCursor(false)
	sellGUI.window.visible = false	
	if source == sellGUI.yes then
		Button_VS_sl.enabled = true
		local sellInfo = localPlayer.vehicle:getData("sellInfo") 
		if type(sellInfo) ~= "table" then
			return
		end			
		local r1, g1, b1, r2, g2, b2 = getVehicleColor(localPlayer.vehicle, true)
		triggerServerEvent("onBuyNewVehicle", localPlayer, 
			localPlayer.vehicle.model, 
			sellInfo.price, 
			r1, g1, b1, r2, g2, b2,
			true
		)
	else
		setControlState("enter_exit", true)
	end
end)
