﻿addEvent("openinf_kop", true)
screenWidth, screenHeight = guiGetScreenSize()
function windm ()
	if not getElementData ( localPlayer, "kop_working") then
		  infowind = guiCreateWindow(screenWidth - 700, screenHeight - 500, 300, 359, "Работа гангстера", false)
		  infotext = guiCreateMemo(25,165,250,80,"Ваша задача осматривать районы. Для этого вам понадобится ваша собственная машина - 3000руб за каждый район.",false,infowind)
		Logo = guiCreateStaticImage( 0.10, 0.10, 0.80, 0.3, "logo.png", true,infowind)
		guiSetFont(whatLabelQ, "default-bold-small")
		  
		  guiMemoSetReadOnly( infotext, true )
		  --guiSetFont(text2, "default-bold-small") 
		  showCursor(true)
		  guiSetVisible ( infowind , true) 
		  guiWindowSetSizable(infowind, false) 
		  Button_Glose = guiCreateButton(25, 300, 250, 45, "Закрыть", false, infowind)
		  Button_Start = guiCreateButton(25, 250, 250, 45, "Начать", false, infowind)
		 
		  addEventHandler("onClientGUIClick", Button_Start, kit )
			addEventHandler("onClientGUIClick", Button_Glose, noshow )
	else
		for i, v in ipairs ( createdPickups ) do
			if isElement ( v ) then
				local pick2 = getElementData ( v, "pick2")
				if isElement ( pick2 ) then
					destroyElement ( pick2 )
				end
				local icon = getElementData ( v, "icon" )
				if isElement ( icon ) then
					destroyElement ( icon )
				end
				destroyElement ( v )
			end
		end
		triggerServerEvent ( "finitoWork_kop", localPlayer )
		setElementData ( localPlayer, "kop_working", false )
		outputChatBox ( "Вы закончили работу", 255, 255, 255, true )
	end
end
addEventHandler("openinf_kop", root, windm )
function noshow ()
 if ( source == Button_Glose ) then
   destroyElement ( infowind )
   showCursor ( false ) 
 end
end
function kit ()
 if ( source == Button_Start ) then
	startWorking()
    destroyElement ( infowind )
    showCursor ( false )
  end 
end

addEvent ( "pay", true )
function giving ()
 outputChatBox ( "Вы собрали мусор и получили - 50$ ", getRootElement(), 255, 100, 100, true )
end
addEventHandler("pay", resourceRoot, giving )
addEvent ( "get", true )
function peremen (plr)
 plr = getLocalPlayer ()
end
addEventHandler("get", resourceRoot, peremen )

pickupSpawns = {
	{ -1878,735,45 },
    { -1894,903,35 },
    { -1881,1161,45 },
    { -1627,1184,7 },
	{ -1563,949,7 },
	{ -1564,808,7 },
		
}

max_pickups_set = 6 

createdPickups = {}

max_pickups = max_pickups_set
if max_pickups_set > #pickupSpawns then
	max_pickups = #pickupSpawns
end

function startWorking ()
	if not getElementData ( localPlayer, "kop_working" ) then
		createdPickups = {}
		triggerServerEvent ( "picku_kop", localPlayer )
		setElementData ( localPlayer, "kop_working", true )
		setElementData ( localPlayer, "kop_working_hit", 0 )
			outputChatBox ( "Теперь вы гангстер, садитесь в тачку\nА затем езжайте на красный крест на радаре!", 255, 255, 255, true )		
				
		for i, v in ipairs ( pickupSpawns ) do
			local pick = createMarker ( v[1],v[2],v[3], "checkpoint", 4.0, 0, 0, 255 )
			local pick2 = createPickup ( v[1],v[2],v[3], 3, 1314, 10000 )
			setElementData ( pick, "taken", false )
			setElementData ( pick, "pick2", pick2 )
			setElementData ( pick, "kop_pickup", true )
			setElementData ( pick, "id", i )
			if i == 1 then
				local icon = createBlipAttachedTo ( pick, 41, 2 )
				setElementData ( pick, "icon", icon )
			end
			table.insert (createdPickups, pick)
		end
	end
end	

addEventHandler("onClientVehicleExit", getRootElement(),
    function(thePlayer, seat)
        if thePlayer == getLocalPlayer() then
			if getElementData ( localPlayer, "kop_working" ) then
				for i, v in ipairs ( createdPickups ) do
					if isElement ( v ) then
						local pick2 = getElementData ( v, "pick2")
						if isElement ( pick2 ) then
							destroyElement ( pick2 )
						end
						local icon = getElementData ( v, "icon" )
						if isElement ( icon ) then
							destroyElement ( icon )
						end
						destroyElement ( v )
					end
				end
				triggerServerEvent ( "finitoWork_kop", localPlayer )
				setElementData ( localPlayer, "kop_working", false )
				outputChatBox ( "Вы закончили работу", 255, 255, 255, true )
			end
        end
    end
)

function clientPickupHit(thePlayer, matchingDimension)
	if thePlayer == localPlayer and getElementData ( source, "kop_pickup" ) and not getElementData ( source, "taken" ) then
		setElementData ( source, "taken", true )
					setElementVelocity ( getPedOccupiedVehicle(localPlayer), 0, 0, 0 )
					toggleAllControls ( false, true, false )
					setTimer ( finishBoarding, 5000, 1 )
					outputChatBox("Подождите,идёт осмотр территории", 0,153,51)
					setElementData ( localPlayer, "kop_pickup", 2 )
		local hitted = getElementData ( localPlayer, "kop_working_hit" ) or 0
		hitted = hitted+1
		local pick2 = getElementData ( source, "pick2" )
		if isElement ( pick2 ) then
			destroyElement ( pick2 )
		end
		local icon = getElementData ( source, "icon" )
		if isElement ( icon ) then
			destroyElement ( icon )
		end
		destroyElement(source)
		setElementData ( localPlayer, "kop_working_hit", hitted)
		if hitted >= max_pickups then
			for i, v in ipairs ( createdPickups ) do
				if isElement ( v ) then
					local pick2 = getElementData ( v, "pick2")
					if isElement ( pick2 ) then
						destroyElement ( pick2 )
					end
					local icon = getElementData ( v, "icon" )
					if isElement ( icon ) then
						destroyElement ( icon )
					end
					destroyElement ( v )
				end
			end
			setElementData ( localPlayer, "kop_working", false )
			triggerServerEvent ( "finitoWork_kop", localPlayer )
			outputChatBox ( "Вы закончили работу,подождите", 255, 255, 255, true )
			return true
		end
		if createdPickups[hitted+1] then
			local icon = createBlipAttachedTo ( createdPickups[hitted+1], 41, 2 )
			setElementData ( createdPickups[hitted+1], "icon", icon )
		end
	end
end

function finishBoarding ()
	toggleAllControls ( true, true, true )
			outputChatBox ( "Вы проверили район. Местные пацаны заплатили вам - 1000$ ", 255, 100, 100, true )
			exports [ "urovni" ]:givePlayerXP (localPlayer, 20) 
			triggerServerEvent ( "giveMoneyFromClient_kop", localPlayer, 1000 ) 
				setElementPosition ( trailer, bases[getElementData ( localPlayer, "nearestBase" )][1], bases[getElementData ( localPlayer, "nearestBase" )][2], bases[getElementData ( localPlayer, "nearestBase" )][3] )
end
addEventHandler ( "onClientMarkerHit", getRootElement(), clientPickupHit )







