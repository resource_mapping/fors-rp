function switchLights(veh)
	if veh then
		local cur = getElementData(veh,"lights")
		if cur == 0 then
			setElementData(veh,"lights",1)
		elseif cur == 1 then
			setElementData(veh,"lights",2)
		elseif cur == 2 then
			setElementData(veh,"lights",0)
		else
			setElementData(veh,"lights",0)
		end
	end
end
addEvent("switchLights",true)
addEventHandler("switchLights",root,switchLights)
