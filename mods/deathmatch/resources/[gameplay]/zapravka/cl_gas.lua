local fuelWindow = nil

for k,v in pairs(fuelMarkers) do
	local marker = createMarker(v[1],v[2],v[3],"corona",3,200,100,0,150)
	--local blip = createBlipAttachedTo(marker,blipID)
	setBlipVisibleDistance( blip, 200 )
	setElementData(marker,"fuelstation",true)
end

addEventHandler ( "onClientMarkerHit", getRootElement(), function(ply)
	if ply == localPlayer then
		if getElementData(source,"fuelstation") then
			local veh = getPedOccupiedVehicle(ply)
			if veh and getVehicleController(veh) == ply	then
				initFuelWindow()
			end
		end
	end
end)

local scx,scy = guiGetScreenSize()
local px = scx/1920
if px <= 0.8 then px = 0.8 end

local sizeX,sizeY = 700*px,600*px
local posX,posY = scx/2-sizeX/2,scy/2-sizeY/2

local function isCursorOverRectangle(x,y,w,h)
	if isCursorShowing() then
		local mx,my = getCursorPosition() -- relative
		local cursorx,cursory = mx*scx,my*scy
		if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
			return true
		end
	end
return false
end

local fuelWindowShown = false
local selection = 5
local font1 = dxCreateFont("files/font.otf",14*px)
local font2 = dxCreateFont("files/font.otf",12*px)
local font3 = dxCreateFont("files/font.otf",25*px)
local scrollX = 0
local fuelAmount = 0
local click = false
local fillingNow = false

function initFuelWindow()
	local veh = getPedOccupiedVehicle( localPlayer )
	if fuelWindowShown then
		removeEventHandler("onClientRender",root,drawFuelWindow)
		showCursor(false)
		setElementFrozen(veh,false)
	else
		addEventHandler("onClientRender",root,drawFuelWindow)
		showCursor(true)
		setElementFrozen(veh,true)
	end
	fuelWindowShown = not fuelWindowShown
end
addEvent("initFuelWindow",true)
addEventHandler("initFuelWindow",root,initFuelWindow)





function drawFuelWindow()
	--showCursor(true)
	local veh = getPedOccupiedVehicle( localPlayer )
	local current = math.floor(getElementData(veh,"fuel"))
	local max = getElementData(veh,"maxFuel")
	
	dxDrawImage ( posX-2*px,posY-32*px,sizeX+4*px,sizeY+34*px, 'files/window.png', angle, 0, -120 )

	
	if isCursorOverRectangle(posX+620*px,posY-30*px,50*px,50*px) then
	
	dxDrawImage (posX+620*px,posY-30*px,50*px,50*px, 'files/close.png', angle, 0, -120 )
	
		if getKeyState("mouse1") and not click then
			initFuelWindow()
		end
	else
	
	dxDrawImage (posX+620*px,posY-30*px,50*px,50*px, 'files/closea.png', angle, 0, -120 )
	end
	


	dxDrawText(current.."/"..max.." Литров ",posX+300*px,posY+740*px,posX+550*px,posY+130*px,tocolor(95,95,95),1.2,font1,"center","center")--Количество



	local lx,ly = posX+265*px,posY+265*px
	local side = "left"
	for k,v in pairs(fuelTypes) do
		if isCorrectFuel(getElementModel(veh),k) then
			dxDrawImage(lx,ly,180*px,60*px,"files/rbUnactive.png")
		else
			dxDrawImage(lx,ly,180*px,60*px,"files/rbUnactive.png")
		end
		if selection == k then
			dxDrawImage(lx,ly,183*px,63*px,"files/rbActive.png")
		end
		if side == "left" then
			dxDrawText(v[1],lx+60*px,ly,lx,ly+63*px,tocolor(255,255,255),1.1,font1,side,"center")

		else
			dxDrawText(v[1],lx,ly,lx+110*px,ly+63*px,tocolor(255,255,255),1.1,font1,side,"center")
			
		end

		if isCursorOverRectangle(lx-20*px,ly,180*px,60*px) then
			if getKeyState("mouse1") and not click then
				if isCorrectFuel(getElementModel(veh),k) then
					selection = k
				end
			end
		end

		ly = ly + 70*px
		if k == 2 then
			side = "right"
			lx = posX+470*px
			ly = posY+265*px
		end
	end

		dxDrawRectangle(posX+300*px,posY+460*px,300*px,10*px,tocolor(95,95,95))--полоска заправки

	if isCursorOverRectangle(posX+290*px,posY+450*px,320*px,30*px) then
		if getKeyState("mouse1") and not fillingNow then
			local cx,cy = getCursorPosition()
			local mx,my = cx*scx,cy*scy
			scrollX = mx-posX-302.5*px
			--outputChatBox(scrollX)
			if scrollX >= 295*px then scrollX = 295*px
			elseif scrollX <= 0 then scrollX = 0 end
			fuelAmount = math.floor(scrollX/295*px*(max-current))
			if fuelAmount <= 0 then fuelAmount = 0 end
		end
	end

    dxDrawRectangle(posX+300*px+scrollX,posY+455*px,5*px,20*px,tocolor(159,36,22))

	dxDrawText(fuelAmount.." л",posX+850*px,posY+410*px,posX+70*px,posY+580*px,tocolor(95,95,95),1.1,font1,"center","center")

	sum = math.floor(fuelAmount*fuelTypes[selection][2])

	if isCursorOverRectangle(posX+240*px,posY+520*px,200*px,50*px) then
	dxDrawImage (posX+240*px,posY+520*px,200*px,50*px, 'files/buy.png', angle, 0, -120 )
		dxDrawText(""..sum.." руб. ",posX+600*px,posY+545*px,posX+60*px,posY+570*px,tocolor(255,255,255),1,font1,"center","center")--Оплатить
		if getKeyState("mouse1") and not click then
			if getPlayerMoney(localPlayer) >= sum then
				if isCorrectFuel(getElementModel(veh),selection) then
					fillVeh(fuelAmount)
				else
					outputChatBox("Выбран некорректный вид топлива!",200,50,50)
				end
			else
				outputChatBox("Недостаточно денег!",200,50,50)
			end
		end
	else
		dxDrawImage (posX+240*px,posY+520*px,200*px,50*px, 'files/buya.png', angle, 0, -120 )
		dxDrawText(""..sum.." руб. ",posX+600*px,posY+545*px,posX+60*px,posY+570*px,tocolor(255,255,255),1,font1,"center","center")--цена
	end

	if getKeyState("mouse1") or fillingNow then click = true else click = false end

end
--initFuelWindow()

local toFill = 0

function fillVeh(amount)
	local amount = tonumber(amount) or 0
	local veh = getPedOccupiedVehicle(localPlayer)
	toFill = getElementData(veh,"fuel")+amount
	addEventHandler("onClientPreRender",root,filling)
	fillingNow = true
end

function filling()
	local veh = getPedOccupiedVehicle(localPlayer)
	local current = getElementData(veh,"fuel")
	if current < toFill then
		setElementData(veh,"fuel",current+0.5,false)
	else
		triggerServerEvent("fillVeh2",localPlayer,veh,selection,fuelAmount)
		removeEventHandler("onClientPreRender",root,filling)
		toFill = 0
		fillingNow = false
		fuelAmount = 0
	end
end

function checkFuel()
	local veh = getPedOccupiedVehicle(localPlayer)
	if veh and getVehicleController(veh) == localPlayer then
		if not getElementData(veh,"fuel") then return end
		if getElementData(veh,"fuel") <= 1 then
			setVehicleEngineState(veh,false)
		end
	end
end
addEventHandler('onClientPreRender',root,checkFuel)

