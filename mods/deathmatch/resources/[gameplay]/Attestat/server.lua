﻿function takeMoneyEducation(player)
	if tonumber(getPlayerMoney(player)) >= tonumber(moneyEducation) then
		setElementData(player, "Education", true)
		takePlayerMoney(player, moneyEducation)
		outputChatBox("#00FF00[Обучение] #FFFFFFВы успешно начали обучение. #00FF00"..moneyEducation.." #FFFFFFруб. списано с вашего счета.", player, 255, 255, 255, true)
	else
		outputChatBox("#FF0000[Обучение] #FFFFFFУ вас недостаточно денег.", player, 255, 255, 255, true)
	end
end
addEvent("takeMoneyEducation", true)
addEventHandler("takeMoneyEducation", getRootElement(), takeMoneyEducation)

function FinishEducation(player)
	setElementData(player, "Education", false)
	setElementData(player, "CertificateProcess", nil)
	setElementData(player, "mathematics", false)
	setElementData(player, "Russianlanguage", false)
	setElementData(player, "Physics", false)
	setElementData(player, "Biology", false)
	setElementData(player, "Certificate", true) -- Получение аттестата.
	outputChatBox("#00FF00[Обучение] #FFFFFFВы успешно прошли весь курс обучения и получили аттестат.", player, 255, 255, 255, true)
end
addEvent("FinishEducation", true)
addEventHandler("FinishEducation", getRootElement(), FinishEducation)

addEventHandler("onPlayerQuit", getRootElement(), 
    function ()
	    local account = getPlayerAccount(source)
	    setAccountData(account, "Education", getElementData(source, "Education"))
		setAccountData(account, "CertificateProcess", getElementData(source, "CertificateProcess"))
        setAccountData(account, "mathematics", getElementData(source, "mathematics"))
        setAccountData(account, "Russianlanguage", getElementData(source, "Russianlanguage"))
		setAccountData(account, "Physics", getElementData(source, "Physics"))
        setAccountData(account, "Biology", getElementData(source, "Biology"))
        setAccountData(account, "Certificate", getElementData(source, "Certificate"))
	end
)

addEventHandler("onPlayerLogin", getRootElement(), 
    function (_,account)
	    setElementData(source,"isPlayerLogin", true)
        setElementData(source, "Education", (getAccountData(account, "Education") or false))
		setElementData(source, "CertificateProcess", (getAccountData(account, "CertificateProcess") or 0))
        setElementData(source, "mathematics", (getAccountData(account, "mathematics") or false))
        setElementData(source, "Russianlanguage", (getAccountData(account, "Russianlanguage") or false))
		setElementData(source, "Physics", (getAccountData(account, "Physics") or false))
		setElementData(source, "Biology", (getAccountData(account, "Biology") or false))
		setElementData(source, "Certificate", (getAccountData(account, "Certificate") or false))
	end
)