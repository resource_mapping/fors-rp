db = dbConnect("sqlite","db.db")
if db then
	outputDebugString(getResourceName(getThisResource()).." подключился к базе данных")
end

function getPlayerLogin(player)
	local acc = getPlayerAccount(player)
	if acc then 
		local accName = getAccountName(acc)
		if accName then 
			return accName
		else
			return false
		end
	else
		return false
	end
end

function isVehicleExist(ID)
	assert(ID and type(ID) == "number", "Неверный ID @isVehicleExist")
	local result = dbPoll(dbQuery(db,"SELECT * FROM cars WHERE ID = ?",ID),-1)
	if #result > 0 then 
		return true
	else
		return false
	end
end

function getPlayerCars(login)
	assert(login and type(login) == "string", "Неверный логин @getPlayerCars")
	local query = dbQuery(db,"SELECT * FROM cars WHERE Login = ?",login)
	local poll = dbPoll(query,-1);
	if #poll > 0 then 
		return poll;
	else
		return false
	end
end

function getCarInfo(ID)
	assert(ID and type(ID) == "number", "Неверный ID @getCarInfo")
	local result = dbPoll(dbQuery(db,"SELECT * FROM cars WHERE ID = ?",ID),-1)
	if #result > 0 then 
		return result
	else
		return false
	end
end

function addPlayerCar(ID,login)
	assert(ID and type(ID) == "number", "Неверный ID @addPlayerCar")
	assert(login and type(login) == "string","Неверный логин @addPlayerCar")
	local color = {math.random(0,255),math.random(0,255),math.random(0,255),math.random(0,255),math.random(0,255),math.random(0,255)}
	local Handling = getModelHandling(ID)
	local exec = dbExec(db,"INSERT INTO cars(Login,carID,Color,Handling) values(?,?,?,?)",login,ID,toJSON(color),toJSON(Handling))
	if exec then 
		return true 
	else
		return false
	end
end

function updatePlayerCar(ID)
	assert(ID and type(ID) == "number", "Неверный ID @updatePlayerCar")
	local vehicle = isCarSpawned(ID)
	if vehicle then 
		local twelveColors = {getVehicleColor(vehicle)}
		local color = {twelveColors[1],twelveColors[2],twelveColors[3],twelveColors[4],twelveColors[5],twelveColors[6]}
		local Handling = getVehicleHandling(vehicle)
		local exec = dbExec(db,"UPDATE cars SET Color = ?, Handling = ? WHERE ID = ?",toJSON(color),toJSON(Handling),ID)
		if exec then 
			return true 
		else
			return false
		end
	else
		return false
	end
end

function deletePlayerCar(ID,login)
	assert(ID and type(ID) == "number", "Неверный ID @deletePlayerCar")
	assert(login and type(login) == "string","Неверный логин @deletePlayerCar")
	if isVehicleExist(ID) then 
		local exec = dbExec(db,"DELETE FROM cars WHERE Login = ? AND ID = ?",login,ID)
		if exec then 
			return true
		else
			return false
		end
	else
		return false
	end
end

function setVehicleHandlingTable(vehicle,table)
	assert(vehicle and isElement(vehicle) and getElementType(vehicle) == "vehicle","неверный аргумент с автомобилем @setVehicleHandlingTable")
	assert(table and type(table) == "table","Неверная таблица @setVehicleHandlingTable")
	for i,v in pairs(table) do 
		setVehicleHandling(vehicle,i,v)
	end
end

function spawnCar(ID,player)
	assert(ID and type(ID) == "number", "Неверный ID @spawnCar")
	if isVehicleExist(ID) then 
		local info = getCarInfo(ID)
		local x,y,z = getElementPosition(player)
		local vehicle = createVehicle(info[1]["carID"],x,y+5,z)
		warpPedIntoVehicle(player,vehicle)
		setElementData(vehicle,"ID",ID)
		setElementData(vehicle,"Owner",info[1]["Login"])

		local color = fromJSON(info[1]["Color"])
		setVehicleColor(vehicle,color[1],color[2],color[3],color[4],color[5],color[6])

		local Handling = fromJSON(info[1]["Handling"])
		setVehicleHandlingTable(vehicle,Handling)
	else
		return false
	end
end

function destroyCar(ID)
	assert(ID and type(ID) == "number", "Неверный ID @isCarSpawned")
	local vehicle = isCarSpawned(ID)
	if vehicle then 
		updatePlayerCar(ID)
		destroyElement(vehicle)
	else
		return false
	end
end

addEvent('getPlayerCarsByClient',true)
addEventHandler("getPlayerCarsByClient",root,function()
	local login = getPlayerLogin(source)
	if login then 
		local cars = getPlayerCars(login)
		if cars then 
			triggerClientEvent(source,"updatePlayerCarsInPanel",source,cars)
		end
	end
end)

addEvent("destroyCar",true)
addEventHandler("destroyCar",root,destroyCar)

addEvent("spawnCar",true)
addEventHandler("spawnCar",root,spawnCar)

addCommandHandler("addV",function(player,cmd,login,ID)
	local account = getPlayerAccount(player)
	local accName = getAccountName(account)
	if account and accName and isObjectInACLGroup("user."..accName,aclGetGroup("Admin")) then
		if login and ID then 
			if getAccount(login) then
				ID = tonumber(ID)
				if addPlayerCar(ID,login) then
					outputChatBox("#FF00CC[MyCars] #FFFFFFМашина добавлена игроку с логином "..login,player,255,255,255,true)
					local player2 = getAccountPlayer(getAccount(login))
					if player2 then 
						outputChatBox("#FF00CC[MyCars] #FFFFFF Администратор "..getPlayerName(player).." выдал вам личное авто "..getVehNameFromModel(ID),player2,255,255,255,true)
						triggerClientEvent(player2,"updateCars",player2)
					end
				else
					outputChatBox("#FF00CC[MyCars] #FFFFFFНе удалось добавить машину",player,255,255,255,true)
				end
			else
				outputChatBox("#FF00CC[MyCars] #FFFFFFТакого пользователя не существует",player,255,255,255,true)
			end
		else
			outputChatBox("#FF00CC[MyCars] #FFFFFFПравильно /addV [Логин] [ID]",player,255,255,255,true)
		end
	else
		outputChatBox("#FF00CC[MyCars] #FFFFFFУ вас нет доступа к данной команде",player,255,255,255,true)
	end
end)

addCommandHandler("delV",function(player,cmd,login,ID)
	local account = getPlayerAccount(player)
	local accName = getAccountName(account)
	if account and accName and isObjectInACLGroup("user."..accName,aclGetGroup("Admin")) then
		if login and ID then 
			if getAccount(login) then
				ID = tonumber(ID)
				local originalID = getCarInfo(ID)[1]["carID"]
				if deletePlayerCar(ID,login) then 
					outputChatBox("#FF00CC[MyCars] #FFFFFFВы удалили машину игрока "..login,player,255,255,255,true)

					local player2 = getAccountPlayer(getAccount(login))
					if player2 then 

						outputChatBox("#FF00CC[MyCars] #FFFFFF Администратор "..getPlayerName(player).." забрал у вас личное авто "..getVehNameFromModel(originalID),player2,255,255,255,true)
						triggerClientEvent(player2,"updateCars",player2)
					end

					local car = isCarSpawned(ID)
					if car then 
						destroyElement(car)
					end
				else
					outputChatBox("#FF00CC[MyCars] #FFFFFFНе удалось удалить машину",player,255,255,255,true)
				end
			else
				outputChatBox("#FF00CC[MyCars] #FFFFFFТакого пользователя не существует",player,255,255,255,true)
			end
		else
			outputChatBox("#FF00CC[MyCars] #FFFFFFПравильно /addV [Логин] [ID]",player,255,255,255,true)
		end
	else
		outputChatBox("#FF00CC[MyCars] #FFFFFFУ вас нет доступа к данной команде",player,255,255,255,true)
	end
end)