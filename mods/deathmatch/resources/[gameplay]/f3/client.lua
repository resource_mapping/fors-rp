local sw,sh = guiGetScreenSize()
local px,py = sw/1920,sh/1080

local carsWindow = guiCreateWindow(810,290,300,500,"Мои автомобили",false)
local carsGrid = guiCreateGridList(20,40,260,300,false,carsWindow)
local columnID = guiGridListAddColumn( carsGrid, "ID", 0.2 )
local columnName = guiGridListAddColumn( carsGrid, "Название", 0.7 )

local metka = guiCreateButton(20,360,260,30,"Поставить/убрать метку",false,carsWindow)
local carSpawn = guiCreateButton(20,400,260,30,"Заспавнить/убрать",false,carsWindow)
local teleport = guiCreateButton(20,440,260,30,"Тп",false,carsWindow)


--- поставить/убрать метку, заспавнить/убрать, тп 

function detectCars()
	if guiGetVisible(carsWindow) then 
		guiSetVisible(carsWindow,false)
		showCursor(false)
	else
		guiSetVisible(carsWindow,true)
		showCursor(true)
		updateCars()
	end
end
bindKey(keyToOpen,"down",detectCars)
detectCars()

function updateCars()
	triggerServerEvent("getPlayerCarsByClient",localPlayer)
	addEvent("updatePlayerCarsInPanel",true)
	addEventHandler("updatePlayerCarsInPanel",root,function(table)
		guiGridListClear(carsGrid)
		for i,v in ipairs(table) do 
			guiGridListAddRow(carsGrid,v["ID"],getVehNameFromModel( v["carID"] ))
		end
		guiGridListSetSelectedItem(carsGrid,0,1)
	end)
end
addEvent("updateCars",true)
addEventHandler("updateCars",root,updateCars)

addEventHandler("onClientGUIClick",getResourceRootElement(),function(button,state)
	if button == "left" and state == "up" then
		if source == metka or source == carSpawn or source == teleport then 
			local row,column = guiGridListGetSelectedItem(carsGrid)
			if row == -1 or column == -1 then return outputChatBox("#FF00CC[MyCars] #FFFFFFВыберите машину",255,255,255,true) end

			local carID = tonumber(guiGridListGetItemText(carsGrid,row,1))

			local car = isCarSpawned(carID)
			if source == metka then
				if not car then return outputChatBox("#FF00CC[MyCars] #FFFFFFМашина не заспавнена",255,255,255,true) end

				local blip = getElementData(car,"blip")
				if blip then
					destroyElement(getElementData(car,"blip")) 
					setElementData(car,"blip",nil)
					outputChatBox("#FF00CC[MyCars] #FFFFFFМетка убрана",255,255,255,true)
				else
					local blip = createBlipAttachedTo(car,41)
					setElementData(car,"blip",blip)
					outputChatBox("#FF00CC[MyCars] #FFFFFFМетка поставлена",255,255,255,true)
				end
			elseif source == carSpawn then 
				if car then 
					triggerServerEvent("destroyCar",localPlayer,carID)
					outputChatBox("#FF00CC[MyCars] #FFFFFFМашина убрана",255,255,255,true)
				else
					triggerServerEvent("spawnCar",localPlayer,carID,localPlayer)
					outputChatBox("#FF00CC[MyCars] #FFFFFFМашина заспавнена",255,255,255,true)
				end
			elseif source == teleport then
				if not car then return outputChatBox("#FF00CC[MyCars] #FFFFFFМашина не заспавнена",255,255,255,true) end
				local x,y,z = getElementPosition(localPlayer)
				setElementPosition(car,x,y+5,z)
				outputChatBox("#FF00CC[MyCars] #FFFFFFМашина телепортирована",255,255,255,true)
			end
		end
	end
end)