﻿--[[
    -------------
    Русский игровой редактор настроек транспорта для MTASA v1.2 и выше
    ---------

    Новые версии, баги, запросы:
        http://multi-theft-auto-ru.googlecode.com/

    Авторы текущего файла:
        MX_Master ( http://multi-theft-auto.ru/ )

    Легенда по префиксам в названиях переменных:
        b   boolean, булевое
        n   number, число
        s   string, строка
        t   table, таблица
        f   function, функция
        u   userdata, в MTASA этот тип юзают элементы
        th  thread, поток
]]

---
-- НАСТРОЕЧКИ --------------------------
---

-- Название ElementData переменной игрока, которая дает право
-- этому самому игроку юзать этот редактор. Переменная синхрится с клиентом.
-- Эта переменная выставляется игроку, если его аккаунт находится в ACL группе,
-- указанной ниже.
-- По умолчанию: '164986523'
local sPlayerFlagForAccess = '164986523' -- не менять!

-- Таблицы с именами ACL групп, которые дают право юзать редактор.
-- Если аккаунт игрока находится в любой из этих групп, то он может
-- юзать редактор. В группе 'Everyone' находятся все игроки.
-- Допустим, чтобы разрешить редактор админам и модерам, нужно указать
-- {'Admin','SuperModerator','Moderator'}
-- По умолчанию: {'Everyone'}
local tAllowedACLGroups = {'Everyone'}




---
-- ДОБАВЛЕНИЕ / УДАЛЕНИЕ ФЛАГА ДОСТУПА ДЛЯ ИГРОКОВ -------------
---

-- общая функция для добавления/удаления флага доступа
function fSetPlayerAccessFlag ( uPlayer, bSet )
    if type(uPlayer) ~= 'userdata' or getElementType(uPlayer) ~= 'player' then
        return
    end

    if bSet then
        setElementData( uPlayer, sPlayerFlagForAccess, true )
    elseif getElementData( uPlayer, sPlayerFlagForAccess ) then
        removeElementData( uPlayer, sPlayerFlagForAccess )
    end
end


-- если у игрока, есть право доступа, то в его данных добавить
-- соответствующий флажок
function fSetAdminFlagForPlayerIfNeeded ( uPlayer )
    if type(uPlayer) ~= 'userdata' or getElementType(uPlayer) ~= 'player' then
        return
    end

    -- если у игрока уже есть флаг доступа, то ничего не делать
    if getElementData( uPlayer, sPlayerFlagForAccess ) then return end

    -- узнаем аккаунт игрока
    local uAccount = getPlayerAccount(uPlayer)
    if not uAccount then return end

    -- узнает имя объекта аккаунта
    local sObject = 'user.' .. (getAccountName(uAccount) or '')
    local tObjectGroups = {}

    -- узнаем в каких ACL группах есть аккаунт игрока
    for _, uGroup in ipairs( aclGroupList() or {} ) do
        if isObjectInACLGroup( sObject, uGroup ) then
            table.insert( tObjectGroups, uGroup )
        end
    end

    if #tObjectGroups <= 0 then return end

    -- если хотя бы одна из ACL групп аккаунта имеет право юзать редактор
    -- то выставим игроку флаг доступа
    local uGroup = false
    for _, sGroup in ipairs(tAllowedACLGroups) do
        uGroup = aclGetGroup(sGroup)
        for _, uObjectGroup in ipairs(tObjectGroups) do
            if uObjectGroup == uGroup then
                fSetPlayerAccessFlag( uPlayer, true )
                return
            end
        end
    end
end


-- автодобавление флага доступа при логине
function fSetAdminFlagOnPlayerLogin ( uLastAccount, uCurAccount )
    fSetAdminFlagForPlayerIfNeeded( getAccountPlayer(uCurAccount) )
end
addEventHandler( 'onPlayerLogin', root, fSetAdminFlagOnPlayerLogin )


-- автоудаление флага доступа при логауте
function fSetAdminFlagOnPlayerLogout ( )
    -- если в разрешенных ACL группа есть общая, то удалять флаг не надо
    for _, sGroup in ipairs(tAllowedACLGroups) do
        if sGroup == 'Everyone' then
            return
        end
    end

    fSetPlayerAccessFlag( source, nil )
end
addEventHandler( 'onPlayerLogout', root, fSetAdminFlagOnPlayerLogout )


-- автодобавление флага доступа при полном конекте
function fSetAdminFlagOnPlayerJoin ( )
    -- если в разрешенных ACL группа есть общая, то выставить флаг
    for _, sGroup in ipairs(tAllowedACLGroups) do
        if sGroup == 'Everyone' then
            fSetPlayerAccessFlag( source, true )
            return
        end
    end
end
addEventHandler( 'onPlayerJoin', root, fSetAdminFlagOnPlayerJoin )


-- автодобавление флага доступа для онлайн игроков при старте ресурса
function fSetAdminFlagsOnResourceStart ( )
    local bEveryPlayer = false
    for _, sGroup in ipairs(tAllowedACLGroups) do
        if sGroup == 'Everyone' then
            bEveryPlayer = true; break
        end
    end

    if bEveryPlayer then
        for _, uPlayer in ipairs( getElementsByType('player') or {} ) do
            fSetPlayerAccessFlag( uPlayer, true )
        end

        return
    end

    for _, uPlayer in ipairs( getElementsByType('player') or {} ) do
        fSetAdminFlagForPlayerIfNeeded(uPlayer)
    end
end
addEventHandler( 'onResourceStart', resourceRoot, fSetAdminFlagsOnResourceStart )


-- автоудаление флага доступа для онлайн игроков при остановке ресурса
function fSetAdminFlagsOnResourceStop ( )
    for _, uPlayer in ipairs( getElementsByType('player') or {} ) do
        fSetPlayerAccessFlag( uPlayer, nil )
    end
end
addEventHandler( 'onResourceStop', resourceRoot, fSetAdminFlagsOnResourceStop )




---
-- ПРИМЕНЕНИЕ НАСТРОЕК К ТРАНСПОРТУ --
---

function fApplyClientHandlingToVehicle ( tHandling, uVehicle )
    if  type(source) ~= 'userdata' or getElementType(source) ~= 'player' or
        not getElementData( source, sPlayerFlagForAccess ) or
        type(tHandling) ~= 'table' or
        type(uVehicle) ~= 'userdata' or getElementType(uVehicle) ~= 'vehicle'
    then
        return
    end


    -- применим к транспорту только разрешенные настройки
    for sHandlingParameter, value in pairs(tHandling) do
        if tHandlingParametersToSet[sHandlingParameter] then
            setVehicleHandling( uVehicle, sHandlingParameter, value )
        end
    end
end
addEvent( 'applyClientHandlingToVehicle', true )
addEventHandler( 'applyClientHandlingToVehicle', root, fApplyClientHandlingToVehicle )
