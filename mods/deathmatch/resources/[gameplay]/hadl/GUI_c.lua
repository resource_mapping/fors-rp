﻿--[[
    -------------
    Русский игровой редактор настроек транспорта для MTASA v1.2 и выше
    ---------

    Новые версии, баги, запросы:
        http://multi-theft-auto-ru.googlecode.com/

    Авторы текущего файла:
        MX_Master ( http://multi-theft-auto.ru/ )

    Отдельное спасибо:
        R3mp за gui editor
            http://community.mtasa.com/index.php?p=resources&s=details&id=141
            http://forum.mtasa.com/viewtopic.php?f=91&t=22831

    Легенда по префиксам в названиях переменных:
        b   boolean, булевое
        n   number, число
        s   string, строка
        t   table, таблица
        f   function, функция
        u   userdata, в MTASA этот тип юзают элементы
        th  thread, поток
]]

---
-- GUI --
---

tGUI_Window = {}
tGUI_TabPanel = {}
tGUI_Tab = {}
tGUI_Button = {}
tGUI_Label = {}
tGUI_Combo = {}
tGUI_Edit = {}
tGUI_Grid = {}

function fCreateHandlingGUI ( )
    if tGUI_Window[1] then return end

    local tScreenSize = { guiGetScreenSize() }

    tGUI_Window[1] = guiCreateWindow (
        (tScreenSize[1] - 478)/2, (tScreenSize[2] - 360)/2, -- в центре экрана
        478,360,
        fLangStr('mainWindowTitle'),
        false
    )
    guiSetVisible( tGUI_Window[1], false )
    guiSetAlpha(tGUI_Window[1],0.7)

    tGUI_TabPanel[1] = guiCreateTabPanel(10,88,457,230,false,tGUI_Window[1])

    tGUI_Tab[1] = guiCreateTab(fLangStr('tabEngine'),tGUI_TabPanel[1])
    tGUI_Label[1] = guiCreateLabel(11,19,230,19,fLangStr('numberOfGears'),false,tGUI_Tab[1])
    tGUI_Label[2] = guiCreateLabel(11,173,230,19,fLangStr('collisionDamageMultiplier'),false,tGUI_Tab[1])
    tGUI_Label[3] = guiCreateLabel(11,151,230,19,fLangStr('steeringLock'),false,tGUI_Tab[1])
    tGUI_Label[4] = guiCreateLabel(11,129,230,19,fLangStr('engineType'),false,tGUI_Tab[1])
    tGUI_Label[5] = guiCreateLabel(11,107,230,19,fLangStr('driveType'),false,tGUI_Tab[1])
    tGUI_Label[6] = guiCreateLabel(11,85,230,19,fLangStr('engineInertia'),false,tGUI_Tab[1])
    tGUI_Label[7] = guiCreateLabel(11,63,230,19,fLangStr('engineAcceleration'),false,tGUI_Tab[1])
    tGUI_Label[8] = guiCreateLabel(11,41,230,19,fLangStr('maxVelocity'),false,tGUI_Tab[1])
    tGUI_Edit[1] = guiCreateEdit(244,17,92,21,"gears",false,tGUI_Tab[1])
    tGUI_Edit[2] = guiCreateEdit(244,83,92,21,"inertia",false,tGUI_Tab[1])
    tGUI_Edit[3] = guiCreateEdit(244,61,92,21,"acceleration",false,tGUI_Tab[1])
    tGUI_Edit[4] = guiCreateEdit(244,39,92,21,"velocity",false,tGUI_Tab[1])
    tGUI_Edit[5] = guiCreateEdit(244,149,92,21,"steering",false,tGUI_Tab[1])
    tGUI_Edit[6] = guiCreateEdit(244,171,92,21,"collisions",false,tGUI_Tab[1])

    tGUI_Combo[1] = guiCreateComboBox(244,105,92,44+12*3,"driveType",false,tGUI_Tab[1])
    guiComboBoxAddItem( tGUI_Combo[1], fLangStr('fwd') )
    guiComboBoxAddItem( tGUI_Combo[1], fLangStr('rwd') )
    guiComboBoxAddItem( tGUI_Combo[1], fLangStr('awd') )
    guiComboBoxSetSelected( tGUI_Combo[1], 0 )
    setElementData( tGUI_Combo[1], 'itemsCount', 3 )

    tGUI_Combo[2] = guiCreateComboBox(244,127,92,44+12*3,"engineType",false,tGUI_Tab[1])
    guiComboBoxAddItem( tGUI_Combo[2], fLangStr('petrol') )
    guiComboBoxAddItem( tGUI_Combo[2], fLangStr('diesel') )
    guiComboBoxAddItem( tGUI_Combo[2], fLangStr('electric') )
    guiComboBoxSetSelected( tGUI_Combo[2], 0 )
    setElementData( tGUI_Combo[2], 'itemsCount', 3 )

    tGUI_Label[9] = guiCreateLabel(345,41,39,19,fLangStr('kph'),false,tGUI_Tab[1])
    tGUI_Label[10] = guiCreateLabel(345,174,58,19,fLangStr('percents/100'),false,tGUI_Tab[1])
    tGUI_Label[11] = guiCreateLabel(345,152,58,19,fLangStr('degrees'),false,tGUI_Tab[1])
    tGUI_Label[12] = guiCreateLabel(345,64,80,19,fLangStr('m/sec2'),false,tGUI_Tab[1])
    tGUI_Label[13] = guiCreateLabel(345,85,80,19,fLangStr('kg*m2'),false,tGUI_Tab[1])

    tGUI_Tab[2] = guiCreateTab(fLangStr('tabBody'),tGUI_TabPanel[1])
    tGUI_Edit[9] = guiCreateEdit(243,17,92,21,"mass",false,tGUI_Tab[2])
    tGUI_Edit[10] = guiCreateEdit(243,105,92,21,"percSubm",false,tGUI_Tab[2])
    tGUI_Edit[11] = guiCreateEdit(222,83,44,21,"cen",false,tGUI_Tab[2])
    tGUI_Edit[12] = guiCreateEdit(243,61,92,21,"dragCoeff",false,tGUI_Tab[2])
    tGUI_Edit[13] = guiCreateEdit(243,39,92,21,"turnMass",false,tGUI_Tab[2])
    tGUI_Label[14] = guiCreateLabel(12,19,210,18,fLangStr('mass'),false,tGUI_Tab[2])
    tGUI_Label[15] = guiCreateLabel(12,108,210,18,fLangStr('percentSubmerged'),false,tGUI_Tab[2])
    tGUI_Label[16] = guiCreateLabel(12,86,210,18,fLangStr('centerOfMass'),false,tGUI_Tab[2])
    tGUI_Label[17] = guiCreateLabel(12,64,210,18,fLangStr('dragCoeff'),false,tGUI_Tab[2])
    tGUI_Label[18] = guiCreateLabel(12,41,210,18,fLangStr('turnMass'),false,tGUI_Tab[2])
    tGUI_Edit[14] = guiCreateEdit(267,83,44,21,"of",false,tGUI_Tab[2])
    tGUI_Edit[15] = guiCreateEdit(312,83,44,21,"mas",false,tGUI_Tab[2])
    tGUI_Label[19] = guiCreateLabel(364,86,32,18,fLangStr('m'),false,tGUI_Tab[2])
    tGUI_Label[20] = guiCreateLabel(345,108,75,18,fLangStr('percents'),false,tGUI_Tab[2])
    tGUI_Label[21] = guiCreateLabel(345,42,75,18,fLangStr('kg'),false,tGUI_Tab[2])
    tGUI_Label[22] = guiCreateLabel(345,20,75,18,fLangStr('kg'),false,tGUI_Tab[2])
    tGUI_Edit[16] = guiCreateEdit(243,149,92,21,"tailLight",false,tGUI_Tab[2])
    guiEditSetReadOnly(tGUI_Edit[16],true)
    guiSetAlpha(tGUI_Edit[16],0.6)
    tGUI_Edit[17] = guiCreateEdit(243,127,92,21,"headLight",false,tGUI_Tab[2])
    guiEditSetReadOnly(tGUI_Edit[17],true)
    guiSetAlpha(tGUI_Edit[17],0.6)
    tGUI_Label[23] = guiCreateLabel(12,151,210,18,fLangStr('tailLight'),false,tGUI_Tab[2])
    tGUI_Label[24] = guiCreateLabel(12,129,210,18,fLangStr('headLight'),false,tGUI_Tab[2])
    tGUI_Edit[41] = guiCreateEdit(243,171,92,21,"monetary",false,tGUI_Tab[2])
    guiEditSetReadOnly(tGUI_Edit[41],true)
    guiSetAlpha(tGUI_Edit[41],0.6)
    tGUI_Label[50] = guiCreateLabel(12,173,230,19,fLangStr('monetary'),false,tGUI_Tab[2])
    tGUI_Label[51] = guiCreateLabel(342,173,31,19,fLangStr('dollars'),false,tGUI_Tab[2])

    tGUI_Tab[3] = guiCreateTab(fLangStr('tabSuspension'),tGUI_TabPanel[1])
    tGUI_Edit[18] = guiCreateEdit(243,83,92,21,"suspUpper",false,tGUI_Tab[3])
    tGUI_Edit[19] = guiCreateEdit(243,61,92,21,"suspHiSpeed",false,tGUI_Tab[3])
    tGUI_Edit[20] = guiCreateEdit(243,39,92,21,"suspDamp",false,tGUI_Tab[3])
    tGUI_Edit[21] = guiCreateEdit(243,17,92,21,"suspForce",false,tGUI_Tab[3])
    tGUI_Edit[22] = guiCreateEdit(243,149,92,21,"suspBias",false,tGUI_Tab[3])
    tGUI_Edit[23] = guiCreateEdit(243,127,92,21,"suspAntiDive",false,tGUI_Tab[3])
    tGUI_Edit[24] = guiCreateEdit(243,105,92,21,"suspLower",false,tGUI_Tab[3])
    tGUI_Label[25] = guiCreateLabel(12,152,210,18,fLangStr('suspensionFrontRearBias'),false,tGUI_Tab[3])
    tGUI_Label[26] = guiCreateLabel(12,130,210,18,fLangStr('suspensionAntiDiveMultiplier'),false,tGUI_Tab[3])
    tGUI_Label[27] = guiCreateLabel(12,108,210,18,fLangStr('suspensionLowerLimit'),false,tGUI_Tab[3])
    tGUI_Label[28] = guiCreateLabel(12,87,210,18,fLangStr('suspensionUpperLimit'),false,tGUI_Tab[3])
    tGUI_Label[29] = guiCreateLabel(12,65,210,18,fLangStr('suspensionHighSpeedDamping'),false,tGUI_Tab[3])
    tGUI_Label[30] = guiCreateLabel(12,42,210,18,fLangStr('suspensionDamping'),false,tGUI_Tab[3])
    tGUI_Label[31] = guiCreateLabel(12,19,210,18,fLangStr('suspensionForceLevel'),false,tGUI_Tab[3])

    tGUI_Tab[4] = guiCreateTab(fLangStr('tabWheels'),tGUI_TabPanel[1])
    tGUI_Edit[25] = guiCreateEdit(243,17,92,21,"tractMult",false,tGUI_Tab[4])
    tGUI_Edit[26] = guiCreateEdit(243,105,92,21,"brakeBias",false,tGUI_Tab[4])
    tGUI_Edit[27] = guiCreateEdit(243,83,92,21,"brakeDesel",false,tGUI_Tab[4])
    tGUI_Edit[28] = guiCreateEdit(243,61,92,21,"tractBias",false,tGUI_Tab[4])
    tGUI_Edit[29] = guiCreateEdit(243,39,92,21,"tractLoss",false,tGUI_Tab[4])
    tGUI_Label[32] = guiCreateLabel(12,19,210,18,fLangStr('tractionMultiplier'),false,tGUI_Tab[4])
    tGUI_Label[33] = guiCreateLabel(12,108,210,18,fLangStr('brakeBias'),false,tGUI_Tab[4])
    tGUI_Label[34] = guiCreateLabel(12,86,210,18,fLangStr('brakeDeceleration'),false,tGUI_Tab[4])
    tGUI_Label[35] = guiCreateLabel(12,64,210,18,fLangStr('tractionBias'),false,tGUI_Tab[4])
    tGUI_Label[36] = guiCreateLabel(12,42,210,18,fLangStr('tractionLoss'),false,tGUI_Tab[4])
    tGUI_Label[37] = guiCreateLabel(343,86,62,18,fLangStr('m/sec2'),false,tGUI_Tab[4])
    tGUI_Label[38] = guiCreateLabel(12,130,210,18,fLangStr('ABS'),false,tGUI_Tab[4])

    tGUI_Combo[19] = guiCreateComboBox(243,127,92,44+12*2,"ABS",false,tGUI_Tab[4])
    guiComboBoxAddItem( tGUI_Combo[19], fLangStr('no') )
    guiComboBoxAddItem( tGUI_Combo[19], fLangStr('yes') )
    guiComboBoxSetSelected( tGUI_Combo[19], 0 )
    setElementData( tGUI_Combo[19], 'itemsCount', 2 )

    tGUI_Tab[5] = guiCreateTab(fLangStr('tabPEDs'),tGUI_TabPanel[1])
    tGUI_Edit[31] = guiCreateEdit(243,17,92,21,"animGroup",false,tGUI_Tab[5])
    guiEditSetReadOnly(tGUI_Edit[31],true)
    guiSetAlpha(tGUI_Edit[31],0.6)
    tGUI_Edit[32] = guiCreateEdit(243,39,92,21,"seatOffset",false,tGUI_Tab[5])
    tGUI_Label[39] = guiCreateLabel(12,19,210,18,fLangStr('animGroup'),false,tGUI_Tab[5])
    tGUI_Label[40] = guiCreateLabel(12,42,210,18,fLangStr('seatOffsetDistance'),false,tGUI_Tab[5])
    tGUI_Label[41] = guiCreateLabel(342,42,66,18,fLangStr('m'),false,tGUI_Tab[5])


    -- modelFlags
    tGUI_Tab[6] = guiCreateTab(fLangStr('tabModelFlags'),tGUI_TabPanel[1])
    tGUI_Grid[1] = guiCreateGridList( 11, 11, 457 - 11*2, 230 - 11*4, false, tGUI_Tab[6] )
    guiGridListSetSelectionMode( tGUI_Grid[1], 1 ) -- мультистроковое выделение мышью
    guiGridListSetSortingEnabled( tGUI_Grid[1], false ) -- без сортировки

    local nNumCol = guiGridListAddColumn( tGUI_Grid[1], fLangStr('#'), 0.1 )
    local nParamCol = guiGridListAddColumn( tGUI_Grid[1], fLangStr('flagsColumnTitle'), 0.8 )

    -- modelFlags[1]
    local nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('IS_VAN'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('IS_BUS'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('IS_LOW'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('IS_BIG'), false, false )

    -- modelFlags[2]
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('REVERSE_BONNET'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('HANGING_BOOT'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('TAILGATE_BOOT'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('NOSWING_BOOT'), false, false )

    -- modelFlags[3]
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('NO_DOORS'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('TANDEM_SEATS'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('SIT_IN_BOAT'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('CONVERTIBLE'), false, false )

    -- modelFlags[4]
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('NO_EXHAUST'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('DBL_EXHAUST'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('NO1FPS_LOOK_BEHIND'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('FORCE_DOOR_CHECK'), false, false )

    -- modelFlags[5]
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('AXLE_F_NOTILT'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('AXLE_F_SOLID'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('AXLE_F_MCPHERSON'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('AXLE_F_REVERSE'), false, false )

    -- modelFlags[6]
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('AXLE_R_NOTILT'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('AXLE_R_SOLID'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('AXLE_R_MCPHERSON'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('AXLE_R_REVERSE'), false, false )

    -- modelFlags[7]
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('IS_BIKE'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('IS_HELI'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('IS_PLANE'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('IS_BOAT'), false, false )

    -- modelFlags[8]
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('BOUNCE_PANELS'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('DOUBLE_RWHEELS'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('FORCE_GROUND_CLEARANCE'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[1] )
    guiGridListSetItemText( tGUI_Grid[1], nRow, nParamCol, fLangStr('IS_HATCHBACK'), false, false )

    for n = 0, nRow do
        -- каждому флагу присвоим номер для практичности
        guiGridListSetItemText( tGUI_Grid[1], n, nNumCol, tostring(n+1), false, false )

        -- попеременно красим каждые четыре строки в разные цвета для наглядности
        if math.ceil( (n + 1) / 4 ) % 2 == 0 then
            guiGridListSetItemColor( tGUI_Grid[1], n, nParamCol, 128,255,128,255 )
        end
    end


    -- handlingFlags
    tGUI_Tab[7] = guiCreateTab(fLangStr('tabHandlingFlags'),tGUI_TabPanel[1])
    tGUI_Grid[2] = guiCreateGridList( 11, 11, 457 - 11*2, 230 - 11*4, false, tGUI_Tab[7] )
    guiGridListSetSelectionMode( tGUI_Grid[2], 1 ) -- мультистроковое выделение мышью
    guiGridListSetSortingEnabled( tGUI_Grid[2], false ) -- без сортировки

    nNumCol = guiGridListAddColumn( tGUI_Grid[2], fLangStr('#'), 0.1 )
    setElementData( tGUI_Grid[2], 'columnHeader'..nNumCol, fLangStr('#') )
    nParamCol = guiGridListAddColumn( tGUI_Grid[2], fLangStr('flagsColumnTitle'), 0.8 )
    setElementData( tGUI_Grid[2], 'columnHeader'..nParamCol, fLangStr('flagsColumnTitle') )

    -- handlingFlags[1]
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('1G_BOOST'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('2G_BOOST'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('NPC_ANTI_ROLL'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('NPC_NEUTRAL_HANDL'), false, false )

    -- handlingFlags[2]
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('NO_HANDBRAKE'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('STEER_REARWHEELS'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('HB_REARWHEEL_STEER'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('ALT_STEER_OPT'), false, false )

    -- handlingFlags[3]
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('WHEEL_F_NARROW2'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('WHEEL_F_NARROW'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('WHEEL_F_WIDE'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('WHEEL_F_WIDE2'), false, false )

    -- handlingFlags[4]
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('WHEEL_R_NARROW2'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('WHEEL_R_NARROW'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('WHEEL_R_WIDE'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('WHEEL_R_WIDE2'), false, false )

    -- handlingFlags[5]
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('HYDRAULIC_GEOM'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('HYDRAULIC_INST'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('HYDRAULIC_NONE'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('NOS_INST'), false, false )

    -- handlingFlags[6]
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('OFFROAD_ABILITY'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('OFFROAD_ABILITY2'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('HALOGEN_LIGHTS'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('PROC_REARWHEEL_1ST'), false, false )

    -- handlingFlags[7]
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('USE_MAXSP_LIMIT'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('LOW_RIDER'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('STREET_RACER'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('UNDEFINED1'), false, false )

    -- handlingFlags[8]
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('SWINGING_CHASSIS'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('UNDEFINED2'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('UNDEFINED3'), false, false )
    nRow = guiGridListAddRow( tGUI_Grid[2] )
    guiGridListSetItemText( tGUI_Grid[2], nRow, nParamCol, fLangStr('UNDEFINED4'), false, false )

    for n = 0, nRow do
        -- каждому флагу присвоим номер для практичности
        guiGridListSetItemText( tGUI_Grid[2], n, nNumCol, tostring(n+1), false, false )

        -- попеременно красим каждые четыре строки в разные цвета для наглядности
        if math.ceil( (n + 1) / 4 ) % 2 == 0 then
            guiGridListSetItemColor( tGUI_Grid[2], n, nParamCol, 255,128,255,255 )
        end
    end


    -- разноцветные кнопки внизу окна
    tGUI_Button[1] = guiCreateButton(10,328,100,20,fLangStr('btnDefaults'),false,tGUI_Window[1])
    guiSetProperty( tGUI_Button[1], 'NormalTextColour', 'FFFF8000' )
    guiSetProperty( tGUI_Button[1], 'HoverTextColour', 'FFFFFF00' )
    tGUI_Button[2] = guiCreateButton(403,328,64,20,fLangStr('btnClose'),false,tGUI_Window[1])
    guiSetProperty( tGUI_Button[2], 'NormalTextColour', 'FFFFFFFF' )
    guiSetProperty( tGUI_Button[2], 'HoverTextColour', 'FFFFFF00' )
    tGUI_Button[3] = guiCreateButton(317,328,77,20,fLangStr('btnApply'),false,tGUI_Window[1])
    guiSetProperty( tGUI_Button[3], 'NormalTextColour', 'FF00FF00' )
    guiSetProperty( tGUI_Button[3], 'HoverTextColour', 'FFFFFF00' )


    -- выбор языка
    tGUI_Combo[20] = guiCreateComboBox( 467-92, 27 , 92, 44 + (#tLanguages * 12), "lang", false, tGUI_Window[1] )
    for nLangID = 1, #tLanguages do
        guiComboBoxAddItem( tGUI_Combo[20], tLanguages[nLangID]['langName'] )
    end
    guiComboBoxSetSelected( tGUI_Combo[20], nCurrentLanguage - 1 )


    -- заголовки с инфо о транспорте в самом верху окна
    tGUI_Label[61] = guiCreateLabel(11,23,100,18,fLangStr('modelName'),false,tGUI_Window[1])
    guiLabelSetColor(tGUI_Label[61],255,255,0)
    tGUI_Label[60] = guiCreateLabel(11,43,100,18,fLangStr('modelID'),false,tGUI_Window[1])
    guiLabelSetColor(tGUI_Label[60],255,255,0)
    tGUI_Label[64] = guiCreateLabel(11,63,100,18,fLangStr('vehicleName'),false,tGUI_Window[1])
    guiLabelSetColor(tGUI_Label[64],255,255,0)

    tGUI_Label[62] = guiCreateLabel(120,23,100,18,"modelname",false,tGUI_Window[1])
    guiLabelSetColor(tGUI_Label[62],128,255,128)
    tGUI_Label[63] = guiCreateLabel(120,43,100,18,"000",false,tGUI_Window[1])
    guiLabelSetColor(tGUI_Label[63],128,255,128)
    tGUI_Label[65] = guiCreateLabel(120,63,300,18,"Vehicle Name",false,tGUI_Window[1])
    guiLabelSetColor(tGUI_Label[65],128,255,128)


    -- дополнительное мини окошко, которое будет показываться на несколько
    -- секунд, сообщая о результатах после нажатия кнопок
    tGUI_Window[2] = guiCreateWindow (
        (tScreenSize[1] - 200)/2, (tScreenSize[2] - 80)/2, -- в центре экрана
        200, 80,
        fLangStr('reportWindowTitle'),
        false
    )
    guiSetVisible( tGUI_Window[2], false )
    guiSetAlpha( tGUI_Window[2], 1 )

    tGUI_Label[66] = guiCreateLabel( 10,10, 180,70, 'report', false, tGUI_Window[2] )
    guiLabelSetHorizontalAlign( tGUI_Label[66], 'center', true )
    guiLabelSetVerticalAlign( tGUI_Label[66], 'center' )
    guiLabelSetColor( tGUI_Label[66], 0,255,0 )
end
