﻿--[[
    -------------
    Русский игровой редактор настроек транспорта для MTASA v1.2 и выше
    ---------

    Новые версии, баги, запросы:
        http://multi-theft-auto-ru.googlecode.com/

    Авторы текущего файла:
        MX_Master ( http://multi-theft-auto.ru/ )

    Легенда по префиксам в названиях переменных:
        b   boolean, булевое
        n   number, число
        s   string, строка
        t   table, таблица
        f   function, функция
        u   userdata, в MTASA этот тип юзают элементы
        th  thread, поток
]]

---
-- НАСТРОЙКИ И ДАННЫЕ ДЛЯ ПРАВКИ ХЭНДЛИНГА --
---

-- какие параметры можно менять, а какие нет
tHandlingParametersToSet = { -- кол-во = 33
--  ['параметр'] = true/false,
    ['mass'] = true,
    ['turnMass'] = true,
    ['dragCoeff'] = true,
    ['centerOfMass'] = true,
    ['percentSubmerged'] = true,
    ['tractionMultiplier'] = true,
    ['tractionLoss'] = true,
    ['tractionBias'] = true,
    ['numberOfGears'] = true,
    ['maxVelocity'] = true,
    ['engineAcceleration'] = true,
    ['engineInertia'] = true,
    ['driveType'] = true,
    ['engineType'] = true,
    ['brakeDeceleration'] = true,
    ['brakeBias'] = true,
    ['ABS'] = true,
    ['steeringLock'] = true,
    ['suspensionForceLevel'] = true,
    ['suspensionDamping'] = true,
    ['suspensionHighSpeedDamping'] = true,
    ['suspensionUpperLimit'] = true,
    ['suspensionLowerLimit'] = true,
    ['suspensionFrontRearBias'] = true,
    ['suspensionAntiDiveMultiplier'] = true,
    ['seatOffsetDistance'] = true,
    ['collisionDamageMultiplier'] = true,
    ['monetary'] = false, -- только для чтения
    ['modelFlags'] = true,
    ['handlingFlags'] = true,
    ['headLight'] = false, -- только для чтения
    ['tailLight'] = false, -- только для чтения
    ['animGroup'] = false -- только для чтения
}
