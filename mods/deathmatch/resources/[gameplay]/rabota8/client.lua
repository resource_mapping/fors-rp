﻿addEvent("openinf_voditel", true)
screenWidth, screenHeight = guiGetScreenSize()
function windm ()
	if not getElementData ( localPlayer, "trash_working") then
		  infowind = guiCreateWindow(screenWidth - 700, screenHeight - 500, 200, 229, "Работа водителем пикапа", false)
		  infotext = guiCreateMemo(15,20,165,150,"Ваша задача подбирать пикапы с изображением человечков,за вознаграждение - 300$!",false,infowind)
		  guiMemoSetReadOnly( infotext, true )
		  --guiSetFont(text2, "default-bold-small") 
		  showCursor(true)
		  guiSetVisible ( infowind , true) 
		  guiWindowSetSizable(infowind, false) 
		  Button_Glose = guiCreateButton(10, 174, 90, 45, "Закрыть", false, infowind)
		  Button_Start = guiCreateButton(100, 174, 90, 45, "Начать", false, infowind)
		  addEventHandler("onClientGUIClick", Button_Start, kit )
			addEventHandler("onClientGUIClick", Button_Glose, noshow )
	else
		for i, v in ipairs ( createdPickups ) do
			if isElement ( v ) then
				local pick2 = getElementData ( v, "pick2")
				if isElement ( pick2 ) then
					destroyElement ( pick2 )
				end
				local icon = getElementData ( v, "icon" )
				if isElement ( icon ) then
					destroyElement ( icon )
				end
				destroyElement ( v )
			end
		end
		triggerServerEvent ( "finitoWork_voditel", localPlayer )
		setElementData ( localPlayer, "trash_working", false )
		outputChatBox ( "Вы закончили работу", 255, 255, 255, true )
	end
end
addEventHandler("openinf_voditel", root, windm )
function noshow ()
 if ( source == Button_Glose ) then
   destroyElement ( infowind )
   showCursor ( false ) 
 end
end
function kit ()
 if ( source == Button_Start ) then
	startWorking()
    destroyElement ( infowind )
    showCursor ( false )
  end 
end

addEvent ( "pay", true )
function giving ()
 outputChatBox ( "Вы собрали мусор и получили - 50$ ", getRootElement(), 255, 100, 100, true )
end
addEventHandler("pay", resourceRoot, giving )
addEvent ( "get", true )
function peremen (plr)
 plr = getLocalPlayer ()
end
addEventHandler("get", resourceRoot, peremen )

pickupSpawns = {
	{ -1990,166,28 },
        { -2003,212,28 },
        { -2001,277,34 },
        { -2009,322,36 },
	{ -2037,323,36 },
	{ -2086,324,36 },
	{ -2131,323,36 },
	{ -2148,314,36 },
	{ -2149,288,36 },
	{ -2149,224,36 },
	{ -2147,190,36 },
	{ -2149,141,36 },

        { -2156,113,36 },
	{ -2133,109,36 },
	{ -2108,110,36 },
	{ -2073,109,36 },
	{ -2038,108,36 },
	{ -2010,109,36 },
	{ -1991,120,36 },		
}

max_pickups_set = 19 

createdPickups = {}

max_pickups = max_pickups_set
if max_pickups_set > #pickupSpawns then
	max_pickups = #pickupSpawns
end

function startWorking ()
	if not getElementData ( localPlayer, "trash_working" ) then
		createdPickups = {}
		triggerServerEvent ( "picku_voditel", localPlayer )
		setElementData ( localPlayer, "trash_working", true )
		setElementData ( localPlayer, "trash_working_hit", 0 )
			outputChatBox ( "Вы начали работу\nЕзжайте по пикапам человечков", 255, 255, 255, true )		
				
		for i, v in ipairs ( pickupSpawns ) do
			local pick = createMarker ( v[1],v[2],v[3], "checkpoint", 4.0, 255, 255, 255 )
			local pick2 = createPickup ( v[1],v[2],v[3], 3, 1314, 10000 )
			setElementData ( pick, "taken", false )
			setElementData ( pick, "pick2", pick2 )
			setElementData ( pick, "bus_pickup", true )
			setElementData ( pick, "id", i )
			if i == 1 then
				local icon = createBlipAttachedTo ( pick, 41, 2 )
				setElementData ( pick, "icon", icon )
			end
			table.insert (createdPickups, pick)
		end
	end
end	

function clientPickupHit(thePlayer, matchingDimension)
	if thePlayer == localPlayer and getElementData ( source, "bus_pickup" ) and not getElementData ( source, "taken" ) then
		setElementData ( source, "taken", true )
		outputChatBox ( "Вы подобрали пассажиров и получили- 300$ ", 255, 100, 100, true )
		exports [ "urovni" ]:givePlayerXP (thePlayer, 5) 
		triggerServerEvent ( "giveMoneyFromClient_voditel", localPlayer, 300) -- 50 награда
		local hitted = getElementData ( localPlayer, "trash_working_hit" ) or 0
		hitted = hitted+1
		local pick2 = getElementData ( source, "pick2" )
		if isElement ( pick2 ) then
			destroyElement ( pick2 )
		end
		local icon = getElementData ( source, "icon" )
		if isElement ( icon ) then
			destroyElement ( icon )
		end
		destroyElement(source)
		setElementData ( localPlayer, "trash_working_hit", hitted)
		if hitted >= max_pickups then
			for i, v in ipairs ( createdPickups ) do
				if isElement ( v ) then
					local pick2 = getElementData ( v, "pick2")
					if isElement ( pick2 ) then
						destroyElement ( pick2 )
					end
					local icon = getElementData ( v, "icon" )
					if isElement ( icon ) then
						destroyElement ( icon )
					end
					destroyElement ( v )
				end
			end
			setElementData ( localPlayer, "trash_working", false )
			triggerServerEvent ( "finitoWork_voditel", localPlayer )
			outputChatBox ( "Вы закончили работу", 255, 255, 255, true )
			return true
		end
		if createdPickups[hitted+1] then
			local icon = createBlipAttachedTo ( createdPickups[hitted+1], 41, 2 )
			setElementData ( createdPickups[hitted+1], "icon", icon )
		end
	end
end
addEventHandler ( "onClientMarkerHit", getRootElement(), clientPickupHit )







